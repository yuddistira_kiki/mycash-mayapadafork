package com.sgo.mayapada.coreclass;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.provider.ContactsContract;
import android.support.multidex.MultiDex;

import com.facebook.stetho.Stetho;
import com.joanzapata.iconify.Iconify;
import com.joanzapata.iconify.fonts.FontAwesomeModule;
import com.sgo.mayapada.BuildConfig;
import com.sgo.mayapada.R;
import com.sgo.mayapada.contactsEngine.ContactObserver;
import com.sgo.mayapada.entityDAO.DaoMaster;
import com.sgo.mayapada.entityDAO.DaoSession;
import com.sgo.mayapada.entityRealm.Account_Collection_Model;
import com.sgo.mayapada.entityRealm.Biller_Data_Model;
import com.sgo.mayapada.entityRealm.Biller_Type_Data_Model;
import com.sgo.mayapada.entityRealm.Denom_Data_Model;
import com.sgo.mayapada.entityRealm.List_Account_Nabung;
import com.sgo.mayapada.entityRealm.List_Bank_Nabung;
import com.sgo.mayapada.entityRealm.Target_Saving_Model;
import com.sgo.mayapada.entityRealm.bank_biller_model;
import com.sgo.mayapada.services.ContactUpdateService;
import com.sgo.mayapada.syncengine.SyncAdapterManager;
import com.uphyca.stetho_realm.RealmInspectorModulesProvider;

import org.greenrobot.greendao.database.Database;
import org.greenrobot.greendao.query.QueryBuilder;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Locale;
import java.util.Random;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.annotations.RealmModule;
import timber.log.Timber;

/*
  Created by Administrator on 8/15/2014.
 */
public class CoreApp extends Application implements ContactObserver.ContentObserverCallback{

    private Activity mCurrentActivity = null;
    public static RealmConfiguration configBiller;
    private static CoreApp _instance;
    private DaoSession daoSession;

    private ContactObserver myContentOberver;

    public static CoreApp get_instance() {
        return _instance;
    }

    private static void set_instance(CoreApp _instance) {
        CoreApp._instance = _instance;
    }
    public static final long SECONDS_PER_MINUTE = 60L;
    public static final long SYNC_INTERVAL_IN_MINUTES = 60L;
    public static final long SYNC_INTERVAL =
            SYNC_INTERVAL_IN_MINUTES *
                    SECONDS_PER_MINUTE;

    @RealmModule(classes = { Account_Collection_Model.class, bank_biller_model.class, Biller_Data_Model.class, Biller_Type_Data_Model.class,
            Denom_Data_Model.class,})
    private class BillerModule {
    }

    @RealmModule(classes = { List_Account_Nabung.class, List_Bank_Nabung.class, Target_Saving_Model.class})
    private class AppModule {
    }


    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    @Override
    public void onCreate() {
        super.onCreate();
//        RunSyncAdapter();

        Stetho.initialize(
                Stetho.newInitializerBuilder(this)
                        .enableDumpapp(Stetho.defaultDumperPluginsProvider(this))
                        .enableWebKitInspector(RealmInspectorModulesProvider.builder(this).build())
                        .build());

        set_instance(this);
        if(BuildConfig.DEBUG)
            Timber.plant(new Timber.DebugTree());
        else
            Timber.plant(new Timber.Tree() {
                @Override
                protected void log(int priority, String tag, String message, Throwable t) {
                }
            });

        Iconify.with(new FontAwesomeModule());
//        FacebookSdk.sdkInitialize(getApplicationContext());
//        AppEventsLogger.activateApp(this);

        CustomSecurePref.initialize(this);
        MyApiClient myApiClient = MyApiClient.Initialize(this);
        setsDefSystemLanguage(null);

        copyBundledRealmFile(CoreApp.this.getResources().openRawResource(R.raw.unikdev), getString(R.string.realmname));
        Realm.init(this);
        configBiller = new RealmConfiguration.Builder()
                .name(getString(R.string.realmname))
                .schemaVersion(getResources().getInteger(R.integer.realscheme))
                .modules(new BillerModule())
                .migration(new CustomRealMigration())
                .build();

        RealmConfiguration config = new RealmConfiguration.Builder()
                .name(getString(R.string.apprealmname))
                .schemaVersion(getResources().getInteger(R.integer.apprealscheme))
                .modules(new AppModule())
                .migration(new AppRealMigration())
                .build();

        Realm.setDefaultConfiguration(config);

        PackageInfo pInfo;
        try {
            pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            DefineValue.VERSION_NAME = pInfo.versionName;
            DefineValue.VERSION_CODE = String.valueOf(pInfo.versionCode);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        if(MyApiClient.PROD_FLAG_ADDRESS){
            MyApiClient.COMM_ID = MyApiClient.COMM_ID_PROD;
            MyApiClient.URL_FAQ = MyApiClient.URL_FAQ_PROD;
            MyApiClient.URL_TERMS = MyApiClient.URL_TERMS_PROD;
        }
        else {
            MyApiClient.COMM_ID = MyApiClient.COMM_ID_DEV;
            MyApiClient.URL_FAQ = MyApiClient.URL_FAQ_DEV;
            MyApiClient.URL_TERMS = MyApiClient.URL_TERMS_DEV;
        }

        myApiClient.InitializeAddress();
        Timber.wtf("isi headaddressfinal:"+MyApiClient.headaddressfinal);

        DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(this, getString(R.string.dbname));
        Database db = helper.getWritableDb();
        daoSession = new DaoMaster(db).newSession();
        QueryBuilder.LOG_SQL = true;
        QueryBuilder.LOG_VALUES = true;
        registerActivityLifecycleCallbacks(new LifeCycleHandler(this));

        if(myContentOberver == null){
            myContentOberver = new ContactObserver(this);
        }

//        getContentResolver().registerContentObserver(
//                // observable uri
//                ContactsContract.Contacts.CONTENT_URI,
//                true,
//                myContentOberver);


//        RunSyncAdapter();
    }

    public void RunSyncAdapter(){
//        Random randomGenerator = new Random();
//
//        int randomInt = randomGenerator.nextInt(11);
        SyncAdapterManager syncAdapterManager = new SyncAdapterManager(this);
//        syncAdapterManager.beginPeriodicSync(60+randomInt);
//        syncAdapterManager.createSyncAccount();
        syncAdapterManager.syncImmediately();
    }

    @Override
    public void update(Uri uri) {
        /* Starting Download Service */
        Intent intent = new Intent(Intent.ACTION_SYNC, null, this, ContactUpdateService.class);
        /* Send optional extras to Download IntentService */
        startService(intent);
    }

    private String copyBundledRealmFile(InputStream inputStream, String outFileName) {
        try {
            File file = new File(this.getFilesDir(), outFileName);
            long sizeraw = inputStream.available();
            long sizefile = 0;
            if(file.exists()) {
                sizefile = file.length();
                Timber.d("sizeRaw / sizeFile = "+ String.valueOf(sizeraw)+" / "+String.valueOf(sizefile));
            }

            if(sizeraw != sizefile) {
                FileOutputStream outputStream = new FileOutputStream(file);
                byte[] buf = new byte[1024];
                int bytesRead;
                while ((bytesRead = inputStream.read(buf)) > 0) {
                    outputStream.write(buf, 0, bytesRead);
                }
                outputStream.close();
                Timber.d("file baru dicopy");
                return file.getAbsolutePath();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        Timber.d("file tidak dicopy");
        return null;
    }

    @Override
    public void onConfigurationChanged(android.content.res.Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        setsDefSystemLanguage(newConfig);
    }

    private void setsDefSystemLanguage (android.content.res.Configuration newConfig){

        String delanguage ;
        if(newConfig == null){
            delanguage = Locale.getDefault().getLanguage();
        }
        else {
            delanguage = newConfig.locale.getLanguage();
        }

        Timber.d("isi delanguage system:"+delanguage);
        if(delanguage.equals("en")) {
           DefineValue.sDefSystemLanguage = delanguage.toUpperCase();
        }
        else DefineValue.sDefSystemLanguage = "ID";

    }
	
	@Override
    public void onTerminate() {
        super.onTerminate();
//        getContentResolver().unregisterContentObserver(myContentOberver);

        MyApiClient.CancelRequestWS(this, true);
    }

    public static Context getAppContext(){
        return get_instance().getApplicationContext();
    }

    public Activity getCurrentActivity(){
        return mCurrentActivity;
    }
    public void setCurrentActivity(Activity mCurrentActivity){
        this.mCurrentActivity = mCurrentActivity;
    }

    public DaoSession getDaoSession() {
        return daoSession;
    }

}
