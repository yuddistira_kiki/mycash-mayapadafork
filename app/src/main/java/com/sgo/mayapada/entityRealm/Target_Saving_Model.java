package com.sgo.mayapada.entityRealm;

import com.sgo.mayapada.coreclass.DateTimeFormat;

import org.json.JSONObject;

import io.realm.Realm;
import io.realm.RealmObject;

/**
 * Created by yuddistirakiki on 4/4/16.
 */

public class Target_Saving_Model extends RealmObject{

    private String targeted_amount;
    private String achieved_amount;
    private String target_start;
    private String target_end;

    private String ccy_id;

    private String last_update;

    public Target_Saving_Model(){

    }

    public Target_Saving_Model(String targeted_amount, String achieved_amount, String target_start, String target_end,
                               String ccy_id){
        this.setTargeted_amount(targeted_amount);
        this.setAchieved_amount(achieved_amount);
        this.setTarget_start(target_start);
        this.setTarget_end(target_end);
        this.setCcy_id(ccy_id);
    }


    public String getTargeted_amount() {
        return targeted_amount;
    }

    private void setTargeted_amount(String targeted_amount) {
        this.targeted_amount = targeted_amount;
    }

    public String getAchieved_amount() {
        if(achieved_amount == null || achieved_amount.isEmpty())
            return "0";
        return achieved_amount;
    }

    public void setAchieved_amount(String achieved_amount) {
        this.achieved_amount = achieved_amount;
    }

    public String getTarget_start() {
        return target_start;
    }

    private void setTarget_start(String target_start) {
        this.target_start = target_start;
    }

    public String getTarget_end() {
        return target_end;
    }

    private void setTarget_end(String target_end) {
        this.target_end = target_end;
    }

    public String getCcy_id() {
        return ccy_id;
    }

    private void setCcy_id(String ccy_id) {
        this.ccy_id = ccy_id;
    }

    public void insertLastUpdate(){
        String curr_date = DateTimeFormat.getCurrentDate();
        setLast_update(curr_date);
    }

    public String getLast_update() {
        return last_update;
    }

    private void setLast_update(String last_update) {
        this.last_update = last_update;
    }

    public static void insertData(final JSONObject response){

        Realm realm = Realm.getDefaultInstance();
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.delete(Target_Saving_Model.class);
                Target_Saving_Model temp_target_saving = realm.createObjectFromJson(Target_Saving_Model.class, response);
                temp_target_saving.insertLastUpdate();
            }
        });
        realm.close();
    }

    public static void deleteOrClear(){
        Realm realm = Realm.getDefaultInstance();
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.delete(Target_Saving_Model.class);
            }
        });
        realm.close();
    }
}
