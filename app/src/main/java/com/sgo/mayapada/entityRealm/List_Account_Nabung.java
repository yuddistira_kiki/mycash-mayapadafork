package com.sgo.mayapada.entityRealm;

import com.sgo.mayapada.coreclass.DateTimeFormat;
import com.sgo.mayapada.coreclass.DefineValue;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by yuddistirakiki on 4/4/16.
 */

public class List_Account_Nabung extends RealmObject{

    @PrimaryKey
    private String saving_id;

    private String bank_code;

    private String bank_name;

    private String ccy_id;

    private String acct_no;

    private String acct_name;

    private String is_default;

    private String last_update;

    public List_Account_Nabung(){

    }

    public List_Account_Nabung(String _saving_id, String _bank_code, String _bank_name, String _ccy_id,
                               String _acct_no, String _acct_name, String _is_default){
        this.setSaving_id(_saving_id);
        this.setBank_code(_bank_code);
        this.setBank_name(_bank_name);
        this.setCcy_id(_ccy_id);
        this.setAcct_no(_acct_no);
        this.setAcct_name(_acct_name);
        this.setIs_default(_is_default);

    }


    public Boolean isDefault() {
        return getIs_default().equals(DefineValue.STRING_YES);
    }

    public void setDefault(Boolean aDefault) {
        if(aDefault)
            setIs_default(DefineValue.STRING_YES);
        else
            setIs_default(DefineValue.STRING_NO);
    }


    public String getBank_code() {
        return bank_code;
    }

    public void setBank_code(String bank_code) {
        this.bank_code = bank_code;
    }

    public String getBank_name() {
        return bank_name;
    }

    public void setBank_name(String bank_name) {
        this.bank_name = bank_name;
    }

    public String getCcy_id() {
        return ccy_id;
    }

    public void setCcy_id(String ccy_id) {
        this.ccy_id = ccy_id;
    }

    public String getLast_update() {
        return last_update;
    }

    private void setLast_update(String last_update) {
        this.last_update = last_update;
    }

    public void insertLastUpdate(){
        String curr_date = DateTimeFormat.getCurrentDate();
        setLast_update(curr_date);
    }

    public String getAcct_no() {
        return acct_no;
    }

    public void setAcct_no(String acct_no) {
        this.acct_no = acct_no;
    }

    public String getAcct_name() {
        return acct_name;
    }

    public void setAcct_name(String acct_name) {
        this.acct_name = acct_name;
    }

    private String getIs_default() {
        return is_default;
    }

    public void setIs_default(String is_default) {
        this.is_default = is_default;
    }

    public String getSaving_id() {
        return saving_id;
    }

    public void setSaving_id(String saving_id) {
        this.saving_id = saving_id;
    }
}
