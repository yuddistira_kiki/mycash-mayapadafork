package com.sgo.mayapada.entityRealm;

import com.sgo.mayapada.coreclass.DateTimeFormat;
import com.sgo.mayapada.coreclass.DefineValue;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;

/**
 * Created by yuddistirakiki on 4/4/16.
 */

public class List_Bank_Nabung extends RealmObject{

    @PrimaryKey
    private String bank_code;
    @Required
    private String bank_name;
    @Required
    private String bank_gateway;

    private String last_update;

    public String getBank_code() {
        return bank_code;
    }

    public void setBank_code(String bank_code) {
        this.bank_code = bank_code;
    }

    public String getBank_name() {
        return bank_name;
    }

    public void setBank_name(String bank_name) {
        this.bank_name = bank_name;
    }

    public Boolean isBank_gateway() {
        return getBank_gateway().equalsIgnoreCase(DefineValue.STRING_YES);
    }

    private String getBank_gateway() {
        return bank_gateway;
    }

    public void setBank_gateway(String bank_gateway) {
        this.bank_gateway = bank_gateway;
    }

    public String getLast_update() {
        return last_update;
    }

    private void setLast_update(String last_update) {
        this.last_update = last_update;
    }
    public void insertLastUpdate(){
        String curr_date = DateTimeFormat.getCurrentDate();
        setLast_update(curr_date);
    }
}
