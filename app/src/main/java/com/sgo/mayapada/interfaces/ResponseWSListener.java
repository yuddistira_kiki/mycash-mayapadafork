package com.sgo.mayapada.interfaces;

import android.os.Bundle;

import org.json.JSONObject;

/**
 * Created by yuddistirakiki on 9/14/17.
 */

public interface ResponseWSListener {
    void onSuccess(JSONObject response);
    void onFailure(String message);
}
