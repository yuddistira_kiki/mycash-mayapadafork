package com.sgo.mayapada.interfaces;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;

/**
 * Created by yuddistirakiki on 8/17/16.
 */
@SuppressLint("ParcelCreator")
public class DownloadResultReceiver extends ResultReceiver {

    public DownloadResultReceiver(Handler handler) {
        super(handler);
    }

    private Receiver mReceiver;


    public void setReceiver(Receiver receiver) {
        mReceiver = receiver;
    }

    public interface Receiver {
        void onReceiveResult(int resultCode, Bundle resultData);
    }

    @Override
    protected void onReceiveResult(int resultCode, Bundle resultData) {
        if (mReceiver != null) {
            mReceiver.onReceiveResult(resultCode, resultData);
        }
    }
}
