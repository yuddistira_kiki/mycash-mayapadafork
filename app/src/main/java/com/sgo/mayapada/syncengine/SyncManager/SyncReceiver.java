package com.sgo.mayapada.syncengine.SyncManager;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.os.ResultReceiver;

/**
 * Created by yuddistirakiki on 4/10/17.
 */

public class SyncReceiver extends ResultReceiver {
    /**
     * Create a new ResultReceive to receive results.  Your
     * {@link #onReceiveResult} method will be called from the thread running
     * <var>handler</var> if given, or from an arbitrary thread if null.
     *
     * @param handler
     */
    public SyncReceiver(Handler handler) {
        super(handler);
    }

    private Receiver mReceiver;

    public interface Receiver {
        void onReceiveResult(int resultCode, Bundle resultData);
    }
    public void setReceiver(Receiver receiver) {
        mReceiver = receiver;
    }

    @Override
    protected void onReceiveResult(int resultCode, Bundle resultData) {
        super.onReceiveResult(resultCode, resultData);
        if (mReceiver != null) {
            mReceiver.onReceiveResult(resultCode, resultData);
        }
    }
}
