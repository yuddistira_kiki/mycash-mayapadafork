package com.sgo.mayapada.syncengine;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;

/**
 * Created by yuddistirakiki on 4/10/17.
 */

public class AuthService extends Service {
    private DummyAuth auth;

    @Override
    public void onCreate() {
        super.onCreate();
        auth = new DummyAuth(this);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return auth.getIBinder();
    }
}
