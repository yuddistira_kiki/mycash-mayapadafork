package com.sgo.mayapada.syncengine;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.ContentResolver;
import android.content.Context;
import android.os.Bundle;

import com.sgo.mayapada.R;

import timber.log.Timber;

import static android.content.Context.ACCOUNT_SERVICE;

/**
 * Created by yuddistirakiki on 4/13/17.
 */

public class SyncAdapterManager {

    private static final String TAG = SyncAdapterManager.class.getSimpleName();
    private final String authority;
    private final String type;

    private Account account;
    private Context context;

    public SyncAdapterManager(final Context context) {
        this.context = context;

        type = context.getString(R.string.account_type);
        authority = context.getString(R.string.content_auth);
        account = new Account(context.getString(R.string.appname), type);
    }

    @SuppressWarnings ("MissingPermission")
    public void beginPeriodicSync(final long updateConfigInterval) {
        Timber.d("beginPeriodicSync() called with: updateConfigInterval = [" +
                updateConfigInterval + "]");

        final AccountManager accountManager = (AccountManager) context
                .getSystemService(ACCOUNT_SERVICE);
        ContentResolver.setSyncAutomatically(account, authority, true);
        if (!accountManager.addAccountExplicitly(account, null, null)) {
            account = accountManager.getAccountsByType(type)[0];
        }

        setAccountSyncable();

        ContentResolver.addPeriodicSync(account, authority,
                Bundle.EMPTY, updateConfigInterval);
    }

    public void syncImmediately() {
        Timber.d("masuk syncImmediately");
        setAccountSyncable();
        Bundle settingsBundle = new Bundle();
        settingsBundle.putBoolean(ContentResolver.SYNC_EXTRAS_MANUAL, true);
        settingsBundle.putBoolean(ContentResolver.SYNC_EXTRAS_EXPEDITED, true);

        ContentResolver.requestSync(account, authority, settingsBundle);
    }

    private void setAccountSyncable() {
        if (ContentResolver.getIsSyncable(account, authority) == 0) {
            ContentResolver.setIsSyncable(account, authority, 1);
        }
    }

}
