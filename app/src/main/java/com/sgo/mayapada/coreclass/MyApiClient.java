package com.sgo.mayapada.coreclass;

import android.content.Context;
import android.os.Looper;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.MySSLSocketFactory;
import com.loopj.android.http.PersistentCookieStore;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.SyncHttpClient;
import com.sgo.mayapada.BuildConfig;
import com.sgo.mayapada.R;

import org.apache.commons.codec.binary.Base64;
import org.apache.http.conn.ssl.SSLSocketFactory;

import java.io.InputStream;
import java.security.KeyStore;
import java.util.StringTokenizer;
import java.util.UUID;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import timber.log.Timber;

/**
 Created by Administrator on 7/14/2014.
 */
public class MyApiClient {

    private static MyApiClient singleton = null;
    private Context mContext;
    private AsyncHttpClient asyncHttpClient;
    private AsyncHttpClient syncHttpClient;

    public MyApiClient(){

    }
    public MyApiClient(Context _context){
        this.setmContext(_context);
    }

    private static MyApiClient getInstance() {
        return singleton;
    }

    public static MyApiClient Initialize(Context _context) {
        if(singleton == null) {
            singleton = new MyApiClient(_context);
            singleton.asyncHttpClient=new AsyncHttpClient();
            singleton.asyncHttpClient.addHeader("Authorization", "Basic " + getBasicAuth());
            singleton.syncHttpClient=new SyncHttpClient();
            singleton.syncHttpClient.addHeader("Authorization", "Basic " + getBasicAuth());
        }
        return singleton;
    }

    public static Boolean PROD_FAILURE_FLAG = true;
    public static Boolean IS_PROD = BuildConfig.isProdDomain;
    public static Boolean PROD_FLAG_ADDRESS = BuildConfig.isProdDomain;
    private static Boolean IS_INTERNET_BANKING;
    private static final String PRIVATE_KEY = "590mobil3";
    public static String COMM_ID;

    public static String headaddressfinal = BuildConfig.HeadAddress;

    //Link webservices Signature
    private static String LINK_LOGIN;
    public static String LINK_VALID_TOPUP;
    public static String LINK_LIST_MEMBER;
    public static String LINK_REQ_TOKEN_SGOL;
    public static String LINK_RESEND_TOKEN_SGOL;
    public static String LINK_INSERT_TRANS_TOPUP;
    public static String LINK_SALDO;
    public static String LINK_BANK_LIST;
    private static String LINK_REQ_TOKEN_REGIST;
    private static String LINK_GET_ALL_BANK;
    private static String LINK_TOPUP_PULSA_RETAIL;
    public static String LINK_UPDATE_PROFILE;
    public static String LINK_CHANGE_PASSWORD;
    private static String LINK_REQ_FORGOT_PASSWORD;
    private static String LINK_FORGOT_PASSWORD;
    private static String LINK_MEMBER_PULSA;
    public static String LINK_USER_CONTACT_INSERT;
    public static String LINK_USER_CONTACT_UPDATE;
    private static String LINK_PROD_TOPUP_RETAIL;
    private static String LINK_GET_BILLER_TYPE;
    public static String LINK_LIST_BILLER;
    public static String LINK_DENOM_RETAIL;
    public static String LINK_RESENT_TOKEN_BILLER;
    public static String LINK_UPLOAD_PROFILE_PIC;
    public static String LINK_LIST_BANK_BILLER;
    public static String LINK_REQ_TOKEN_P2P;
    public static String LINK_CONFIRM_TRANS_P2P;
    public static String LINK_RESENT_TOKEN_P2P;
    public static String LINK_ASKFORMONEY_SUBMIT;
    public static String LINK_NOTIF_RETRIEVE;
    private static String LINK_NOTIF_READ;
    public static String LINK_REQ_TOKEN_P2P_NOTIF;
    public static String LINK_CONFIRM_TRANS_P2P_NOTIF;
    public static String LINK_GET_TRX_STATUS;
	public static String LINK_GROUP_LIST;
    public static String LINK_ADD_GROUP;
    public static String LINK_TIMELINE_LIST;
    public static String LINK_COMMENT_LIST;
    public static String LINK_ADD_COMMENT;
    public static String LINK_REMOVE_COMMENT;
    public static String LINK_LIKE_LIST;
    public static String LINK_ADD_LIKE;
    public static String LINK_REMOVE_LIKE;
    private static String LINK_CREATE_PIN;
    public static String LINK_CHANGE_PIN;
    public static String LINK_INQUIRY_BILLER;
    public static String LINK_PAYMENT_BILLER;
    public static String LINK_TRANSACTION_REPORT;
    public static String LINK_PROMO_LIST;
    public static String LINK_BANK_ACCOUNT_COLLECTION;
    public static String LINK_TOP_UP_ACCOUNT_COLLECTION;
    public static String LINK_COMM_ACCOUNT_COLLECTION;
    private static String LINK_COMM_ESPAY;
	private static String LINK_APP_VERSION;
    private static String LINK_HELP_LIST;
    public static String LINK_INQUIRY_MOBILE;
    public static String LINK_REQUEST_TOKEN_SB;
    public static String LINK_CONFIRM_TOKEN_SB;
    public static String LINK_REPORT_ESPAY;
    public static String LINK_INQUIRY_MOBILE_JATIM;
    public static String LINK_CONFIRM_TOKEN_JATIM;
    public static String LINK_LIST_BANK_SMS_REGIST;
    private static String LINK_REGISTER_SOCMED;
    public static String LINK_UPDATE_SOCMED;
	public static String LINK_LOGOUT;
    private static String LINK_CREATE_PIN_PASS;
    public static String LINK_REPORT_MONEY_REQUEST;
    public static String LINK_ASK4MONEY_REJECT;
    private static String LINK_INQUIRY_CUST;
    public static String LINK_EXEC_CUST;
	public static String LINK_REQUEST_CASHOUT;
    public static String LINK_CONFIRM_CASHOUT;
    private static String LINK_HELP_PIN;
    public static String LINK_INQUIRY_WITHDRAW;
    public static String LINK_REQCODE_WITHDRAW;
    public static String LINK_DELTRX_WITHDRAW;
    private static String LINK_CREATE_PASS;
    public static String LINK_GET_FAILED_PIN;
    private static String LINK_ATMTOPUP;
    public static String LINK_BANKCASHOUT;
    private static String LINK_USER_PROFILE;
    private static String LINK_INQUIRY_SMS;
    public static String LINK_CLAIM_TRANSFER_NON_MEMBER;
    public static String LINK_INVOICE_TAGIHAN;
    public static String LINK_COMM_INV;
    public static String LINK_PAYMENT_INV;
    public static String LINK_GET_TRX_STATUS2;
    public static String LINK_REQUEST_CASH_IN;
    public static String LINK_CONFIRM_CASH_IN;
    public static String LINK_INITIATE_ATC;
    public static String LINK_INQUIRY_DATA_ATC;
    public static String LINK_CANCEL_ATC;
    public static String LINK_INQUIRY_TOKEN_ATC;
    public static String LINK_CONFIRM_TOKEN_ATC;
    public static String LINK_RESEND_TOKEN_LKD;
    public static String LINK_REPORT_LKD;
    public static String LINK_BANK_SAVING_ACCOUNT;
    public static String LINK_SAVING_ACCOUNT_LIST;
    public static String LINK_DELETE_ACCOUNT_LIST;
    public static String LINK_DEFAULT_ACCOUNT_LIST;
    public static String LINK_REQ_ADD_ACCOUNT;
    public static String LINK_CONFIRM_ADD_ACCOUNT;
    public static String LINK_INQ_SAVING_TARGET;
    public static String LINK_ASK4REWARD_SUBMIT;
    public static String LINK_ASK4REWARD_DECLINE;
    public static String LINK_SAVING_TARGET;
    public static String LINK_INQ_AUTOSAVING;
    public static String LINK_DELETE_AUTOSAVING;
    public static String LINK_ADD_AUTOSAVING;
    public static String LINK_DELETE_TARGETSAVING;
    public static String LINK_CONFIRMTOKEN_AUTOSAVING;
    public static String LINK_SET_AUTOSAVING_STATUS;
    public static String LINK_CANTEEN_MERCHANTS;
    public static String LINK_CANTEEN_CATALOGS;
    public static String LINK_REQ_ORDER_CATALOG;
    public static String LINK_CONFIRM_ORDER_CATALOG;
    public static String LINK_PURCHASE_LIST;
    public static String LINK_CHECK_COUPON_CODE;
    public static String LINK_CONFIRM_COUPON_CODE;
    public static String LINK_TODAY_SALES;
    public static String LINK_UNTAKEN_ITEM;
    public static String LINK_ACTIVE_DEACTIVE_ITEM;
    public static String LINK_BUYER_LIST;
    public static String LINK_ADD_ITEM;
    public static String LINK_DELETE_ITEM;
    public static String LINK_UPDATE_ITEM;
    public static String LINK_DETAILS_ITEM;
    public static String LINK_CANCEL_ORDER;
    private static String LINK_REG_STEP1;
    private static String LINK_REG_STEP2;
    private static String LINK_REG_STEP3;
    private static String LINK_REG_STEP3FB;
    public static String LINK_REQ_CHANGE_EMAIL;
    public static String LINK_CONFIRM_CHANGE_EMAIL;
    public static String LINK_REQUEST_UPGRADE;
    public static String LINK_CONFIRM_UPGRADE;

    public static String LINK_UOB_PAIRING_REQUEST;
    public static String LINK_UOB_PAIRING_CANCEL;
    public static String LINK_UOB_PAIRING_CONFIRM;

    public void InitializeAddress(){
        LINK_LOGIN               = headaddressfinal + "MemberLogin/SignIn";
        LINK_VALID_TOPUP         = headaddressfinal + "TopUp/Invoke";
        LINK_LIST_MEMBER         = headaddressfinal + "Member/Retrieve";
        LINK_REQ_TOKEN_SGOL      = headaddressfinal + "InquiryTrx/Retrieve";
        LINK_RESEND_TOKEN_SGOL   = headaddressfinal + "InquiryResendToken/Invoke";
        LINK_INSERT_TRANS_TOPUP  = headaddressfinal + "InsertTrx/Invoke";
        LINK_SALDO               = headaddressfinal + "Balance/Retrieve";
        LINK_BANK_LIST           = headaddressfinal + "BankMember/Retrieve";
        LINK_REQ_TOKEN_REGIST    = headaddressfinal + "ResendTokenCust/Invoke";
        LINK_GET_ALL_BANK        = headaddressfinal + "ServiceBank/GetAllBank";
        LINK_TOPUP_PULSA_RETAIL  = headaddressfinal + "TopUpPulsa/Invoke";
        LINK_UPDATE_PROFILE      = headaddressfinal + "UserProfile/Update";
        LINK_CHANGE_PASSWORD     = headaddressfinal + "ChangePassword/Invoke";
        LINK_REQ_FORGOT_PASSWORD = headaddressfinal + "ReqForgotPassChal/Invoke";
        LINK_FORGOT_PASSWORD     = headaddressfinal + "ForgotPassChal/Invoke";
        LINK_MEMBER_PULSA        = headaddressfinal + "MemberPulsa/Retrieve";
        LINK_USER_CONTACT_INSERT = headaddressfinal + "UserContact/Insert";
        LINK_USER_CONTACT_UPDATE = headaddressfinal + "UserContact/Update";
        LINK_PROD_TOPUP_RETAIL =   headaddressfinal + "TopUpPulsaProd/Invoke";
        LINK_GET_BILLER_TYPE     = headaddressfinal + "ServiceBillerType/getBillerType";
        LINK_LIST_BILLER         = headaddressfinal + "BillerEspay/Retrieve";
        LINK_DENOM_RETAIL        = headaddressfinal + "DenomBiller/Retrieve";
        LINK_RESENT_TOKEN_BILLER = headaddressfinal + "ResendToken/Invoke";
        LINK_LIST_BANK_BILLER    = headaddressfinal + "BankBiller/Retrieve";
        LINK_UPLOAD_PROFILE_PIC  = headaddressfinal + "UploadProfPic/Submit";
        LINK_REQ_TOKEN_P2P       = headaddressfinal + "RequestTransferChal/Invoke";
        LINK_CONFIRM_TRANS_P2P   = headaddressfinal + "ConfirmTransferChal/Invoke";
        LINK_RESENT_TOKEN_P2P    = headaddressfinal + "ResendTransfer/Invoke";
        LINK_ASKFORMONEY_SUBMIT  = headaddressfinal + "Ask4Money/Submit";
        LINK_NOTIF_RETRIEVE      = headaddressfinal + "UserNotif/Retrieve";
        LINK_NOTIF_READ          = headaddressfinal + "UserNotif/isRead";
        LINK_REQ_TOKEN_P2P_NOTIF = headaddressfinal + "PayFriend/Invoke";
        LINK_CONFIRM_TRANS_P2P_NOTIF = headaddressfinal + "ConfirmPayFriend/Invoke";
        LINK_GET_TRX_STATUS      = headaddressfinal + "TrxStatus/Retrieve";
        LINK_GROUP_LIST          = headaddressfinal + "UserGroup/Retrieve";
        LINK_ADD_GROUP           = headaddressfinal + "UserGroup/Insert";
        LINK_TIMELINE_LIST       = headaddressfinal + "UserPosts/Retrieve";
        LINK_COMMENT_LIST        = headaddressfinal + "UserComments/Retrieve";
        LINK_ADD_COMMENT         = headaddressfinal + "UserComments/Insert";
        LINK_REMOVE_COMMENT      = headaddressfinal + "UserComments/Remove";
        LINK_LIKE_LIST           = headaddressfinal + "UserLikes/Retrieve";
        LINK_ADD_LIKE            = headaddressfinal + "UserLikes/Insert";
        LINK_REMOVE_LIKE         = headaddressfinal + "UserLikes/Remove";
        LINK_CREATE_PIN          = headaddressfinal + "CreatePinChal/Invoke";
        LINK_CHANGE_PIN          = headaddressfinal + "ChangePinChal/Invoke";
        LINK_PAYMENT_BILLER      = headaddressfinal + "PaymentBiller/Invoke";
        LINK_TRANSACTION_REPORT  = headaddressfinal +"ReportTrx/Retrieve";
        LINK_PROMO_LIST          = headaddressfinal + "ServicePromo/PromoList";
        LINK_INQUIRY_BILLER      = headaddressfinal + "InquiryBiller/Invoke";
        LINK_BANK_ACCOUNT_COLLECTION      = headaddressfinal + "BankCollect/Retrieve";
        LINK_TOP_UP_ACCOUNT_COLLECTION    = headaddressfinal + "TopUpCollect/Invoke";
        LINK_COMM_ACCOUNT_COLLECTION      = headaddressfinal + "CommAcct/Retrieve";
		LINK_APP_VERSION         = headaddressfinal + "ServiceApp/getAppVersion";
        LINK_HELP_LIST           = headaddressfinal + "ContactCenter/Retrieve";
        LINK_INQUIRY_MOBILE      = headaddressfinal + "InquiryMobile/Invoke";
        LINK_REQUEST_TOKEN_SB    = headaddressfinal + "SendTokenSMS/Invoke";
        LINK_CONFIRM_TOKEN_SB    = headaddressfinal + "ConfirmTokenSMS/Invoke";
		LINK_COMM_ESPAY          = headaddressfinal + "CommEspay/Retrieve";
        LINK_REPORT_ESPAY        = headaddressfinal + "ReportEspay/Retrieve";
        LINK_INQUIRY_MOBILE_JATIM= headaddressfinal + "InquiryMobileJTM/Invoke";
        LINK_CONFIRM_TOKEN_JATIM = headaddressfinal + "ConfirmTokenSMSJTM/Invoke";
        LINK_LIST_BANK_SMS_REGIST= headaddressfinal + "BankRegisSMS/Retrieve";
        LINK_REGISTER_SOCMED     = headaddressfinal + "InsertSocialProfile/Invoke";
        LINK_UPDATE_SOCMED     = headaddressfinal + "FacebookProfile/Update";
		LINK_LOGOUT             = headaddressfinal + "ServiceLogout/SignOut";
        LINK_CREATE_PIN_PASS    = headaddressfinal + "CreatePinPass/Invoke";
        LINK_REPORT_MONEY_REQUEST = headaddressfinal + "ReportMoneyReq/Retrieve";
        LINK_ASK4MONEY_REJECT   = headaddressfinal + "Ask4Money/Decline";
        LINK_INQUIRY_CUST = headaddressfinal + "InquiryCustomer/Retrieve";
        LINK_EXEC_CUST   = headaddressfinal + "ExecCustomer/Invoke";
		LINK_REQUEST_CASHOUT    = headaddressfinal + "RequestCashoutChal/Invoke";
        LINK_CONFIRM_CASHOUT    = headaddressfinal + "ConfirmCashoutChal/Invoke";
        LINK_HELP_PIN           = headaddressfinal + "HelpPIN/Retrieve";
        LINK_INQUIRY_WITHDRAW    = headaddressfinal + "InquiryWithdraw/Retrieve";
        LINK_REQCODE_WITHDRAW    = headaddressfinal + "ReqCodeWithdraw/Invoke";
        LINK_DELTRX_WITHDRAW     = headaddressfinal + "DelWithdrawTrx/Invoke";
        LINK_CREATE_PASS    = headaddressfinal + "CreatePass/Invoke";
        LINK_GET_FAILED_PIN = headaddressfinal + "GetFailedPIN/Retrieve";
        LINK_ATMTOPUP       = headaddressfinal + "ATMTopUp/Retrieve";
        LINK_BANKCASHOUT    = headaddressfinal + "BankCashout/Retrieve";
        LINK_USER_PROFILE   = headaddressfinal + "UserProfile/Retrieve";

        if(BuildConfig.isProdDomain)
            LINK_INQUIRY_SMS   = "https://mobile.goworld.asia/edik2/" + "InquirySMS/Retrieve";
        else
            LINK_INQUIRY_SMS   = headaddressfinal + "InquirySMS/Retrieve";
        LINK_CLAIM_TRANSFER_NON_MEMBER = headaddressfinal + "ClaimNonMbrTrf/Invoke";
        LINK_INVOICE_TAGIHAN = headaddressfinal + "InvList/Retrieve";
        LINK_COMM_INV       = headaddressfinal + "CommInv/Retrieve";
        LINK_PAYMENT_INV    = headaddressfinal + "PaymentInv/Invoke";
        LINK_GET_TRX_STATUS2      = headaddressfinal + "TrxStatus2/Retrieve";
        LINK_REQUEST_CASH_IN    = headaddressfinal + "RequestCashIn/Invoke";
        LINK_CONFIRM_CASH_IN    = headaddressfinal + "ConfirmCashIn/Invoke";
        LINK_INITIATE_ATC       = headaddressfinal + "InitiateATC/Invoke";
        LINK_INQUIRY_DATA_ATC   = headaddressfinal + "InquiryDataATC/Retrieve";
        LINK_CANCEL_ATC         = headaddressfinal + "CancelATC/Invoke";
        LINK_INQUIRY_TOKEN_ATC  = headaddressfinal + "InquiryTokenATC/Retrieve";
        LINK_CONFIRM_TOKEN_ATC  = headaddressfinal + "ConfirmTokenATC/Invoke";
        LINK_RESEND_TOKEN_LKD  = headaddressfinal + "ResendToken/Invoke";
        LINK_REPORT_LKD         = headaddressfinal + "ReportLKD/Retrieve";
        LINK_BANK_SAVING_ACCOUNT = headaddressfinal + "BankSaving/Retrieve";
        LINK_SAVING_ACCOUNT_LIST = headaddressfinal + "BankSaving/ListAccount";
        LINK_DELETE_ACCOUNT_LIST = headaddressfinal + "DelSavingAcct/Invoke";
        LINK_DEFAULT_ACCOUNT_LIST = headaddressfinal + "SetDefaultSaving/Invoke";
        LINK_REQ_ADD_ACCOUNT = headaddressfinal + "ReqAddSavingAcct/Invoke";
        LINK_CONFIRM_ADD_ACCOUNT = headaddressfinal + "ConfirmAddSavingAcct/Invoke";
        LINK_INQ_SAVING_TARGET = headaddressfinal + "InquirySavingTarget/Retrieve";
        LINK_ASK4REWARD_SUBMIT  = headaddressfinal + "Ask4Reward/Submit";
        LINK_ASK4REWARD_DECLINE  = headaddressfinal + "Ask4Reward/Decline";
        LINK_SAVING_TARGET = headaddressfinal + "InsertSavingTarget/Invoke";
        LINK_INQ_AUTOSAVING = headaddressfinal + "InquiryAutoSaving/Retrieve";
        LINK_ADD_AUTOSAVING = headaddressfinal + "ReqAddAutoSaving/Invoke";
        LINK_DELETE_AUTOSAVING = headaddressfinal + "DelAutoSaving/Invoke";
        LINK_DELETE_TARGETSAVING = headaddressfinal + "DelSavingTarget/Invoke";
        LINK_CONFIRMTOKEN_AUTOSAVING = headaddressfinal + "ConfirmAddAutoSaving/Invoke";
        LINK_SET_AUTOSAVING_STATUS = headaddressfinal + "SetAutoSaving/Invoke";
        LINK_CANTEEN_MERCHANTS = headaddressfinal + "CanteenMerchants/Retrieve";
        LINK_CANTEEN_CATALOGS = headaddressfinal + "CanteenCatalogs/Retrieve";
        LINK_REQ_ORDER_CATALOG = headaddressfinal + "ReqOrderCatalog/Invoke";
        LINK_CONFIRM_ORDER_CATALOG = headaddressfinal + "ConfirmOrderCatalog/Invoke";
        LINK_PURCHASE_LIST =  headaddressfinal + "PurchaseList/Retrieve";
        LINK_CHECK_COUPON_CODE = headaddressfinal + "CheckCouponCode/Retrieve";
        LINK_CONFIRM_COUPON_CODE = headaddressfinal + "ConfirmCouponCode/Invoke";
        LINK_TODAY_SALES = headaddressfinal + "TodaySales/Retrieve";
        LINK_UNTAKEN_ITEM = headaddressfinal + "ListUntakenItem/Retrieve";
        LINK_ACTIVE_DEACTIVE_ITEM = headaddressfinal + "ActiveDeactiveItem/Invoke";
        LINK_BUYER_LIST = headaddressfinal + "BuyerList/Retrieve";
        LINK_ADD_ITEM = headaddressfinal + "AddItem/Invoke";
        LINK_DELETE_ITEM = headaddressfinal + "DelItem/Invoke";
        LINK_UPDATE_ITEM = headaddressfinal + "EditItem/Invoke";
        LINK_DETAILS_ITEM = headaddressfinal + "ItemDetail/Retrieve";
        LINK_CANCEL_ORDER = headaddressfinal + "CancelOrder/Invoke";
        LINK_REG_STEP1 = headaddressfinal + "RegStep1/Invoke";
        LINK_REG_STEP2 = headaddressfinal + "RegStep2/Invoke";
        LINK_REG_STEP3 = headaddressfinal + "RegStep3/Invoke";
        LINK_REG_STEP3FB = headaddressfinal + "RegStep3Soc/Invoke";
        LINK_REQ_CHANGE_EMAIL = headaddressfinal + "ReqChangeEmail/Invoke";
        LINK_CONFIRM_CHANGE_EMAIL = headaddressfinal + "ConfirmChangeEmail/Invoke";
        LINK_REQUEST_UPGRADE = headaddressfinal + "RequestUpgrade/Invoke";
        LINK_CONFIRM_UPGRADE = headaddressfinal + "ConfUpgrade/Invoke";

        LINK_UOB_PAIRING_REQUEST = headaddressfinal + "PairingRequest/Invoke";
        LINK_UOB_PAIRING_CONFIRM = headaddressfinal + "PairingConfirm/Invoke";
        LINK_UOB_PAIRING_CANCEL = headaddressfinal + "PairingCancel/Invoke";

		getInstance().syncHttpClient.setTimeout(TIMEOUT);
        if(PROD_FLAG_ADDRESS)
            getInstance().syncHttpClient.setSSLSocketFactory(getUntrustSSLSocketFactory());
        getInstance().syncHttpClient.setMaxRetriesAndTimeout(2, 10000);

        getInstance().asyncHttpClient.setTimeout(TIMEOUT);
        if(PROD_FLAG_ADDRESS)
            getInstance().asyncHttpClient.setSSLSocketFactory(getUntrustSSLSocketFactory());
        getInstance().asyncHttpClient.setMaxRetriesAndTimeout(2, 10000);
    }

    public static String URL_FAQ;
    public static String URL_FAQ_PROD = "https://mobile.goworld.asia/static/pages/help/unik-faq.html";
    public static String URL_FAQ_DEV = "https://mobile-dev.espay.id/static/pages/help/unik-faq.html";

    public static String URL_TERMS;
    public static String URL_TERMS_PROD = "https://mobile.goworld.asia/static/pages/help/unik_terms_conditions_id.html";
    public static String URL_TERMS_DEV = "https://mobile-dev.espay.id/static/pages/help/unik_terms_conditions_id.html";
    //-----------------------------------------------------------------------------------------------------------------

    private static final int TIMEOUT = 600000; // 200 x 1000 = 3 menit
    public static String COMM_ID_DEV = "MYCASH14997746915ZTVT";
    public static String COMM_ID_PROD = "EMOEDIKK1450692888Y4AYU";  //prod

    public static String INCOMINGSMS_INFOBIP = "+628111946677";
    public static String INCOMINGSMS_SPRINT = "+6281333332000";

    public static String APP_ID = BuildConfig.AppID;
    public static String CCY_VALUE = "IDR";

    public static UUID getUUID(){
        return UUID.randomUUID();
    }

    public static String getWebserviceName(String link){
        StringTokenizer tokens = new StringTokenizer(link, "/");
        int index = 0;
        while(index<3) {
            tokens.nextToken();
            index++;
        }
        return tokens.nextToken();
    }
    public static String getSignature(UUID uuidnya, String date, String WebServiceName, String noID, String apinya){
        String msgnya = uuidnya+date+APP_ID+WebServiceName+noID;

        String hash = null;
        Mac sha256_HMAC;
        try {
            sha256_HMAC = Mac.getInstance("HmacSHA256");
            SecretKeySpec secret_key = new SecretKeySpec(apinya.getBytes(), "HmacSHA256");
            sha256_HMAC.init(secret_key);

            byte[] hmacData = sha256_HMAC.doFinal(msgnya.getBytes("UTF-8"));

            hash = new String(encodeUrlSafe(hmacData));

        }
        catch (Exception ex){
            ex.printStackTrace();
        }

        return hash;
    }

    public static RequestParams getSignatureWithParams(String commID, String linknya, String user_id,String access_key){

        String webServiceName = getWebserviceName(linknya);
        UUID uuidnya = getUUID();
        String dtime = DateTimeFormat.getCurrentDateTime();
        String msgnya = uuidnya+dtime+APP_ID+webServiceName+ commID + user_id;

        String hash = null;
        Mac sha256_HMAC;
        try {
            sha256_HMAC = Mac.getInstance("HmacSHA256");
            SecretKeySpec secret_key = new SecretKeySpec(access_key.getBytes(), "HmacSHA256");
            sha256_HMAC.init(secret_key);

            byte[] hmacData = sha256_HMAC.doFinal(msgnya.getBytes("UTF-8"));

            hash = new String(encodeUrlSafe(hmacData));

        }
        catch (Exception ex){
            ex.printStackTrace();
        }

        RequestParams params = new RequestParams();
        params.put(WebParams.RC_UUID, uuidnya);
        params.put(WebParams.RC_DTIME, dtime);
        params.put(WebParams.SIGNATURE, hash);

        return params;
    }

    private static byte[] encodeUrlSafe(byte[] data) {
        byte[] encode = Base64.encodeBase64(data);
        for (int i = 0; i < encode.length; i++) {
            if (encode[i] == '+') {
                encode[i] = '-';
            } else if (encode[i] == '=') {
                encode[i] = '_';
            } else if (encode[i] == '/') {
                encode[i] = '~';
            }
        }
        return encode;
    }

    public static void setCookieStore(PersistentCookieStore cookieStore) {
        getClient().setCookieStore(cookieStore);
    }

    private static void get(Context mContext, String url, AsyncHttpResponseHandler responseHandler) {
        getClient().get(mContext, url, responseHandler);
    }

    private static void post(Context mContext, String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        getClient().post(mContext, url, params, responseHandler);
        Timber.d("isis timeoutnya : " + String.valueOf(getClient().getConnectTimeout()));
    }

    private static void postSync(Context mContext, String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        getInstance().syncHttpClient.post(mContext, url, params, responseHandler);
    }

    private static AsyncHttpClient getClient()
    {
        // Return the synchronous HTTP client when the thread is not prepared
        if (Looper.myLooper() == null) {
            return getInstance().syncHttpClient;
        }

        return getInstance().asyncHttpClient;
    }

    private static String getBasicAuth() {
        String stringEncode = BuildConfig.headerUser+":"+BuildConfig.headerPass;
        byte[] encodeByte = Base64.encodeBase64(stringEncode.getBytes());
        String encode = new String(encodeByte);
        return encode.replace('+','-').replace('/','_');
    }
    private SSLSocketFactory getSSLSocketFactory(){
        try {
            // Get an instance of the Bouncy Castle KeyStore format
            KeyStore trusted = KeyStore.getInstance("BKS");
            // Get the raw resource, which contains the keystore with
            // your trusted certificates (root and any intermediate certs)
            InputStream in = getmContext().getResources().openRawResource(R.raw.mobile_goworld_asia);
            try {
                // Initialize the keystore with the provided trusted certificates
                // Also provide the password of the keystore
                trusted.load(in, PRIVATE_KEY.toCharArray());
            } finally {
                in.close();
            }
            // Pass the keystore to the SSLSocketFactory. The factory is responsible
            // for the verification of the server certificate.
            SSLSocketFactory sf = new SSLSocketFactory(trusted);
            // Hostname verification from certificate
            // http://hc.apache.org/httpcomponents-client-ga/tutorial/html/connmgmt.html#d4e506
            sf.setHostnameVerifier(SSLSocketFactory.STRICT_HOSTNAME_VERIFIER);
            return sf;
        } catch (Exception e) {
            throw new AssertionError(e);
        }
    }

    private MySSLSocketFactory getUntrustSSLSocketFactory(){
        try {
            // Get an instance of the Bouncy Castle KeyStore format
            KeyStore trusted = KeyStore.getInstance("BKS");
            // Get the raw resource, which contains the keystore with
            // your trusted certificates (root and any intermediate certs)
            InputStream in = getmContext().getResources().openRawResource(R.raw.mobile_goworld_asia);
            try {
                // InitializeAddress the keystore with the provided trusted certificates
                // Also provide the password of the keystore
                trusted.load(in, PRIVATE_KEY.toCharArray());
            } finally {
                in.close();
            }
            // Pass the keystore to the SSLSocketFactory. The factory is responsible
            // for the verification of the server certificate.

            MySSLSocketFactory test = new MySSLSocketFactory(trusted);
            test.setHostnameVerifier(MySSLSocketFactory.STRICT_HOSTNAME_VERIFIER);

            return test;
        } catch (Exception e) {
            throw new AssertionError(e);
        }
    }

    public static void CancelRequestWS(Context _context,Boolean interruptIfRunning)
    {
        getClient().cancelRequests(_context, interruptIfRunning);
    }
    //----------------------------------------------------------------------------------------------------

    public static void sentDataLogin(Context mContext, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        Timber.wtf("address Login:" + LINK_LOGIN);
        post(mContext, LINK_LOGIN, params, responseHandler);
    }

    public static void sentDataListMember(Context mContext, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        Timber.wtf("address Member Retrieve:"+LINK_LIST_MEMBER);
        post(mContext,LINK_LIST_MEMBER, params, responseHandler);
    }

    public static void sentDataReqTokenSGOL(Context mContext, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        Timber.wtf("address Inquiry Trx:"+LINK_REQ_TOKEN_SGOL);
        post(mContext,LINK_REQ_TOKEN_SGOL, params, responseHandler);
    }

    public static void sentResendTokenSGOL(Context mContext, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        Timber.wtf("address InquiryResendToken:"+LINK_RESEND_TOKEN_SGOL);
        post(mContext,LINK_RESEND_TOKEN_SGOL, params, responseHandler);
    }

    public static void sentInsertTransTopup(Context mContext, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        Timber.wtf("address Insert Trx:"+LINK_INSERT_TRANS_TOPUP);
        post(mContext,LINK_INSERT_TRANS_TOPUP, params, responseHandler);
    }

    public static void getSaldo(Context mContext, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        Timber.wtf("address Get Saldo:"+LINK_SALDO);
        post(mContext,LINK_SALDO, params, responseHandler);
    }

    public static void getBankList(Context mContext, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        Timber.wtf("address Get Bank list:"+LINK_BANK_LIST);
        post(mContext,LINK_BANK_LIST, params, responseHandler);
    }

    public static void sentReqTokenRegister(Context mContext, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        Timber.wtf("address Resend token Register:"+LINK_REQ_TOKEN_REGIST);
        post(mContext,LINK_REQ_TOKEN_REGIST, params, responseHandler);
    }

    public static void sentTopupPulsaRetailValidation(Context mContext, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        String linknya = LINK_TOPUP_PULSA_RETAIL ;
        if(IS_INTERNET_BANKING){
            if(IS_PROD)linknya = LINK_PROD_TOPUP_RETAIL;
        }

        Timber.wtf("address TOPUP Pulsa:"+linknya);
        post(mContext,linknya, params, responseHandler);
    }

    public static void sentUpdateProfile(Context mContext, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        Timber.wtf("address Update Profile:"+LINK_UPDATE_PROFILE);
        post(mContext,LINK_UPDATE_PROFILE, params, responseHandler);
    }

    public static void sentValidTopUp(Context mContext, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        Timber.wtf("address Validation Topup:"+LINK_VALID_TOPUP);
        post(mContext,LINK_VALID_TOPUP, params, responseHandler);
    }

    public static void sentChangePassword(Context mContext, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        Timber.wtf("address CHange Password:"+LINK_CHANGE_PASSWORD);
        post(mContext,LINK_CHANGE_PASSWORD, params, responseHandler);
    }

    public static void sentReqForgotPassword(Context mContext, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        Timber.wtf("address req Forget Password:"+LINK_REQ_FORGOT_PASSWORD);
        post(mContext,LINK_REQ_FORGOT_PASSWORD, params, responseHandler);
    }

    public static void sentForgotPassword(Context mContext, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        Timber.wtf("address Forget Password:"+LINK_FORGOT_PASSWORD);
        post(mContext,LINK_FORGOT_PASSWORD, params, responseHandler);
    }

    public static void sentMemberPulsa(Context mContext, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        Timber.wtf("address Get Member Pulsa:"+LINK_MEMBER_PULSA);
        post(mContext,LINK_MEMBER_PULSA, params, responseHandler);
    }

    public static void sentInsertContact(Context mContext, RequestParams params, Boolean isSync, AsyncHttpResponseHandler responseHandler) {
        Timber.wtf("address InquiryResendToken:"+LINK_USER_CONTACT_INSERT);
        if (isSync) {
            postSync(mContext,LINK_USER_CONTACT_INSERT, params, responseHandler);
        }else
            post(mContext,LINK_USER_CONTACT_INSERT, params, responseHandler);
    }

    public static void sentUpdateContact(Context mContext, RequestParams params, Boolean isSync, AsyncHttpResponseHandler responseHandler) {
        Timber.wtf("address Update Contact:"+LINK_USER_CONTACT_UPDATE);
        if (isSync) {
            postSync(mContext, LINK_USER_CONTACT_UPDATE, params, responseHandler);
        }else
            post(mContext,LINK_USER_CONTACT_UPDATE, params, responseHandler);
    }

    public static void sentListBiller(Context mContext, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        Timber.wtf("address List Biller:"+LINK_LIST_BILLER);
        post(mContext,LINK_LIST_BILLER, params, responseHandler);
    }

    public static void sentDenomRetail(Context mContext, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        Timber.wtf("address Denom Retail:"+LINK_DENOM_RETAIL);
        post(mContext,LINK_DENOM_RETAIL, params, responseHandler);
    }

    public static void sentResendToken(Context mContext, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        Timber.wtf("address Resent Token :"+LINK_RESENT_TOKEN_BILLER);
        post(mContext,LINK_RESENT_TOKEN_BILLER, params, responseHandler);
    }

    public static void sentProfilePicture(Context mContext, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        Timber.wtf("address Upload Profile Picture:"+LINK_UPLOAD_PROFILE_PIC);
        post(mContext,LINK_UPLOAD_PROFILE_PIC, params, responseHandler);
    }

    public static void sentReqTokenP2P(Context mContext, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        Timber.wtf("address ReqToken P2P:"+LINK_REQ_TOKEN_P2P);
        post(mContext,LINK_REQ_TOKEN_P2P, params, responseHandler);
    }

    public static void sentConfirmTransP2P(Context mContext, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        Timber.wtf("address sent Confirm Trans P2P:"+LINK_CONFIRM_TRANS_P2P);
        post(mContext,LINK_CONFIRM_TRANS_P2P, params, responseHandler);
    }

    public static void sentResentTokenP2P(Context mContext, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        Timber.wtf("address sent Resent Token P2P:"+LINK_RESENT_TOKEN_P2P);
        post(mContext,LINK_RESENT_TOKEN_P2P, params, responseHandler);
    }

    public static void sentSubmitAskForMoney(Context mContext, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        Timber.wtf("address sent AskForMoneySubmit:"+LINK_ASKFORMONEY_SUBMIT);
        post(mContext,LINK_ASKFORMONEY_SUBMIT, params, responseHandler);
    }

    public static void sentRetrieveNotif(Context mContext, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        Timber.wtf("address sent Notif Retrieve:"+LINK_NOTIF_RETRIEVE);
        post(mContext,LINK_NOTIF_RETRIEVE, params, responseHandler);
    }

    public static void sentReadNotif(Context mContext, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        Timber.wtf("address sent Notif Read:"+LINK_NOTIF_READ);
        post(mContext,LINK_NOTIF_READ, params, responseHandler);
    }

    public static void sentReqTokenP2PNotif(Context mContext, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        Timber.wtf("address sent Req Token p2p Notif:"+LINK_REQ_TOKEN_P2P_NOTIF);
        post(mContext,LINK_REQ_TOKEN_P2P_NOTIF, params, responseHandler);
    }

    public static void sentConfirmTransP2PNotif(Context mContext, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        Timber.wtf("address sent confirm trans p2p notif:"+LINK_CONFIRM_TRANS_P2P_NOTIF);
        post(mContext,LINK_CONFIRM_TRANS_P2P_NOTIF, params, responseHandler);
    }

    public static void sentGetTRXStatus(Context mContext, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        Timber.wtf("address sent get trx status:"+LINK_GET_TRX_STATUS);
        post(mContext,LINK_GET_TRX_STATUS, params, responseHandler);
    }

    public static void sentGetTrxReport(Context mContext, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        Timber.wtf("address sent transaction report:"+LINK_TRANSACTION_REPORT);
        post(mContext,LINK_TRANSACTION_REPORT, params, responseHandler);
    }

    public static void sentPaymentBiller(Context mContext, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        Timber.wtf("address sent link payment biller:"+LINK_PAYMENT_BILLER);
        post(mContext,LINK_PAYMENT_BILLER, params, responseHandler);
    }
	
	public static void getGroupList(Context mContext, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        Timber.wtf("address sent group list:"+LINK_GROUP_LIST);
        post(mContext,LINK_GROUP_LIST, params, responseHandler);
    }

    public static void sentAddGroup(Context mContext, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        Timber.wtf("address sent add grup:"+LINK_ADD_GROUP);
        post(mContext,LINK_ADD_GROUP, params, responseHandler);
    }

    public static void getTimelineList(Context mContext, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        Timber.wtf("address sent timeline list:"+LINK_TIMELINE_LIST);
        post(mContext,LINK_TIMELINE_LIST, params, responseHandler);
    }

    public static void getCommentList(Context mContext, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        Timber.wtf("address sent comment list:"+LINK_COMMENT_LIST);
        post(mContext,LINK_COMMENT_LIST, params, responseHandler);
    }

    public static void sentAddComment(Context mContext, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        Timber.wtf("address sent add coment:"+LINK_ADD_COMMENT);
        post(mContext,LINK_ADD_COMMENT, params, responseHandler);
    }

    public static void sentRemoveComment(Context mContext, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        Timber.wtf("address sent remove comment:"+LINK_REMOVE_COMMENT);
        post(mContext,LINK_REMOVE_COMMENT, params, responseHandler);
    }

    public static void getLikeList(Context mContext, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        Timber.wtf("address sent like list:"+LINK_LIKE_LIST);
        post(mContext,LINK_LIKE_LIST, params, responseHandler);
    }

    public static void sentAddLike(Context mContext, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        Timber.wtf("address sent add like:"+LINK_ADD_LIKE);
        post(mContext,LINK_ADD_LIKE, params, responseHandler);
    }

    public static void sentRemoveLike(Context mContext, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        Timber.wtf("address sent remove like:"+LINK_REMOVE_LIKE);
        post(mContext,LINK_REMOVE_LIKE, params, responseHandler);
    }

    public static void sentCreatePin(Context mContext, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        Timber.wtf("address sent create pin:"+LINK_CREATE_PIN);
        post(mContext,LINK_CREATE_PIN, params, responseHandler);
    }

    public static void sentChangePin(Context mContext, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        Timber.wtf("address sent change pin:"+LINK_CHANGE_PIN);
        post(mContext,LINK_CHANGE_PIN, params, responseHandler);
    }

    public static void getPromoList(Context mContext, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        Timber.wtf("address sent promo list:"+LINK_PROMO_LIST);
        post(mContext,LINK_PROMO_LIST, params, responseHandler);
    }

    public static void sentInquiryBiller(Context mContext, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        Timber.wtf("address sent Inquiry Biller:"+LINK_INQUIRY_BILLER);
        post(mContext,LINK_INQUIRY_BILLER, params, responseHandler);
    }

    public static void sentBankAccountCollection(Context mContext, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        Timber.wtf("address sent Bank Account Collect:"+LINK_BANK_ACCOUNT_COLLECTION);
        post(mContext,LINK_BANK_ACCOUNT_COLLECTION, params, responseHandler);
    }

    public static void sentTopUpAccountCollection(Context mContext, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        Timber.wtf("address sent Top up Account collect:"+LINK_TOP_UP_ACCOUNT_COLLECTION);
        post(mContext,LINK_TOP_UP_ACCOUNT_COLLECTION, params, responseHandler);
    }

    public static void sentCommAccountCollection(Context mContext, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        Timber.wtf("address sent comm account collect:"+LINK_COMM_ACCOUNT_COLLECTION);
        post(mContext,LINK_COMM_ACCOUNT_COLLECTION, params, responseHandler);
    }

    public static void sentListBankBiller(Context mContext, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        Timber.wtf("address sent list bank biller:"+LINK_LIST_BANK_BILLER);
        post(mContext,LINK_LIST_BANK_BILLER, params, responseHandler);
    }

    public static void sentCommEspay(Context mContext, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        Timber.wtf("address sent list bank biller:"+LINK_COMM_ESPAY);
        post(mContext,LINK_COMM_ESPAY, params, responseHandler);
	}
		
	public static void getHelpList(Context mContext, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        Timber.wtf("address sent Help List:"+LINK_HELP_LIST);
        post(mContext,LINK_HELP_LIST, params, responseHandler);
    }

    public static void getDataSB(Context mContext, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        Timber.wtf("address get Data SB:"+LINK_INQUIRY_MOBILE);
        post(mContext,LINK_INQUIRY_MOBILE, params, responseHandler);
    }

    public static void sentReqTokenSB(Context mContext, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        Timber.wtf("address sent Req Token SB:"+LINK_REQUEST_TOKEN_SB);
        post(mContext,LINK_REQUEST_TOKEN_SB, params, responseHandler);
    }

    public static void sentConfTokenSB(Context mContext, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        Timber.wtf("address sent confirm token SB:"+LINK_CONFIRM_TOKEN_SB);
        post(mContext,LINK_CONFIRM_TOKEN_SB, params, responseHandler);
    }

    public static void sentReportEspay(Context mContext, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        Timber.wtf("address sent ReportEspay:"+LINK_REPORT_ESPAY);
        post(mContext,LINK_REPORT_ESPAY, params, responseHandler);
    }

    public static void sentInquiryMobileJatim(Context mContext, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        Timber.wtf("address sent Inquiry mobile Jatim:"+LINK_INQUIRY_MOBILE_JATIM);
        post(mContext,LINK_INQUIRY_MOBILE_JATIM, params, responseHandler);
    }

    public static void sentConfirmTokenJatim(Context mContext, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        Timber.wtf("address sent Confirm token Jatim:"+LINK_CONFIRM_TOKEN_JATIM);
        post(mContext,LINK_CONFIRM_TOKEN_JATIM, params, responseHandler);
    }

    public static void sentListBankSMSRegist(Context mContext, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        Timber.wtf("address sent list bank sms regist:"+LINK_LIST_BANK_SMS_REGIST);
        post(mContext,LINK_LIST_BANK_SMS_REGIST, params, responseHandler);
    }

    public static void sentUpdateSocMed(Context mContext, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        Timber.wtf("isi address sent update SocMend:"+LINK_UPDATE_SOCMED);
        post(mContext,LINK_UPDATE_SOCMED, params, responseHandler);
    }
	
	public static void sentLogout(Context mContext, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        Timber.wtf("address sent logout:"+LINK_LOGOUT);
        post(mContext,LINK_LOGOUT, params, responseHandler);
    }

    public static void sentCreatePinPass(Context mContext, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        Timber.wtf("address sent create pin pass:"+LINK_CREATE_PIN_PASS);
        post(mContext,LINK_CREATE_PIN_PASS, params, responseHandler);
    }

    public static void sentReportAsk(Context mContext, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        Timber.wtf("address sent report ask:"+LINK_REPORT_MONEY_REQUEST);
        post(mContext,LINK_REPORT_MONEY_REQUEST, params, responseHandler);
    }

    public static void sentAsk4MoneyReject(Context mContext, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        Timber.wtf("address sent ask for money reject:"+LINK_ASK4MONEY_REJECT);
        post(mContext,LINK_ASK4MONEY_REJECT, params, responseHandler);
    }

    public static void inqCustomer(Context mContext, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        Timber.wtf("address sent inquiry customer:"+LINK_INQUIRY_CUST);
        post(mContext,LINK_INQUIRY_CUST, params, responseHandler);
    }

    public static void sentExecCust(Context mContext, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        Timber.wtf("address sent exec customer:"+LINK_EXEC_CUST);
        post(mContext,LINK_EXEC_CUST, params, responseHandler);
    }
	
	public static void sentReqCashout(Context mContext, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        Timber.wtf("address sent req cashout:"+LINK_REQUEST_CASHOUT);
        post(mContext,LINK_REQUEST_CASHOUT, params, responseHandler);
    }

    public static void sentConfCashout(Context mContext, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        Timber.wtf("address sent confirm cashout:"+LINK_CONFIRM_CASHOUT);
        post(mContext,LINK_CONFIRM_CASHOUT, params, responseHandler);
    }

    public static void sentInqWithdraw(Context mContext, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        Timber.wtf("address sent inq withdraw:"+LINK_INQUIRY_WITHDRAW);
        post(mContext,LINK_INQUIRY_WITHDRAW, params, responseHandler);
    }

    public static void sentReqCodeWithdraw(Context mContext, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        Timber.wtf("address sent reg code withdraw:"+LINK_REQCODE_WITHDRAW);
        post(mContext,LINK_REQCODE_WITHDRAW, params, responseHandler);
    }

    public static void sentDelTrxWithdraw(Context mContext, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        Timber.wtf("address sent del trx withdraw:"+LINK_DELTRX_WITHDRAW);
        post(mContext,LINK_DELTRX_WITHDRAW, params, responseHandler);
    }

    public static void sentCreatePass(Context mContext, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        Timber.wtf("address sent create pass:"+LINK_CREATE_PASS);
        post(mContext,LINK_CREATE_PASS, params, responseHandler);
    }

    public static void sentGetFailedPIN(Context mContext, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        Timber.wtf("address sent get failed pin:"+LINK_GET_FAILED_PIN);
        post(mContext,LINK_GET_FAILED_PIN, params, responseHandler);
    }

    public static void getATMTopUp(Context mContext, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        Timber.wtf("address get ATM top up:"+LINK_ATMTOPUP);
        post(mContext,LINK_ATMTOPUP, params, responseHandler);
    }

    public static void getBankCashout(Context mContext, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        Timber.wtf("address get bank cashout:"+LINK_BANKCASHOUT);
        post(mContext,LINK_BANKCASHOUT, params, responseHandler);
    }

    public static void sentUserProfile(Context mContext, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        Timber.wtf("address sent user profile:"+LINK_USER_PROFILE);
        post(mContext,LINK_USER_PROFILE, params, responseHandler);
    }

    public static void sentInquirySMS(Context mContext, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        Timber.wtf("address sent inquiry sms:"+LINK_INQUIRY_SMS);
        post(mContext,LINK_INQUIRY_SMS, params, responseHandler);
    }
    public static void sentClaimNonMemberTrf(Context mContext, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        Timber.wtf("address sent claim non member transfer:"+LINK_CLAIM_TRANSFER_NON_MEMBER);
        post(mContext,LINK_CLAIM_TRANSFER_NON_MEMBER, params, responseHandler);
    }

    public static void getInvoiceTagihan(Context mContext, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        Timber.wtf("address get invoice tagihan:"+LINK_INVOICE_TAGIHAN);
        post(mContext,LINK_INVOICE_TAGIHAN, params, responseHandler);
    }

    public static void getCommInv(Context mContext, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        Timber.wtf("address get comm invoice:"+LINK_COMM_INV);
        post(mContext,LINK_COMM_INV, params, responseHandler);
    }

    public static void sentPaymentInv(Context mContext, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        Timber.wtf("address sent payment inv:"+LINK_PAYMENT_INV);
        post(mContext,LINK_PAYMENT_INV, params, responseHandler);
    }

    public static void sentGetTRXStatus2(Context mContext, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        Timber.wtf("address sent get trx status 2:"+LINK_GET_TRX_STATUS2);
        post(mContext,LINK_GET_TRX_STATUS2, params, responseHandler);
    }

    public static void sentRequestCashIn(Context mContext, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        Timber.wtf("address sent request cash in:"+LINK_REQUEST_CASH_IN);
        post(mContext,LINK_REQUEST_CASH_IN, params, responseHandler);
    }

    public static void sentConfirmCashIn(Context mContext, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        Timber.wtf("address sent confirm cash in:"+LINK_CONFIRM_CASH_IN);
        post(mContext,LINK_CONFIRM_CASH_IN, params, responseHandler);
    }

    public static void sentInitiateATC(Context mContext, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        Timber.wtf("address sent initiate ATC:"+LINK_INITIATE_ATC);
        post(mContext,LINK_INITIATE_ATC, params, responseHandler);
    }

    public static void sentInquiryDataATC(Context mContext, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        Timber.wtf("address sent inquiry data ATC:"+LINK_INQUIRY_DATA_ATC);
        post(mContext,LINK_INQUIRY_DATA_ATC, params, responseHandler);
    }

    public static void sentCancelATC(Context mContext, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        Timber.wtf("address sent cancel ATC:"+LINK_CANCEL_ATC);
        post(mContext,LINK_CANCEL_ATC, params, responseHandler);
    }

    public static void sentInquiryTokenATC(Context mContext, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        Timber.wtf("address sent inquiry token ATC:"+LINK_INQUIRY_TOKEN_ATC);
        post(mContext,LINK_INQUIRY_TOKEN_ATC, params, responseHandler);
    }

    public static void sentConfirmTokenATC(Context mContext, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        Timber.wtf("address sent confirm token ATC:"+LINK_CONFIRM_TOKEN_ATC);
        post(mContext,LINK_CONFIRM_TOKEN_ATC, params, responseHandler);
    }

    public static void sentResendTokenLKD(Context mContext, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        Timber.wtf("address sent resend token LKD:"+LINK_RESEND_TOKEN_LKD);
        post(mContext,LINK_RESEND_TOKEN_LKD, params, responseHandler);
    }

    public static void sentReportLKD(Context mContext, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        Timber.wtf("address sent report LKD:"+LINK_REPORT_LKD);
        post(mContext,LINK_REPORT_LKD, params, responseHandler);
    }

    public static void sentRegisterSocMed(Context mContext, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        Timber.wtf("isi address sent register SocMend:" + LINK_REGISTER_SOCMED);
        post(mContext, LINK_REGISTER_SOCMED, params, responseHandler);
    }

    public static void sentListBankSavingAccount(Context mContext, RequestParams params,Boolean isSync, AsyncHttpResponseHandler responseHandler) {
        Timber.wtf("address ListBankSavingAccount:"+LINK_BANK_SAVING_ACCOUNT);
        if(isSync)
            postSync(mContext,LINK_BANK_SAVING_ACCOUNT, params, responseHandler);
        else
            post(mContext,LINK_BANK_SAVING_ACCOUNT, params, responseHandler);
    }

    public static void sentListSavingAccount(Context mContext, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        Timber.wtf("address listSavingAccount:"+LINK_SAVING_ACCOUNT_LIST);
        post(mContext,LINK_SAVING_ACCOUNT_LIST, params, responseHandler);
    }

    public static void sentDeleteSavingAccount(Context mContext, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        Timber.wtf("address deleteSavingAccount:"+LINK_DELETE_ACCOUNT_LIST);
        post(mContext,LINK_DELETE_ACCOUNT_LIST, params, responseHandler);
    }

    public static void sentDefaultSavingAccount(Context mContext, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        Timber.wtf("address defaultSavingAccount:"+LINK_DEFAULT_ACCOUNT_LIST);
        post(mContext,LINK_DEFAULT_ACCOUNT_LIST, params, responseHandler);
    }

    public static void sentReqAddAccount(Context mContext, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        Timber.wtf("address reqAddAccount:"+LINK_REQ_ADD_ACCOUNT);
        post(mContext,LINK_REQ_ADD_ACCOUNT, params, responseHandler);
    }

    public static void sentConfirmAddAccount(Context mContext, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        Timber.wtf("address confirmAddAccount:"+LINK_CONFIRM_ADD_ACCOUNT);
        post(mContext,LINK_CONFIRM_ADD_ACCOUNT, params, responseHandler);
    }

    public static void sentInqSavingTarget(Context mContext, RequestParams params,Boolean isSync, AsyncHttpResponseHandler responseHandler) {
        Timber.wtf("address inqSavingTarget:"+LINK_INQ_SAVING_TARGET);
        if(isSync)
            postSync(mContext,LINK_INQ_SAVING_TARGET,params,responseHandler);
        else
            post(mContext,LINK_INQ_SAVING_TARGET, params, responseHandler);
    }
    public static void sentAsk4RewardSubmit(Context mContext, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        Timber.wtf("address sent ask4reward submit:"+LINK_ASK4REWARD_SUBMIT);
        post(mContext,LINK_ASK4REWARD_SUBMIT, params, responseHandler);
    }

    public static void sentAsk4RewardDecline(Context mContext, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        Timber.wtf("address sent ask4reward decline:"+LINK_ASK4REWARD_DECLINE);
        post(mContext,LINK_ASK4REWARD_DECLINE, params, responseHandler);
    }

    public static void insertSavingTarget(Context mContext, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        Timber.wtf("address insert Target Saving:"+LINK_SAVING_TARGET);
        post(mContext,LINK_SAVING_TARGET, params, responseHandler);
    }

    public static void getInqAutoSaving(Context mContext, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        Timber.wtf("address InqAutoSaving:"+ LINK_INQ_AUTOSAVING);
        post(mContext, LINK_INQ_AUTOSAVING, params, responseHandler);
    }

    public static void deleteAutoSaving(Context mContext, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        Timber.wtf("address deleteAutoSaving:"+ LINK_DELETE_AUTOSAVING);
        post(mContext, LINK_DELETE_AUTOSAVING, params, responseHandler);
    }

    public static void addAutoSaving(Context mContext, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        Timber.wtf("address addAutoSaving:"+ LINK_ADD_AUTOSAVING);
        post(mContext, LINK_ADD_AUTOSAVING, params, responseHandler);
    }

    public static void deleteTargetSaving(Context mContext, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        Timber.wtf("address deleteTargetSaving:" + LINK_DELETE_TARGETSAVING);
        post(mContext, LINK_DELETE_TARGETSAVING, params, responseHandler);
    }

    public static void confirmtokenAutoSaving(Context mContext, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        Timber.wtf("address list saving:"+ LINK_CONFIRMTOKEN_AUTOSAVING);
        post(mContext, LINK_CONFIRMTOKEN_AUTOSAVING, params, responseHandler);
    }

    public static void setStatusAutoSaving(Context mContext, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        Timber.wtf("address list saving:"+ LINK_SET_AUTOSAVING_STATUS);
        post(mContext, LINK_SET_AUTOSAVING_STATUS, params, responseHandler);
    }

    public static void getCanteenMerchants(Context mContext, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        Timber.wtf("address canteen merchants:"+ LINK_CANTEEN_MERCHANTS);
        post(mContext, LINK_CANTEEN_MERCHANTS, params, responseHandler);
    }

    public static void getCanteenCatalogs(Context mContext, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        Timber.wtf("address canteen catalogs:"+ LINK_CANTEEN_CATALOGS);
        post(mContext, LINK_CANTEEN_CATALOGS, params, responseHandler);
    }

    public static void sentReqOrderCatalog(Context mContext, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        Timber.wtf("address req order catalog:"+ LINK_REQ_ORDER_CATALOG);
        post(mContext, LINK_REQ_ORDER_CATALOG, params, responseHandler);
    }

    public static void sentConfirmOrderCatalog(Context mContext, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        Timber.wtf("address confirm order catalog:"+ LINK_CONFIRM_ORDER_CATALOG);
        post(mContext, LINK_CONFIRM_ORDER_CATALOG, params, responseHandler);
    }
    public static void getPurchaseList(Context mContext, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        Timber.wtf("address confirm order catalog:"+ LINK_PURCHASE_LIST);
        post(mContext, LINK_PURCHASE_LIST, params, responseHandler);
    }
    public static void sentCheckCouponCode(Context mContext, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        Timber.wtf("address confirm order catalog:"+ LINK_CHECK_COUPON_CODE);
        post(mContext, LINK_CHECK_COUPON_CODE, params, responseHandler);
    }
    public static void sentConfirmCouponCode(Context mContext, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        Timber.wtf("address confirm order catalog:"+ LINK_CONFIRM_COUPON_CODE);
        post(mContext, LINK_CONFIRM_COUPON_CODE, params, responseHandler);
    }
    public static void getTodaySales(Context mContext, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        Timber.wtf("address get today sales:"+ LINK_TODAY_SALES);
        post(mContext, LINK_TODAY_SALES, params, responseHandler);
    }
    public static void getListUntakenItem(Context mContext, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        Timber.wtf("address get untaken item:"+ LINK_UNTAKEN_ITEM);
        post(mContext, LINK_UNTAKEN_ITEM, params, responseHandler);
    }
    public static void sentActiveDeactiveItem(Context mContext, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        Timber.wtf("address sent active deactive:"+ LINK_ACTIVE_DEACTIVE_ITEM);
        post(mContext, LINK_ACTIVE_DEACTIVE_ITEM, params, responseHandler);
    }
    public static void getBuyerList(Context mContext, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        Timber.wtf("address get buyer list:"+ LINK_BUYER_LIST);
        post(mContext, LINK_BUYER_LIST, params, responseHandler);
    }
    public static void sentAddItem(Context mContext, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        Timber.wtf("address add item stock:"+ LINK_ADD_ITEM);
        post(mContext, LINK_ADD_ITEM, params, responseHandler);
    }
    public static void sentDeleteItem(Context mContext, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        Timber.wtf("address add item stock:"+ LINK_DELETE_ITEM);
        post(mContext, LINK_DELETE_ITEM, params, responseHandler);
    }
    public static void sentUpdateItem(Context mContext, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        Timber.wtf("address update item stock:"+ LINK_UPDATE_ITEM);
        post(mContext, LINK_UPDATE_ITEM, params, responseHandler);
    }
    public static void sentDetailsItem(Context mContext, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        Timber.wtf("address add item stock:"+ LINK_DETAILS_ITEM);
        post(mContext, LINK_DETAILS_ITEM, params, responseHandler);
    }

    public static void sentCancelOrder(Context mContext, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        Timber.wtf("address cancel order:"+ LINK_CANCEL_ORDER);
        post(mContext, LINK_CANCEL_ORDER, params, responseHandler);
    }

    public static void sentRegStep1(Context mContext, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        Timber.wtf("address reg step 1:"+ LINK_REG_STEP1);
        post(mContext, LINK_REG_STEP1, params, responseHandler);
    }

    public static void sentRegStep2(Context mContext, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        Timber.wtf("address reg step 2:"+ LINK_REG_STEP2);
        post(mContext, LINK_REG_STEP2, params, responseHandler);
    }

    public static void sentRegStep3(Context mContext, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        Timber.wtf("address reg step 3:"+ LINK_REG_STEP3);
        post(mContext, LINK_REG_STEP3, params, responseHandler);
    }

    public static void sentRegStep3FB(Context mContext, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        Timber.wtf("address reg step 3FB:"+ LINK_REG_STEP3FB);
        post(mContext, LINK_REG_STEP3FB, params, responseHandler);
    }

    public static void sentReqChangeEmail(Context mContext, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        Timber.wtf("address req change email:"+ LINK_REQ_CHANGE_EMAIL);
        post(mContext, LINK_REQ_CHANGE_EMAIL, params, responseHandler);
    }

    public static void sentConfirmChangeEmail(Context mContext, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        Timber.wtf("address confirm change email:"+ LINK_CONFIRM_CHANGE_EMAIL);
        post(mContext, LINK_CONFIRM_CHANGE_EMAIL, params, responseHandler);
    }

    public static void sentRequestUpgradeUser(Context mContext, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        Timber.wtf("address request upgrade user:"+ LINK_REQUEST_UPGRADE);
        post(mContext, LINK_REQUEST_UPGRADE, params, responseHandler);
    }

    public static void sentConfirmUpgradeUser(Context mContext, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        Timber.wtf("address confirm upgrade user:"+ LINK_CONFIRM_UPGRADE);
        post(mContext, LINK_CONFIRM_UPGRADE, params, responseHandler);
    }

    public static void sentPairingRequest(Context mContext, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        Timber.wtf("address pairing request:"+ LINK_UOB_PAIRING_REQUEST);
        post(mContext, LINK_UOB_PAIRING_REQUEST, params, responseHandler);
    }

    public static void sentPairingCancel(Context mContext, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        Timber.wtf("address pairing cancel:"+ LINK_UOB_PAIRING_CANCEL);
        post(mContext, LINK_UOB_PAIRING_CANCEL, params, responseHandler);
    }

    public static void sentPairingConfirm(Context mContext, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        Timber.wtf("address pairing confirm:"+ LINK_UOB_PAIRING_CONFIRM);
        post(mContext, LINK_UOB_PAIRING_CONFIRM, params, responseHandler);
    }

    //get Data------------------------------------------------------------------------------------------


    public static void getBillerType(Context mContext, AsyncHttpResponseHandler responseHandler) {
        Timber.wtf("address Get Biller Type:"+LINK_GET_BILLER_TYPE);
        get(mContext, LINK_GET_BILLER_TYPE, responseHandler);
    }

    public static void getAllBank(Context mContext, AsyncHttpResponseHandler responseHandler) {
        get(mContext,LINK_GET_ALL_BANK, responseHandler);
    }

    public static void getAppVersion(Context mContext, AsyncHttpResponseHandler responseHandler) {
        Timber.wtf("address AppVersion:"+LINK_APP_VERSION);
        get(mContext,LINK_APP_VERSION, responseHandler);
    }
	
	public static void getHelpPIN(Context mContext, AsyncHttpResponseHandler responseHandler) {
        Timber.wtf("address getHelpPIN:"+LINK_HELP_PIN);
        get(mContext,LINK_HELP_PIN, responseHandler);
    }

    private Context getmContext() {
        return mContext;
    }

    private void setmContext(Context mContext) {
        this.mContext = mContext;
    }

}
