package com.sgo.mayapada.coreclass;/*
  Created by Administrator on 10/1/2015.
 */

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.share.ShareApi;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareOpenGraphAction;
import com.facebook.share.model.ShareOpenGraphContent;
import com.facebook.share.model.ShareOpenGraphObject;
import com.facebook.share.model.SharePhoto;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.securepreferences.SecurePreferences;
import com.sgo.mayapada.BuildConfig;
import com.sgo.mayapada.R;

import org.apache.http.Header;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.Locale;

import timber.log.Timber;

public class FacebookFunction {

    public interface UpdateDataFBListener{
        void OnUpdateDataFBSuccess();
        void OnUpdateDataFBFailed();
    }

    public interface LoginFBListener{
        void OnLoginFBSuccess();
        void OnLoginFBFailed(FacebookException error);
        void OnProfileChange();
    }

    public interface AccessTokenTrackerListener{
        void OnAccessTokenChange(AccessToken accessToken);
    }

    private AccessToken FinalAT;
    private Profile FinalProf;

    private CallbackManager mCallBackManager;
    private AccessTokenTracker mATT;
    private ProfileTracker mPT;
    private Context mContext;
    private UpdateDataFBListener mUpdateDatalistener;
    private LoginFBListener mLoginFBlistener;
    private AccessTokenTrackerListener mAccessTokenTrackerListener;


    public FacebookFunction(){
        this.setFinalAT(AccessToken.getCurrentAccessToken());
        this.setFinalProf(Profile.getCurrentProfile());
    }

    public void setTargetFragment(Fragment fragment,LoginFBListener listener){
        setTargetFragment(fragment);
        if(listener != null)
            mLoginFBlistener = listener;
    }

    public void setTargetFragment(Fragment fragment){
        if(mUpdateDatalistener == null) {
            if(fragment instanceof UpdateDataFBListener)
                mUpdateDatalistener = (UpdateDataFBListener) fragment;
        }
        if(mLoginFBlistener == null){
            if(fragment instanceof LoginFBListener)
                mLoginFBlistener = (LoginFBListener) fragment;
        }
    }

    public FacebookFunction(Context context){

        Timber.d("FacebookFunction contructor"+"masuk");

//        FacebookSdk.addLoggingBehavior(LoggingBehavior.REQUESTS);
        this.setmContext(context);
        if(mUpdateDatalistener == null) {
            if(context instanceof UpdateDataFBListener)
                mUpdateDatalistener = (UpdateDataFBListener) context;
        }
        if(mLoginFBlistener == null){
            if(context instanceof LoginFBListener)
                mLoginFBlistener = (LoginFBListener) context;
        }

        if(mAccessTokenTrackerListener == null){
            if(context instanceof AccessTokenTrackerListener)
                mAccessTokenTrackerListener = (AccessTokenTrackerListener) context;
        }

        mCallBackManager = CallbackManager.Factory.create();
        AccessTokenTracker aTT = new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(AccessToken oldAccessToken, AccessToken currentAccessToken) {
                if(oldAccessToken != null)
                    Log.d("old access token", oldAccessToken.getToken() + " / " + oldAccessToken.getExpires().toString() + " / " + oldAccessToken.getUserId());
                if(currentAccessToken != null) {
                    Log.d("current access token", currentAccessToken.getToken() + " / " + currentAccessToken.getExpires().toString() + " / " + currentAccessToken.getUserId());
                    setFinalAT(currentAccessToken);
                }
                else
                    setFinalAT(null);
                if(mAccessTokenTrackerListener != null)
                    mAccessTokenTrackerListener.OnAccessTokenChange(getFinalAT());
            }
        };
        setmATT(aTT);

        ProfileTracker pT = new ProfileTracker() {
            @Override
            protected void onCurrentProfileChanged(Profile oldProfile, Profile currentProfile) {
                if(oldProfile != null)
                    Log.d("old profile", oldProfile.toString());
                if(currentProfile != null) {
                    Log.d("current profile", currentProfile.toString());
                    setFinalProf(currentProfile);
                }
                else
                    setFinalProf(null);
                mLoginFBlistener.OnProfileChange();
            }
        };
        setmPT(pT);


        this.setFinalAT(AccessToken.getCurrentAccessToken());
        this.setFinalProf(Profile.getCurrentProfile());

        Timber.d("Tracking profile : "+String.valueOf(getmPT().isTracking()));
        Timber.d("Tracking AccessToken : "+String.valueOf(getmATT().isTracking()));
    }

    public void loginPublish(Activity activity,Boolean isGetDataFB){
        loginPublish(activity,null,isGetDataFB);
    }

    private void loginPublish(Activity activity, UpdateDataFBListener listener, Boolean isGetDataFB){
        if(mLoginFBlistener == null)
            throw new RuntimeException("LoginFBListener not implemented in activity/fragment");
        else {
            if (!isLoginAndPublish()) {
                LoginManager.getInstance().logInWithPublishPermissions(activity, Arrays.asList("publish_actions"));
                loginProccess(listener, isGetDataFB);
            }
        }
    }

    public void loginPublish(Fragment fragment,Boolean isGetDataFB){
       loginPublish(fragment,null,isGetDataFB);
    }

    private void loginPublish(Fragment fragment, UpdateDataFBListener listener, Boolean isGetDataFB){
        if(mLoginFBlistener == null)
            throw new RuntimeException("LoginFBListener not implemented in activity/fragment");
        else {
            if (!isLoginAndPublish()) {
                LoginManager.getInstance().logInWithPublishPermissions(fragment, Arrays.asList("publish_actions"));
                loginProccess(listener, isGetDataFB);
            }
        }
    }

    public void loginRead(Activity activity,Boolean isGetDataFB){
       loginRead(activity,null,isGetDataFB);
    }

    public void loginRead(Activity activity, UpdateDataFBListener listener,Boolean isGetDataFB){
        if(mLoginFBlistener == null)
            throw new RuntimeException("LoginFBListener not implemented in activity/fragment");
        else {
            if (!isLogin()) {
                LoginManager.getInstance().logInWithReadPermissions(activity, Arrays.asList("email", "public_profile", "user_friends"));
                loginProccess(listener, isGetDataFB);
            }
        }
    }

    public void loginRead(Fragment fragment,Boolean isGetDataFB){
        loginRead(fragment,null,isGetDataFB);
    }

    private void loginRead(Fragment fragment, UpdateDataFBListener listener, Boolean isGetDataFB){
        if(mLoginFBlistener == null)
            throw new RuntimeException("LoginFBListener not implemented in activity/fragment");
        else {
            if (!isLogin()) {
                LoginManager.getInstance().logInWithReadPermissions(fragment, Arrays.asList("email", "public_profile", "user_friends"));
                loginProccess(listener, isGetDataFB);
            }
        }
    }

    public Boolean isLogin(){
        return FinalAT != null && !FinalAT.isExpired() && !FinalAT.getToken().isEmpty();
    }

    private Boolean isHavePublishPermission() {
        return isLogin() && getFinalAT().getPermissions().contains("publish_actions");
    }

    public Boolean isLoginAndPublish(){
        return isLogin() && isHavePublishPermission();
    }

    public void logout(){
        if(getmATT()!=null) {
            getmATT().stopTracking();
            getmPT().stopTracking();
            Profile.setCurrentProfile(null);
        }
        LoginManager.getInstance().logOut();
        setFinalAT(null);
        setFinalProf(null);
    }

    public void logoutWithData(UpdateDataFBListener listener){
        logout();
        sentUpdateDataFace(null,true,listener);
    }

    private void loginProccess(final UpdateDataFBListener listener, final Boolean isGetDataFB){
        LoginManager.getInstance().registerCallback(getmCallBackManager(), new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                AccessToken mAT = loginResult.getAccessToken();
                Timber.d("is profile tracker : " + String.valueOf(getmPT().isTracking()) );
                Timber.d("is access tracker : " + String.valueOf(getmATT().isTracking()) );
                if(mAT != null) {
                    setFinalAT(mAT);
                    Timber.d("granted permission," + mAT.getPermissions().toString());
                    Timber.d("denied permission,"+ mAT.getDeclinedPermissions().toString());
                    mLoginFBlistener.OnLoginFBSuccess();
                    if(isGetDataFB) {
                        getUserData(new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(JSONObject object, GraphResponse response) {
                                if (object.has(DefineValue.ERROR))
                                    Timber.d("Error GraphRequest : " + object.toString());
                                else
                                    sentUpdateDataFace(object, false, listener);
                            }

                        });
                    }
                }
                else
                    setFinalAT(null);
                Timber.d("masuk onSUCCESS");
            }

            @Override
            public void onCancel() {
                Timber.d("masuk onCancel");
                Timber.d("Tracking profile onCancel : "+String.valueOf(getmPT().isTracking()));
                Timber.d("Tracking AccessToken onCancel : "+String.valueOf(getmATT().isTracking()));
                mLoginFBlistener.OnLoginFBFailed(null);
            }

            @Override
            public void onError(FacebookException error) {
                Timber.d("masuk onERROR");
                Timber.d("Tracking profile onError : "+String.valueOf(getmPT().isTracking()));
                Timber.d("Tracking AccessToken onError : "+String.valueOf(getmATT().isTracking()));
                mLoginFBlistener.OnLoginFBFailed(error);
            }
        });
    }


    public void getUserData(GraphRequest.GraphJSONObjectCallback mCallBack){

        if (isLogin()) {
            GraphRequest request = GraphRequest.newMeRequest(getFinalAT(), mCallBack);
            Bundle parameters = new Bundle();
            parameters.putString("fields", "id,name,email,gender,link,first_name, last_name, location, locale, timezone, verified");
            request.setParameters(parameters);
            request.executeAsync();
        }
        else{
            Toast.makeText(getmContext(),mContext.getString(R.string.ff_toast_at_empty),Toast.LENGTH_SHORT).show();
        }
    }

    private void sentUpdateDataFace(JSONObject dataFace, final Boolean isLogout , UpdateDataFBListener listener){
        if(mUpdateDatalistener != null)
            listener = mUpdateDatalistener;
        try{
            final SecurePreferences sp = CustomSecurePref.getInstance().getmSecurePrefs();
            String accessKey = sp.getString(DefineValue.ACCESS_KEY,"");
            String userID= sp.getString(DefineValue.USERID_PHONE,"");

            RequestParams params = MyApiClient.getSignatureWithParams(MyApiClient.COMM_ID,MyApiClient.LINK_UPDATE_SOCMED,
                    userID,accessKey);
            params.put(WebParams.USER_ID, userID);
            params.put(WebParams.DATE_TIME, DateTimeFormat.getCurrentDateTime());
            params.put(WebParams.FULL_NAME, Profile.getCurrentProfile().getName());
            params.put(WebParams.VALIDATE_ID, Profile.getCurrentProfile().getId());
            params.put(WebParams.FACEBOOK_CONNECT, DefineValue.NO);
            params.put(WebParams.COMM_ID, MyApiClient.COMM_ID);

            if(!isLogout && dataFace !=null){
                params.put(WebParams.FACEBOOK_CONNECT, DefineValue.YES);
                params.put(WebParams.EMAIL, dataFace.optString(WebParams.EMAIL,""));
                params.put(WebParams.GENDER, dataFace.optString(WebParams.GENDER,""));

                String localeFace = dataFace.getString(WebParams.LOCALE);
                Locale mLoc = LocaleUtils.fromString(localeFace);
                params.put(WebParams.COUNTRY, mLoc.getDisplayCountry());
                params.put(WebParams.LANGUAGE, mLoc.getDisplayLanguage());

                params.put(WebParams.LOCATION,"");
                params.put(WebParams.PROFILE_URL, "https://graph.facebook.com/" + dataFace.optString(WebParams.ID,"") + "/picture?type=large");

            }
            Timber.d("isi params update facebook:" + params.toString());

            final UpdateDataFBListener finalListener = listener;
            MyApiClient.sentUpdateSocMed(getmContext(),params,new JsonHttpResponseHandler() {

                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    Timber.d("isi response update facebook:" + response.toString());
                    try {
                        String code = response.getString(WebParams.ERROR_CODE);
                        if (code.equals(WebParams.SUCCESS_CODE)) {
                            if(!isLogout) {
                                SecurePreferences.Editor mEditor = sp.edit();
                                String nama = response.optString(WebParams.FULL_NAME, "");
                                String email = response.optString(WebParams.EMAIL, "");
                                String country = response.optString(WebParams.COUNTRY, "");
                                String img_url = response.getString(WebParams.IMG_URL);

                                if (!nama.isEmpty()) {
                                    mEditor.putString(DefineValue.USER_NAME, nama);
                                    mEditor.putString(DefineValue.CUST_NAME, nama);
                                    mEditor.putString(DefineValue.PROFILE_FULL_NAME, nama);
                                }

                                if (!country.isEmpty())
                                    mEditor.putString(DefineValue.PROFILE_COUNTRY, country);

                                if (!email.isEmpty())
                                    mEditor.putString(DefineValue.PROFILE_EMAIL, email);

                                if (!img_url.isEmpty()) {
                                    mEditor.putString(DefineValue.IMG_URL, img_url);
                                    mEditor.putString(DefineValue.IMG_SMALL_URL, response.getString(WebParams.IMG_SMALL_URL));
                                    mEditor.putString(DefineValue.IMG_MEDIUM_URL, response.getString(WebParams.IMG_MEDIUM_URL));
                                    mEditor.putString(DefineValue.IMG_LARGE_URL, response.getString(WebParams.IMG_LARGE_URL));
                                }

                                mEditor.apply();
                            }
                            if(finalListener != null)
                                finalListener.OnUpdateDataFBSuccess();

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    super.onFailure(statusCode, headers, responseString, throwable);
                    Timber.w("Error Koneksi update facebook:" + throwable.toString());
                    if(finalListener != null)
                        finalListener.OnUpdateDataFBFailed();
                }
            });
        }catch (Exception e){
            Log.d("httpclient", e.getMessage());
            if(listener != null)
                listener.OnUpdateDataFBFailed();
        }
    }

    private class StoryObject{
        String object_type;
        String object_desc;
        String type_action;
        String type_obj;
        String link;
        String msg;
    }

    private void createStory(StoryObject storyObject){
        String facebookApp = BuildConfig.FacebookApp;
        String url;
        Bitmap image = BitmapFactory.decodeResource(getmContext().getResources(), R.mipmap.ic_launcher);
        if(storyObject.link == null || storyObject.link.isEmpty())
            url = "http://unik.co.id/";
        else
            url = storyObject.link;

        SharePhoto img = new SharePhoto.Builder()
                .setBitmap(image)
                .build();
        ShareOpenGraphObject object = new ShareOpenGraphObject.Builder()
                .putString("og:type", facebookApp+":"+storyObject.type_obj)
                .putString("og:title", "UNIK -" + storyObject.object_desc)
                .putPhoto("og:image", img)
                .putString("og:description", storyObject.msg)
                .putString("og:url", url)
                .putBoolean("og:rich_attachment",true)
//                .putString("og:site_name", "http://unik.co.id/")
                .build();

        ShareOpenGraphAction action = new ShareOpenGraphAction.Builder()
                .setActionType(facebookApp+":"+storyObject.type_action)
                .putObject(storyObject.type_obj, object)
                .putBoolean("fb:explicitly_shared",true)
                .putString("message",storyObject.msg)
                .build();

        ShareOpenGraphContent content = new ShareOpenGraphContent.Builder()
                .setPreviewPropertyName(storyObject.type_obj)
                .setAction(action)
                .build();

        ShareApi.share(content, new FacebookCallback<Sharer.Result>() {
            @Override
            public void onSuccess(Sharer.Result result) {
                Timber.d("isi response posting feed : " + result.toString());
                Toast.makeText(CoreApp.getAppContext(), getmContext().getString(R.string.facebook_toast_success_post), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onCancel() {
                Log.d("isi response posting fb", "cancel");
                Toast.makeText(CoreApp.getAppContext(), getmContext().getString(R.string.facebook_toast_failed_post), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(FacebookException error) {
                Timber.d("isi response posting feed : " + error.toString());
                Toast.makeText(CoreApp.getAppContext(), getmContext().getString(R.string.facebook_toast_failed_post), Toast.LENGTH_SHORT).show();
            }
        });
        Timber.d("sudah di post String");
    }

    public void postPayFriends(String msg){
        SecurePreferences sp = CustomSecurePref.getInstance().getmSecurePrefs();
        StoryObject storyObject = new StoryObject();
        storyObject.object_type = "Friends";
        storyObject.object_desc = "Pay a Friends";
        storyObject.type_action = "Pay";
        storyObject.type_obj    = "friends";
        storyObject.msg         = msg;
        storyObject.link        = sp.getString(DefineValue.SHORT_URL_APP,"");
        createStory(storyObject);
    }

    public void postBuyItems(String msg){
        SecurePreferences sp = CustomSecurePref.getInstance().getmSecurePrefs();
        StoryObject storyObject = new StoryObject();
        storyObject.object_type = "Items";
        storyObject.object_desc = "Buy some Items";
        storyObject.type_action = "Buy";
        storyObject.type_obj    = "items";
        storyObject.msg         = msg;
        storyObject.link        = sp.getString(DefineValue.SHORT_URL_APP,"");
        createStory(storyObject);
    }


    public void postAskMoney(String msg) {
        SecurePreferences sp = CustomSecurePref.getInstance().getmSecurePrefs();
        StoryObject storyObject = new StoryObject();
        storyObject.object_type = "Money";
        storyObject.object_desc = "Ask Money";
        storyObject.type_action = "Ask";
        storyObject.type_obj    = "money";
        storyObject.msg         = msg;
        storyObject.link        = sp.getString(DefineValue.SHORT_URL_APP,"");
        createStory(storyObject);
    }

    public void postTopupBalance(String msg){
        SecurePreferences sp = CustomSecurePref.getInstance().getmSecurePrefs();
        StoryObject storyObject = new StoryObject();
        storyObject.object_type = "Balance";
        storyObject.object_desc = "Topup Balance";
        storyObject.type_action = "Topup";
        storyObject.type_obj    = "balance";
        storyObject.msg         = msg;
        storyObject.link        = sp.getString(DefineValue.SHORT_URL_APP,"");
        createStory(storyObject);
    }

    public AccessTokenTracker getmATT() {
        return mATT;
    }

    private void setmATT(AccessTokenTracker mATT) {
        this.mATT = mATT;
    }

    public ProfileTracker getmPT() {
        return mPT;
    }

    private void setmPT(ProfileTracker mPT) {
        this.mPT = mPT;
    }

    public CallbackManager getmCallBackManager() {
        return mCallBackManager;
    }

    public void setmCallBackManager(CallbackManager mCallBackManager) {
        this.mCallBackManager = mCallBackManager;
    }

    private void setFinalAT(AccessToken finalAT) {
        FinalAT = finalAT;
    }

    public AccessToken getFinalAT() {
        return FinalAT;
    }

    public Profile getFinalProf() {
        return FinalProf;
    }

    private void setFinalProf(Profile finalProf) {
        FinalProf = finalProf;
    }

    private Context getmContext() {
        return mContext;
    }

    private void setmContext(Context mContext) {
        this.mContext = mContext;
    }
}
