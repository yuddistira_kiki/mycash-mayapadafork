package com.sgo.mayapada.coreclass;

import io.realm.DynamicRealm;
import io.realm.RealmMigration;
import io.realm.RealmObjectSchema;
import io.realm.RealmSchema;
import timber.log.Timber;

/**
 * Created by yuddistirakiki on 4/5/16.
 */
class AppRealMigration implements RealmMigration {
    @Override
    public void migrate(DynamicRealm realm, long oldVersion, long newVersion) {

        RealmSchema schema = realm.getSchema();

        if (oldVersion == 0) {
            Timber.wtf("masuk migrate 0");
            RealmObjectSchema targetSavingModel = schema.get("Target_Saving_Model");
            if(targetSavingModel.hasPrimaryKey())
                targetSavingModel.removePrimaryKey();

            oldVersion++;
        }
    }
}
