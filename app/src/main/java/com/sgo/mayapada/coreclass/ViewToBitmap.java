package com.sgo.mayapada.coreclass;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.Looper;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Toast;

import com.sgo.mayapada.R;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import timber.log.Timber;

/**
 * Created by yuddistirakiki on 6/28/16.
 */
public class ViewToBitmap {

    private Context context;
    private File f;
    private ConvertHandler convertHandler;

    public interface ConvertHandler{
        void ConvertResponse(Boolean isSuccess);
    }

    public ViewToBitmap(Context _context){
        this.context = _context;
    }

    public ViewToBitmap(Context _context, ConvertHandler handler){
        this.context = _context;
        this.convertHandler = handler;
    }

    public void Convert(View view, String _filename) {
        Convert(view,_filename,false);
    }

    public Boolean convert(View view, String _filename) {
        return Convert(view, _filename, false) != null;
    }

    public void ConvertAsync(View view, String _filename) {
        MyTaskParams params = new MyTaskParams(view, _filename);
        LongOperation myTask = new LongOperation();
        myTask.execute(params);
    }


    private String Convert(View view, String _filename, Boolean isCache) {
        Bitmap bitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        view.draw(canvas);
        String path = "";

        try {

            if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
                File file = new File(Environment.getExternalStorageDirectory() + File.separator + "DCIM"
                        + File.separator, context.getString(R.string.foldername_struk));
                if (!file.exists()) {
                    file.mkdirs();

                }
                f = new File(Environment.getExternalStorageDirectory() + File.separator + "DCIM" + File.separator
                        + context.getString(R.string.foldername_struk) + File.separator + _filename + ".png");
                path = f.getAbsolutePath();
            }


            FileOutputStream output = new FileOutputStream(f);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, output);
            output.close();

            ContentValues values = new ContentValues();

            values.put(MediaStore.Images.Media.DATE_TAKEN, System.currentTimeMillis());
            values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpeg");
            values.put(MediaStore.MediaColumns.DATA,path);

            context.getApplicationContext().getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);

            if(isCache)
                Timber.d(context.getString(R.string.saved_gallery));
            else {
                if(Looper.myLooper() == Looper.getMainLooper())
                    Toast.makeText(context, context.getString(R.string.saved_gallery), Toast.LENGTH_LONG).show();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return path;
    }

    public void shareIntentApp(View view,String _filename){
        String path = Convert(view,_filename,true);
        try
        {
            Intent i = new Intent(Intent.ACTION_SEND);
            i.setType("image/jpeg");
            i.putExtra(Intent.EXTRA_SUBJECT,context.getString(R.string.appname));

            File bitmapFile = new File(path);
            Uri myUri = Uri.fromFile(bitmapFile);
            i.putExtra(Intent.EXTRA_STREAM, myUri);
            context.startActivity(Intent.createChooser(i, context.getString(R.string.share_title)));
        }
        catch(Exception e)
        {
            Timber.d(e.toString());
        }
    }

    private static class MyTaskParams {
        View view;
        String filename;

        MyTaskParams(View _view, String _fileName) {
            this.view = _view;
            this.filename = _fileName;
        }
    }

    private class LongOperation extends AsyncTask<MyTaskParams, Void, Boolean> {
        @Override
        protected Boolean doInBackground(MyTaskParams... params) {
            String path = Convert(params[0].view,params[0].filename,false);
            Boolean isPath = !path.isEmpty();
            if(convertHandler != null)
                convertHandler.ConvertResponse(isPath);
            return isPath;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            if(aBoolean)
                Toast.makeText(context, context.getString(R.string.saved_gallery), Toast.LENGTH_LONG).show();
        }
    }
}
