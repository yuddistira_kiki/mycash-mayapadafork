package com.sgo.mayapada.fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.sgo.mayapada.R;
import com.sgo.mayapada.activities.LoginActivity;
import com.sgo.mayapada.activities.Registration;

/**
 Created by Administrator on 7/15/2014.
 */
public class FirstScreen extends Fragment {

    private Button btnDaftar;
    private Button btnLogin;
    private Fragment mFragment;
    private View v;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.frag_firstscreen, container, false);
        return v;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        btnDaftar = (Button) v.findViewById(R.id.btn_daftar);
        btnLogin = (Button) v.findViewById(R.id.btn_login);

        TextView tv_termsnconditions = (TextView) v.findViewById(R.id.tv_termsncondition);
        tv_termsnconditions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TermsNConditionWeb mfrag = new TermsNConditionWeb();
                switchFragment(mfrag,getString(R.string.termsncondition_title),true);
            }
        });

        btnDaftar.setOnClickListener(bukaReg);
        btnLogin.setOnClickListener(bukaLogin);
    }

    private Button.OnClickListener bukaReg = new Button.OnClickListener(){
        @Override
        public void onClick(View view) {
            mFragment = new Regist1();
            switchFragment(mFragment,"reg1",true);
        }
    };

    private Button.OnClickListener bukaLogin = new Button.OnClickListener(){
        @Override
        public void onClick(View view) {
            Intent i = new Intent(getActivity(),LoginActivity.class);
            startActivity(i);
        }
    };

    private void switchFragment(Fragment i, String name, Boolean isBackstack){
        if (getActivity() == null)
            return;

        Registration fca = (Registration) getActivity();
        fca.switchContent(i,name,isBackstack);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        getActivity().getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_FULLSCREEN);
    }

    @Override
    public void onStart() {
        super.onStart();
        getActivity().getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_FULLSCREEN);
    }

    @Override
    public void onStop() {
        super.onStop();
    }
}