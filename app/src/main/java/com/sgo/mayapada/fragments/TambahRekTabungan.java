package com.sgo.mayapada.fragments;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.securepreferences.SecurePreferences;
import com.sgo.mayapada.activities.InsertPIN;
import com.sgo.mayapada.entityRealm.List_Account_Nabung;
import com.sgo.mayapada.entityRealm.List_Bank_Nabung;
import com.sgo.mayapada.R;
import com.sgo.mayapada.activities.MainPage;
import com.sgo.mayapada.activities.TabunganActivity;
import com.sgo.mayapada.coreclass.CustomSecurePref;
import com.sgo.mayapada.coreclass.DefineValue;
import com.sgo.mayapada.coreclass.ErrorDefinition;
import com.sgo.mayapada.coreclass.MyApiClient;
import com.sgo.mayapada.coreclass.WebParams;
import com.sgo.mayapada.dialogs.DefinedDialog;
import com.sgo.mayapada.interfaces.OnLoadDataListener;
import com.sgo.mayapada.loader.UtilsLoader;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import io.realm.Realm;
import io.realm.RealmResults;
import timber.log.Timber;

public class TambahRekTabungan extends Fragment implements View.OnClickListener {

    public final static String TAG = "com.sgo.mayapada.fragments.TambahRekTabungan";
    public static final int STATUS_SUCCESS = 0;
    public static final int STATUS_ERROR = 1;

//    private String[] namaBank = new String[]{
//            "BANK MANDIRI","BANK BCA","BANK BRI","BANK PERMATA","BANK DANAMON","BANK BII MAYBANK","BANK BTPN","BANK MAYAPADA",
//            "BANK ACEH","BPD BALI","BPD BENGKULU","BANK DKI","BPD JAMBI","BPD JAWA TENGAH","BPD JAWA BARAT DAN BANTEN","BPD JAWA TIMUR",
//            "BPD KALIMANTAN TIMUR","BPD KALIMANTAN TENGAH","BPD KALIMANTAN BARAT","BPD KALIMANTAN SELATAN","BPD LAMPUNG","BPD MALUKU",
//            "BPD NTB","BPD NTT","BPD PAPUA","BPD RIAU KEPRI","BPD SULAWESI TENGGARA","BPD SULAWESI SELATAN DAN BARAT","BPD SULAWESI TENGAH",
//            "BPD SULAWESI UTARA","BPD SUMATERA BARAT","BPD SUMSEL DAN BABEL","BPD SUMATERA UTARA","BPD YOGYAKARTA"};

    public interface OnSuccessAdd{
        void OnSuccessAddAccount(int status);
    }

    private ArrayList<String> listbank;

    private Spinner spin_namaBank;
    private EditText et_rekBank;
    private ImageView spinningLoad;
    private View layout_input, layout_desc;
    private Realm realm;
    private String userID;
    private String accessKey;
    private String memberID;
    private SecurePreferences sp;
    private ProgressDialog progressDialog;
    private RealmResults<List_Bank_Nabung> list_bank_nabungs;
    private String authType;
    private int attempt=-1;
    private List_Account_Nabung temp_list_account_nabung;
    private OnSuccessAdd mListener;

    public TambahRekTabungan() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        realm = Realm.getDefaultInstance();
        list_bank_nabungs = realm.where(List_Bank_Nabung.class).findAll();
        if(list_bank_nabungs.size() == 0) {
            Toast.makeText(getActivity(), R.string.addbankaccount_no_data_bank,Toast.LENGTH_SHORT).show();
            finishThis(STATUS_ERROR);
        }

        listbank = new ArrayList<>();
        for (int i = 0 ; i < list_bank_nabungs.size() ; i++){
            listbank.add(list_bank_nabungs.get(i).getBank_name());
        }


        sp = CustomSecurePref.getInstance().getmSecurePrefs();
        userID = sp.getString(DefineValue.USERID_PHONE,"");
        accessKey = sp.getString(DefineValue.ACCESS_KEY,"");
        memberID = sp.getString(DefineValue.MEMBER_ID,"");
        authType = sp.getString(DefineValue.AUTHENTICATION_TYPE,"");

        progressDialog = DefinedDialog.CreateProgressDialog(getContext(),"");
        progressDialog.dismiss();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_tambah_rek_tabungan, container, false);
        v.findViewById(R.id.btn_submit).setOnClickListener(this);
        v.findViewById(R.id.btn_cancel).setOnClickListener(this);
        spin_namaBank = (Spinner) v.findViewById(R.id.spinner_nameBank);
        et_rekBank = (EditText) v.findViewById(R.id.et_rek_value);
        layout_input = v.findViewById(R.id.add_input_layout);
        layout_desc = v.findViewById(R.id.add_desc_layout);
        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        setTitle();

        ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, listbank);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spin_namaBank.setAdapter(adapter);

        if(authType.equalsIgnoreCase(DefineValue.AUTH_TYPE_PIN)) {

            new UtilsLoader(getActivity(),sp).getFailedPIN(userID,new OnLoadDataListener() { // get pin attempt
                @Override
                public void onSuccess(Object deData) {
                    attempt = (int) deData;
                }

                @Override
                public void onFail(Bundle message) {

                }

                @Override
                public void onFailure(String message) {

                }
            });
        }
    }

    private void setTitle(){
        if(getActivity() == null)
            return;

        TabunganActivity ta = (TabunganActivity)getActivity();
        ta.setToolbarTitle(getString(R.string.menu_item_title_add_account));
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (getTargetFragment() instanceof OnSuccessAdd) {
            mListener = (OnSuccessAdd) getTargetFragment();
        } else {
            if(context instanceof OnSuccessAdd){
                mListener = (OnSuccessAdd) context;
            }
            else {
                throw new RuntimeException(context.toString()
                        + " must implement OnFragmentInteractionListener");
            }
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_submit :
                if(layout_input.getVisibility() == View.VISIBLE) {
                    if(inputValidation()){
//                        if(list_bank_nabungs.get(spin_namaBank.getSelectedItemPosition()).getBank_code().equals("008"))
//                            reqAddAccount(list_bank_nabungs.get(spin_namaBank.getSelectedItemPosition()).getBank_code(),
//                                    et_rekBank.getText().toString());
//                        else
                            testProcess();
                    }
                }
                else if(layout_desc.getVisibility() == View.VISIBLE){
                    if(authType.equalsIgnoreCase(DefineValue.AUTH_TYPE_PIN))
                        insertPIN();
                }

                break;
            case R.id.btn_cancel :
                getActivity().onBackPressed();
                break;
        }
    }

    private void ProceedToDesc(List_Account_Nabung list_account_nabung){
        layout_desc.setVisibility(View.VISIBLE);
        layout_input.setVisibility(View.GONE);
        ((TextView)layout_desc.findViewById(R.id.addacc_bank_value)).setText(list_account_nabung.getBank_name());
        ((TextView)layout_desc.findViewById(R.id.addacc_rekening_value)).setText(list_account_nabung.getAcct_no());
        ((TextView)layout_desc.findViewById(R.id.addacc_nama_value)).setText(list_account_nabung.getAcct_name());
        temp_list_account_nabung = list_account_nabung;
    }

    public Boolean OnBackPressed(){
        if(layout_desc.getVisibility() == View.VISIBLE) {
            layout_desc.setVisibility(View.GONE);
            layout_input.setVisibility(View.VISIBLE);
            return false;
        }
        return true;
    }

    private boolean inputValidation(){
        if(et_rekBank.getText().toString().length() == 0){
            et_rekBank.requestFocus();
            et_rekBank.setError(getString(R.string.addaccount_account_validation));
            return false;
        }
        return true;
    }

    private void insertPIN(){
        Intent i = new Intent(getActivity(), InsertPIN.class);
        if(attempt != -1 && attempt < 2)
            i.putExtra(DefineValue.ATTEMPT,attempt);
        startActivityForResult(i, MainPage.REQUEST_FINISH);
    }

    @Override
    public void onDestroy() {
        if(realm!=null)
            realm.close();
        super.onDestroy();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == MainPage.REQUEST_FINISH){
            if(resultCode == InsertPIN.RESULT_PIN_VALUE){
                String value_pin = data.getStringExtra(DefineValue.PIN_VALUE);
                if(temp_list_account_nabung != null)
                    confirmAddAccount(temp_list_account_nabung.getSaving_id(), value_pin);
            }
        }
    }

    private void testProcess(){
        progressDialog.show();

//            if(MyApiClient.IS_PROD){
        Handler hand = new Handler();
        hand.postDelayed(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(getActivity(), "Fitur ini Segera Hadir", Toast.LENGTH_SHORT).show();
                if(progressDialog.isShowing())
                    progressDialog.dismiss();
            }
        },1000);
    }

    private void reqAddAccount(String bank_code, String saving_id){
        try{
            progressDialog.show();
            RequestParams params = MyApiClient.getSignatureWithParams(MyApiClient.COMM_ID, MyApiClient.LINK_REQ_ADD_ACCOUNT,
                    userID, accessKey);
            params.put(WebParams.MEMBER_ID, memberID);
            params.put(WebParams.USER_ID, userID);
            params.put(WebParams.COMM_ID, MyApiClient.COMM_ID);
            params.put(WebParams.BANK_CODE, bank_code);
            params.put(WebParams.ACCT_NO, saving_id);

            Timber.d("isi params reqAddAccount:" + params.toString());

            MyApiClient.sentReqAddAccount(getActivity(), params, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    try {
                        String code = response.getString(WebParams.ERROR_CODE);
                        Timber.d("Isi response reqAddAccount: " + response.toString());
                        if (code.equals(WebParams.SUCCESS_CODE)) {
                            List_Account_Nabung list_account_nabung = new List_Account_Nabung();
                            list_account_nabung.setAcct_name(response.optString(WebParams.ACCT_NAME, ""));
                            list_account_nabung.setIs_default(response.optString(WebParams.IS_DEFAULT, ""));
                            list_account_nabung.setAcct_no(response.optString(WebParams.ACCT_NO, ""));
                            list_account_nabung.setCcy_id(response.optString(WebParams.CCY_ID, ""));
                            list_account_nabung.setBank_code(response.optString(WebParams.BANK_CODE, ""));
                            list_account_nabung.setBank_name(response.optString(WebParams.BANK_NAME, ""));
                            list_account_nabung.setSaving_id(response.optString(WebParams.SAVING_ID, ""));

                            ProceedToDesc(list_account_nabung);
                        } else {
                            code = response.getString(WebParams.ERROR_MESSAGE);
                            Toast.makeText(getActivity(), code, Toast.LENGTH_SHORT).show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } finally {
                        if (progressDialog.isShowing())
                            progressDialog.dismiss();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    super.onFailure(statusCode, headers, responseString, throwable);
                    failure(throwable);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    failure(throwable);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    failure(throwable);
                }

                private void failure(Throwable throwable) {
                    if (MyApiClient.PROD_FAILURE_FLAG)
                        Toast.makeText(getActivity(), getString(R.string.network_connection_failure_toast), Toast.LENGTH_SHORT).show();
                    else
                        Toast.makeText(getActivity(), throwable.toString(), Toast.LENGTH_SHORT).show();
                    if (progressDialog.isShowing())
                        progressDialog.dismiss();
                    Timber.w("Error Koneksi reqAddAccount :" + throwable.toString());
                }
            });
        }catch (Exception e){
            Log.d("httpclient:",e.getMessage());
        }
    }

    private void confirmAddAccount(String saving_id, String token_id){
        try{

            progressDialog.show();

            RequestParams params = MyApiClient.getSignatureWithParams(MyApiClient.COMM_ID,MyApiClient.LINK_CONFIRM_ADD_ACCOUNT,
                    userID,accessKey);
            params.put(WebParams.MEMBER_ID, memberID);
            params.put(WebParams.USER_ID, userID);
            params.put(WebParams.COMM_ID, MyApiClient.COMM_ID);
            params.put(WebParams.TOKEN_ID, token_id);
            params.put(WebParams.SAVING_ID, saving_id);

            Timber.d("isi params confirmAddAccount:" + params.toString());

            MyApiClient.sentConfirmAddAccount(getActivity(), params, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    try {
                        String code = response.getString(WebParams.ERROR_CODE);
                        Timber.d("Isi response confirmAddAccount: "+response.toString());
                        if (code.equals(WebParams.SUCCESS_CODE)) {
                            realm.beginTransaction();
                            realm.copyToRealm(temp_list_account_nabung);
                            realm.commitTransaction();
                            Toast.makeText(getActivity(),getString(R.string.addaccount_toast_success),Toast.LENGTH_LONG).show();
                            finishThis(STATUS_SUCCESS);
                        }
                        else if(code.equals(ErrorDefinition.WRONG_PIN_P2P)){
                            code = response.getString(WebParams.ERROR_MESSAGE);
                            Dialog dialognya = DefinedDialog.MessageDialog(getActivity(), getString(R.string.blocked_pin_title),
                                    code, new DefinedDialog.DialogButtonListener() {
                                        @Override
                                        public void onClickButton(View v, boolean isLongClick) {
                                            finishThis(STATUS_ERROR);
                                        }
                                    }) ;
                            dialognya.show();
                            sp.edit().putBoolean(DefineValue.PIN_BLOCKED,true).apply();
                        }
                        else {
                            code = response.getString(WebParams.ERROR_MESSAGE);
                            Toast.makeText(getActivity(),code,Toast.LENGTH_SHORT).show();
                            if(authType.equalsIgnoreCase(DefineValue.AUTH_TYPE_PIN)) {
                                attempt = attempt-1;
                                insertPIN();
                            }
                            else {
                                OnBackPressed();
                            }
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    finally {
                        if(progressDialog.isShowing())
                            progressDialog.dismiss();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    super.onFailure(statusCode, headers, responseString, throwable);
                    failure(throwable);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    failure(throwable);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    failure(throwable);
                }

                private void failure(Throwable throwable) {
                    if(MyApiClient.PROD_FAILURE_FLAG)
                        Toast.makeText(getActivity(), getString(R.string.network_connection_failure_toast), Toast.LENGTH_SHORT).show();
                    else
                        Toast.makeText(getActivity(), throwable.toString(), Toast.LENGTH_SHORT).show();
                    if(progressDialog.isShowing())
                        progressDialog.dismiss();
                    Timber.w("Error Koneksi reqAddAccount :"+throwable.toString());
                }
            });
        }catch (Exception e){
            Log.d("httpclient:",e.getMessage());
        }
    }

    private void finishThis(int status){
        mListener.OnSuccessAddAccount(status);
        getFragmentManager().popBackStack();
    }

}
