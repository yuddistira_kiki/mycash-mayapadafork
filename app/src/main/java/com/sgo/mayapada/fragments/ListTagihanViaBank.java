package com.sgo.mayapada.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.securepreferences.SecurePreferences;
import com.sgo.mayapada.Beans.ListBankModel;
import com.sgo.mayapada.R;
import com.sgo.mayapada.activities.TagihanActivity;
import com.sgo.mayapada.adapter.EasyAdapter;
import com.sgo.mayapada.coreclass.CustomSecurePref;
import com.sgo.mayapada.coreclass.DefineValue;
import com.sgo.mayapada.coreclass.WebParams;
import com.sgo.mayapada.dialogs.InformationDialog;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

import timber.log.Timber;

/**
 * Created by thinkpad on 5/12/2016.
 */
public class ListTagihanViaBank extends ListFragment implements InformationDialog.OnDialogOkCallback {

    private View v;
    private SecurePreferences sp;
    private ArrayList<String> _listType;
    private String listBankIB;
    private String listBankSMS;
    private String userID;
    private String accessKey;
    private String memberID;
    private String comm_id;
    private String comm_name;
    private String comm_code;
    private String api_key;
    private String callback_url;
    private String amount;
    private String bank_data;
    private String invoices;
    private EasyAdapter adapter;
    private ArrayList<ListBankModel> mlistbankIB = null;
    private ArrayList<ListBankModel> mlistbankSMS = null;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        v = inflater.inflate(R.layout.frag_list_tagihan_via_bank, container, false);
        return v;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        sp = CustomSecurePref.getInstance().getmSecurePrefs();
        userID = sp.getString(DefineValue.USERID_PHONE,"");
        accessKey = sp.getString(DefineValue.ACCESS_KEY,"");
        memberID = sp.getString(DefineValue.MEMBER_ID, "");

        listBankIB = null;
        listBankSMS = null;

        Bundle mArgs = getArguments();
        if(mArgs != null && !mArgs.isEmpty()) {
            comm_id = mArgs.getString(DefineValue.COMMUNITY_ID, "");
            comm_code = mArgs.getString(DefineValue.COMMUNITY_CODE, "");
            comm_name = mArgs.getString(DefineValue.COMMUNITY_NAME, "");
            api_key = mArgs.getString(DefineValue.API_KEY, "");
            callback_url = mArgs.getString(DefineValue.CALLBACK_URL, "");
            amount = mArgs.getString(DefineValue.AMOUNT_TAGIHAN, "");
            bank_data = mArgs.getString(DefineValue.BANKLIST_DATA, "");
            invoices = mArgs.getString(DefineValue.INVOICES, "");
        }

        _listType = new ArrayList<>();

        adapter = new EasyAdapter(getActivity(),R.layout.list_view_item_with_arrow, _listType);

        ListView listView1 = (ListView) v.findViewById(android.R.id.list);
        listView1.setAdapter(adapter);

        try {
            insertBankList(new JSONArray(bank_data));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    private void insertBankList(JSONArray arrayJson){
        ListBankModel mLB;
        try {

            if(mlistbankIB != null)
                mlistbankIB.clear();

            if(mlistbankSMS != null)
                mlistbankSMS.clear();


            for (int i = 0; i < arrayJson.length(); i++) {
                mLB = new ListBankModel();
                mLB.setBank_name(arrayJson.getJSONObject(i).getString(WebParams.BANK_NAME));
                mLB.setBank_code(arrayJson.getJSONObject(i).getString(WebParams.BANK_CODE));
                mLB.setProduct_code(arrayJson.getJSONObject(i).getString(WebParams.PRODUCT_CODE));
                mLB.setProduct_name(arrayJson.getJSONObject(i).getString(WebParams.PRODUCT_NAME));
                mLB.setProduct_type(arrayJson.getJSONObject(i).getString(WebParams.PRODUCT_TYPE));
                mLB.setProduct_h2h(arrayJson.getJSONObject(i).optString(WebParams.PRODUCT_H2H, ""));

                if(mLB.getProduct_type().equals(DefineValue.BANKLIST_TYPE_IB)){
                    if(mlistbankIB == null)
                        mlistbankIB = new ArrayList<>();

                    mlistbankIB.add(mLB);

                }

                if(mLB.getProduct_type().equals(DefineValue.BANKLIST_TYPE_SMS)){
                    if(mlistbankSMS == null)
                        mlistbankSMS = new ArrayList<>();

                    mlistbankSMS.add(mLB);
                }
            }

            final GsonBuilder gsonBuilder = new GsonBuilder();
            gsonBuilder.setPrettyPrinting();
            final Gson gson = gsonBuilder.create();

            if(mlistbankIB != null) {
                if(!mlistbankIB.isEmpty()){
                    _listType.add(getString(R.string.internetBanking_ab_title));
                    listBankIB = gson.toJson(mlistbankIB);
                }
            }

//            if (mlistbankSMS != null) {
//                if(!mlistbankSMS.isEmpty()) {
//                    _listType.add(getString(R.string.smsBanking_ab_title));
//                    listBankSMS = gson.toJson(mlistbankSMS);
//                }
//            }
            Timber.wtf("isi data :"+listBankIB);
            adapter.notifyDataSetChanged();

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        String itemName = String.valueOf(l.getAdapter().getItem(position));

        Fragment mFrag = new SgoPlus_input();
        Bundle mBun = new Bundle();
        mBun.putBoolean(DefineValue.TAGIHAN, true);
        mBun.putString(DefineValue.COMMUNITY_ID, comm_id);
        mBun.putString(DefineValue.COMMUNITY_CODE, comm_code);
        mBun.putString(DefineValue.COMMUNITY_NAME, comm_name);
        mBun.putString(DefineValue.API_KEY, api_key);
        mBun.putString(DefineValue.CALLBACK_URL, callback_url);
        mBun.putString(DefineValue.AMOUNT_TAGIHAN, amount);
        mBun.putString(DefineValue.INVOICES, invoices);

        if (itemName.equals(getString(R.string.internetBanking_ab_title))) {
            mBun.putString(DefineValue.TRANSACTION_TYPE, DefineValue.INTERNET_BANKING);
            mBun.putString(DefineValue.BANKLIST_DATA, listBankIB);
        } else if (itemName.equals(getString(R.string.smsBanking_ab_title))) {
            mBun.putString(DefineValue.TRANSACTION_TYPE, DefineValue.SMS_BANKING);
            mBun.putString(DefineValue.BANKLIST_DATA, listBankSMS);
        }

        mFrag.setArguments(mBun);
        switchFragmentTagihanActivity(mFrag, itemName, true);

    }

    private void switchFragmentTagihanActivity(android.support.v4.app.Fragment i, String name, Boolean isBackstack){
        if (getActivity() == null)
            return;

        TagihanActivity fca = (TagihanActivity) getActivity();
        fca.switchContent(i,name,isBackstack);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if(getActivity().getSupportFragmentManager().getBackStackEntryCount() > 0)
                    getActivity().getSupportFragmentManager().popBackStack();
                else
                    getActivity().finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume() {
        super.onResume();
        setTitle(getString(R.string.choose_payment));
    }

    @Override
    public void onOkButton() {

    }

    private void setTitle(String _title){
        if (getActivity() == null)
            return;

        TagihanActivity fca = (TagihanActivity) getActivity();
        fca.setTitleFragment(_title);
    }
}
