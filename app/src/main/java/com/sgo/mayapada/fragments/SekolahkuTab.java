package com.sgo.mayapada.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.securepreferences.SecurePreferences;
import com.sgo.mayapada.R;
import com.sgo.mayapada.activities.MainPage;
import com.sgo.mayapada.adapter.SekolahkuTabAdapter;
import com.sgo.mayapada.dialogs.InformationDialog;
import com.viewpagerindicator.TabPageIndicator;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by thinkpad on 3/31/2016.
 */
public class SekolahkuTab extends Fragment implements InformationDialog.OnDialogOkCallback {

    private SekolahkuTabAdapter currentAdapternya;
    SecurePreferences sp;
    private View currentView;
    private List<Fragment> mList;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        View v = inflater.inflate(R.layout.frag_sekolahku_tab, container, false);
        setCurrentView(v);
        return v;
    }

//    @Override
//    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
//        inflater.inflate(R.menu.information, menu);
//        super.onCreateOptionsMenu(menu, inflater);
//    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (savedInstanceState == null) {
            final int pageMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 4, getResources()
                    .getDisplayMetrics());
            SekolahkuTabAdapter adapternya;
            TabPageIndicator tabs;
            ViewPager pager;
            String[] titles = getActivity().getResources().getStringArray(R.array.sekolahku_list);

            mList = new ArrayList<>();
            mList.add(ListCommunityTagihan.newInstance());
            mList.add(FragKantin.newInstance());

            tabs = (TabPageIndicator) getCurrentView().findViewById(R.id.sekolahku_tabs);
            pager = (ViewPager) getCurrentView().findViewById(R.id.sekolahku_pager);
            adapternya = new SekolahkuTabAdapter(getChildFragmentManager(), getActivity(), mList, titles);
            setTargetFragment(this, 0);
            pager.setAdapter(adapternya);
            pager.setPageMargin(pageMargin);
            tabs.setViewPager(pager);
            pager.setCurrentItem(0);
            ToggleFAB(false);

            pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                }

                @Override
                public void onPageSelected(int position) {
//                    ToggleFAB(!(mList.get(position) instanceof FragKantin));
                }

                @Override
                public void onPageScrollStateChanged(int state) {

                }
            });

            setCurrentAdapternya(adapternya);
        }
    }

    private void ToggleFAB(Boolean isShow){
        if (getActivity() == null)
            return;

        if(getActivity() instanceof MainPage) {
            MainPage fca = (MainPage) getActivity();
            if(isShow)
                fca.materialSheetFab.showFab();
            else
                fca.materialSheetFab.hideSheetThenFab();
        }
    }

    private View getCurrentView() {
        return currentView;
    }

    private void setCurrentView(View currentView) {
        this.currentView = currentView;
    }

    public SekolahkuTabAdapter getCurrentAdapternya() {
        return currentAdapternya;
    }

    private void setCurrentAdapternya(SekolahkuTabAdapter currentAdapternya) {
        this.currentAdapternya = currentAdapternya;
    }

    public Fragment getFragment(int position){
        return currentAdapternya.getItem(position);
    }

//    public void getList(){
//        Fragment frag = getCurrentAdapternya().mCurrentFragment;
//       ((ListCommunityTagihan)frag).startPairingIDActivity();
//    }
//    @Override
//    public boolean onOptionsItemSelected(android.view.MenuItem item) {
//        switch (item.getItemId()) {
//            case R.id.action_information:
//                dialogI.show(getActivity().getSupportFragmentManager(), InformationDialog.TAG);
//                return true;
//            default:
//                return super.onOptionsItemSelected(item);
//        }
//    }

    @Override
    public void onOkButton() {

    }

}

