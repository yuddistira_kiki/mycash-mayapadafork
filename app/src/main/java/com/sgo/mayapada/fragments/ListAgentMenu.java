package com.sgo.mayapada.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.securepreferences.SecurePreferences;
import com.sgo.mayapada.R;
import com.sgo.mayapada.activities.AgentActivity;
import com.sgo.mayapada.activities.MainPage;
import com.sgo.mayapada.adapter.EasyAdapter;
import com.sgo.mayapada.coreclass.CustomSecurePref;
import com.sgo.mayapada.coreclass.DefineValue;
import com.sgo.mayapada.dialogs.InformationDialog;

/**
 * Created by thinkpad on 7/13/2016.
 */
public class ListAgentMenu extends ListFragment implements InformationDialog.OnDialogOkCallback {
    private View v;

    private SecurePreferences sp;
//    private InformationDialog dialogI;
//    String authType;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        v = inflater.inflate(R.layout.frag_list_agent_menu, container, false);
        return v;
    }

//    @Override
//    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
//        inflater.inflate(R.menu.information, menu);
//        super.onCreateOptionsMenu(menu, inflater);
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(android.view.MenuItem item) {
//        switch(item.getItemId())
//        {
//            case R.id.action_information:
//                if(!dialogI.isAdded())
//                    dialogI.show(getActivity().getSupportFragmentManager(), InformationDialog.TAG);
//                return true;
//            default:
//                return super.onOptionsItemSelected(item);
//        }
//    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        sp = CustomSecurePref.getInstance().getmSecurePrefs();
//        authType = sp.getString(DefineValue.AUTHENTICATION_TYPE,"");


//        dialogI = InformationDialog.newInstance(this,11);
        String[] _data;
        _data = getResources().getStringArray(R.array.agent_menu_list);

        EasyAdapter adapter = new EasyAdapter(getActivity(),R.layout.list_view_item_with_arrow, _data);

        ListView listView1 = (ListView) v.findViewById(android.R.id.list);
        listView1.setAdapter(adapter);

    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        Intent i;
//        Fragment f;
        switch (position) {
            case 0:
                i = new Intent(getActivity(), AgentActivity.class);
                i.putExtra(DefineValue.INDEX, position);
                switchActivity(i);
                break;
            case 1:
                i = new Intent(getActivity(), AgentActivity.class);
                i.putExtra(DefineValue.INDEX, position);
                switchActivity(i);
                break;
            case 2:
                i = new Intent(getActivity(), AgentActivity.class);
                i.putExtra(DefineValue.INDEX, position);
                switchActivity(i);
                break;
        }

    }

    private void switchFragment(android.support.v4.app.Fragment i, String name, Boolean isBackstack){
        if (getActivity() == null)
            return;

        MainPage fca = (MainPage) getActivity();
        fca.switchContent(i,name,isBackstack);
    }

    private void switchActivity(Intent mIntent){
        if (getActivity() == null)
            return;

        MainPage fca = (MainPage) getActivity();
        fca.switchActivity(mIntent,MainPage.ACTIVITY_RESULT);
    }

    @Override
    public void onOkButton() {

    }
}
