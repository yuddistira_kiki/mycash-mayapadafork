package com.sgo.mayapada.fragments;

import android.Manifest;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.sgo.mayapada.R;
import com.sgo.mayapada.activities.MainPage;
import com.sgo.mayapada.adapter.PayByQRTypeAdapter;

import java.util.ArrayList;
import java.util.List;

import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;

/**
 * Created by Lenovo Thinkpad on 9/20/2016.
 */
public class PayByQR extends Fragment implements EasyPermissions.PermissionCallbacks {
    private static final int REQUEST_CODE_QRCODE_PERMISSIONS = 1;
    View v;
    public static ArrayList<String> paybyqr_type = new ArrayList<>();
    private ListView mListView;
    private PayByQRTypeAdapter mAdapter;
    public static String type = "5";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.frag_pay_by_qr, container, false);
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mListView = (ListView) getActivity().findViewById(R.id.list_paybyqr_type);

        paybyqr_type.clear();
        mAdapter = new PayByQRTypeAdapter(getActivity());
        paybyqr_type.add("Corporate Merchant");
        paybyqr_type.add("Personal Merchant");
        mListView.setAdapter(mAdapter);

        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Fragment newFragment;
                newFragment = new ScanQRFragment();
                type = String.valueOf(position);
                switchFragment(newFragment, getString(R.string.toolbar_title_paybyqr));
            }
        });
    }

    private void switchFragment(Fragment i, String name){
        if (getActivity() == null)
            return;

        MainPage fca = (MainPage) getActivity();
        fca.switchContent(i,name);
    }

    @Override
    public void onStart() {
        super.onStart();
        requestCodeQrcodePermissions();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {
    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {
    }

    @AfterPermissionGranted(REQUEST_CODE_QRCODE_PERMISSIONS)
    private void requestCodeQrcodePermissions() {
        String[] perms = {Manifest.permission.CAMERA};
        if (!EasyPermissions.hasPermissions(getActivity(), perms)) {
            EasyPermissions.requestPermissions(this, "Need Permission To Open Camera", REQUEST_CODE_QRCODE_PERMISSIONS, perms);
        }
    }
}
