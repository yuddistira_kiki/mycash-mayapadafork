package com.sgo.mayapada.fragments;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.securepreferences.SecurePreferences;
import com.sgo.mayapada.R;
import com.sgo.mayapada.activities.InsertPIN;
import com.sgo.mayapada.activities.MainPage;
import com.sgo.mayapada.coreclass.CustomSecurePref;
import com.sgo.mayapada.coreclass.DefineValue;
import com.sgo.mayapada.coreclass.MyApiClient;
import com.sgo.mayapada.coreclass.WebParams;
import com.sgo.mayapada.dialogs.DefinedDialog;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import timber.log.Timber;

/**
 * Created by User on 6/21/2017.
 */

public class FragRegPairingidDesciption extends Fragment {

    private View v;
    private TextView comm_name_field;
    private TextView cust_name_field;
    private TextView pairing_id_field;
    private String pairing_id;
    private String response_data;
    private String comm_name;
    private String comm_id;
    private String cust_name;
    private String user_id;
    private String access_key;
    private int attempt=-1;
    private JSONObject response_jsonobj;
    private Button cancel_btn;
    private Button procedd_btn;

    private SecurePreferences sp;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v=inflater.inflate(R.layout.frag_regpairid_descrip_layout, container, false);

        comm_name_field = (TextView) v.findViewById(R.id.pairing_comm_name_info);
        cust_name_field = (TextView) v.findViewById(R.id.pairing_cust_name_info);
        pairing_id_field = (TextView) v.findViewById(R.id.pair_id_info);
        cancel_btn = (Button) v.findViewById(R.id.btn_cancel_pairing_descrip);
        procedd_btn = (Button) v.findViewById(R.id.btn_submit_pairing_descrip);

        return v;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();

        pairing_id = bundle.getString("pairing_id");
        response_data = bundle.getString("data");
        try {
//            response_jsonarray = new JSONArray(response_data);
            response_jsonobj = new JSONObject(response_data);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            comm_name = response_jsonobj.getString("comm_name");
            cust_name = response_jsonobj.getString("cust_name");
            comm_id = response_jsonobj.getString("comm_id");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        sp = CustomSecurePref.getInstance().getmSecurePrefs();
        user_id = sp.getString(DefineValue.USERID_PHONE, "");
        access_key = sp.getString(DefineValue.ACCESS_KEY, "");
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        pairing_id_field.setText(pairing_id);
        comm_name_field.setText(comm_name);
        cust_name_field.setText(cust_name);

        cancel_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog_Cancel();
            }
        });

        procedd_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), InsertPIN.class);
                if(attempt != -1 && attempt < 2)
                    i.putExtra(DefineValue.ATTEMPT,attempt);
                startActivityForResult(i, MainPage.REQUEST_FINISH);
            }
        });
    }

    private void pairingConfirm(String pin_code) {
        final ProgressDialog progdialog = DefinedDialog.CreateProgressDialog(getActivity(), "");
        try {
            final RequestParams params = MyApiClient.getSignatureWithParams(MyApiClient.COMM_ID, MyApiClient.LINK_UOB_PAIRING_CONFIRM,
                    user_id, access_key);
            params.put(WebParams.COMM_ID, comm_id);
            params.put(WebParams.COMM_ID_EMO,MyApiClient.COMM_ID );
            params.put(WebParams.USER_ID, user_id);
            params.put(WebParams.MEMBER_ID, sp.getString(DefineValue.MEMBER_ID, ""));
            params.put(WebParams.PAIRING_ID, pairing_id);
            params.put(WebParams.TOKEN_ID, pin_code);

            Timber.d("isi params pairing request:" + params.toString());

            MyApiClient.sentPairingConfirm(getActivity(), params, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                    try {
                        Timber.d("isi response pairing request: " + response.toString());
                        String code = response.getString(WebParams.ERROR_CODE);
                        if (code.equals(WebParams.SUCCESS_CODE)) {
                            Toast.makeText(getActivity(), "Pairing ID Sukses", Toast.LENGTH_SHORT).show();
                            getActivity().setResult(101);
                            getActivity().finish();
                        }
//                        else if(code.equals(ErrorDefinition.WRONG_PIN_P2P)){
//                            code = response.getString(WebParams.ERROR_MESSAGE);
//                            showDialogError(code);
//                        }
                        else {
                            Timber.d("isi error confirm token p2p:" + response.toString());
                            code = response.getString(WebParams.ERROR_MESSAGE);
                            Toast.makeText(getActivity(), code, Toast.LENGTH_LONG).show();
//                            if(authType.equalsIgnoreCase("PIN")) {
                                Intent i = new Intent(getActivity(), InsertPIN.class);
                                attempt = attempt-1;
                                if(attempt != -1 && attempt < 2)
                                    i.putExtra(DefineValue.ATTEMPT, attempt);
                                startActivityForResult(i,MainPage.REQUEST_FINISH);
//                            }
                            Toast.makeText(getActivity(), response.getString(WebParams.ERROR_MESSAGE), Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (progdialog.isShowing())
                        progdialog.dismiss();
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    super.onFailure(statusCode, headers, responseString, throwable);
                    failure(throwable);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    failure(throwable);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    failure(throwable);
                }

                private void failure(Throwable throwable) {
                    if (MyApiClient.PROD_FAILURE_FLAG)
                        Toast.makeText(getActivity(), getString(R.string.network_connection_failure_toast), Toast.LENGTH_SHORT).show();
                    else
                        Toast.makeText(getActivity(), throwable.toString(), Toast.LENGTH_SHORT).show();

                    Timber.w("Error Koneksi get comm inv:" + throwable.toString());
                    if (progdialog.isShowing())
                        progdialog.dismiss();
                }
            });
        }catch (Exception e){
            Timber.d("httpclient:" + e.getMessage());
            if (progdialog.isShowing())
                progdialog.dismiss();
        }
    }

    private void pairingCancel() {
        final ProgressDialog progdialog = DefinedDialog.CreateProgressDialog(getActivity(), "");
        try {
            final RequestParams params = MyApiClient.getSignatureWithParams(MyApiClient.COMM_ID, MyApiClient.LINK_UOB_PAIRING_CANCEL,
                    user_id, access_key);
            params.put(WebParams.COMM_ID, comm_id);
            params.put(WebParams.COMM_ID_EMO,MyApiClient.COMM_ID );
            params.put(WebParams.USER_ID, user_id);
            params.put(WebParams.MEMBER_ID, sp.getString(DefineValue.MEMBER_ID, ""));
            params.put(WebParams.PAIRING_ID, pairing_id);

            Timber.d("isi params pairing request:" + params.toString());

            MyApiClient.sentPairingCancel(getActivity(), params, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                    try {
                        Timber.d("isi response pairing request: " + response.toString());
                        String code = response.getString(WebParams.ERROR_CODE);
                        if (code.equals(WebParams.SUCCESS_CODE)) {
                            getFragmentManager().popBackStack();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (progdialog.isShowing())
                        progdialog.dismiss();
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    super.onFailure(statusCode, headers, responseString, throwable);
                    failure(throwable);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    failure(throwable);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    failure(throwable);
                }

                private void failure(Throwable throwable) {
                    if (MyApiClient.PROD_FAILURE_FLAG)
                        Toast.makeText(getActivity(), getString(R.string.network_connection_failure_toast), Toast.LENGTH_SHORT).show();
                    else
                        Toast.makeText(getActivity(), throwable.toString(), Toast.LENGTH_SHORT).show();

                    Timber.w("Error Koneksi get comm inv:" + throwable.toString());
                    if (progdialog.isShowing())
                        progdialog.dismiss();
                }
            });
        }catch (Exception e){
            Timber.d("httpclient:" + e.getMessage());
            if (progdialog.isShowing())
                progdialog.dismiss();
        }
    }

    public void dialog_Cancel(){
        AlertDialog.Builder alertbox = new AlertDialog.Builder(getActivity());
        alertbox.setMessage("Apakah anda yakin ingin membatalkan?");
        alertbox.setPositiveButton("Ya", new
                DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                        pairingCancel();
                    }
                });
        alertbox.setNegativeButton("Tidak", new
                DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {

                    }
                });
        alertbox.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == MainPage.REQUEST_FINISH){
            if(resultCode == InsertPIN.RESULT_PIN_VALUE) {
                String value_pin = data.getStringExtra(DefineValue.PIN_VALUE);
                Timber.d("onActivity result" + "Biller Fragment result pin value");
                pairingConfirm(value_pin);
            }
        }
    }
}