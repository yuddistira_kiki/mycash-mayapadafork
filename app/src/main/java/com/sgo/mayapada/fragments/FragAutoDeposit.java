package com.sgo.mayapada.fragments;/*
  Created by Administrator on 8/10/2016.
 */

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.securepreferences.SecurePreferences;
import com.sgo.mayapada.R;
import com.sgo.mayapada.activities.TabunganActivity;
import com.sgo.mayapada.coreclass.CustomSecurePref;
import com.sgo.mayapada.coreclass.DefineValue;
import com.sgo.mayapada.coreclass.MyApiClient;
import com.sgo.mayapada.coreclass.WebParams;
import com.sgo.mayapada.dialogs.AlertDialogLogout;
import com.sgo.mayapada.dialogs.DefinedDialog;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import timber.log.Timber;

public class FragAutoDeposit extends Fragment {
    public final static String TAG = "com.sgo.mayapada.fragments.FragAutoDeposit";

    private static final int REQUEST_CODE = 0;
    private static final int RESULT_CANCEL = 1;
    public static final int RESULT_STOP = 2;
    private static final int RESULT_ONGOING = 3;

    private View v;
    private SecurePreferences sp;

    private Spinner day_spinner;
    private RelativeLayout minimum_layout;
    private RelativeLayout percentage_layout;
    private RelativeLayout nominal_layout;
    private RadioGroup withdraw_button;
    ProgressDialog progdialog;
    private TextView note;
    private TextView start_date;
    private TextView end_date;
    private Button save;
    private Button minimum_rbutton;
    private Button percentage_rbutton;
    private Button nominal_rbutton;
    private Button cancel;
    private ImageButton start_calendar;
    private ImageButton end_calendar;
    private int selected_date=1;
    private int year;
    private int monthOfYear;
    private int dayOfMonth;
    private String withdrawal_type;
    private String amount = "";
    private String startdate_query = "";
    private String enddate_query = "";
    private DatePickerDialog date_dialog_start;
    private DatePickerDialog date_dialog_end;
    private DateFormat dateformat;
    private DateFormat dateformat_query;
    private EditText m_input;
    private EditText p_input;
    private EditText n_input;
    private String userID;
    private String accessKey;
    private String member_id;



    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        v = inflater.inflate(R.layout.frag_autodeposit, container, false);
        return v;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        sp = CustomSecurePref.getInstance().getmSecurePrefs();
        userID = sp.getString(DefineValue.USERID_PHONE,"");
        accessKey = sp.getString(DefineValue.ACCESS_KEY,"");
        member_id = sp.getString(DefineValue.MEMBER_ID, "");

        day_spinner = (Spinner) v.findViewById(R.id.day_picker);
        withdraw_button = (RadioGroup) v.findViewById(R.id.withdraw_radiobutton);
        minimum_rbutton = (Button) v.findViewById(R.id.minimum_rbutton);
        percentage_rbutton = (Button) v.findViewById(R.id.percentage_rbutton);
        nominal_rbutton = (Button) v.findViewById(R.id.nominal_rbutton);
        n_input = (EditText) v.findViewById(R.id.n_input_balance);
        m_input = (EditText) v.findViewById(R.id.m_input_balance);
        p_input = (EditText) v.findViewById(R.id.p_input_balance);
        minimum_layout = (RelativeLayout) v.findViewById(R.id.minimum_layout);
        percentage_layout = (RelativeLayout) v.findViewById(R.id.percentage_layout);
        nominal_layout = (RelativeLayout) v.findViewById(R.id.nominal_layout);
        save = (Button) v.findViewById(R.id.save);
        cancel = (Button) v.findViewById(R.id.cancel);
        note = (TextView) v.findViewById(R.id.notes);
        start_calendar = (ImageButton) v.findViewById(R.id.start_picker);
        end_calendar = (ImageButton) v.findViewById(R.id.end_picker);
        start_date = (TextView) v.findViewById(R.id.start_date);
        end_date = (TextView) v.findViewById(R.id.end_date);

        List<String> categories = new ArrayList<>();
        for(int x=1 ; x<=31; x++)
            categories.add(String.valueOf(x));

        ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, categories);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        day_spinner.setAdapter(adapter);
        day_spinner.setOnItemSelectedListener(spinner_listener);

        withdraw_button.setOnCheckedChangeListener(radiogroup_listener);
        withdraw_button.check(minimum_rbutton.getId());
        withdrawal_type = minimum_rbutton.getText().toString();

        save.setOnClickListener(save_listener);
        cancel.setOnClickListener(cancel_listener);

        Calendar c = Calendar.getInstance();
        dateformat = new SimpleDateFormat("dd/MM/yyyy");
        dateformat_query = new SimpleDateFormat("yyyy-MM-dd");
        date_dialog_start = DatePickerDialog.newInstance(
                dobPickerStart,
                c.get(java.util.Calendar.YEAR),
                c.get(java.util.Calendar.MONTH),
                c.get(java.util.Calendar.DAY_OF_MONTH)
        );

        date_dialog_start.setMinDate(c);
//        date_dialog_end.setMinDate(c);

        start_calendar.setOnClickListener(start);
        end_calendar.setOnClickListener(end);

    }

    private void setdialogEndCalendar(){
        Calendar cal = new GregorianCalendar(year, monthOfYear, dayOfMonth);
        cal.add(Calendar.DAY_OF_WEEK, selected_date);

        date_dialog_end = DatePickerDialog.newInstance(
                dobPickerEnd,
                cal.get(java.util.Calendar.YEAR),
                cal.get(java.util.Calendar.MONTH),
                cal.get(java.util.Calendar.DAY_OF_MONTH)
        );
        date_dialog_end.setMinDate(cal);
    }

    private void saveTime(int year, int monthOfYear, int dayOfMonth){
        this.year = year;
        this.monthOfYear = monthOfYear;
        this.dayOfMonth = dayOfMonth;
    }

    private ImageButton.OnClickListener start = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            date_dialog_start.show(getActivity().getFragmentManager(), "Datepickerdialog");
        }
    };

    private ImageButton.OnClickListener end = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (startdate_query.equals("")){
                Toast.makeText(getActivity(), getString(R.string.alert_start_calendar_blank), Toast.LENGTH_SHORT).show();
            }else {
                try {
                    date_dialog_end.show(getActivity().getFragmentManager(), "Datepickerdialog");
                }catch (Exception e){

                }
            }
        }
    };

    private DatePickerDialog.OnDateSetListener dobPickerStart = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
            Calendar calendar_dialog = new GregorianCalendar(year, monthOfYear, dayOfMonth);
            saveTime(year, monthOfYear, dayOfMonth);
            Date dedate = calendar_dialog.getTime();
            start_date.setText(dateformat.format(dedate));
            startdate_query = dateformat_query.format(dedate);
            setdialogEndCalendar();
        }
    };


    private DatePickerDialog.OnDateSetListener dobPickerEnd = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
            Calendar calendar = new GregorianCalendar(year, monthOfYear, dayOfMonth);

            Date dedate = calendar.getTime();
            end_date.setText(dateformat.format(dedate));
            enddate_query = dateformat_query.format(dedate);


        }
    };

    private Button.OnClickListener save_listener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            String balance_type;

            if (minimum_layout.getVisibility() == View.VISIBLE) {
                amount = m_input.getText().toString();
                withdrawal_type = DefineValue.BALANCE_REMAIN;
                balance_type = WebParams.BALANCE;
            } else if (percentage_layout.getVisibility() == View.VISIBLE) {
                amount = p_input.getText().toString();
                withdrawal_type = DefineValue.PERCENTAGE;
                balance_type = WebParams.PERCENTAGE;
            } else{
                amount = n_input.getText().toString();
                withdrawal_type = DefineValue.FIXED_AMOUNT;
                balance_type = WebParams.AMOUNT;
            }

            if(amount.equals("")){
                Toast.makeText(getActivity(), getString(R.string.alert_amount), Toast.LENGTH_SHORT).show();
            }else if (startdate_query.equals("")) {
                Toast.makeText(getActivity(), getString(R.string.alert_start_calendar), Toast.LENGTH_SHORT).show();
            }else if(enddate_query.equals("")) {
                Toast.makeText(getActivity(), getString(R.string.alert_end_calendar), Toast.LENGTH_SHORT).show();
            }else{
                addAutoSaving(balance_type);
            }

        }
    };

    private Button.OnClickListener cancel_listener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            getActivity().finish();

        }
    };

    private Spinner.OnItemSelectedListener spinner_listener = new OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            selected_date = position+1;
            try {
                setdialogEndCalendar();
            }catch (Exception e){

            }

        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };

    private RadioGroup.OnCheckedChangeListener radiogroup_listener = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {

            switch (checkedId) {
                case R.id.minimum_rbutton:
                    minimum_layout.setVisibility(View.VISIBLE);
                    percentage_layout.setVisibility(View.GONE);
                    nominal_layout.setVisibility(View.GONE);
                    note.setText(R.string.autodeposit_note_minimum_withdrawal);
                    withdrawal_type = minimum_rbutton.getText().toString();
                    break;
                case R.id.percentage_rbutton:
                    minimum_layout.setVisibility(View.GONE);
                    percentage_layout.setVisibility(View.VISIBLE);
                    nominal_layout.setVisibility(View.GONE);
                    note.setText(R.string.autodeposit_note_percentage_withdrawal);
                    withdrawal_type = percentage_rbutton.getText().toString();
                    break;
                case R.id.nominal_rbutton:
                    minimum_layout.setVisibility(View.GONE);
                    percentage_layout.setVisibility(View.GONE);
                    nominal_layout.setVisibility(View.VISIBLE);
                    note.setText(R.string.autodeposit_note_nominal_withdrawal);
                    withdrawal_type = nominal_rbutton.getText().toString();
                    break;
            }

        }
    };

    private void addAutoSaving(String balance_type) {

        final ProgressDialog prodDialog = DefinedDialog.CreateProgressDialog(getActivity(), "");
        RequestParams params = MyApiClient.getSignatureWithParams(MyApiClient.COMM_ID, MyApiClient.LINK_ADD_AUTOSAVING,
                userID, accessKey);
        params.put(WebParams.COMM_ID, MyApiClient.COMM_ID);
        params.put(WebParams.MEMBER_ID, member_id);
        params.put(WebParams.USER_ID, userID);
        params.put(balance_type, amount);
        params.put(WebParams.AMOUNT_TYPE, withdrawal_type);
        params.put(WebParams.DATE_FROM, startdate_query);
        params.put(WebParams.DATE_TO, enddate_query);
        params.put(WebParams.REPETITION, selected_date);
        params.put(WebParams.CCY_ID, "IDR");

        Timber.d("parameter addAutoSaving: " + params.toString());
        MyApiClient.addAutoSaving(getActivity(), params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                Bundle bundle = new Bundle();
                Fragment frag;

                try {
                    Timber.d("respose addAutoSaving: " + response.toString());

                    String code = response.getString(WebParams.ERROR_CODE);

                    if (code.equals(WebParams.SUCCESS_CODE)) {

                        frag = new Confirm_autodeposit();
                        bundle.putString(DefineValue.DATE_FROM, startdate_query);
                        bundle.putString(DefineValue.DATE_TO, enddate_query);
                        bundle.putString(DefineValue.DATEFROM_DISPLAY, start_date.getText().toString());
                        bundle.putString(DefineValue.DATETO_DISPLAY, end_date.getText().toString());
                        bundle.putInt(DefineValue.REPETITION, selected_date);
                        bundle.putString(DefineValue.SAVING_TYPE, withdrawal_type);
                        bundle.putString(DefineValue.VALUE, amount);
                        bundle.putString(DefineValue.AUTO_ID, response.getString(WebParams.AUTO_ID));

                        frag.setArguments(bundle);
                        switchFragment(frag,"",true,Confirm_autodeposit.TAG);

                    }
                    else if (code.equals(WebParams.LOGOUT_CODE)) {
                        Timber.d("isi response autologout:" + response.toString());
                        String message = response.getString(WebParams.ERROR_MESSAGE);
                        AlertDialogLogout test = AlertDialogLogout.getInstance();
                        test.showDialoginActivity(getActivity(), message);
                    } else {
                        Timber.d("Error addAutoSaving:" + response.toString());
                        code = response.getString(WebParams.ERROR_MESSAGE);
                        Toast.makeText(getActivity(), code, Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (prodDialog.isShowing()) {
                    prodDialog.dismiss();
                }

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                super.onFailure(statusCode, headers, responseString, throwable);
                failure(throwable);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                failure(throwable);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                failure(throwable);
            }

            private void failure(Throwable throwable) {
                if (getActivity() != null && !getActivity().isFinishing()) {
                    if (MyApiClient.PROD_FAILURE_FLAG)
                        Toast.makeText(getActivity(), getString(R.string.network_connection_failure_toast), Toast.LENGTH_SHORT).show();
                    else
                        Toast.makeText(getActivity(), throwable.toString(), Toast.LENGTH_SHORT).show();

                    if (prodDialog.isShowing()) {
                        prodDialog.dismiss();
                    }
                }
                Timber.w("Error Koneksi addAutoSaving:" + throwable.toString());
            }

        });
    }

    private void switchFragment(android.support.v4.app.Fragment i, String name, Boolean isBackstack, String tag){
        if (getActivity() == null)
            return;

        TabunganActivity fca = (TabunganActivity ) getActivity();
        fca.switchContent(i,name,isBackstack,tag);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_CODE){
            switch (resultCode) {
                case RESULT_CANCEL:
                    Toast.makeText(getActivity(), "sukses kembali ke fragment auto deposit", Toast.LENGTH_SHORT).show();
                    break;
                case RESULT_ONGOING:
                    getFragmentManager().popBackStack();

            }
        }
    }
}
