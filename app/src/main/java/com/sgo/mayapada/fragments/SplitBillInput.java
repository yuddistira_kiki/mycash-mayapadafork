package com.sgo.mayapada.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.securepreferences.SecurePreferences;
import com.sgo.mayapada.Beans.SplitbillRecipient;
import com.sgo.mayapada.R;
import com.sgo.mayapada.activities.MainPage;
import com.sgo.mayapada.activities.SplitBillActivity;
import com.sgo.mayapada.adapter.SplitbillRecipientAdapter;
import com.sgo.mayapada.coreclass.CurrencyFormat;
import com.sgo.mayapada.coreclass.CustomSecurePref;
import com.sgo.mayapada.coreclass.DefineValue;
import com.sgo.mayapada.coreclass.Fab;
import com.sgo.mayapada.coreclass.InetHandler;
import com.sgo.mayapada.coreclass.ListViewHeight;
import com.sgo.mayapada.coreclass.WebParams;
import com.sgo.mayapada.dialogs.DefinedDialog;

import java.util.ArrayList;
import java.util.List;

import pub.devrel.easypermissions.EasyPermissions;
import rx.Subscription;

/**
 * Created by thinkpad on 7/14/2017.
 */

public class SplitBillInput extends ListFragment implements EasyPermissions.PermissionCallbacks {

    private Activity act;
    private View v;
    private ListView lv_recipient;
    private EditText etAmount;
    private ImageView myImg;
    private TextView myName, myHPNumber, myAmount;
    private CheckBox cbBagiRata;
    private Button btnSplitBill;
    private SecurePreferences sp;
    private String _memberId, _userid, accessKey;
    private long minimalAmount;
    private SplitbillRecipientAdapter recipientAdapter;
    private ArrayList<SplitbillRecipient> listRecipient;
    private Subscription amountSub;
    private boolean isCheckedSplit = false;
    private AlertDialog dialogEmpty =  null, dialogOver = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        sp = CustomSecurePref.getInstance().getmSecurePrefs();
        _memberId = sp.getString(DefineValue.MEMBER_ID,"");
        _userid = sp.getString(DefineValue.USERID_PHONE,"");
        accessKey = sp.getString(DefineValue.ACCESS_KEY,"");
        minimalAmount = Long.parseLong(sp.getString(WebParams.MIN_REQ_MONEY,"1"));

        listRecipient = new ArrayList<>();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.frag_splitbill_input, container, false);
        return v;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        act = getActivity();

        etAmount = (EditText) v.findViewById(R.id.splitbill_value_amount);
        myImg = (ImageView) v.findViewById(R.id.splitbill_recipient_myimg);
        myName = (TextView) v.findViewById(R.id.splitbill_recipient_myname);
        myHPNumber = (TextView) v.findViewById(R.id.splitbill_recipient_myhpnumber);
        myAmount = (TextView) v.findViewById(R.id.splitbill_recipient_myamount);
        lv_recipient = (ListView) v.findViewById(android.R.id.list);
        cbBagiRata = (CheckBox) v.findViewById(R.id.cb_bagi_rata);
        btnSplitBill = (Button) v.findViewById(R.id.btn_split_bill);

        etAmount.addTextChangedListener(amountTextWatcher);
        myName.setText(sp.getString(DefineValue.USER_NAME,""));
        myHPNumber.setText(_userid);

        SplitbillRecipientAdapter.AdapterInterface listener = new SplitbillRecipientAdapter.AdapterInterface()
        {
            @Override
            public void onDelete(String value)
            {
                setAmountSubAction(value);
            }

            @Override
            public void onChangeMyAmount(String value) {
                myAmount.setText(value);
            }
        };

        recipientAdapter = new SplitbillRecipientAdapter(listener, act, listRecipient);
        lv_recipient.setAdapter(recipientAdapter);

//        MyScrollListener scrollListener = new MyScrollListener();
//        lv_recipient.setOnScrollListener(scrollListener);

        cbBagiRata.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(buttonView.isChecked()) {
                    isCheckedSplit = true;
                    countBagiRata();
                }
                else {
                    isCheckedSplit = false;
                }
            }
        });
        btnSplitBill.setOnClickListener(btnSplitBillListener);
        setupFab();
        ToggleFAB(false);
    }

    private Button.OnClickListener btnSplitBillListener = new Button.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (InetHandler.isNetworkAvailable(getActivity())) {
                if (inputValidation()) {
                    if(listRecipient.size() > 0) {
                        boolean isBelowMinimum = false;
                        for (int i = 0; i < listRecipient.size(); i++) {
                            if (Long.parseLong(listRecipient.get(i).getAmount()) < minimalAmount)
                                isBelowMinimum = true;
                        }
                        if (isBelowMinimum) {
                            AlertDialog.Builder builder = new AlertDialog.Builder(act)
                                    .setMessage(getString(R.string.nominal_split_bill_minimum_text) + " " + CurrencyFormat.format(Long.toString(minimalAmount)))
                                    .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            dialogInterface.dismiss();
                                        }
                                    });
                            AlertDialog alertDialogMinimum = builder.create();
                            alertDialogMinimum.show();
                        } else {
                            hiddenKeyboard(v);
                        }
                    }
                    else {
                        AlertDialog.Builder builder = new AlertDialog.Builder(act)
                                .setMessage(getString(R.string.tujuan_split_bill_empty_message))
                                .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                    }
                                });
                        AlertDialog alertDialog = builder.create();
                        alertDialog.show();
                    }
                }
            }
            else DefinedDialog.showErrorDialog(getActivity(), getString(R.string.inethandler_dialog_message));
        }
    };

    private TextWatcher amountTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            String totalRecipient;
            if(listRecipient.size() > 0 && !s.toString().equals("")) {
                //hitung total amount list recipient
                long total = 0;
                for (int i = 0; i < listRecipient.size(); i++) {
                    if(!listRecipient.get(i).getAmount().equals(""))
                        total += Long.parseLong(listRecipient.get(i).getAmount());
                }
                totalRecipient = Long.toString(total);
            }
            else
                totalRecipient = s.toString();

            setAmountSubAction(totalRecipient);
        }
    };

    private void setAmountSubAction(String totalRecipient) {
        //set total tagihan untuk perhitungan di adapter
        recipientAdapter.setTotal(etAmount.getText().toString());
        if(isCheckedSplit) {
            //jika centang bagi rata
            countBagiRata();
        }
        else {
            if(listRecipient.size() == 0) {
                myAmount.setText(etAmount.getText().toString());
            }
            else {
                if (!etAmount.getText().toString().equals("")) {
                    long firstAmount = Long.parseLong(etAmount.getText().toString()) - Long.parseLong(totalRecipient);
                    if (firstAmount < 0) {
                        long total = Long.parseLong(myAmount.getText().toString()) + Long.parseLong(totalRecipient);
                        showDialogTotalTagihanOver(Long.toString(total));
                    } else {
                        myAmount.setText(Long.toString(firstAmount));
                    }
                } else {
                    myAmount.setText("0");
                }
            }
        }
    }

    private void countBagiRata(){
        long total;
        if(etAmount.getText().toString().equals(""))
            total = 0;
        else
            total = Long.parseLong(etAmount.getText().toString());
        long eachAmount = total / (listRecipient.size()+1);
        for(int i = 0 ; i < listRecipient.size() ; i++) {
            listRecipient.get(i).setAmount(Long.toString(eachAmount));
        }
        //jika perhitungan bagi rata ada lebih, maka lebihnya masuk di amount sendiri
        myAmount.setText(Long.toString(total - (eachAmount*listRecipient.size())));
        recipientAdapter.notifyDataSetChanged();
    }

    private void setupFab() {
        Fab fab = (Fab) v.findViewById(R.id.splitbill_fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!etAmount.getText().toString().equals("") && Long.parseLong(etAmount.getText().toString()) > 0){
                    Intent intent = new Intent(getActivity(), SplitBillActivity.class);
                    intent.putExtra(DefineValue.SPLIT_BILL, true);
                    if(listRecipient.size() > 0) {
                        String recipient_no_hp = _userid + ",";
                        for (int i = 0; i < listRecipient.size(); i++) {
                            if(i == listRecipient.size()-1)
                                recipient_no_hp += listRecipient.get(i).getHp_number();
                            else
                                recipient_no_hp += listRecipient.get(i).getHp_number() + ",";
                        }
                        intent.putExtra(DefineValue.RECIPIENTS, recipient_no_hp);
                    }
                    switchActivity(intent);
                }
                else {
                    showDialogTotalTagihanEmpty();
                }
            }
        });
    }

    public void addFriend(String name, String phone){
        SplitbillRecipient sbr = new SplitbillRecipient();
        sbr.setName(name);
        sbr.setHp_number(phone);
        sbr.setAmount("0");
        listRecipient.add(sbr);
        recipientAdapter.notifyDataSetChanged();
        ListViewHeight.setTotalHeightofListView(lv_recipient);
        if(isCheckedSplit)
            countBagiRata();
    }

    private void showDialogTotalTagihanEmpty() {
        if(dialogEmpty == null) {
            AlertDialog.Builder builder = new AlertDialog.Builder(act)
                    .setMessage(getString(R.string.splitbill_total_tagihan_empty_message))
                    .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    });
            dialogEmpty = builder.create();
            dialogEmpty.show();
        }
        else if(!dialogEmpty.isShowing())
            dialogEmpty.show();
    }

    private void showDialogTotalTagihanOver(final String total) {
//        if(dialogOver == null) {
            AlertDialog.Builder builder = new AlertDialog.Builder(act)
                    .setMessage(getString(R.string.splitbill_total_tagihan_minus_message))
                    .setCancelable(false)
                    .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            etAmount.setText(total);
                            etAmount.requestFocus();
                            etAmount.setSelection(etAmount.length());
                            dialogInterface.dismiss();
                        }
                    });
            dialogOver = builder.create();
            dialogOver.show();
//        }
//        else if(!dialogOver.isShowing())
//            dialogOver.show();
    }

    private void ToggleFAB(Boolean isShow){
        if (getActivity() == null)
            return;

        if(getActivity() instanceof MainPage) {
            MainPage fca = (MainPage) getActivity();
            if(isShow)
                fca.materialSheetFab.showFab();
            else
                fca.materialSheetFab.hideSheetThenFab();
        }
    }

//    private class MyScrollListener implements AbsListView.OnScrollListener {
//
//        @Override
//        public void onScroll(AbsListView view, int firstVisibleItem,
//                             int visibleItemCount, int totalItemCount) {
//
//        }
//
//        @Override
//        public void onScrollStateChanged(AbsListView view, int scrollState) {
//            if (SCROLL_STATE_TOUCH_SCROLL == scrollState) {
//                View currentFocus = getActivity().getCurrentFocus();
//                if (currentFocus != null) {
//                    currentFocus.clearFocus();
//                    hiddenKeyboard(v);
//                }
//            }
//        }
//    }

    private boolean inputValidation(){
        if(etAmount.getText().toString().length()==0){
            etAmount.requestFocus();
            etAmount.setError(getString(R.string.splitbill_total_tagihan_empty_message));
            return false;
        }
        else if(Long.parseLong(etAmount.getText().toString()) < minimalAmount){
            etAmount.requestFocus();
            etAmount.setError(getString(R.string.payfriends_amount_minimal) + " Rp " +
                    CurrencyFormat.format(Long.toString(minimalAmount)));
            return false;
        }
        return true;
    }

    private void switchActivity(Intent mIntent){
        if (getActivity() == null)
            return;

        /*MainPage fca = (MainPage) getActivity();
        fca.switchActivity(mIntent,MainPage.ACTIVITY_RESULT);*/
        getActivity().startActivityForResult(mIntent,MainPage.REQUEST_FINISH);
    }

    private void hiddenKeyboard(View v) {
        InputMethodManager keyboard = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        keyboard.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {

    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {

    }
}