package com.sgo.mayapada.fragments;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.ListFragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.securepreferences.SecurePreferences;
import com.sgo.mayapada.Beans.KantinDaftarPembeliModel;
import com.sgo.mayapada.R;
import com.sgo.mayapada.activities.KantinActivity;
import com.sgo.mayapada.adapter.KantinDaftarPembeliAdapter;
import com.sgo.mayapada.coreclass.CustomSecurePref;
import com.sgo.mayapada.coreclass.DefineValue;
import com.sgo.mayapada.coreclass.InetHandler;
import com.sgo.mayapada.coreclass.MyApiClient;
import com.sgo.mayapada.coreclass.WebParams;
import com.sgo.mayapada.dialogs.AlertDialogLogout;
import com.sgo.mayapada.dialogs.DefinedDialog;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import timber.log.Timber;

/**
 * Created by thinkpad on 8/10/2016.
 */
public class FragKantinDaftarPembeli extends ListFragment {

    private SecurePreferences sp;
    private View v;
    private ProgressDialog out;
    private LinearLayout layout_check_buyer;
    private LinearLayout layout_list_buyer;
    private ListView lvBuyer;
    private EditText etSearchBuyer;

    private KantinDaftarPembeliAdapter adapter;
    private String user_id;
    private String access_key;
    private String member_id;
    private String merchant_subcategory;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        v = inflater.inflate(R.layout.frag_kantin_daftar_pembeli, container, false);
        layout_check_buyer = (LinearLayout) v.findViewById(R.id.layout_check_buyer);
        layout_list_buyer = (LinearLayout) v.findViewById(R.id.layout_list_buyer);
        layout_check_buyer.setVisibility(View.GONE);
        layout_list_buyer.setVisibility(View.VISIBLE);
        lvBuyer = (ListView) v.findViewById(android.R.id.list);
        etSearchBuyer = (EditText) v.findViewById(R.id.etSearchBuyer);

        etSearchBuyer.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                adapter.getFilter().filter(s);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        return v;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        sp = CustomSecurePref.getInstance().getmSecurePrefs();
        user_id = sp.getString(DefineValue.USERID_PHONE, "");
        access_key = sp.getString(DefineValue.ACCESS_KEY, "");
        member_id = sp.getString(DefineValue.MEMBER_ID, "");
        merchant_subcategory = sp.getString(DefineValue.MERCHANT_SUBCATEGORY, "");

        if(adapter == null) {
            ArrayList<KantinDaftarPembeliModel> listBuyer = new ArrayList<>();
            adapter = new KantinDaftarPembeliAdapter(getActivity(), R.layout.list_kantin_daftar_pembeli_item, listBuyer);
        }
        setListAdapter(adapter);
        getListView().setTextFilterEnabled(true);

        getDataBuyer("");
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        if(InetHandler.isNetworkAvailable(getActivity())) {
            ArrayList<KantinDaftarPembeliModel> filter = adapter.getFilteredData();
            sentCheckCouponCode(filter.get(position).getCoupon());
        }
        else DefinedDialog.showErrorDialog(getActivity(), getString(R.string.inethandler_dialog_message));
    }

    private void getDataBuyer(String _noHpMember) {
        try{
            out = DefinedDialog.CreateProgressDialog(getActivity(), null);
            out.show();

            RequestParams params = MyApiClient.getSignatureWithParams(MyApiClient.COMM_ID, MyApiClient.LINK_BUYER_LIST,
                    user_id, access_key);
            params.put(WebParams.MERCHANT_ID, sp.getString(DefineValue.MEMBER_ID,""));
            params.put(WebParams.USER_ID, user_id);
            params.put(WebParams.COMM_ID, MyApiClient.COMM_ID);
            params.put(WebParams.NO_HP_MEMBER, _noHpMember);

            Timber.d("isi params data buyer:" + params.toString());

            MyApiClient.getBuyerList(getActivity(), params, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    try {
                        out.dismiss();
                        String code = response.getString(WebParams.ERROR_CODE);
                        if (code.equals(WebParams.SUCCESS_CODE) || code.equals("0220")) {
                            Timber.d("isi response data buyer:" + response.toString());

                            String data = response.getString(WebParams.SALES);
                            if(!data.equals("") && !data.equals("null")) {
                                JSONArray array = new JSONArray(data);
                                ArrayList<KantinDaftarPembeliModel> listdata = new ArrayList<>();
                                for (int i = 0; i < array.length(); i++) {
                                    KantinDaftarPembeliModel buyer = new KantinDaftarPembeliModel();
                                    buyer.setOrder_id(array.getJSONObject(i).getString(WebParams.ORDER_ID));
                                    buyer.setNoHp(array.getJSONObject(i).getString(WebParams.NO_HP_MEMBER));
                                    buyer.setCoupon(array.getJSONObject(i).getString(WebParams.KUPON));
                                    buyer.setStatus(array.getJSONObject(i).getString(WebParams.STATUS));
                                    buyer.setTgl(array.getJSONObject(i).getString(WebParams.TGL));
                                    listdata.add(buyer);
                                }
                                adapter.swapItems(listdata);
                                adapter.refreshAdapter();
                            }
                        } else if (code.equals(WebParams.LOGOUT_CODE)) {
                            Timber.d("isi response autologout:" + response.toString());
                            String message = response.getString(WebParams.ERROR_MESSAGE);
                            AlertDialogLogout test = AlertDialogLogout.getInstance();
                            test.showDialoginActivity(getActivity(), message);
                        }
                        else {
                            Timber.d("Error data buyer:" + response.toString());
                            code = response.getString(WebParams.ERROR_CODE) + ":" + response.getString(WebParams.ERROR_MESSAGE);
                            Toast.makeText(getActivity(), code, Toast.LENGTH_LONG).show();
                            getActivity().finish();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    super.onFailure(statusCode, headers, responseString, throwable);
                    failure(throwable);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    failure(throwable);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    failure(throwable);
                }

                private void failure(Throwable throwable) {
                    out.dismiss();
                    if (MyApiClient.PROD_FAILURE_FLAG)
                        Toast.makeText(getActivity(), getString(R.string.network_connection_failure_toast), Toast.LENGTH_SHORT).show();
                    else
                        Toast.makeText(getActivity(), throwable.toString(), Toast.LENGTH_SHORT).show();

                    Timber.w("Error Koneksi data buyer:" + throwable.toString());
                }
            });
        } catch (Exception e) {
            Timber.d("httpclient:" + e.getMessage());
        }
    }

    private void sentCheckCouponCode(final String coupon) {
        try{
            out = DefinedDialog.CreateProgressDialog(getActivity(), null);
            out.show();

            RequestParams params = MyApiClient.getSignatureWithParams(MyApiClient.COMM_ID, MyApiClient.LINK_CHECK_COUPON_CODE,
                    user_id, access_key);
            params.put(WebParams.USER_ID,user_id);
            params.put(WebParams.COMM_ID, MyApiClient.COMM_ID);
            params.put(WebParams.MERCHANT_ID, member_id);
            params.put(WebParams.KUPON, coupon);
            params.put(WebParams.MERCHANT_SUBCATEGORY, merchant_subcategory);

            Timber.d("isi params check coupon code:"+params.toString());

            MyApiClient.sentCheckCouponCode(getActivity(), params, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                    try {
                        out.dismiss();

                        String code = response.getString(WebParams.ERROR_CODE);
                        Timber.w("isi response check coupon code:" + response.toString());

                        if (code.equals(WebParams.SUCCESS_CODE)) {
                            JSONObject obj = new JSONObject(response.getString(WebParams.ORDER));
                            Fragment newFrag = new FragKantinDetailBelanja();
                            Bundle args = new Bundle();
                            args.putBoolean(DefineValue.CONFIRM, false);
                            args.putString(DefineValue.LIST_ORDER, obj.getString(WebParams.ITEM));
                            args.putString(DefineValue.COUPON_CODE, coupon);
                            args.putString(DefineValue.MEMBER_NAME, obj.getString(WebParams.MEMBER_NAME));
                            args.putString(DefineValue.NO_HP_MEMBER, obj.getString(WebParams.NO_HP_MEMBER));
                            args.putString(DefineValue.AMOUNT, obj.getString(WebParams.AMOUNT));
                            args.putString(DefineValue.CCY_ID, obj.getString(WebParams.CCY_ID));
                            args.putString(DefineValue.TX_DATETIME, obj.getString(WebParams.TX_DATETIME));
                            args.putString(DefineValue.KADALUARSA, obj.getString(WebParams.KADALUARSA));
                            args.putString(DefineValue.STATUS, obj.getString(WebParams.STATUS));
                            newFrag.setArguments(args);
                            switchFragment(FragKantinDaftarPembeli.this, newFrag, "FragDetail", true);

                        } else if (code.equals(WebParams.LOGOUT_CODE)) {
                            Timber.d("isi response autologout:" + response.toString());
                            String message = response.getString(WebParams.ERROR_MESSAGE);
                            AlertDialogLogout test = AlertDialogLogout.getInstance();
                            test.showDialoginActivity(getActivity(), message);
                        }
                        else {
                            String message = response.getString(WebParams.ERROR_MESSAGE);
                            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                            builder.setMessage(message)
                                    .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();
                                        }
                                    });
                            AlertDialog dialog = builder.create();
                            dialog.show();
                        }

                    } catch (JSONException e) {
                        Toast.makeText(getActivity(), getString(R.string.internal_error), Toast.LENGTH_LONG).show();
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    super.onFailure(statusCode, headers, responseString, throwable);
                    failure(throwable);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    failure(throwable);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    failure(throwable);
                }

                private void failure(Throwable throwable) {
                    out.dismiss();
                    if (MyApiClient.PROD_FAILURE_FLAG)
                        Toast.makeText(getActivity(), getString(R.string.network_connection_failure_toast), Toast.LENGTH_SHORT).show();
                    else
                        Toast.makeText(getActivity(), throwable.toString(), Toast.LENGTH_SHORT).show();

                    Timber.w("Error Koneksi check coupon code:" + throwable.toString());
                }
            });
        } catch (Exception e) {
            String err = (e.getMessage()==null)?"Connection failed":e.getMessage();
            Timber.e("http err:" + err);
        }
    }

    private void switchFragment(Fragment current, android.support.v4.app.Fragment newFrag, String name, Boolean isBackstack){
        if (getActivity() == null)
            return;

        KantinActivity fca = (KantinActivity) getActivity();
        fca.switchContent(current, newFrag, name, isBackstack);
        etSearchBuyer.setText("");
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if(getActivity().getSupportFragmentManager().getBackStackEntryCount() > 0)
                    getActivity().getSupportFragmentManager().popBackStack();
                else
                    getActivity().finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume() {
        super.onResume();
        setTitle(getString(R.string.kantin_list_buyer));
    }

    private void setTitle(String _title){
        if (getActivity() == null)
            return;

        KantinActivity fca = (KantinActivity) getActivity();
        fca.setTitleFragment(_title);
    }
}