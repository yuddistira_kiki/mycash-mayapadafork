package com.sgo.mayapada.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.ListFragment;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.securepreferences.SecurePreferences;
import com.sgo.mayapada.Beans.navdrawmainmenuModel;
import com.sgo.mayapada.R;
import com.sgo.mayapada.activities.MainPage;
import com.sgo.mayapada.activities.MyProfileActivity;
import com.sgo.mayapada.adapter.NavDrawMainMenuAdapter;
import com.sgo.mayapada.coreclass.CurrencyFormat;
import com.sgo.mayapada.coreclass.CustomSecurePref;
import com.sgo.mayapada.coreclass.DefineValue;
import com.sgo.mayapada.coreclass.LevelClass;
import com.sgo.mayapada.coreclass.MyApiClient;
import com.sgo.mayapada.coreclass.MyPicasso;
import com.sgo.mayapada.coreclass.RoundImageTransformation;
import com.sgo.mayapada.dialogs.ViewImageDialog;
import com.sgo.mayapada.interfaces.OnLoadDataListener;
import com.sgo.mayapada.loader.UtilsLoader;
import com.sgo.mayapada.services.BalanceService;
import com.sgo.mayapada.services.UserProfileService;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import timber.log.Timber;

/*
  Created by Administrator on 12/8/2014.
 */
public class NavigationDrawMenu extends ListFragment{

    public static final String TAG = "com.sgo.mayapada.fragments.NavigationDrawMenu";

    /**
     * Untuk Navigasi sekarang pake Constant int, jadi tiap menu punya idx int sendiri-sendiri,
     * untuk nambah menu baru buat Constant int baru sesuai dengan format dan ganti nama , tentukan nilai int-nya
     * trus tambahin di {@link #generateData()} object navigation drawernya, ikutin aja formatnya
     * selanjutnya tambahin di {@link #selectItem(int, Bundle)} untuk behaviour menu navigasinya
     */

    public static final int MUPGRADEUSER = 0;
    public static final int MTOPUP = 1;
    public static final int MPAYFRIENDS= 2;
    public static final int MASK4MONEY= 3;
    public static final int MBUY= 4;
    public static final int MCASHOUT= 5;
    public static final int MSPLITBILL= 6;
    public static final int MMYFRIENDS= 7;
    public static final int MREPORT= 8;
    private static final int MSETTINGS= 9;
    private static final int MLOGOUT= 10;

    private ImageView headerCustImage;
    private TextView headerCustName,headerCustID,headerCurrency,balanceValue, currencyLimit, limitValue,periodeLimit;
    private LinearLayout layoutRemainLimit;

    private Animation frameAnimation;
    private ImageView btn_refresh_balance;

    private ListView mListView;
    private View v;
    private NavDrawMainMenuAdapter mAdapter;
    private Bundle _SaveInstance;
    private SecurePreferences sp;
    private String NameUser;
    private Activity act;
    ProgressDialog progdialog;
    private String _url_profpic_large;
    private LevelClass levelClass;
    private IntentFilter filter;
    int lvl;
    private ArrayList<navdrawmainmenuModel> navData;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        filter = new IntentFilter();
        filter.addAction(BalanceService.INTENT_ACTION_BALANCE);
        filter.addAction(UserProfileService.INTENT_ACTION_USER_PROFILE);
        act = getActivity();
        sp = CustomSecurePref.getInstance().getmSecurePrefs();

        lvl = sp.getInt(DefineValue.LEVEL_VALUE,0);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.frag_navigation_draw_menu_main, container, false);
        return v;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        _SaveInstance = savedInstanceState;
        generateData();
        mAdapter = new NavDrawMainMenuAdapter(getActivity(), navData);
        mListView = (ListView) v.findViewById(android.R.id.list);
        mListView.setAdapter(mAdapter);

        LinearLayout llHeaderProfile = (LinearLayout) v.findViewById(R.id.llHeaderProfile);
        headerCustImage = (ImageView) v.findViewById(R.id.header_cust_image);
        headerCurrency = (TextView) v.findViewById(R.id.currency_value);
        headerCustName = (TextView) v.findViewById(R.id.header_cust_name);
        headerCustID = (TextView) v.findViewById(R.id.header_cust_id);
        balanceValue = (TextView) v.findViewById(R.id.balance_value);
        currencyLimit = (TextView) v.findViewById(R.id.currency_limit_value);
        limitValue = (TextView) v.findViewById(R.id.limit_value);
        periodeLimit = (TextView) v.findViewById(R.id.periode_limit_value);
        layoutRemainLimit = (LinearLayout) v.findViewById(R.id.layout_remain_limit);

        layoutRemainLimit.setVisibility(View.VISIBLE);

        refreshUINavDrawer();
        refreshDataNavDrawer();

        llHeaderProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), MyProfileActivity.class);
                switchActivity(i, MainPage.ACTIVITY_RESULT);
            }
        });

        btn_refresh_balance = (ImageView) v.findViewById(R.id.btn_refresh_balance);
        frameAnimation = AnimationUtils.loadAnimation(getActivity(), R.anim.spinner_animation);
        frameAnimation.setRepeatCount(Animation.INFINITE);
        btn_refresh_balance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btn_refresh_balance.setEnabled(false);
                btn_refresh_balance.startAnimation(frameAnimation);
                getBalance(false);
            }
        });
        headerCustImage.setOnClickListener(viewppimage);
        levelClass = new LevelClass(getActivity(),sp);

        setBalanceToUI();
    }

    private ImageView.OnClickListener viewppimage = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            imagedialog();
        }
    };

    private void imagedialog(){
        String url =  sp.getString(DefineValue.IMG_URL, "");
        if(url != null && !url.isEmpty()){
            String name = sp.getString(DefineValue.CUST_NAME, getString(R.string.text_strip));
            DialogFragment via = ViewImageDialog.CreateInstance(name,url);
            FragmentTransaction fm = getFragmentManager().beginTransaction();
            via.show(fm, ViewImageDialog.TAG);
        }
    }

    private void setBalanceToUI(){
        headerCurrency.setText(sp.getString(DefineValue.BALANCE_CCYID,""));
        balanceValue.setText(CurrencyFormat.format(sp.getString(DefineValue.BALANCE_AMOUNT,"")));
        currencyLimit.setText(sp.getString(DefineValue.BALANCE_CCYID,""));
        limitValue.setText(CurrencyFormat.format(sp.getString(DefineValue.BALANCE_REMAIN_LIMIT,"")));

        if (sp.getString(DefineValue.BALANCE_PERIOD_LIMIT,"").equals("Monthly"))
            periodeLimit.setText(R.string.header_monthly_limit);
        else
            periodeLimit.setText(R.string.header_daily_limit);
    }

    public void getBalance(Boolean isAuto){
        new UtilsLoader(getActivity(),sp).getDataBalance(isAuto,new OnLoadDataListener() {
            @Override
            public void onSuccess(Object deData) {
                setBalanceToUI();
                btn_refresh_balance.setEnabled(true);
                btn_refresh_balance.clearAnimation();
            }

            @Override
            public void onFail(Bundle message) {
                btn_refresh_balance.setEnabled(true);
                btn_refresh_balance.clearAnimation();
            }

            @Override
            public void onFailure(String message) {
                btn_refresh_balance.setEnabled(true);
                btn_refresh_balance.clearAnimation();
            }
        });
    }

    private void setImageProfPic(){
        float density = getResources().getDisplayMetrics().density;
        String _url_profpic;

        if(density <= 1) _url_profpic = sp.getString(DefineValue.IMG_SMALL_URL, null);
        else if(density < 2) _url_profpic = sp.getString(DefineValue.IMG_MEDIUM_URL, null);
        else _url_profpic = sp.getString(DefineValue.IMG_LARGE_URL, null);

        Timber.wtf("url prof pic:" + _url_profpic);

        Bitmap bm = BitmapFactory.decodeResource(getResources(), R.drawable.user_unknown_menu);
        RoundImageTransformation roundedImage = new RoundImageTransformation(bm);

        Picasso mPic = MyPicasso.getImageLoader(getActivity());

        if(_url_profpic !=null && _url_profpic.isEmpty()){
            mPic.load(R.drawable.user_unknown_menu)
                    .error(roundedImage)
                    .fit().centerInside()
                    .placeholder(R.drawable.progress_animation)
                    .transform(new RoundImageTransformation()).into(headerCustImage);
        }
        else {
            mPic.load(_url_profpic)
                    .error(roundedImage)
                    .fit().centerInside()
                    .placeholder(R.drawable.progress_animation)
                    .transform(new RoundImageTransformation()).into(headerCustImage);
        }
    }

    public void initializeNavDrawer(){
       if(!getActivity().isFinishing()) {
           Fragment newFragment = new Home();
           switchFragment(newFragment, getString(R.string.toolbar_title_home));

           refreshDataNavDrawer();
       }
    }

    public void refreshUINavDrawer(){
        setImageProfPic();
        headerCustName.setText(sp.getString(DefineValue.CUST_NAME, getString(R.string.text_strip)));
        headerCustID.setText(sp.getString(DefineValue.CUST_ID, getString(R.string.text_strip)));
        NameUser = sp.getString(DefineValue.CUST_NAME, getString(R.string.text_strip));
        _url_profpic_large = sp.getString(DefineValue.IMG_LARGE_URL, null);
    }

    public void refreshDataNavDrawer(){
        if(levelClass != null)
            levelClass.refreshData();
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        mAdapter.setSelectedItem(position);
        mAdapter.notifyDataSetChanged();
        selectItem(mAdapter.getItem(position).getNavItemId(), null);
    }

    private void generateData(){
        navData = new ArrayList<>();
        navData.add(new navdrawmainmenuModel(getString(R.string.menu_group_title_main_menu)));
        if(lvl == 1)
            navData.add(new navdrawmainmenuModel(R.drawable.ic_groups_white,R.drawable.ic_groups_icon_color,getString(R.string.menu_item_title_upgrade_user),MUPGRADEUSER));
        navData.add(new navdrawmainmenuModel(R.drawable.ic_topup_white,R.drawable.ic_topup_icon_color,getString(R.string.menu_item_title_topup),MTOPUP));
        navData.add(new navdrawmainmenuModel(R.drawable.ic_payfriends_white,R.drawable.ic_payfriends_icon_color,getString(R.string.menu_item_title_pay_friends),MPAYFRIENDS));
        navData.add(new navdrawmainmenuModel(R.drawable.ic_ask_white,R.drawable.ic_ask_icon_color,getString(R.string.menu_item_title_ask_for_money),MASK4MONEY));
        navData.add(new navdrawmainmenuModel(R.drawable.ic_buy_white,R.drawable.ic_buy_icon_color,getString(R.string.menu_item_title_buy),MBUY));
        navData.add(new navdrawmainmenuModel(R.drawable.ic_cashout_white,R.drawable.ic_cashout_icon_color,getString(R.string.menu_item_title_cash_out),MCASHOUT));
        navData.add(new navdrawmainmenuModel(R.drawable.ic_account_balance_white_36dp,R.drawable.ic_account_balance_blue_36dp,getString(R.string.menu_item_title_split_bill),MSPLITBILL));
        navData.add(new navdrawmainmenuModel(R.drawable.ic_report_white,R.drawable.ic_report,getString(R.string.menu_item_title_report),MREPORT));

        navData.add(new navdrawmainmenuModel(getString(R.string.menu_group_title_account)));
        navData.add(new navdrawmainmenuModel(R.drawable.ic_friends_white,R.drawable.ic_friends_icon_color,getString(R.string.menu_item_title_my_friends),MMYFRIENDS));
        navData.add(new navdrawmainmenuModel(R.drawable.ic_setting_white,R.drawable.ic_setting,getString(R.string.menu_item_title_setting),MSETTINGS));
        navData.add(new navdrawmainmenuModel(R.drawable.ic_logout_white,R.drawable.ic_logout_icon,getString(R.string.menu_item_title_logout),MLOGOUT));
    }

    public void updateMenuAfterUpgrade(){
        navData.remove(1);
        mAdapter.notifyDataSetChanged();
    }

    public void selectItem(int itemId, Bundle data){
        Fragment newFragment;
        Boolean masuk =!levelClass.isLevel1QAC();
        switch (itemId) {
            case MUPGRADEUSER:
                newFragment = new UpgradeUserInput();
                switchFragment(newFragment, getString(R.string.toolbar_title_upgrade_user));
                break;
            case MTOPUP:
                newFragment = new ListTopUp();
                switchFragment(newFragment, getString(R.string.toolbar_title_topup));
                break;
            case MPAYFRIENDS:
                if (masuk) {
                    newFragment = new FragPayFriends();
                    if (data != null && !data.isEmpty()) newFragment.setArguments(data);
                    switchFragment(newFragment, getString(R.string.menu_item_title_pay_friends));
                } else {
                    levelClass.showDialogLevel();
                }
                break;
            case MASK4MONEY:
                if (masuk) {
                    newFragment = new FragAskForMoney();
                    if (data != null && !data.isEmpty()) newFragment.setArguments(data);
                    switchFragment(newFragment, getString(R.string.menu_item_title_ask_for_money));
                } else {
                    levelClass.showDialogLevel();
                }
                break;
            case MBUY:
                newFragment = new ListBuy();
                switchFragment(newFragment, getString(R.string.toolbar_title_purchase));
                break;
            case MCASHOUT:
                if (masuk) {
                    newFragment = new ListCashOut();
                    switchFragment(newFragment, getString(R.string.menu_item_title_cash_out));
                } else {
                    levelClass.showDialogLevel();
                }
                break;
            case MSPLITBILL:
                newFragment = new SplitBillInput();
                switchFragment(newFragment, getString(R.string.toolbar_title_split_bill));
                break;
            case MMYFRIENDS:
                newFragment = new ListMyFriends();
                switchFragment(newFragment, getString(R.string.toolbar_title_myfriends));
                break;
            case MREPORT:
                newFragment = new ReportTab();
                switchFragment(newFragment, getString(R.string.menu_item_title_report));
                break;
            case MSETTINGS:
                newFragment = new ListSettings();
                switchFragment(newFragment, getString(R.string.menu_item_title_setting));
                break;
            case MLOGOUT:
                AlertDialog.Builder alertbox = new AlertDialog.Builder(getActivity());
                alertbox.setTitle(getString(R.string.warning));
                alertbox.setMessage(getString(R.string.exit_message));
                alertbox.setPositiveButton(getString(R.string.ok), new
                        DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface arg0, int arg1) {
                                switchLogout();
                            }
                        });
                alertbox.setNegativeButton(getString(R.string.cancel), new
                        DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface arg0, int arg1) {
                            }
                        });
                alertbox.show();
                break;
        }
    }

    private void switchFragment(Fragment i, String name){
        if (getActivity() == null)
            return;

        MainPage fca = (MainPage) getActivity();
        fca.switchContent(i,name);
    }

    private void switchLogout(){
        if (getActivity() == null)
            return;

        MainPage fca = (MainPage) getActivity();
        fca.switchLogout();
    }


    private void switchActivity(Intent mIntent,int j){
        if (getActivity() == null)
            return;

        MainPage fca = (MainPage) getActivity();
        fca.switchActivity(mIntent,j);
    }

    public void setPositionNull(){
        mAdapter.setDefault();
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(receiver,filter);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(receiver);
    }

    private BroadcastReceiver receiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            if(action.equals(BalanceService.INTENT_ACTION_BALANCE)){
                setBalanceToUI();
            }
            else if(action.equals(UserProfileService.INTENT_ACTION_USER_PROFILE)){
                refreshUINavDrawer();
            }


        }
    };

}