package com.sgo.mayapada.fragments;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.securepreferences.SecurePreferences;
import com.sgo.mayapada.R;
import com.sgo.mayapada.activities.InsertPIN;
import com.sgo.mayapada.activities.MainPage;
import com.sgo.mayapada.activities.RegisterSMSBankingActivity;
import com.sgo.mayapada.activities.TagihanActivity;
import com.sgo.mayapada.coreclass.CurrencyFormat;
import com.sgo.mayapada.coreclass.CustomSecurePref;
import com.sgo.mayapada.coreclass.DateTimeFormat;
import com.sgo.mayapada.coreclass.DefineValue;
import com.sgo.mayapada.coreclass.InetHandler;
import com.sgo.mayapada.coreclass.LevelClass;
import com.sgo.mayapada.coreclass.MyApiClient;
import com.sgo.mayapada.coreclass.WebParams;
import com.sgo.mayapada.dialogs.AlertDialogLogout;
import com.sgo.mayapada.dialogs.DefinedDialog;
import com.sgo.mayapada.dialogs.PreviewDetailTagihanDialog;
import com.sgo.mayapada.dialogs.ReportBillerDialog;
import com.sgo.mayapada.interfaces.OnLoadDataListener;
import com.sgo.mayapada.loader.UtilsLoader;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import timber.log.Timber;

/**
 * Created by thinkpad on 5/17/2016.
 */
public class PreviewTagihanViaScash extends Fragment implements ReportBillerDialog.OnDialogOkCallback{
    private View v;
    private ProgressDialog progdialog;
    private LinearLayout layout_token;
    private TextView mTranscID;
    private TextView mBankName;
    private TextView mProductName;
    private TextView mAmount;
    private TextView mFee;
    private EditText mToken;
    private Button btnProses;
    private Button btnCancel;
    private Button btnShow;
    private SecurePreferences sp;

    private String userID;
    private String accessKey;
    private String productCode;
    private String productName;
    private String commId;
    private String commCode;
    private String commName;
    private String callbackUrl;
    private String apiKey;
    private String bankCode;
    private String bankName;
    private String amount;
    private String invoices;
    private String shareType = "1";
    private String txID;
    private String fee;
    private String bankData;
    private String results;
    private String ccy;
    private String tx_multiple;
    private boolean isPIN = false;
    private ArrayList<String> listDocId;
    private int pin_attempt=-1;
    private PreviewDetailTagihanDialog dialogDetail;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        v = inflater.inflate(R.layout.frag_preview_tagihan, container, false);
        return v;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        sp = CustomSecurePref.getInstance().getmSecurePrefs();
        userID = sp.getString(DefineValue.USERID_PHONE, "");
        accessKey = sp.getString(DefineValue.ACCESS_KEY, "");

        listDocId = new ArrayList<>();

        Bundle args = getArguments();
        commId = args.getString(DefineValue.COMMUNITY_ID,"");
        commCode = args.getString(DefineValue.COMMUNITY_CODE, "");
        commName = args.getString(DefineValue.COMMUNITY_NAME,"");
        callbackUrl = args.getString(DefineValue.CALLBACK_URL,"");
        apiKey = args.getString(DefineValue.API_KEY, "");
        amount = args.getString(DefineValue.AMOUNT_TAGIHAN, "");
        invoices = args.getString(DefineValue.INVOICES, "");
        bankData = args.getString(DefineValue.BANKLIST_DATA, "");

        Timber.d("isi args:" + args.toString());

        layout_token = (LinearLayout) v.findViewById(R.id.tagihan_edittext_token);
        mTranscID = (TextView) v.findViewById(R.id.tagihan_id_transaksi_value);
        mBankName = (TextView) v.findViewById(R.id.tagihan_nama_bank_value);
        mProductName = (TextView) v.findViewById(R.id.tagihan_nama_produk_value);
        mToken = (EditText) v.findViewById(R.id.tagihan_token_value);
        mAmount = (TextView) v.findViewById(R.id.tagihan_amount_value);
        mFee = (TextView) v.findViewById(R.id.tagihan_fee_value);
        btnProses = (Button) v.findViewById(R.id.tagihan_btn_proses);
        btnCancel = (Button) v.findViewById(R.id.tagihan_btn_cancel);
        btnShow = (Button) v.findViewById(R.id.tagihan_btn_show);

        btnProses.setOnClickListener(prosesListener);
        btnCancel.setOnClickListener(cancelListener);
        btnShow.setOnClickListener(showListener);

        try {
            JSONArray mArrayInv = new JSONArray(invoices);
            for(int i=0 ; i<mArrayInv.length() ; i++) {
                listDocId.add(mArrayInv.getJSONObject(i).getString(WebParams.DOC_ID));
            }

            JSONArray mArrayBank = new JSONArray(bankData);
            for(int j=0 ; j<mArrayBank.length() ; j++) {
                JSONObject objBank = mArrayBank.getJSONObject(j);
                if(objBank.getString(WebParams.PRODUCT_CODE).equalsIgnoreCase(DefineValue.SCASH)) {
                    bankCode = objBank.getString(WebParams.BANK_CODE);
                    bankName = objBank.getString(WebParams.BANK_NAME);
                    productCode = objBank.getString(WebParams.PRODUCT_CODE);
                    productName = objBank.getString(WebParams.PRODUCT_NAME);
                }
            }

            sentDataTagihan();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private Button.OnClickListener prosesListener = new Button.OnClickListener() {
        @Override
        public void onClick(View view) {
            if(InetHandler.isNetworkAvailable(getActivity())){

                Timber.d("hit button submit");
                btnProses.setEnabled(false);

                if(isPIN){
                    CallPINinput();
                    btnProses.setEnabled(true);
                }
                else{
                    if(inputValidation()) {
                        sentInsertTrans(mToken.getText().toString());
                    }
                    else btnProses.setEnabled(true);
                }
            }
            else DefinedDialog.showErrorDialog(getActivity(), getString(R.string.inethandler_dialog_message));
        }
    };

    private Button.OnClickListener cancelListener = new Button.OnClickListener() {
        @Override
        public void onClick(View view) {
            getActivity().getSupportFragmentManager().popBackStack();
        }
    };

    private Button.OnClickListener showListener = new Button.OnClickListener() {
        @Override
        public void onClick(View view) {
            dialogDetail = PreviewDetailTagihanDialog.newInstance(getActivity());

            Bundle args = new Bundle();
            args.putString(DefineValue.INVOICES, invoices);
            args.putString(DefineValue.RESULTS, results);
            args.putString(DefineValue.CCY_ID, ccy);

            dialogDetail.setArguments(args);
            dialogDetail.setTargetFragment(PreviewTagihanViaScash.this,0);
            dialogDetail.show(getActivity().getSupportFragmentManager(), PreviewDetailTagihanDialog.TAG);
        }
    };

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if(getActivity().getSupportFragmentManager().getBackStackEntryCount() > 0)
                    getActivity().getSupportFragmentManager().popBackStack();
                else
                    getActivity().finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == MainPage.REQUEST_FINISH){
            if(resultCode == InsertPIN.RESULT_PIN_VALUE){
                String value_pin = data.getStringExtra(DefineValue.PIN_VALUE);
                sentInsertTrans(value_pin);
            }
        }
    }

    private void CallPINinput(){
        Intent i = new Intent(getActivity(), InsertPIN.class);
        if(pin_attempt != -1 && pin_attempt < 2)
            i.putExtra(DefineValue.ATTEMPT,pin_attempt);
        startActivityForResult(i, MainPage.REQUEST_FINISH);
    }


    private boolean inputValidation(){
        if(mToken.getText().toString().length()==0){
            mToken.requestFocus();
            mToken.setError(this.getString(R.string.validation_otp));
            return false;
        }
        return true;
    }

    private void sentDataTagihan() {
        try{
            progdialog = DefinedDialog.CreateProgressDialog(getActivity(), "");
            progdialog.show();

            RequestParams params = MyApiClient.getSignatureWithParams(MyApiClient.COMM_ID, MyApiClient.LINK_PAYMENT_INV,
                    userID, accessKey);
            params.put(WebParams.CUST_ID, userID);
            params.put(WebParams.BANK_CODE, bankCode);
            params.put(WebParams.PRODUCT_CODE, productCode);
            params.put(WebParams.CCY_ID, MyApiClient.CCY_VALUE);
            params.put(WebParams.COMM_ID_INV, commId);
            params.put(WebParams.USER_ID, userID);
            params.put(WebParams.COMM_ID, MyApiClient.COMM_ID);
            params.put(WebParams.INVOICES, invoices);
            params.put(WebParams.PRODUCT_TYPE, DefineValue.EMO);
            params.put(WebParams.COMM_CODE, sp.getString(DefineValue.COMMUNITY_CODE,""));
            params.put(WebParams.COMM_CODE_INV, commCode);

            Timber.d("isi params payment inv:" + params.toString());

            MyApiClient.sentPaymentInv(getActivity(), params, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    try {
                        String code = response.getString(WebParams.ERROR_CODE);
                        if (code.equals(WebParams.SUCCESS_CODE)) {
                            Timber.d("isi response payment inv:" + response.toString());

                            tx_multiple = response.getString(WebParams.TX_MULTIPLE);
                            String auth_type = response.getString(WebParams.AUTH_TYPE);
                            if (auth_type.equalsIgnoreCase("pin")) {
                                isPIN = true;

                                if (isPIN) {
                                    layout_token.setVisibility(View.GONE);
                                    new UtilsLoader(getActivity(), sp).getFailedPIN(userID, new OnLoadDataListener() { //get pin attempt
                                        @Override
                                        public void onSuccess(Object deData) {
                                            pin_attempt = (int) deData;
                                        }

                                        @Override
                                        public void onFail(Bundle message) {

                                        }

                                        @Override
                                        public void onFailure(String message) {

                                        }
                                    });
                                }
                            }
                            txID = response.getString(WebParams.TX_ID);
                            ccy = response.getString(WebParams.CCY_ID);
                            results = response.getString(WebParams.RESULTS);
                            fee = response.getString(WebParams.ADMIN_FEE);
                            if (fee.equals("")) fee = "0";

                            mTranscID.setText(txID);
                            mBankName.setText(bankName);
                            if(productName.equalsIgnoreCase("S-Cash"))
                            {
                                mProductName.setText("UNIK");
                            }
                            else
                            {
                                mProductName.setText(productName);
                            }
                            if (ccy.equalsIgnoreCase("idr")) {
                                mAmount.setText("Rp. " + CurrencyFormat.format(amount));
                                mFee.setText("Rp. " + CurrencyFormat.format(fee));
                            } else {
                                mAmount.setText(amount);
                                mFee.setText(fee);
                            }

                            sentDataReqToken(txID, productCode, bankCode, commCode, fee, amount, productName);
                        } else if (code.equals(WebParams.LOGOUT_CODE)) {
                            Timber.d("isi response autologout:" + response.toString());
                            String message = response.getString(WebParams.ERROR_MESSAGE);
                            AlertDialogLogout test = AlertDialogLogout.getInstance();
                            test.showDialoginActivity(getActivity(), message);
                        } else {
                            Timber.d("Error payment inv:" + response.toString());
                            code = response.getString(WebParams.ERROR_CODE) + ":" + response.getString(WebParams.ERROR_MESSAGE);
                            progdialog.dismiss();
                            Toast.makeText(getActivity(), code, Toast.LENGTH_LONG).show();
                            getActivity().getSupportFragmentManager().popBackStack();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    super.onFailure(statusCode, headers, responseString, throwable);
                    failure(throwable);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    failure(throwable);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    failure(throwable);
                }

                private void failure(Throwable throwable) {
                    if (MyApiClient.PROD_FAILURE_FLAG)
                        Toast.makeText(getActivity(), getString(R.string.network_connection_failure_toast), Toast.LENGTH_SHORT).show();
                    else
                        Toast.makeText(getActivity(), throwable.toString(), Toast.LENGTH_SHORT).show();
                    if (progdialog.isShowing())
                        progdialog.dismiss();
                    Timber.w("Error Koneksi valid payment inv:" + throwable.toString());
                }
            });
        } catch (Exception e) {
            Timber.d("httpclient:" + e.getMessage());
        }
    }

    private void sentDataReqToken(final String _tx_id, final String _product_code, final String _bank_code, final String _comm_code, final String _fee,
                                  final String _amount, final String product_name){
        try{

            SecurePreferences sp = CustomSecurePref.getInstance().getmSecurePrefs();

            RequestParams params = MyApiClient.getSignatureWithParams(commId,MyApiClient.LINK_REQ_TOKEN_SGOL,
                    userID,accessKey);
            params.put(WebParams.COMM_CODE, _comm_code);
            params.put(WebParams.TX_ID, _tx_id);
            params.put(WebParams.PRODUCT_CODE, _product_code);
            params.put(WebParams.USER_ID, sp.getString(DefineValue.USERID_PHONE, ""));
            params.put(WebParams.COMM_ID, commId);

            Timber.d("isi params regtoken Sgo+:"+params.toString());

            MyApiClient.sentDataReqTokenSGOL(getActivity(), params, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    try {
                        String code = response.getString(WebParams.ERROR_CODE);
                        Timber.d("response reqtoken :" + response.toString());
                        if (code.equals(WebParams.SUCCESS_CODE)) {
                            if (!isPIN) layout_token.setVisibility(View.VISIBLE);
                            progdialog.dismiss();
                        } else if (code.equals(WebParams.LOGOUT_CODE)) {
                            Timber.d("isi response autologout:" + response.toString());
                            String message = response.getString(WebParams.ERROR_MESSAGE);
                            AlertDialogLogout test = AlertDialogLogout.getInstance();
                            test.showDialoginActivity(getActivity(), message);
                        } else {
                            progdialog.dismiss();
                            if (code.equals("0059")) {
                                showDialogSMS(bankName);
                                if (!isPIN) layout_token.setVisibility(View.VISIBLE);
                            } else {
                                code = response.getString(WebParams.ERROR_CODE) + ":" + response.getString(WebParams.ERROR_MESSAGE);
                                Toast.makeText(getActivity(), code, Toast.LENGTH_LONG).show();
                            }
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    super.onFailure(statusCode, headers, responseString, throwable);
                    failure(throwable);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    failure(throwable);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    failure(throwable);
                }

                private void failure(Throwable throwable) {
                    if (MyApiClient.PROD_FAILURE_FLAG)
                        Toast.makeText(getActivity(), getString(R.string.network_connection_failure_toast), Toast.LENGTH_SHORT).show();
                    else
                        Toast.makeText(getActivity(), throwable.toString(), Toast.LENGTH_SHORT).show();
                    if (progdialog.isShowing())
                        progdialog.dismiss();
                    Timber.w("Error Koneksi reg token sgo input:" + throwable.toString());
                }
            });
        }catch (Exception e){
            Timber.d("httpclient:"+e.getMessage());
        }
    }

    private void sentInsertTrans(String _token){
        try{
            progdialog = DefinedDialog.CreateProgressDialog(getActivity(), "");
            progdialog.show();

            final RequestParams params = MyApiClient.getSignatureWithParams(commId, MyApiClient.LINK_INSERT_TRANS_TOPUP,
                    userID, accessKey);
            params.put(WebParams.TX_ID, txID);
            params.put(WebParams.PRODUCT_CODE, productCode);
            params.put(WebParams.COMM_CODE, commCode);
            params.put(WebParams.COMM_ID, commId);
            params.put(WebParams.MEMBER_ID,sp.getString(DefineValue.MEMBER_ID,""));
            params.put(WebParams.PRODUCT_VALUE, _token);
            params.put(WebParams.USER_ID, userID);

            Timber.d("isi params insertTrxGOL:" + params.toString());

            MyApiClient.sentInsertTransTopup(getActivity(), params, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    try {
                        String code = response.getString(WebParams.ERROR_CODE);
                        Timber.d("isi response insertTrxSGOL:" + response.toString());
                        if (code.equals(WebParams.SUCCESS_CODE)) {
                            getActivity().setResult(MainPage.RESULT_BALANCE);

                            getTrxStatus2(sp.getString(DefineValue.USER_NAME, ""), txID, userID,
                                    bankName, productName, fee, amount);

                        } else if (code.equals(WebParams.LOGOUT_CODE)) {
                            Timber.d("isi response autologout:" + response.toString());
                            String message = response.getString(WebParams.ERROR_MESSAGE);
                            AlertDialogLogout test = AlertDialogLogout.getInstance();
                            test.showDialoginActivity(getActivity(), message);
                        } else {
                            String message = response.getString(WebParams.ERROR_MESSAGE);
                            if(message.equalsIgnoreCase("PIN tidak sesuai") && isPIN){
                                Intent i = new Intent(getActivity(), InsertPIN.class);
                                pin_attempt = pin_attempt-1;
                                if(pin_attempt != -1 && pin_attempt < 2)
                                    i.putExtra(DefineValue.ATTEMPT, pin_attempt);
                                btnProses.setEnabled(true);
                                startActivityForResult(i, MainPage.REQUEST_FINISH);
                            }

                            code = response.getString(WebParams.ERROR_CODE) + ":" + message;
                            Toast.makeText(getActivity(), code, Toast.LENGTH_LONG).show();

                            btnProses.setEnabled(true);
                        }
                        progdialog.dismiss();
                    } catch (JSONException e) {
                        e.printStackTrace();
                        btnProses.setEnabled(true);
                    }

                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    super.onFailure(statusCode, headers, responseString, throwable);
                    failure(throwable);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    failure(throwable);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    failure(throwable);
                }

                private void failure(Throwable throwable) {
                    if (MyApiClient.PROD_FAILURE_FLAG)
                        Toast.makeText(getActivity(), getString(R.string.network_connection_failure_toast), Toast.LENGTH_SHORT).show();
                    else
                        Toast.makeText(getActivity(), throwable.toString(), Toast.LENGTH_SHORT).show();
                    if (progdialog.isShowing())
                        progdialog.dismiss();
                    btnProses.setEnabled(true);
                    Timber.w("Error Koneksi insert trx token:" + throwable.toString());
                }
            });
        }catch (Exception e){
            Timber.d("httpclient:"+e.getMessage());
        }
    }

    private void getTrxStatus2(final String userName, final String txId, final String userId, final String bankName, final String bankProduct,
                               final String fee, final String amount){
        try{
            final ProgressDialog out = DefinedDialog.CreateProgressDialog(getActivity(), getString(R.string.check_status));
            out.show();

            RequestParams params =  MyApiClient.getSignatureWithParams(commId,MyApiClient.LINK_GET_TRX_STATUS2,
                    userID,accessKey);
            params.put(WebParams.TX_ID, txId);
            params.put(WebParams.COMM_ID, commId);
            params.put(WebParams.TYPE, DefineValue.TAGIHAN_TYPE);
            params.put(WebParams.PRIVACY, shareType);
            params.put(WebParams.TX_TYPE, DefineValue.SCASH);
            params.put(WebParams.USER_ID, userId);

            Timber.d("isi params sent get Trx Status 2:" + params.toString());

            MyApiClient.sentGetTRXStatus2(getActivity(), params, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    try {
                        out.dismiss();
                        Timber.d("isi response sent get Trx Status 2:" + response.toString());
                        String code = response.getString(WebParams.ERROR_CODE);
                        if (code.equals(WebParams.SUCCESS_CODE) || code.equals("0003")) {
                            String txstatus = response.getString(WebParams.TX_STATUS);

                            if (tx_multiple.equalsIgnoreCase("N")) {
                                showReportBillerDialog(userName, DateTimeFormat.formatToID(response.optString(WebParams.CREATED, "")), txId, userId, bankName, bankProduct, fee, amount,
                                        txstatus, response.getString(WebParams.TX_REMARK));
                            } else if (tx_multiple.equalsIgnoreCase("Y")) {
                                String data_detail = response.getString(WebParams.DATA_DETAIL);
                                if (!data_detail.equals("")) {
                                    showReportActivity(data_detail, fee);
                                } else {
                                    String msg = "Detail Data kosong";
                                    showDialog(msg);
                                }
                            }
                        } else if (code.equals(WebParams.LOGOUT_CODE)) {
                            Timber.d("isi response autologout:" + response.toString());
                            String message = response.getString(WebParams.ERROR_MESSAGE);
                            AlertDialogLogout test = AlertDialogLogout.getInstance();
                            test.showDialoginActivity(getActivity(), message);
                        } else {
                            String msg = response.getString(WebParams.ERROR_MESSAGE);
                            showDialog(msg);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    super.onFailure(statusCode, headers, responseString, throwable);
                    failure(throwable);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    failure(throwable);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    failure(throwable);
                }

                private void failure(Throwable throwable) {
                    if (MyApiClient.PROD_FAILURE_FLAG)
                        Toast.makeText(getActivity(), getString(R.string.network_connection_failure_toast), Toast.LENGTH_SHORT).show();
                    else
                        Toast.makeText(getActivity(), throwable.toString(), Toast.LENGTH_SHORT).show();

                    if (out.isShowing()) {
                        out.dismiss();
                        showDialog(getString(R.string.network_connection_failure_toast));
                    }
                    Timber.w("Error Koneksi trx status 2:" + throwable.toString());
                }
            });
        }catch (Exception e){
            Timber.d("httpclient:"+ e.getMessage());
        }
    }

    private void showDialogSMS(final String _nama_bank) {
        // Create custom dialog object
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCanceledOnTouchOutside(false);
        // Include dialog.xml file
        dialog.setContentView(R.layout.dialog_notification);

        // set values for custom dialog components - text, image and button
        Button btnDialogOTP = (Button)dialog.findViewById(R.id.btn_dialog_notification_ok);
        TextView Title = (TextView)dialog.findViewById(R.id.title_dialog);
        TextView Message = (TextView)dialog.findViewById(R.id.message_dialog);
        final LevelClass levelClass = new LevelClass(getActivity());
        Message.setVisibility(View.VISIBLE);
        Title.setText(getString(R.string.topup_dialog_not_registered));
        Message.setText(getString(R.string.topup_not_registered, _nama_bank));
        btnDialogOTP.setText(getString(R.string.firstscreen_button_daftar));
        if(levelClass.isLevel1QAC())
            btnDialogOTP.setText(getString(R.string.ok));

        btnDialogOTP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(!levelClass.isLevel1QAC()) {
                    Intent newIntent = new Intent(getActivity(), RegisterSMSBankingActivity.class);
                    newIntent.putExtra(DefineValue.BANK_NAME, _nama_bank);
                    switchActivity(newIntent);
                }
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    private void showDialog(String msg) {
        // Create custom dialog object
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCanceledOnTouchOutside(false);
        // Include dialog.xml file
        dialog.setContentView(R.layout.dialog_notification);

        // set values for custom dialog components - text, image and button
        Button btnDialogOTP = (Button)dialog.findViewById(R.id.btn_dialog_notification_ok);
        TextView Title = (TextView)dialog.findViewById(R.id.title_dialog);
        TextView Message = (TextView)dialog.findViewById(R.id.message_dialog);

        Message.setVisibility(View.VISIBLE);
        Title.setText(getString(R.string.error));
        Message.setText(msg);

        btnDialogOTP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    private void showReportActivity(String data_detail, String fee){
        FragReportTagihanEspay fragment = (FragReportTagihanEspay)getActivity().getSupportFragmentManager().findFragmentByTag("report_fragment");
        if(fragment == null) {
            fragment = FragReportTagihanEspay.newInstance(data_detail, fee,"IB");
            FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
            ft.add(android.R.id.content, fragment, "report_fragment");
            ft.commit();
        }
    }

    private void showReportBillerDialog(String userName, String date,String txId, String userId, String bankName, String bankProduct,
                                        String fee, String amount, String txStatus, String txRemark) {
        Bundle args = new Bundle();
        ReportBillerDialog dialog = new ReportBillerDialog();
        args.putString(DefineValue.USER_NAME, userName);
        args.putString(DefineValue.DATE_TIME, date);
        args.putString(DefineValue.TX_ID, txId);
        args.putString(DefineValue.REPORT_TYPE, DefineValue.MYSCHOOL_TAGIHAN);
        args.putString(DefineValue.USERID_PHONE, userId);
        args.putString(DefineValue.BANK_NAME, bankName);
        args.putString(DefineValue.BANK_PRODUCT, bankProduct);
        args.putString(DefineValue.FEE, MyApiClient.CCY_VALUE + ". " + CurrencyFormat.format(fee));
        args.putString(DefineValue.AMOUNT, MyApiClient.CCY_VALUE + ". " + CurrencyFormat.format(amount));

        double dAmount = Double.valueOf(amount);
        double dFee = Double.valueOf(fee);
        double total_amount = dAmount + dFee;

        args.putString(DefineValue.TOTAL_AMOUNT, MyApiClient.CCY_VALUE + ". " + CurrencyFormat.format(total_amount));

        Boolean txStat = false;
        if (txStatus.equals(DefineValue.SUCCESS)){
            txStat = true;
            args.putString(DefineValue.TRX_MESSAGE, getString(R.string.transaction_success));
        }else if(txStatus.equals(DefineValue.ONRECONCILED)){
            txStat = true;
            args.putString(DefineValue.TRX_MESSAGE, getString(R.string.transaction_pending));
        }else if(txStatus.equals(DefineValue.SUSPECT)){
            args.putString(DefineValue.TRX_MESSAGE, getString(R.string.transaction_suspect));
        }
        else if(!txStatus.equals(DefineValue.FAILED)){
            args.putString(DefineValue.TRX_MESSAGE, getString(R.string.transaction)+" "+txStatus);
        }
        else {
            args.putString(DefineValue.TRX_MESSAGE, getString(R.string.transaction_failed));
        }
        args.putBoolean(DefineValue.TRX_STATUS, txStat);
        if(!txStat)args.putString(DefineValue.TRX_REMARK, txRemark);

        dialog.setArguments(args);
        dialog.setTargetFragment(this, 0);
        dialog.show(getActivity().getSupportFragmentManager(), ReportBillerDialog.TAG);
    }

    private void switchActivity(Intent mIntent){
        if (getActivity() == null)
            return;

        TagihanActivity fca = (TagihanActivity) getActivity();
        fca.switchActivity(mIntent, MainPage.ACTIVITY_RESULT);
    }

    @Override
    public void onOkButton() {
        getActivity().setResult(MainPage.RESULT_BALANCE);
        getActivity().finish();
    }

    @Override
    public void onResume() {
        super.onResume();
        setTitle(getString(R.string.tagihan_ab_title));
    }

    private void setTitle(String _title){
        if (getActivity() == null)
            return;

        TagihanActivity fca = (TagihanActivity) getActivity();
        fca.setTitleFragment(_title);
    }
}
