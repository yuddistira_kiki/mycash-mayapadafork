package com.sgo.mayapada.fragments;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.securepreferences.SecurePreferences;
import com.sgo.mayapada.R;
import com.sgo.mayapada.activities.MainPage;
import com.sgo.mayapada.activities.PayFriendsConfirmTokenActivity;
import com.sgo.mayapada.activities.TopUpActivity;
import com.sgo.mayapada.coreclass.CustomSecurePref;
import com.sgo.mayapada.coreclass.DefineValue;
import com.sgo.mayapada.coreclass.ErrorDefinition;
import com.sgo.mayapada.coreclass.MyApiClient;
import com.sgo.mayapada.coreclass.WebParams;
import com.sgo.mayapada.dialogs.AlertDialogFrag;
import com.sgo.mayapada.dialogs.AlertDialogLogout;
import com.sgo.mayapada.dialogs.DefinedDialog;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;
import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

import cn.bingoogolapple.qrcode.core.QRCodeView;
import cn.bingoogolapple.qrcode.zbar.ZBarView;
import timber.log.Timber;

/**
 * Created by Lenovo Thinkpad on 9/21/2016.
 */
public class ScanQRFragment extends Fragment implements QRCodeView.Delegate{
    private static final String TAG = ScanQRFragment.class.getSimpleName();
    private String jenis;
    private Fragment newFragment;

    private static final int REQUEST_CODE_QRCODE_PERMISSIONS = 1;
    private QRCodeView mQRCodeView;
    private SecurePreferences sp;
    private String _result;
    private String[] resultArray;
    String[] transresult;
    private String authType;
    private List<String> listName;
    private ProgressDialog progdialog;
    private boolean isNotification = false;
    private String _memberId;
    private String userID;
    private String accessKey;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = getActivity().getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }

        return inflater.inflate(R.layout.frag_scanqr, container, false);
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        Bundle bundle = this.getArguments();
        jenis = bundle.getString("jenis");

        Log.d("denny_test", PayByQR.type);
        mQRCodeView = (ZBarView) getActivity().findViewById(R.id.zbarview);
        mQRCodeView.setDelegate(this);

        mQRCodeView.startSpot();
    }

    @Override
    public void onStart() {
        super.onStart();
        mQRCodeView.startCamera();
    }

    @Override
    public void onResume() {
        super.onResume();
        mQRCodeView.setDelegate(this);
        mQRCodeView.startSpot();
        mQRCodeView.startCamera();
    }

//    @Override
//    public void onStop() {
//        mQRCodeView.stopCamera();
//        super.onStop();
//    }

    private void vibrate() {
        Vibrator vibrator = (Vibrator) getActivity().getSystemService(getActivity().VIBRATOR_SERVICE);
        vibrator.vibrate(200);
    }

    @Override
    public void onScanQRCodeSuccess(String result) {
        Log.d(TAG, "result:" + result);

        sp = CustomSecurePref.getInstance().getmSecurePrefs();

        if(jenis.equalsIgnoreCase("paybyqr"))
        {
            Cipher decryptCipher;
            String keyword = "SGOPAYBYQR";
//            String userID = sp.getString(DefineValue.USERID_PHONE,"");
            String userID = "6281382555096";
            String AppID = "EDIK2";
            String webServices = "ServiceCoupon";
            String comm_id = MyApiClient.COMM_ID;

            String keyPassEncrypt = keyword + AppID + webServices + comm_id + userID;

            try {
                decryptCipher = Cipher.getInstance("AES");
                decryptCipher.init(Cipher.DECRYPT_MODE, generateMySQLAESKey(keyPassEncrypt, "UTF-8"));
                _result = new String(decryptCipher.doFinal(Hex.decodeHex(result.toCharArray())));
                resultArray = _result.split(";");
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            } catch (NoSuchPaddingException e) {
                e.printStackTrace();
            } catch (BadPaddingException e) {
                e.printStackTrace();
            } catch (DecoderException e) {
                e.printStackTrace();
            } catch (IllegalBlockSizeException e) {
                e.printStackTrace();
            } catch (InvalidKeyException e) {
                e.printStackTrace();
            }

            try {
                if (resultArray[0].equalsIgnoreCase("1")) //personal
                {
                    newFragment = new FragConfirmation();
                    Bundle args = new Bundle();
                    args.putString("type", resultArray[0]);
                    args.putString("userid", resultArray[1]);
                    args.putString("merchantid", resultArray[2]);
                    args.putString("merchantname", resultArray[3]);
                    args.putString("commid", resultArray[4]);
                    args.putString("appid", resultArray[5]);
                    newFragment.setArguments(args);
                    switchFragment(newFragment, getString(R.string.toolbar_title_paybyqr));
                } else if (resultArray[0].equalsIgnoreCase("2")) //corporate
                {
                    Log.d("item_denny", resultArray[6]);
                    newFragment = new FragConfirmation();
                    Bundle args = new Bundle();
                    args.putString("type", resultArray[0]);
                    args.putString("expired", resultArray[1]);
                    args.putString("userid", resultArray[2]);
                    args.putString("merchantid", resultArray[3]);
                    args.putString("merchantname", resultArray[4]);
                    args.putString("total", resultArray[5]);
                    args.putString("item", resultArray[6]);
                    args.putString("price", resultArray[7]);
                    args.putString("fee", resultArray[8]);
                    args.putString("trxid", resultArray[9]);
                    args.putString("commid", resultArray[10]);
                    args.putString("appid", resultArray[11]);
                    newFragment.setArguments(args);
                    switchFragment(newFragment, getString(R.string.toolbar_title_paybyqr));
                } else if (resultArray[0].equalsIgnoreCase("3")) //vending
                {
                    newFragment = new FragConfirmation();
                    Bundle args = new Bundle();
                    args.putString("type", resultArray[0]);
                    args.putString("userid", resultArray[1]);
                    args.putString("merchantid", resultArray[2]);
                    args.putString("merchantname", resultArray[3]);
                    args.putString("total", resultArray[4]);
                    args.putString("price", resultArray[5]);
                    args.putString("fee", resultArray[6]);
                    args.putString("commid", resultArray[7]);
                    args.putString("appid", resultArray[8]);
                    newFragment.setArguments(args);
                    switchFragment(newFragment, getString(R.string.toolbar_title_paybyqr));
                } else {
                    Toast.makeText(getActivity(), result, Toast.LENGTH_SHORT).show();
                }
            }
            catch (Exception ex)
            {
                keyPassEncrypt = "AskAndTrans";

                try {
                    decryptCipher = Cipher.getInstance("AES");
                    decryptCipher.init(Cipher.DECRYPT_MODE, generateMySQLAESKey(keyPassEncrypt, "UTF-8"));
                    _result = new String(decryptCipher.doFinal(Hex.decodeHex(result.toCharArray())));
                    resultArray = _result.split(";");
                } catch (NoSuchAlgorithmException e) {
                    e.printStackTrace();
                } catch (NoSuchPaddingException e) {
                    e.printStackTrace();
                } catch (BadPaddingException e) {
                    e.printStackTrace();
                } catch (DecoderException e) {
                    e.printStackTrace();
                } catch (IllegalBlockSizeException e) {
                    e.printStackTrace();
                } catch (InvalidKeyException e) {
                    e.printStackTrace();
                }

                authType = sp.getString(DefineValue.AUTHENTICATION_TYPE, "");

                try{
                    String amount = resultArray[0]; //amount
                    String message = resultArray[1]; // message
                    Boolean recipientValidation = true;
                    ArrayList<TempObjectData> mTempObjectDataList = new ArrayList<>();

                    String finalName;
                    listName = new ArrayList<>();
                    finalName = resultArray[3];
                    listName.add(finalName);
                    mTempObjectDataList.add(new TempObjectData(resultArray[2], DefineValue.IDR, amount, finalName));

                    if (recipientValidation) {
                        final GsonBuilder gsonBuilder = new GsonBuilder();
                        gsonBuilder.setPrettyPrinting();
                        final Gson gson = gsonBuilder.create();
                        String testJson = gson.toJson(mTempObjectDataList);
                        String nameJson = gson.toJson(listName);
                        //  Timber.v("isi json build", testJson + numberJson);
                        sentData(message, testJson, nameJson);
                    }
                }
                catch (Exception exc)
                {
                    Toast.makeText(getActivity(), "Gagal Membaca QR.. QR Tidak Sesuai", Toast.LENGTH_SHORT).show();
                }
            }
        }
        else // scan qr transfer
        {
            Cipher decryptCipher;
            String keyPassEncrypt = "AskAndTrans";
            _result = "";
            resultArray = null;

            try {
                decryptCipher = Cipher.getInstance("AES");
                decryptCipher.init(Cipher.DECRYPT_MODE, generateMySQLAESKey(keyPassEncrypt, "UTF-8"));
                _result = new String(decryptCipher.doFinal(Hex.decodeHex(result.toCharArray())));
                resultArray = _result.split(";");
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            } catch (NoSuchPaddingException e) {
                e.printStackTrace();
            } catch (BadPaddingException e) {
                e.printStackTrace();
            } catch (DecoderException e) {
                e.printStackTrace();
            } catch (IllegalBlockSizeException e) {
                e.printStackTrace();
            } catch (InvalidKeyException e) {
                e.printStackTrace();
            }

            authType = sp.getString(DefineValue.AUTHENTICATION_TYPE, "");

            try{
                String amount = resultArray[0]; //amount
                String message = resultArray[1]; // message
                Boolean recipientValidation = true;
                ArrayList<TempObjectData> mTempObjectDataList = new ArrayList<>();

                String finalName;
                listName = new ArrayList<>();
                finalName = resultArray[3];
                listName.add(finalName);
                mTempObjectDataList.add(new TempObjectData(resultArray[2], DefineValue.IDR, amount, finalName));

                if (recipientValidation) {
                    final GsonBuilder gsonBuilder = new GsonBuilder();
                    gsonBuilder.setPrettyPrinting();
                    final Gson gson = gsonBuilder.create();
                    String testJson = gson.toJson(mTempObjectDataList);
                    String nameJson = gson.toJson(listName);
                    //  Timber.v("isi json build", testJson + numberJson);
                    sentData(message, testJson, nameJson);
                }
            }
            catch (Exception ex) {
                String keyword = "SGOPAYBYQR";
//            String userID = sp.getString(DefineValue.USERID_PHONE,"");
                String userID = "6281382555096";
                String AppID = "EDIK2";
                String webServices = "ServiceCoupon";
                String comm_id = MyApiClient.COMM_ID;

                keyPassEncrypt = keyword + AppID + webServices + comm_id + userID;

                try {
                    decryptCipher = Cipher.getInstance("AES");
                    decryptCipher.init(Cipher.DECRYPT_MODE, generateMySQLAESKey(keyPassEncrypt, "UTF-8"));
                    _result = new String(decryptCipher.doFinal(Hex.decodeHex(result.toCharArray())));
                    resultArray = _result.split(";");
                } catch (NoSuchAlgorithmException e) {
                    e.printStackTrace();
                } catch (NoSuchPaddingException e) {
                    e.printStackTrace();
                } catch (BadPaddingException e) {
                    e.printStackTrace();
                } catch (DecoderException e) {
                    e.printStackTrace();
                } catch (IllegalBlockSizeException e) {
                    e.printStackTrace();
                } catch (InvalidKeyException e) {
                    e.printStackTrace();
                }

                try {
                    if (resultArray[0].equalsIgnoreCase("1")) //personal
                    {
                        newFragment = new FragConfirmation();
                        Bundle args = new Bundle();
                        args.putString("type", resultArray[0]);
                        args.putString("userid", resultArray[1]);
                        args.putString("merchantid", resultArray[2]);
                        args.putString("merchantname", resultArray[3]);
                        args.putString("commid", resultArray[4]);
                        args.putString("appid", resultArray[5]);
                        newFragment.setArguments(args);
                        switchFragment(newFragment, getString(R.string.toolbar_title_paybyqr));
                    } else if (resultArray[0].equalsIgnoreCase("2")) //corporate
                    {
                        Log.d("item_denny", resultArray[6]);
                        newFragment = new FragConfirmation();
                        Bundle args = new Bundle();
                        args.putString("type", resultArray[0]);
                        args.putString("expired", resultArray[1]);
                        args.putString("userid", resultArray[2]);
                        args.putString("merchantid", resultArray[3]);
                        args.putString("merchantname", resultArray[4]);
                        args.putString("total", resultArray[5]);
                        args.putString("item", resultArray[6]);
                        args.putString("price", resultArray[7]);
                        args.putString("fee", resultArray[8]);
                        args.putString("trxid", resultArray[9]);
                        args.putString("commid", resultArray[10]);
                        args.putString("appid", resultArray[11]);
                        newFragment.setArguments(args);
                        switchFragment(newFragment, getString(R.string.toolbar_title_paybyqr));
                    } else if (resultArray[0].equalsIgnoreCase("3")) //vending
                    {
                        newFragment = new FragConfirmation();
                        Bundle args = new Bundle();
                        args.putString("type", resultArray[0]);
                        args.putString("userid", resultArray[1]);
                        args.putString("merchantid", resultArray[2]);
                        args.putString("merchantname", resultArray[3]);
                        args.putString("total", resultArray[4]);
                        args.putString("price", resultArray[5]);
                        args.putString("fee", resultArray[6]);
                        args.putString("commid", resultArray[7]);
                        args.putString("appid", resultArray[8]);
                        newFragment.setArguments(args);
                        switchFragment(newFragment, getString(R.string.toolbar_title_paybyqr));
                    } else {
                        Toast.makeText(getActivity(), result, Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception exc)
                {
                    Toast.makeText(getActivity(), "Gagal Membaca QR.. QR Tidak Sesuai", Toast.LENGTH_SHORT).show();
                }
            }
        }

        vibrate();
        mQRCodeView.startSpot();

    }

    private void sentData(String _message, String _data, final String _nameJson){
        try{
            progdialog = DefinedDialog.CreateProgressDialog(getActivity(), "");
            progdialog.show();

            sp = CustomSecurePref.getInstance().getmSecurePrefs();


            userID = sp.getString(DefineValue.USERID_PHONE, "");
            accessKey = sp.getString(DefineValue.ACCESS_KEY, "");
            _memberId = sp.getString(DefineValue.MEMBER_ID,"");


            RequestParams params;
            if(isNotification) {
                params = MyApiClient.getSignatureWithParams(MyApiClient.COMM_ID,MyApiClient.LINK_REQ_TOKEN_P2P_NOTIF,
                        userID,accessKey);
            }
            else
                params = MyApiClient.getSignatureWithParams(MyApiClient.COMM_ID,MyApiClient.LINK_REQ_TOKEN_P2P,
                        userID,accessKey);

            params.put(WebParams.MEMBER_ID, _memberId);
            params.put(WebParams.MEMBER_REMARK, _message);
            params.put(WebParams.DATA, _data);
            params.put(WebParams.COMM_ID, MyApiClient.COMM_ID);
            params.put(WebParams.USER_ID, userID);
            params.put(WebParams.PRIVACY, 1);
//            if(isNotification){
//                params.put(WebParams.REQUEST_ID, bundle.getString(DefineValue.REQUEST_ID));
//                params.put(WebParams.TRX_ID, bundle.getString(DefineValue.TRX));
//            }

            Timber.d("isi params sent req token p2p:"+params.toString());

            JsonHttpResponseHandler myHandler = new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    progdialog.dismiss();

                    try {
                        String code = response.getString(WebParams.ERROR_CODE);
                        if (code.equals(WebParams.SUCCESS_CODE)) {
                            Timber.d("isi response req token p2p:"+response.toString());
                            JSONArray mArrayData = new JSONArray(response.getString(WebParams.DATA_TRANSFER));
                            int isFailed=0 ;
                            String msg = "";
                            for(int i = 0 ; i < mArrayData.length() ; i++) {
                                if(mArrayData.getJSONObject(i).getString(WebParams.MEMBER_STATUS).equals(DefineValue.FAILED)){
                                    isFailed++ ;
                                    msg = mArrayData.getJSONObject(i).getString(WebParams.MEMBER_REMARK);
                                }
                            }
                            if(isFailed != mArrayData.length()){
                                String dataTransfer = response.getString(WebParams.DATA_TRANSFER);
                                showDialog(dataTransfer, _nameJson, response.getString(WebParams.MESSAGE),
                                        response.getString(WebParams.DATA_MAPPER),response.getString(WebParams.PINCHAL_1),
                                        response.getString(WebParams.PINCHAL_2));
                            }
                            else Toast.makeText(getActivity(),msg,Toast.LENGTH_SHORT).show();
                        }
                        else if(code.equals(WebParams.LOGOUT_CODE)){
                            Timber.d("isi response autologout:"+response.toString());
                            String message = response.getString(WebParams.ERROR_MESSAGE);
                            AlertDialogLogout test = AlertDialogLogout.getInstance();
                            test.showDialoginMain(getActivity(),message);
                        }
                        else if(code.equals(ErrorDefinition.WRONG_PIN_P2P)){
                            code = response.getString(WebParams.ERROR_MESSAGE);
                            showDialogError(code);
                        }
                        else {
                            Timber.d("isi error req token p2p:"+response.toString());
                            String code_msg = response.getString(WebParams.ERROR_MESSAGE);
                            if(code.equals(ErrorDefinition.ERROR_CODE_DUPLICATED_RECIPIENT)){
//                                phoneRetv.requestFocus();
//                                phoneRetv.setError(getString(R.string.payfriends_recipients_duplicate_validation));
                            }
                            else if(code.equals(ErrorDefinition.ERROR_CODE_LESS_BALANCE)){

                                String message_dialog = "\""+code_msg+"\" \n"+getString(R.string.dialog_message_less_balance);

                                AlertDialogFrag dialog_frag = AlertDialogFrag.newInstance(getString(R.string.dialog_title_less_balance),
                                        message_dialog,getString(R.string.ok),getString(R.string.cancel),false);
                                dialog_frag.setOkListener(new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        Intent mI = new Intent(getActivity(),TopUpActivity.class);
                                        mI.putExtra(DefineValue.IS_ACTIVITY_FULL,true);
                                        getActivity().startActivityForResult(mI,MainPage.ACTIVITY_RESULT);
                                    }
                                });
                                dialog_frag.setTargetFragment(ScanQRFragment.this, 0);
                                dialog_frag.show(getActivity().getSupportFragmentManager(), AlertDialogFrag.TAG);
                            }
                            else {
                                Toast.makeText(getActivity(), code_msg, Toast.LENGTH_LONG).show();
                            }

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    super.onFailure(statusCode, headers, responseString, throwable);
                    failure(throwable);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    failure(throwable);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    failure(throwable);
                }

                private void failure(Throwable throwable){
                    if(MyApiClient.PROD_FAILURE_FLAG)
                        Toast.makeText(getActivity(), getString(R.string.network_connection_failure_toast), Toast.LENGTH_SHORT).show();
                    else
                        Toast.makeText(getActivity(), throwable.toString(), Toast.LENGTH_SHORT).show();
                    if (progdialog.isShowing())
                        progdialog.dismiss();

                    Timber.w("Error Koneksi req token p2p:"+throwable.toString());
                }
            };

            if(isNotification) {
                Timber.d("masuk ke reqTokenP2P notif");
                MyApiClient.sentReqTokenP2PNotif(getActivity(),params, myHandler);
            }
            else
                MyApiClient.sentReqTokenP2P(getActivity(),params, myHandler );
        }catch (Exception e){
            Timber.d("httpclient:"+e.getMessage());
        }
    }

    private void switchFragment(Fragment i, String name){
        if (getActivity() == null)
            return;

        MainPage fca = (MainPage) getActivity();
        fca.switchContent(i,name);
    }

    @Override
    public void onScanQRCodeOpenCameraError() {
        Log.e(TAG, "Camera Error");
    }

    private static SecretKeySpec generateMySQLAESKey(final String key, final String encoding) {
        try {
            final byte[] finalKey = new byte[16];
            int i = 0;
            for(byte b : key.getBytes(encoding))
                finalKey[i++%16] ^= b;
            return new SecretKeySpec(finalKey, "AES");
        } catch(UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    private static String md5(String p) throws NoSuchAlgorithmException{

        MessageDigest digest = MessageDigest.getInstance("MD5");
        digest.update(p.getBytes());
        byte messageDigest[] = digest.digest();

        StringBuffer hexString = new StringBuffer();
        for (int i = 0; i < messageDigest.length; i++) {
            String h = Integer.toHexString(0xFF & messageDigest[i]);
            while (h.length() < 2)
                h = "0" + h;
            hexString.append(h);
        }
        return hexString.toString();
    }

    private class TempObjectData{

        private String member_code_to;
        private String ccy_id;
        private String amount;
        private String name;

        public TempObjectData(String _member_code_to, String _ccy_id, String _amount, String _name){
            this.setMember_code_to(_member_code_to);
            this.setCcy_id(_ccy_id);
            this.setAmount(_amount);
            this.setName(_name);
        }

        public String getMember_code_to() {
            return member_code_to;
        }

        public void setMember_code_to(String member_code_to) {
            this.member_code_to = member_code_to;
        }

        public String getCcy_id() {
            return ccy_id;
        }

        public void setCcy_id(String ccy_id) {
            this.ccy_id = ccy_id;
        }

        public String getAmount() {
            return amount;
        }

        public void setAmount(String amount) {
            this.amount = amount;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }

    public boolean isAlpha(String name) {
        Pattern p = Pattern.compile("[a-zA-Z]");
        Matcher m = p.matcher(name);
        return m.find();
    }

    private void showDialogError(String message){
        Dialog mdialog = DefinedDialog.MessageDialog(getActivity(), getString(R.string.blocked_pin_title), message,
                new DefinedDialog.DialogButtonListener() {
                    @Override
                    public void onClickButton(View v, boolean isLongClick) {

                    }
                });
        mdialog.show();
    }

    private void showDialog(final String _data_transfer, final String _nameJson, final String _message,
                            final String _data_mapper, final String _pinchal_1, final String _pinchal_2) {
//        phoneRetv.setText(null);
        if(authType.equalsIgnoreCase("OTP")) {
            // Create custom dialog object
            final Dialog dialog = new Dialog(getActivity());
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCanceledOnTouchOutside(false);
            // Include dialog.xml file
            dialog.setContentView(R.layout.dialog_notification);

            // set values for custom dialog components - text, image and button
            Button btnDialogOTP = (Button) dialog.findViewById(R.id.btn_dialog_notification_ok);
            TextView Title = (TextView) dialog.findViewById(R.id.title_dialog);
            TextView Message = (TextView) dialog.findViewById(R.id.message_dialog);

            Message.setVisibility(View.VISIBLE);
            Title.setText(getString(R.string.payfriends_dialog_validation_title));
            Message.setText(getString(R.string.appname)+" "+getString(R.string.dialog_token_message_sms));

            btnDialogOTP.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                    Intent i = new Intent(getActivity(), PayFriendsConfirmTokenActivity.class);
                    i.putExtra(WebParams.DATA_TRANSFER, _data_transfer);
                    i.putExtra(WebParams.DATA, _nameJson);
                    i.putExtra(WebParams.MESSAGE, _message);
                    i.putExtra(DefineValue.TRANSACTION_TYPE, isNotification);
                    i.putExtra(WebParams.DATA_MAPPER, _data_mapper);
                    i.putExtra(DefineValue.PINCHAL_1, _pinchal_1);
                    i.putExtra(DefineValue.PINCHAL_2, _pinchal_2);
                    switchActivity(i);
                }
            });

            dialog.show();
        }
        else if(authType.equalsIgnoreCase("PIN")) {
            Intent i = new Intent(getActivity(), PayFriendsConfirmTokenActivity.class);
            i.putExtra(WebParams.DATA_TRANSFER, _data_transfer);
            i.putExtra(WebParams.DATA, _nameJson);
            i.putExtra(WebParams.MESSAGE, _message);
            i.putExtra(DefineValue.TRANSACTION_TYPE, isNotification);
            i.putExtra(WebParams.DATA_MAPPER, _data_mapper);
            i.putExtra(DefineValue.PINCHAL_1, _pinchal_1);
            i.putExtra(DefineValue.PINCHAL_2, _pinchal_2);
            switchActivity(i);
        }
    }

    private void switchActivity(Intent mIntent){
        if (getActivity() == null)
            return;

        /*MainPage fca = (MainPage) getActivity();
        fca.switchActivity(mIntent,MainPage.ACTIVITY_RESULT);*/
        getActivity().startActivityForResult(mIntent,MainPage.REQUEST_FINISH);
    }
}
