package com.sgo.mayapada.fragments;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.sgo.mayapada.Beans.KantinStudentDetailModel;
import com.sgo.mayapada.R;
import com.sgo.mayapada.activities.KantinActivity;
import com.sgo.mayapada.adapter.KantinStudentDetailAdapter;
import com.sgo.mayapada.coreclass.DefineValue;
import com.sgo.mayapada.coreclass.MyApiClient;
import com.sgo.mayapada.coreclass.WebParams;
import com.sgo.mayapada.dialogs.CouponQrCodeDialog;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

/**
 * Created by thinkpad on 8/3/2016.
 */
public class FragKantinStudentDetail extends Fragment {
    private View v;
    private View kadaluarsa_layout;
    private ListView lvOrder;
    private ImageButton btnQrcode;
    private TextView txtCoupon;
    private TextView txtDateTimeKadaluarsa;
    private TextView txtTxDatetime;
    private Button btnCheckStatus;
    private Button btnBack;
    private KantinStudentDetailAdapter adapter;
    private ArrayList<KantinStudentDetailModel> listOrder;
    private String coupon_code;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        v = inflater.inflate(R.layout.frag_kantin_student_detail, container, false);
        return v;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        Bundle bundle = getArguments();
        coupon_code = bundle.getString(DefineValue.COUPON_CODE);
        String tx_datetime = bundle.getString(DefineValue.TX_DATETIME);
        String item = bundle.getString(DefineValue.LIST_ORDER);
        String status = bundle.getString(DefineValue.STATUS);
        String tx_kadaluarsa = bundle.getString(DefineValue.KADALUARSA);
        listOrder = new ArrayList<>();

        try {
            JSONArray array = new JSONArray(item);
            for(int i = 0 ; i < array.length() ; i++) {
                KantinStudentDetailModel model = new KantinStudentDetailModel();
                model.setName(array.getJSONObject(i).getString(WebParams.ITEM_NAME));
                model.setCount(array.getJSONObject(i).getString(WebParams.QTY));
                model.setAmount(array.getJSONObject(i).getString(WebParams.TOTAL_AMOUNT_THIS_ITEM));
                listOrder.add(model);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        btnQrcode = (ImageButton) v.findViewById(R.id.qrcode_button);
        kadaluarsa_layout = v.findViewById(R.id.kadaluarsa_layout);
        txtCoupon = (TextView) v.findViewById(R.id.coupon_code_value);
        txtDateTimeKadaluarsa = (TextView) v.findViewById(R.id.datetime_kl_value);
        txtTxDatetime = (TextView) v.findViewById(R.id.datetime_value);
        btnCheckStatus = (Button) v.findViewById(R.id.kantindetail_check_status_button);
        btnBack = (Button) v.findViewById(R.id.kantindetail_back_button);
        txtCoupon.setText(coupon_code);
        if (status.equalsIgnoreCase("LN")) {
            kadaluarsa_layout.setVisibility(View.GONE);
        }
        else {
            kadaluarsa_layout.setVisibility(View.VISIBLE);
            txtDateTimeKadaluarsa.setText(tx_kadaluarsa);
        }
        txtTxDatetime.setText(tx_datetime);
        lvOrder = (ListView) v.findViewById(R.id.lv_order);
        adapter = new KantinStudentDetailAdapter(getActivity(), listOrder, MyApiClient.CCY_VALUE);
        lvOrder.setAdapter(adapter);

        btnBack.setOnClickListener(backListener);
        btnQrcode.setOnClickListener(qrcodeListener);
    }

    private Button.OnClickListener backListener = new Button.OnClickListener() {
        @Override
        public void onClick(View view) {
            getActivity().finish();
        }
    };

    private ImageButton.OnClickListener qrcodeListener = new ImageButton.OnClickListener() {
        @Override
        public void onClick(View view) {
            DialogFragment via = CouponQrCodeDialog.CreateInstance(coupon_code);
            FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
            via.show(ft, CouponQrCodeDialog.TAG);
        }
    };

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if(getActivity().getSupportFragmentManager().getBackStackEntryCount() > 0)
                    getActivity().getSupportFragmentManager().popBackStack();
                else
                    getActivity().finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume() {
        super.onResume();
        setTitle(getString(R.string.kantin_detail_title));
    }

    private void setTitle(String _title){
        if (getActivity() == null)
            return;

        KantinActivity fca = (KantinActivity) getActivity();
        fca.setTitleFragment(_title);
    }
}
