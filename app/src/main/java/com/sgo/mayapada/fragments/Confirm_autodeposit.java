package com.sgo.mayapada.fragments;/*
  Created by Administrator on 8/11/2016.
 */

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.securepreferences.SecurePreferences;
import com.sgo.mayapada.R;
import com.sgo.mayapada.activities.InsertPIN;
import com.sgo.mayapada.activities.MainPage;
import com.sgo.mayapada.activities.TabunganActivity;
import com.sgo.mayapada.coreclass.CurrencyFormat;
import com.sgo.mayapada.coreclass.CustomSecurePref;
import com.sgo.mayapada.coreclass.DefineValue;
import com.sgo.mayapada.coreclass.ErrorDefinition;
import com.sgo.mayapada.coreclass.MyApiClient;
import com.sgo.mayapada.coreclass.WebParams;
import com.sgo.mayapada.dialogs.AlertDialogLogout;
import com.sgo.mayapada.dialogs.DefinedDialog;
import com.sgo.mayapada.interfaces.OnLoadDataListener;
import com.sgo.mayapada.loader.UtilsLoader;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import timber.log.Timber;

public class Confirm_autodeposit extends Fragment {

    public final static String TAG = "com.sgo.mayapada.fragments.Confirm_autodeposit";

    private View v;
    private SecurePreferences sp;
    private Button confirm;
    private Button cancel;
    private TextView title;
    private TextView tv_time;
    private TextView tv_period;
    private TextView tv_withdrawal;
    private TextView tv_balance;
    private TextView tv_title_balance;
    private int period;
    private String withdrawal_type;
    private String date_from;
    private String date_to;
    String status;
    private String amount;
    private String auto_id;
    private String authType;
    private String accessKey;
    private String memberID;
    private int attempt=-1;
    private String userID;
    private ProgressDialog progressDialog;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        v = inflater.inflate(R.layout.confirm_autodeposit_layout, container, false);
        return v;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        sp = CustomSecurePref.getInstance().getmSecurePrefs();
        userID = sp.getString(DefineValue.USERID_PHONE,"");
        accessKey = sp.getString(DefineValue.ACCESS_KEY,"");
        memberID = sp.getString(DefineValue.MEMBER_ID,"");
        authType = sp.getString(DefineValue.AUTHENTICATION_TYPE,"");

        progressDialog = DefinedDialog.CreateProgressDialog(getContext(),"");
        progressDialog.dismiss();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        confirm = (Button) v.findViewById(R.id.confirm);
        cancel = (Button) v.findViewById(R.id.cancel);
        title = (TextView) v.findViewById(R.id.title_detail);
        tv_period = (TextView) v.findViewById(R.id.period_detail);
        tv_time = (TextView) v.findViewById(R.id.time_detail);
        tv_withdrawal = (TextView) v.findViewById(R.id.withdrawal_detail);
        tv_balance = (TextView) v.findViewById(R.id.balance_detail);
        tv_title_balance = (TextView) v.findViewById(R.id.title_balance);
        Bundle bundle = getArguments();

        date_from = bundle.getString(DefineValue.DATE_FROM);
        date_to = bundle.getString(DefineValue.DATE_TO);
        period = bundle.getInt(DefineValue.REPETITION, 0);
        withdrawal_type = bundle.getString(DefineValue.SAVING_TYPE);
        amount = bundle.getString(DefineValue.VALUE);
        auto_id = bundle.getString(DefineValue.AUTO_ID);

        tv_time.setText(period+ " hari");
        tv_period.setText(bundle.getString(DefineValue.DATEFROM_DISPLAY) + " s/d " + bundle.getString(DefineValue.DATETO_DISPLAY));

        if(withdrawal_type.equals(DefineValue.BALANCE_REMAIN)){
            tv_title_balance.setText(getString(R.string.minimum_btn_text));
            tv_withdrawal.setText(getString(R.string.minimum_btn_text));
        }else if (withdrawal_type.equals(DefineValue.FIXED_AMOUNT)){
            tv_title_balance.setText(getString(R.string.nominal_btn_text));
            tv_withdrawal.setText(getString(R.string.nominal_btn_text));
        }else {
            tv_title_balance.setText(getString(R.string.percentage_btn_text));
            tv_withdrawal.setText(getString(R.string.percentage_btn_text));
        }

        if(withdrawal_type.equals(DefineValue.PERCENTAGE))
            tv_balance.setText(amount + " %");
        else
            tv_balance.setText(MyApiClient.CCY_VALUE+". "+CurrencyFormat.format(amount));

        confirm.setOnClickListener(confirm_listener);
        cancel.setOnClickListener(cancel_listener);

        if(authType.equalsIgnoreCase(DefineValue.AUTH_TYPE_PIN)) {

            new UtilsLoader(getActivity(),sp).getFailedPIN(userID,new OnLoadDataListener() { // get pin attempt
                @Override
                public void onSuccess(Object deData) {
                    attempt = (int) deData;
                }

                @Override
                public void onFail(Bundle message) {

                }

                @Override
                public void onFailure(String message) {

                }
            });
        }
    }

    private Button.OnClickListener confirm_listener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            insertPIN();
        }
    };

    private Button.OnClickListener cancel_listener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
           getFragmentManager().popBackStack();
        }
    };

    private void insertPIN(){
        Intent i = new Intent(getActivity(), InsertPIN.class);
        if(attempt != -1 && attempt < 2)
            i.putExtra(DefineValue.ATTEMPT,attempt);
        startActivityForResult(i, MainPage.REQUEST_FINISH);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == MainPage.REQUEST_FINISH) {
            switch (resultCode) {
                case InsertPIN.RESULT_PIN_VALUE:
                    String value_pin = data.getStringExtra(DefineValue.PIN_VALUE);
                    confirmTokenAutoSaving(value_pin);
                    break;
            }
        }
    }

    private void switchFragment(android.support.v4.app.Fragment i, String name, Boolean isBackstack, String tag){
        if (getActivity() == null)
            return;

        TabunganActivity fca = (TabunganActivity ) getActivity();
        fca.switchContent(i,name,isBackstack,tag);
    }


    private void confirmTokenAutoSaving(String value_pin) {

        progressDialog.show();
        RequestParams params = MyApiClient.getSignatureWithParams(MyApiClient.COMM_ID, MyApiClient.LINK_CONFIRMTOKEN_AUTOSAVING,
                userID, accessKey);
        params.put(WebParams.COMM_ID, MyApiClient.COMM_ID);
        params.put(WebParams.MEMBER_ID, memberID);
        params.put(WebParams.USER_ID, userID);
        params.put(WebParams.AUTO_ID, auto_id);
        params.put(WebParams.TOKEN_ID, value_pin);

        Timber.d("parameter confirmAutoSaving: " + params.toString());
        MyApiClient.confirmtokenAutoSaving(getActivity(), params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                try {
                    Timber.d("respose confirmAutoSaving: " + response.toString());
                    String code = response.getString(WebParams.ERROR_CODE);
                    if (code.equals(WebParams.SUCCESS_CODE)) {
                        Bundle bundle = new Bundle();
                        Fragment frag;
                        frag = new On_Going_autodeposit();
                        bundle.putString(DefineValue.DATE_FROM, date_from);
                        bundle.putString(DefineValue.DATE_TO, date_to);
                        bundle.putString(DefineValue.AMOUNT_TYPE, withdrawal_type);
                        bundle.putInt(DefineValue.REPETITION, period);
                        bundle.putString(DefineValue.STATUS, DefineValue.ACTIVE);
                        bundle.putString(DefineValue.AUTO_VALUE, amount);
                        bundle.putString(DefineValue.AUTO_ID, auto_id);
                        frag.setArguments(bundle);
                        getFragmentManager().popBackStack(TAG, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                        getActivity().getSupportFragmentManager().popBackStack(null,FragmentManager.POP_BACK_STACK_INCLUSIVE);
                        switchFragment(frag,"",false,On_Going_autodeposit.TAG);
                    } else if (code.equals(WebParams.LOGOUT_CODE)) {
                        Timber.d("isi response autologout:" + response.toString());
                        String message = response.getString(WebParams.ERROR_MESSAGE);
                        AlertDialogLogout test = AlertDialogLogout.getInstance();
                        test.showDialoginActivity(getActivity(), message);
                    } else if(code.equals(ErrorDefinition.WRONG_PIN_P2P)){
                        code = response.getString(WebParams.ERROR_MESSAGE);
                        Dialog dialognya = DefinedDialog.MessageDialog(getActivity(), getString(R.string.blocked_pin_title),
                                code, new DefinedDialog.DialogButtonListener() {
                                    @Override
                                    public void onClickButton(View v, boolean isLongClick) {
                                        getActivity().finish();
                                    }
                                }) ;
                        dialognya.show();
                        sp.edit().putBoolean(DefineValue.PIN_BLOCKED,true).apply();
                    }else {
                        Timber.d("Error confirmAutoSaving:" + response.toString());
                        code = response.getString(WebParams.ERROR_MESSAGE);
                        Toast.makeText(getContext(), code, Toast.LENGTH_LONG).show();
                        if(authType.equalsIgnoreCase(DefineValue.AUTH_TYPE_PIN)) {
                            attempt = attempt-1;
                            insertPIN();
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                super.onFailure(statusCode, headers, responseString, throwable);
                failure(throwable);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                failure(throwable);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                failure(throwable);
            }

            private void failure(Throwable throwable) {
                if (getActivity() != null && !getActivity().isFinishing()) {
                    if (MyApiClient.PROD_FAILURE_FLAG)
                        Toast.makeText(getContext(), getString(R.string.network_connection_failure_toast), Toast.LENGTH_SHORT).show();
                    else
                        Toast.makeText(getContext(), throwable.toString(), Toast.LENGTH_SHORT).show();

                    if (progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }
                }
                Timber.w("Error Koneksi get katalog:" + throwable.toString());
            }

        });
    }

}
