package com.sgo.mayapada.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.securepreferences.SecurePreferences;
import com.sgo.mayapada.Beans.KantinStudentOrderModel;
import com.sgo.mayapada.R;
import com.sgo.mayapada.activities.InsertPIN;
import com.sgo.mayapada.activities.KantinActivity;
import com.sgo.mayapada.activities.MainPage;
import com.sgo.mayapada.adapter.KantinStudentOrderAdapter;
import com.sgo.mayapada.coreclass.CurrencyFormat;
import com.sgo.mayapada.coreclass.CustomSecurePref;
import com.sgo.mayapada.coreclass.DefineValue;
import com.sgo.mayapada.coreclass.InetHandler;
import com.sgo.mayapada.coreclass.MyApiClient;
import com.sgo.mayapada.coreclass.WebParams;
import com.sgo.mayapada.dialogs.AlertDialogLogout;
import com.sgo.mayapada.dialogs.CanteenConfirmOrderDialog;
import com.sgo.mayapada.dialogs.DefinedDialog;
import com.sgo.mayapada.dialogs.ReportBillerDialog;
import com.sgo.mayapada.interfaces.OnLoadDataListener;
import com.sgo.mayapada.loader.UtilsLoader;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import timber.log.Timber;

/**
 * Created by thinkpad on 8/3/2016.
 */
public class FragKantinStudentOrder extends ListFragment implements ReportBillerDialog.OnDialogOkCallback, CanteenConfirmOrderDialog.OnDialogConfirmCallback {

    private SecurePreferences sp;
    private View v;
    private ProgressDialog out;
    private ListView lv_menu;
//    private PtrFrameLayout mPtrFrame;
    private View emptyLayout;
//    Animation frameAnimation;
//    ImageView spining_progress;
//    MaterialRippleLayout btn_loadmore;
//    ViewGroup footerLayout;
    private TextView tvTotalBayar;
    private Button btn_refresh;
    private Button btnCancel;
    private Button btnPay;
    private KantinStudentOrderAdapter adapter;
    private ArrayList<KantinStudentOrderModel> listMenu;
//    int page;
    private int attempt = -1;
    private int failed;
    private String user_id;
    private String access_key;
    private String merchant_id;
    private String merchant_name = "";
    private String subcategory_code;
    private String member_id;
    private String tx_id = "";
    private Activity activity;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        v = inflater.inflate(R.layout.frag_kantin_student_order, container, false);
        return v;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        activity=getActivity();
        sp = CustomSecurePref.getInstance().getmSecurePrefs();
        user_id = sp.getString(DefineValue.USERID_PHONE, "");
        access_key = sp.getString(DefineValue.ACCESS_KEY, "");
        if(sp.getBoolean(DefineValue.IS_MERCHANT,false))
            subcategory_code = sp.getString(DefineValue.MERCHANT_SUBCATEGORY,"");
        else
            subcategory_code = sp.getString(DefineValue.SUBCATEGORY_CODE,"");
        member_id = sp.getString(DefineValue.MEMBER_ID,"");

        Bundle bundle = getArguments();
        merchant_id = bundle.getString(DefineValue.KANTIN_ID);
        merchant_name = bundle.getString(DefineValue.KANTIN_NAME);

        listMenu = new ArrayList<>();

        lv_menu = (ListView) v.findViewById(android.R.id.list);
        tvTotalBayar = (TextView) v.findViewById(R.id.kantinorder_totalbayar_value);
        btnCancel = (Button) v.findViewById(R.id.kantinorder_batal_button);
        btnPay = (Button) v.findViewById(R.id.kantinorder_bayar_button);
//        footerLayout = (ViewGroup) getActivity().getLayoutInflater().inflate(R.layout.footer_loadmore, lv_menu, false);
//        footerLayout.setLayoutParams(new ListView.LayoutParams(ListView.LayoutParams.MATCH_PARENT, ListView.LayoutParams.WRAP_CONTENT));
//        spining_progress = (ImageView) footerLayout.findViewById(R.id.image_spinning_wheel);
//        btn_loadmore = (MaterialRippleLayout) footerLayout.findViewById(R.id.btn_loadmore);
        emptyLayout = v.findViewById(R.id.empty_layout);
        emptyLayout.setVisibility(View.GONE);
        btn_refresh = (Button) emptyLayout.findViewById(R.id.btnRefresh);

        btnPay.setOnClickListener(payListener);
        btnCancel.setOnClickListener(cancelListener);

//        frameAnimation = AnimationUtils.loadAnimation(getActivity(), R.anim.spinner_animation);
//        frameAnimation.setRepeatCount(Animation.INFINITE);
//
//        page = 1;
//        btn_loadmore.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
////                getDataTagihan(page, false, comm_id);
//            }
//        });

        btn_refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                mPtrFrame.autoRefresh();
            }
        });

        KantinStudentOrderAdapter.AdapterInterface listener = new KantinStudentOrderAdapter.AdapterInterface()
        {
            @Override
            public void onChange(String value)
            {
                tvTotalBayar.setText(value);
            }
        };
        adapter = new KantinStudentOrderAdapter(v, listener, getActivity(), listMenu);
        lv_menu.setAdapter(adapter);
        MyScrollListener scrollListener = new MyScrollListener();
        lv_menu.setOnScrollListener(scrollListener);

//        setLoadMore(false);
//
//        mPtrFrame = (PtrFrameLayout) v.findViewById(R.id.rotate_header_list_view_frame);
//
//        final MaterialHeader header = new MaterialHeader(getActivity());
//        int[] colors = getResources().getIntArray(R.array.google_colors);
//        header.setColorSchemeColors(colors);
//        header.setLayoutParams(new PtrFrameLayout.LayoutParams(-1, -2));
//        header.setPadding(0, 15, 0, 10);
//        header.setPtrFrameLayout(mPtrFrame);
//
//        mPtrFrame.setHeaderView(header);
//        mPtrFrame.addPtrUIHandler(header);
//        mPtrFrame.setPtrHandler(new PtrHandler() {
//            @Override
//            public void onRefreshBegin(PtrFrameLayout frame) {
//                page = 1;
////                getDataTagihan(0, null, comm_id);
////                jumlah = 0;
////                tv_total_bayar.setText(Long.toString(jumlah));
//            }
//
//            @Override
//            public boolean checkCanDoRefresh(PtrFrameLayout frame, View content, View header) {
//                //return PtrDefaultHandler.checkContentCanBePulledDown(frame, content, header);
//                return canScrollUp();
//            }
//        });
//
//        mPtrFrame.postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                mPtrFrame.autoRefresh(false);
//            }
//        }, 50);
//
//        new UtilsLoader(getActivity(),sp).getFailedPIN(user_id, new OnLoadDataListener() { //get pin attempt
//            @Override
//            public void onSuccess(Object deData) {
//                attempt = (int) deData;
//            }
//
//            @Override
//            public void onFail(String message) {
//
//            }
//
//            @Override
//            public void onFailure() {
//
//            }
//        });
        new UtilsLoader(getActivity(),sp).getFailedPIN(user_id,new OnLoadDataListener() { //get pin attempt
            @Override
            public void onSuccess(Object deData) {
                attempt = (int)deData;
            }

            @Override
            public void onFail(Bundle message) {

            }

            @Override
            public void onFailure(String message) {

            }
        });

        getDataCatalog(false);
    }

    private Button.OnClickListener payListener = new Button.OnClickListener() {
        @Override
        public void onClick(View view) {
            prosesBayar();
        }
    };

    private Button.OnClickListener cancelListener = new Button.OnClickListener() {
        @Override
        public void onClick(View view) {
            getActivity().finish();
        }
    };

    private void prosesBayar() {
        if(InetHandler.isNetworkAvailable(getActivity())) {
            if(!tvTotalBayar.getText().toString().equals("") && Integer.parseInt(tvTotalBayar.getText().toString()) > 0) {
                ArrayList<TempObjectData> mTempObjectDataList = new ArrayList<>();
                for(int i=0 ; i<listMenu.size() ; i++) {
                    if(!listMenu.get(i).getCount().equals("") && Integer.parseInt(listMenu.get(i).getCount()) > 0 ) {
                        mTempObjectDataList.add(new TempObjectData(listMenu.get(i).getId(), listMenu.get(i).getCount()));
                    }
                }

                final GsonBuilder gsonBuilder = new GsonBuilder();
                gsonBuilder.setPrettyPrinting();
                final Gson gson = gsonBuilder.create();
                String order = gson.toJson(mTempObjectDataList);

                sentReqOrderCatalog(order);
            }
        }
        else DefinedDialog.showErrorDialog(getActivity(), getString(R.string.inethandler_dialog_message));
    }

    private void getDataCatalog(final boolean checkPrice) {
        try{
            out = DefinedDialog.CreateProgressDialog(getActivity(), null);
            out.show();

            RequestParams params = MyApiClient.getSignatureWithParams(MyApiClient.COMM_ID, MyApiClient.LINK_CANTEEN_CATALOGS,
                    user_id, access_key);
            params.put(WebParams.USER_ID,user_id);
            params.put(WebParams.COMM_ID, MyApiClient.COMM_ID);
            params.put(WebParams.SUBCATEGORY_CODE, subcategory_code);
            params.put(WebParams.MERCHANT_ID, merchant_id);

            Timber.d("isi params get data catalog:"+params.toString());

            MyApiClient.getCanteenCatalogs(getActivity(), params, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                    try {
                        if (isAdded()) {
                            out.dismiss();

                            String code = response.getString(WebParams.ERROR_CODE);
                            Timber.w("isi response get data catalog:" + response.toString());

                            if (code.equals(WebParams.SUCCESS_CODE)) {
                                String catalog = response.getString(WebParams.CATALOG);
                                if (!catalog.equals("")) {
                                    JSONArray arrayData = new JSONArray(catalog);
                                    JSONObject mObj;
                                    if(checkPrice) {
                                        for (int i = 0; i < arrayData.length(); i++) {
                                            mObj = arrayData.getJSONObject(i);
                                            for(int iMenu = 0 ; iMenu < listMenu.size() ; iMenu++) {
                                                if(mObj.getString(WebParams.ITEM_CODE).equalsIgnoreCase(listMenu.get(iMenu).getId())) {
                                                    if (!listMenu.get(iMenu).getCount().equals("") && Integer.parseInt(listMenu.get(iMenu).getCount()) > 0) {
                                                        if (!listMenu.get(iMenu).getPrice().equalsIgnoreCase(mObj.getString(WebParams.PRICE))) {
                                                            listMenu.get(iMenu).setPrice(mObj.getString(WebParams.PRICE));
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        CanteenConfirmOrderDialog dialogConfirm = CanteenConfirmOrderDialog.CreateInstance(listMenu);
                                        dialogConfirm.setCancelable(false);
                                        dialogConfirm.setTargetFragment(FragKantinStudentOrder.this, 0);
                                        dialogConfirm.show(getActivity().getSupportFragmentManager(), CanteenConfirmOrderDialog.TAG);
                                    }
                                    else {
                                        if(listMenu.size() > 0)
                                            listMenu.clear();

                                        for (int i = 0; i < arrayData.length(); i++) {
                                            mObj = arrayData.getJSONObject(i);

                                            if (mObj.getString(WebParams.ITEM_STATUS).equalsIgnoreCase("A")) {
                                                KantinStudentOrderModel menu = new KantinStudentOrderModel();
                                                menu.setId(mObj.getString(WebParams.ITEM_CODE));
                                                menu.setTitle(mObj.getString(WebParams.ITEM_NAME));
                                                menu.setCcy_id(mObj.getString(WebParams.CCY_ID));
                                                menu.setPrice(mObj.getString(WebParams.PRICE));
                                                menu.setStock_type(mObj.getString(WebParams.STOCK_TYPE));
                                                menu.setQty(mObj.getString(WebParams.QTY));
                                                menu.setStatus(mObj.getString(WebParams.ITEM_STATUS));
                                                menu.setCount("");
                                                menu.setImg_url(mObj.getString(WebParams.IMG_MEDIUM_URL));
                                                listMenu.add(menu);
                                            }
                                        }
                                    }
                                }
                                else if(catalog.equals("")) {
                                    lv_menu.setVisibility(View.GONE);
                                    emptyLayout.setVisibility(View.VISIBLE);
                                    listMenu.clear();
                                    adapter.notifyDataSetChanged();
                                }

                                adapter.notifyDataSetChanged();

                            } else if (code.equals(WebParams.LOGOUT_CODE)) {
                                Timber.d("isi response autologout:" + response.toString());
                                String message = response.getString(WebParams.ERROR_MESSAGE);
                                AlertDialogLogout test = AlertDialogLogout.getInstance();
                                test.showDialoginActivity(getActivity(), message);
                            }
                            else {
                                String message = response.getString(WebParams.ERROR_MESSAGE);
                                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                                builder.setMessage(message)
                                        .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                dialog.dismiss();
                                                getActivity().finish();
                                            }
                                        });
                                AlertDialog dialog = builder.create();
                                dialog.show();
                            }
                        }

                    } catch (JSONException e) {

                        Toast.makeText(getActivity(), getString(R.string.internal_error), Toast.LENGTH_LONG).show();
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    super.onFailure(statusCode, headers, responseString, throwable);
                    failure(throwable);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    failure(throwable);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    failure(throwable);
                }

                private void failure(Throwable throwable) {
                    out.dismiss();
                    if (MyApiClient.PROD_FAILURE_FLAG)
                        Toast.makeText(getActivity(), getString(R.string.network_connection_failure_toast), Toast.LENGTH_SHORT).show();
                    else
                        Toast.makeText(getActivity(), throwable.toString(), Toast.LENGTH_SHORT).show();

                    lv_menu.setVisibility(View.GONE);
                    emptyLayout.setVisibility(View.VISIBLE);
                    listMenu.clear();
                    adapter.notifyDataSetChanged();
                    getActivity().finish();

                    Timber.w("Error Koneksi get data catalog:" + throwable.toString());
                }
            });
        } catch (Exception e) {
            String err = (e.getMessage()==null)?"Connection failed":e.getMessage();
            Timber.e("http err:" + err);
        }
    }

    private void sentReqOrderCatalog(String order) {
        try{

            out = DefinedDialog.CreateProgressDialog(getActivity(), null);
            out.show();

            RequestParams params = MyApiClient.getSignatureWithParams(MyApiClient.COMM_ID, MyApiClient.LINK_REQ_ORDER_CATALOG,
                    user_id, access_key);
            params.put(WebParams.USER_ID,user_id);
            params.put(WebParams.COMM_ID, MyApiClient.COMM_ID);
            params.put(WebParams.SUBCATEGORY_CODE, subcategory_code);
            params.put(WebParams.MERCHANT_ID, merchant_id);
            params.put(WebParams.CCY_ID, listMenu.get(0).getCcy_id());
            params.put(WebParams.MEMBER_ID, member_id);
            params.put(WebParams.ORDER, order);

            Timber.d("isi params req order catalog:"+params.toString());

            MyApiClient.sentReqOrderCatalog(getActivity(), params, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    try {
                        out.dismiss();

                        String code = response.getString(WebParams.ERROR_CODE);
                        Timber.w("isi response req order catalog:" + response.toString());

                        if (code.equals(WebParams.SUCCESS_CODE)) {
                            tx_id = response.getString(WebParams.TX_ID);

                            getDataCatalog(true);

                        } else if (code.equals(WebParams.LOGOUT_CODE)) {
                            Timber.d("isi response autologout:" + response.toString());
                            String message = response.getString(WebParams.ERROR_MESSAGE);
                            AlertDialogLogout test = AlertDialogLogout.getInstance();
                            test.showDialoginActivity(getActivity(), message);
                        }
                        else {
                            String message = response.getString(WebParams.ERROR_MESSAGE);
                            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                            builder.setMessage(message)
                                    .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();
                                        }
                                    });
                            AlertDialog dialog = builder.create();
                            dialog.show();
                        }

                    } catch (JSONException e) {
                        Toast.makeText(getActivity(), getString(R.string.internal_error), Toast.LENGTH_LONG).show();
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    super.onFailure(statusCode, headers, responseString, throwable);
                    failure(throwable);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    failure(throwable);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    failure(throwable);
                }

                private void failure(Throwable throwable) {
                    out.dismiss();
                    if (MyApiClient.PROD_FAILURE_FLAG)
                        Toast.makeText(getActivity(), getString(R.string.network_connection_failure_toast), Toast.LENGTH_SHORT).show();
                    else
                        Toast.makeText(getActivity(), throwable.toString(), Toast.LENGTH_SHORT).show();

                    Timber.w("Error Koneksi req order catalog:" + throwable.toString());
                }
            });
        } catch (Exception e) {
            String err = (e.getMessage()==null)?"Connection failed":e.getMessage();
            Timber.e("http err:" + err);
        }
    }

    private void sentConfirmOrderCatalog(String _token_id) {
        try{

            out = DefinedDialog.CreateProgressDialog(getActivity(), null);
            out.show();

            RequestParams params = MyApiClient.getSignatureWithParams(MyApiClient.COMM_ID, MyApiClient.LINK_CONFIRM_ORDER_CATALOG,
                    user_id, access_key);
            params.put(WebParams.USER_ID,user_id);
            params.put(WebParams.COMM_ID, MyApiClient.COMM_ID);
            params.put(WebParams.MEMBER_ID,member_id);
            params.put(WebParams.TX_ID, tx_id);
            params.put(WebParams.TOKEN_ID, _token_id);

            Timber.d("isi params confirm order catalog:"+params.toString());

            MyApiClient.sentConfirmOrderCatalog(getActivity(), params, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                    try {
                        out.dismiss();

                        String code = response.getString(WebParams.ERROR_CODE);
                        Timber.w("isi response confirm order catalog:" + response.toString());

                        if (code.equals(WebParams.SUCCESS_CODE)) {
                            showReportBillerDialog(user_id, merchant_name, response.getString(WebParams.CUPON), response.getString(WebParams.EXPIRED), response.getString(WebParams.TOTAL),
                                    DefineValue.SUCCESS, getResources().getString(R.string.struk_kantin_detail) + " " + response.getString(WebParams.EXP_HOURS) + " " + getResources().getString(R.string.struk_kantin_detail_2));

                        } else if (code.equals(WebParams.LOGOUT_CODE)) {
                            Timber.d("isi response autologout:" + response.toString());
                            String message = response.getString(WebParams.ERROR_MESSAGE);
                            AlertDialogLogout test = AlertDialogLogout.getInstance();
                            test.showDialoginActivity(getActivity(), message);
                        }
                        else {
                            String message = response.getString(WebParams.ERROR_MESSAGE);

                            if(message.equals("PIN tidak sesuai")){
                                Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
                                Intent i = new Intent(getActivity(), InsertPIN.class);

                                attempt = response.optInt(WebParams.FAILED_ATTEMPT, -1);
                                failed = response.optInt(WebParams.MAX_FAILED,0);

                                if(attempt != -1)
                                    i.putExtra(DefineValue.ATTEMPT,failed-attempt);

                                startActivityForResult(i, MainPage.REQUEST_FINISH);
                            }
                            else{
                                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                                builder.setMessage(message)
                                        .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                dialog.dismiss();
                                            }
                                        });
                                AlertDialog dialog = builder.create();
                                dialog.show();
                            }
                        }

                    } catch (JSONException e) {
                        Toast.makeText(getActivity(), getString(R.string.internal_error), Toast.LENGTH_LONG).show();
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    super.onFailure(statusCode, headers, responseString, throwable);
                    failure(throwable);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    failure(throwable);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    failure(throwable);
                }

                private void failure(Throwable throwable) {
                    out.dismiss();
                    if (MyApiClient.PROD_FAILURE_FLAG)
                        Toast.makeText(getActivity(), getString(R.string.network_connection_failure_toast), Toast.LENGTH_SHORT).show();
                    else
                        Toast.makeText(getActivity(), throwable.toString(), Toast.LENGTH_SHORT).show();

                    Timber.w("Error Koneksi confirm order catalog:" + throwable.toString());
                }
            });
        } catch (Exception e) {
            String err = (e.getMessage()==null)?"Connection failed":e.getMessage();
            Timber.e("http err:" + err);
        }
    }

    private void sentCancelOrder() {
        try{

            out = DefinedDialog.CreateProgressDialog(getActivity(), null);
            out.show();

            RequestParams params = MyApiClient.getSignatureWithParams(MyApiClient.COMM_ID, MyApiClient.LINK_CANCEL_ORDER,
                    user_id, access_key);
            params.put(WebParams.USER_ID,user_id);
            params.put(WebParams.COMM_ID, MyApiClient.COMM_ID);
            params.put(WebParams.TX_ID, tx_id);
            params.put(WebParams.MEMBER_ID, member_id);

            Timber.d("isi params sent cancel order:"+params.toString());

            MyApiClient.sentCancelOrder(getActivity(), params, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                    try {
                        out.dismiss();

                        String code = response.getString(WebParams.ERROR_CODE);
                        Timber.w("isi response sent cancel order:" + response.toString());

                        if (code.equals(WebParams.SUCCESS_CODE)) {
                            Toast.makeText(activity, "Berhasil membatalkan pesanan", Toast.LENGTH_LONG).show();
                        } else if (code.equals(WebParams.LOGOUT_CODE)) {
                            Timber.d("isi response autologout:" + response.toString());
                            String message = response.getString(WebParams.ERROR_MESSAGE);
                            AlertDialogLogout test = AlertDialogLogout.getInstance();
                            test.showDialoginActivity(getActivity(), message);
                        }
                        else {
                            String message = response.getString(WebParams.ERROR_MESSAGE);
                            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                            builder.setMessage(message)
                                    .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();
                                        }
                                    });
                            AlertDialog dialog = builder.create();
                            dialog.show();
                        }

                        getDataCatalog(false);
                        tvTotalBayar.setText("");

                    } catch (JSONException e) {
                        Toast.makeText(getActivity(), getString(R.string.internal_error), Toast.LENGTH_LONG).show();
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    super.onFailure(statusCode, headers, responseString, throwable);
                    failure(throwable);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    failure(throwable);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    failure(throwable);
                }

                private void failure(Throwable throwable) {
                    out.dismiss();
                    if (MyApiClient.PROD_FAILURE_FLAG)
                        Toast.makeText(getActivity(), getString(R.string.network_connection_failure_toast), Toast.LENGTH_SHORT).show();
                    else
                        Toast.makeText(getActivity(), throwable.toString(), Toast.LENGTH_SHORT).show();

                    Timber.w("Error Koneksi sent cancel order:" + throwable.toString());
                }
            });
        } catch (Exception e) {
            String err = (e.getMessage()==null)?"Connection failed":e.getMessage();
            Timber.e("http err:" + err);
        }
    }
    private void dismissProgressDialog() {
        if (out != null && out.isShowing()) {
            out.dismiss();
        }
    }

    @Override
    public void onDestroyView() {
        dismissProgressDialog();
        super.onDestroyView();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == MainPage.REQUEST_FINISH){
            if(resultCode == InsertPIN.RESULT_PIN_VALUE){
                String value_pin = data.getStringExtra(DefineValue.PIN_VALUE);
//                Calendar cal = Calendar.getInstance();
//                cal.add(Calendar.HOUR, 3);
//                Date currentLocalTime = cal.getTime();
//                DateFormat date = new SimpleDateFormat("HH:mm");
//
//                String localTime = date.format(currentLocalTime);
                sentConfirmOrderCatalog(value_pin);
            }
            else if(resultCode == InsertPIN.RESULT_CANCEL_ORDER) {
                sentCancelOrder();
            }
        }
    }

    private void CallPINinput(int _attempt){
        Intent i = new Intent(getActivity(), InsertPIN.class);
        if(_attempt == 1)
            i.putExtra(DefineValue.ATTEMPT,_attempt);
        startActivityForResult(i, MainPage.REQUEST_FINISH);
    }

    private void showReportBillerDialog(String userId, String toko, String couponCode, String exp_date,
                                        String amount, String txStatus, String txRemark) {
        ArrayList<KantinStudentOrderModel> listOrder = new ArrayList<>();
        for(int i = 0 ; i < listMenu.size() ; i++) {
            if(!listMenu.get(i).getCount().equals("") && Integer.parseInt(listMenu.get(i).getCount()) > 0) {
                KantinStudentOrderModel order = listMenu.get(i);
                listOrder.add(order);
            }
        }
        Bundle args = new Bundle();
        ReportBillerDialog dialog = new ReportBillerDialog();
//        args.putString(DefineValue.USER_NAME, userName);
//        args.putString(DefineValue.DATE_TIME, date);
        args.putString(DefineValue.TX_ID, couponCode);
        args.putParcelableArrayList(DefineValue.LIST_ORDER, listOrder);
        args.putString(DefineValue.REPORT_TYPE, DefineValue.KANTIN);
        args.putString(DefineValue.USERID_PHONE, userId);
        args.putString(DefineValue.TOKO, toko);
        args.putString(DefineValue.COUPON_CODE, couponCode);
        args.putString(DefineValue.EXP_DATE, exp_date);
        args.putString(DefineValue.AMOUNT, MyApiClient.CCY_VALUE + ". " + CurrencyFormat.format(amount));

//        double dAmount = Double.valueOf(amount);
//        double dFee = Double.valueOf(fee);
//        double total_amount = dAmount + dFee;

//        args.putString(DefineValue.TOTAL_AMOUNT, MyApiClient.CCY_VALUE + ". " + CurrencyFormat.format(total_amount));

        Boolean txStat = false;
        if (txStatus.equals(DefineValue.SUCCESS)){
            txStat = true;
            args.putString(DefineValue.TRX_MESSAGE, getString(R.string.kupon_success));
        }
//        else if(txStatus.equals(DefineValue.ONRECONCILED)){
//            txStat = true;
//            args.putString(DefineValue.TRX_MESSAGE, getString(R.string.transaction_pending));
//        }else if(txStatus.equals(DefineValue.SUSPECT)){
//            args.putString(DefineValue.TRX_MESSAGE, getString(R.string.transaction_suspect));
//        }
//        else if(!txStatus.equals(DefineValue.FAILED)){
//            args.putString(DefineValue.TRX_MESSAGE, getString(R.string.transaction)+" "+txStatus);
//        }
        else {
            args.putString(DefineValue.TRX_MESSAGE, getString(R.string.kupon_failed));
        }
        args.putBoolean(DefineValue.TRX_STATUS, txStat);
        if(txStat)args.putString(DefineValue.TRX_REMARK, txRemark);

        dialog.setArguments(args);
        dialog.setTargetFragment(this, 0);
        dialog.show(getActivity().getSupportFragmentManager(), ReportBillerDialog.TAG);
    }

//    public boolean canScrollUp() {
//        return lv_menu != null && (lv_menu.getAdapter().getCount() == 0 || lv_menu.getFirstVisiblePosition() == 0 && lv_menu.getChildAt(0).getTop() == 0);
//    }
//
//    private void setLoadMore(boolean isLoading)
//    {
//        if (isLoading) {
//            lv_menu.addFooterView(footerLayout,null,false);
//        }
//        else {
//            lv_menu.removeFooterView(footerLayout);
//        }
//    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if(getActivity().getSupportFragmentManager().getBackStackEntryCount() > 0)
                    getActivity().getSupportFragmentManager().popBackStack();
                else
                    getActivity().finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume() {
        super.onResume();
        setTitle(getString(R.string.kantin_order_title) + " > " + merchant_name);
    }

    private void setTitle(String _title){
        if (getActivity() == null)
            return;

        KantinActivity fca = (KantinActivity) getActivity();
        fca.setTitleFragment(_title);
    }

    @Override
    public void onOkButton() {
        getActivity().setResult(MainPage.RESULT_BALANCE);
        setResultActivity(MainPage.RESULT_REFRESH_ORDER);
        getActivity().finish();
    }

    @Override
    public void onConfirmButton() {
        CallPINinput(attempt);
    }

    @Override
    public void onCancelButton() {
        sentCancelOrder();
    }

    private void setResultActivity(int result){
        if (getActivity() == null)
            return;

        KantinActivity fca = (KantinActivity) getActivity();
        fca.setResultActivity(result);
    }

    private class MyScrollListener implements AbsListView.OnScrollListener {

        @Override
        public void onScroll(AbsListView view, int firstVisibleItem,
                             int visibleItemCount, int totalItemCount) {

        }

        @Override
        public void onScrollStateChanged(AbsListView view, int scrollState) {
            if (SCROLL_STATE_TOUCH_SCROLL == scrollState) {
                View currentFocus = getActivity().getCurrentFocus();
                if (currentFocus != null) {
                    currentFocus.clearFocus();
                    hiddenKeyboard(v);
                }
            }
        }

    }

    private void hiddenKeyboard(View v) {
        InputMethodManager keyboard = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        keyboard.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }

    private class TempObjectData {
        private String item_code;
        private String qty;

        public TempObjectData(String _item_code, String _qty) {
            this.setItem_code(_item_code);
            this.setQty(_qty);
        }

        public String getItem_code() {
            return item_code;
        }

        public void setItem_code(String item_code) {
            this.item_code = item_code;
        }

        public String getQty() {
            return qty;
        }

        public void setQty(String qty) {
            this.qty = qty;
        }
    }

}
