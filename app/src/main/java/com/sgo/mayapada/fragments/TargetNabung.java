package com.sgo.mayapada.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.securepreferences.SecurePreferences;
import com.sgo.mayapada.entityRealm.Target_Saving_Model;
import com.sgo.mayapada.R;
import com.sgo.mayapada.activities.TabunganActivity;
import com.sgo.mayapada.coreclass.CustomSecurePref;
import com.sgo.mayapada.coreclass.DateTimeFormat;
import com.sgo.mayapada.coreclass.DefineValue;
import com.sgo.mayapada.coreclass.ErrorDefinition;
import com.sgo.mayapada.coreclass.MyApiClient;
import com.sgo.mayapada.coreclass.WebParams;
import com.sgo.mayapada.dialogs.AlertDialogFrag;
import com.sgo.mayapada.dialogs.AlertDialogLogout;
import com.sgo.mayapada.dialogs.DefinedDialog;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import timber.log.Timber;

public class TargetNabung extends Fragment implements View.OnClickListener {


    public final static String TAG = "com.sgo.mayapada.fragments.TargetNabung";

    private Button confirm;
    private Spinner spinner;
    private ProgressDialog prodDialog;
    private String userID;
    private String accessKey;
    private SecurePreferences sp;
    private String date_from;
    private String date_to;
    private String amount;
    String comm_id;
    private String member_id;
    private EditText amount_et;
    private AlertDialogFrag alertDialogDelete;
    private AlertDialogFrag alertDialogSimpan;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sp = CustomSecurePref.getInstance().getmSecurePrefs();
        userID = sp.getString(DefineValue.USERID_PHONE,"");
        accessKey = sp.getString(DefineValue.ACCESS_KEY,"");
        member_id = sp.getString(DefineValue.MEMBER_ID, "");
        prodDialog = DefinedDialog.CreateProgressDialog(getActivity(), "");
        date_from = DateTimeFormat.getCurrentDate();
        prodDialog.dismiss();
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.target_tabungan_menu,menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.menu_item_delete_target_saving:
                showDialogDelete();
                return true;
            default:
                return super.onOptionsItemSelected(item);

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        View v = inflater.inflate(R.layout.fragment_atur_target_nabung, container, false);
        spinner = (Spinner) v.findViewById(R.id.day_spinner);
        amount_et = (EditText) v.findViewById(R.id.input_nabung);
        confirm = (Button) v.findViewById(R.id.btn_save);
        confirm.setOnClickListener(this);
        v.findViewById(R.id.btn_cancel).setOnClickListener(this);
        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        setTitle();

        List<String> categories = new ArrayList<>();
        for(int x=1 ; x<=30; x++)
            categories.add(String.valueOf(x));

        ArrayAdapter<String> adapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_item, categories);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(spinner_listener);


    }

    private void setTitle(){
        if(getActivity() == null)
            return;

        TabunganActivity ta = (TabunganActivity)getActivity();
        ta.setToolbarTitle(getString(R.string.savings_text_savings_target));
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    private Spinner.OnItemSelectedListener spinner_listener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            date_to = DateTimeFormat.getCurrentDatePlus(position+1);
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };

    private void showDialogDelete(){
        if(alertDialogDelete == null) {
            alertDialogDelete = AlertDialogFrag.newInstance(getString(R.string.menu_title_delete_saving_target),
                    getString(R.string.targetsaving_dialog_msg), false);
            alertDialogDelete.setOkListener(new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    deleteTargetSaving();
                }
            });
            alertDialogDelete.setTargetFragment(this,0);
        }
        alertDialogDelete.show(getFragmentManager(), AlertDialogFrag.TAG);
    }

    private void showDialogTarget(){
        if(alertDialogSimpan == null) {
            alertDialogSimpan = AlertDialogFrag.newInstance(getString(R.string.savings_text_savings_target),
                    getString(R.string.targetsaving_dialog_msg_targetbutton), false);
            alertDialogSimpan.setOkListener(new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    insertTargetSaving();
                }
            });
            alertDialogSimpan.setTargetFragment(this,0);
        }
        alertDialogSimpan.show(getFragmentManager(), AlertDialogFrag.TAG);
    }

    private boolean inputValidation(){
        if(amount_et.getText().toString().length() == 0){
            amount_et.requestFocus();
            amount_et.setError(getString(R.string.billertoken_validation_payment_input_amount));
            return false;
        }
        else if(Long.parseLong(amount_et.getText().toString()) < 1){
            amount_et.requestFocus();
            amount_et.setError(getString(R.string.payfriends_amount_zero));
            return false;
        }
        return true;
    }

    private void insertTargetSaving(){

        prodDialog.show();
        RequestParams params = MyApiClient.getSignatureWithParams(MyApiClient.COMM_ID, MyApiClient.LINK_SAVING_TARGET,
                userID, accessKey);
        params.put(WebParams.COMM_ID, MyApiClient.COMM_ID);
        params.put(WebParams.MEMBER_ID, member_id);
        params.put(WebParams.USER_ID, userID);
        params.put(WebParams.CCY_ID, MyApiClient.CCY_VALUE);
        params.put(WebParams.AMOUNT, amount);
        params.put(WebParams.DATE_FROM, date_from);
        params.put(WebParams.DATE_TO, date_to);
        Timber.d("parameter insertTargetSaving: " + params.toString());
        MyApiClient.insertSavingTarget(getActivity(), params, new JsonHttpResponseHandler(){
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);

                try {
                    Timber.d("respose insertTargetSaving: " + response.toString());

                    String code = response.getString(WebParams.ERROR_CODE);
                    if (code.equals(WebParams.SUCCESS_CODE)) {
                        Target_Saving_Model.insertData(response);
                        Toast.makeText(getContext(), getString(R.string.targetsaving_toast_success_insert), Toast.LENGTH_LONG).show();
                        Timber.d("insertSavingTarget complete");
                        getActivity().onBackPressed();
                    }
                    else if (code.equals(WebParams.LOGOUT_CODE)) {
                        String message = response.getString(WebParams.ERROR_MESSAGE);
                        AlertDialogLogout test = AlertDialogLogout.getInstance();
                        test.showDialoginActivity(getActivity(), message);
                    } else {
                        code = response.getString(WebParams.ERROR_MESSAGE);
                        Toast.makeText(getContext(), code, Toast.LENGTH_LONG).show();
                    }

                }catch (JSONException e) {
                    e.printStackTrace();
                }

                if (prodDialog.isShowing()) {
                    prodDialog.dismiss();
                }

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                super.onFailure(statusCode, headers, responseString, throwable);
                failure(throwable);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                failure(throwable);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                failure(throwable);
            }

            private void failure(Throwable throwable) {
                if(MyApiClient.PROD_FAILURE_FLAG) {
                    if (MyApiClient.PROD_FAILURE_FLAG)
                        Toast.makeText(getActivity(), getString(R.string.network_connection_failure_toast), Toast.LENGTH_SHORT).show();
                    else
                        Toast.makeText(getContext(), throwable.toString(), Toast.LENGTH_SHORT).show();

                    if (prodDialog.isShowing()) {
                        prodDialog.dismiss();
                    }
                }
                Timber.w("Error Koneksi insertTargetSaving:" + throwable.toString());
            }

        });
    }

    private void deleteTargetSaving(){

        prodDialog.show();
        RequestParams params = MyApiClient.getSignatureWithParams(MyApiClient.COMM_ID, MyApiClient.LINK_DELETE_TARGETSAVING,
                userID, accessKey);
        params.put(WebParams.COMM_ID, MyApiClient.COMM_ID);
        params.put(WebParams.MEMBER_ID, member_id);
        params.put(WebParams.USER_ID, userID);
        Timber.d("parameter deleteTargetSaving: " + params.toString());
        MyApiClient.deleteTargetSaving(getActivity(), params, new JsonHttpResponseHandler(){
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);

                try {
                    Timber.d("respose deleteTargetSaving: " + response.toString());

                    String code = response.getString(WebParams.ERROR_CODE);
                    if (code.equals(WebParams.SUCCESS_CODE)) {
                        Target_Saving_Model.deleteOrClear();
                        Toast.makeText(getContext(),getString(R.string.targetsaving_toast_delete_success), Toast.LENGTH_LONG).show();
                        Timber.d("deleteTargetSaving complete");
                    }
                    else if(code.equals(ErrorDefinition.NO_TRANSACTION)){
                        Toast.makeText(getContext(), getString(R.string.targetsaving_toast_no_transaction), Toast.LENGTH_LONG).show();
                    }
                    else {
                        code = response.getString(WebParams.ERROR_MESSAGE);
                        Toast.makeText(getContext(), code, Toast.LENGTH_LONG).show();
                    }

                }catch (JSONException e) {
                    e.printStackTrace();
                }

                if (prodDialog.isShowing()) {
                    prodDialog.dismiss();
                }

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                super.onFailure(statusCode, headers, responseString, throwable);
                failure(throwable);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                failure(throwable);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                failure(throwable);
            }

            private void failure(Throwable throwable) {
                if(MyApiClient.PROD_FAILURE_FLAG) {
                    if (MyApiClient.PROD_FAILURE_FLAG)
                        Toast.makeText(getActivity(), getString(R.string.network_connection_failure_toast), Toast.LENGTH_SHORT).show();
                    else
                        Toast.makeText(getContext(), throwable.toString(), Toast.LENGTH_SHORT).show();

                    if (prodDialog.isShowing()) {
                        prodDialog.dismiss();
                    }
                }
                Timber.w("Error Koneksi deleteTargetSaving:" + throwable.toString());
            }

        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_save:
                if(inputValidation()) {
                    amount = amount_et.getText().toString();
                    showDialogTarget();
                }
                break;
            case R.id.btn_cancel:
                getActivity().onBackPressed();
                break;
        }
    }
}
