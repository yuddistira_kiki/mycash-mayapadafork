package com.sgo.mayapada.fragments;/*
  Created by Administrator on 8/11/2016.
 */

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.securepreferences.SecurePreferences;
import com.sgo.mayapada.R;
import com.sgo.mayapada.coreclass.CurrencyFormat;
import com.sgo.mayapada.coreclass.CustomSecurePref;
import com.sgo.mayapada.coreclass.DefineValue;
import com.sgo.mayapada.coreclass.MyApiClient;
import com.sgo.mayapada.coreclass.WebParams;
import com.sgo.mayapada.dialogs.AlertDialogLogout;
import com.sgo.mayapada.dialogs.DefinedDialog;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import timber.log.Timber;

public class On_Going_autodeposit extends Fragment implements View.OnClickListener {

    public final static String TAG = "com.sgo.mayapada.fragments.On_Going_autodeposit";
    private View v;
    private String userID;
    private String accessKey;
    private String member_id;
    private SecurePreferences sp;
    private ProgressDialog prodDialog;
    private boolean isActive = false;

    private TextView title;
    private TextView tv_time;
    private TextView tv_period;
    private TextView tv_withdrawal;
    private TextView tv_balance;
    private Button btn_proses;
    private Button btn_delete;

    private String date_from;
    private String date_to;
    private String withdrawal_type;
    private String status;
    private String amount;
    private String auto_id;
    private int period;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        v = inflater.inflate(R.layout.ongoing_autodeposit, container, false);
        return v;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        prodDialog = DefinedDialog.CreateProgressDialog(getActivity(), "");
        prodDialog.dismiss();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        sp = CustomSecurePref.getInstance().getmSecurePrefs();
        userID = sp.getString(DefineValue.USERID_PHONE,"");
        accessKey = sp.getString(DefineValue.ACCESS_KEY,"");
        member_id = sp.getString(DefineValue.MEMBER_ID, "");

        title = (TextView) v.findViewById(R.id.title_detail);
        tv_period = (TextView) v.findViewById(R.id.period_detail);
        tv_time = (TextView) v.findViewById(R.id.time_detail);
        tv_balance = (TextView) v.findViewById(R.id.balance_detail);
        tv_withdrawal = (TextView) v.findViewById(R.id.withdrawal_detail);
        btn_proses = (Button) v.findViewById(R.id.btn_process_autodeposit);
        btn_delete = (Button) v.findViewById(R.id.btn_delete_autodeposit);
        Bundle bundle = getArguments();

        date_from = bundle.getString(DefineValue.DATE_FROM);
        date_to = bundle.getString(DefineValue.DATE_TO);
        period = bundle.getInt(DefineValue.REPETITION, 0);
        withdrawal_type = bundle.getString(DefineValue.AMOUNT_TYPE);
        status = bundle.getString(DefineValue.STATUS);
        amount = bundle.getString(DefineValue.AUTO_VALUE);
        auto_id = bundle.getString(DefineValue.AUTO_ID);

        tv_time.setText(period + " hari");
        tv_period.setText(date_from + " s/d " + date_to);


        if(withdrawal_type.equals(DefineValue.BALANCE_REMAIN)){
            tv_withdrawal.setText(getString(R.string.minimum_btn_text));
        }else if (withdrawal_type.equals(DefineValue.FIXED_AMOUNT)){
            tv_withdrawal.setText(getString(R.string.nominal_btn_text));
        }else
            tv_withdrawal.setText(getString(R.string.percentage_btn_text));

        if(withdrawal_type.equals(DefineValue.PERCENTAGE)) {
            tv_balance.setText(amount+" %");
        }
        else
            tv_balance.setText(MyApiClient.CCY_VALUE+". "+CurrencyFormat.format(amount));

        btn_proses.setOnClickListener(this);
        btn_delete.setOnClickListener(this);


        if(status.equals(DefineValue.INACTIVE) ){
            isActive = false;
            btn_proses.setText(getString(R.string.autosavingongoing_btn_active));
        }else{
            isActive = true;
            btn_proses.setText(getString(R.string.autosavingongoing_btn_inactive));
        }

    }

    private void ShowDialogProcess(){

        AlertDialog.Builder delete_alert = new AlertDialog.Builder(getActivity());
        delete_alert.setTitle(getString(R.string.warning));
        if(isActive)
            delete_alert.setMessage(getString(R.string.alert_reactivate_autosaving));
        else
            delete_alert.setMessage(getString(R.string.alert_stop_autosaving));
        delete_alert.setPositiveButton(getString(R.string.ok), new
                DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                        if(isActive)
                            setStatusAutoSaving(DefineValue.INACTIVE);
                        else
                            setStatusAutoSaving(DefineValue.ACTIVE);
                    }
                });
        delete_alert.setNegativeButton(getString(R.string.cancel), new
                DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                    }
                });
        delete_alert.show();
    }


    private void ShowDialogDelete(){
        AlertDialog.Builder delete_alert = new AlertDialog.Builder(getActivity());
        delete_alert.setTitle(getString(R.string.warning));
        delete_alert.setMessage(getString(R.string.alert_delete_autosaving));
        delete_alert.setPositiveButton(getString(R.string.ok), new
                DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                        deleteAutoSaving();
                    }
                });
        delete_alert.setNegativeButton(getString(R.string.cancel), new
                DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                    }
                });
        delete_alert.show();

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_process_autodeposit:
                ShowDialogProcess();
                break;
            case R.id.btn_delete_autodeposit:
                ShowDialogDelete();
                break;
        }
    }

    private void setStatusAutoSaving(String _status) {

        prodDialog.show();
        RequestParams params = MyApiClient.getSignatureWithParams(MyApiClient.COMM_ID, MyApiClient.LINK_SET_AUTOSAVING_STATUS,
                userID, accessKey);
        params.put(WebParams.COMM_ID, MyApiClient.COMM_ID);
        params.put(WebParams.MEMBER_ID, member_id);
        params.put(WebParams.USER_ID, userID);
        params.put(WebParams.STATUS, _status);
        params.put(WebParams.AUTO_ID, auto_id);

        Timber.d("parameter set status auto saving: " + params.toString());
        MyApiClient.setStatusAutoSaving(getActivity(), params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                try {
                    Timber.d("response set status auto saving: " + response.toString());
                    String code = response.getString(WebParams.ERROR_CODE);
                    if (code.equals(WebParams.SUCCESS_CODE)) {
                        if(status.equals(DefineValue.INACTIVE)){
                            isActive = true;
                            status = DefineValue.ACTIVE;
                            btn_proses.setText(getString(R.string.autosavingongoing_btn_inactive));
                            Toast.makeText(getActivity(), getString(R.string.autosavingongoing_toast_active), Toast.LENGTH_SHORT).show();
                        }else{
                            isActive = false;
                            status = DefineValue.INACTIVE;
                            btn_proses.setText(getString(R.string.autosavingongoing_btn_active));
                            Toast.makeText(getActivity(), R.string.autosavingongoing_toast_inactive, Toast.LENGTH_SHORT).show();
                        }

                    } else if (code.equals(WebParams.LOGOUT_CODE)) {
                        Timber.d("isi response autologout:" + response.toString());
                        String message = response.getString(WebParams.ERROR_MESSAGE);
                        AlertDialogLogout test = AlertDialogLogout.getInstance();
                        test.showDialoginActivity(getActivity(), message);
                    } else {
                        Timber.d("Error set status auto saving:" + response.toString());
                        code = response.getString(WebParams.ERROR_MESSAGE);
                        Toast.makeText(getActivity(), code, Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (prodDialog.isShowing()) {
                    prodDialog.dismiss();
                }

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                super.onFailure(statusCode, headers, responseString, throwable);
                failure(throwable);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                failure(throwable);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                failure(throwable);
            }

            private void failure(Throwable throwable) {
                if (getActivity() != null && !getActivity().isFinishing()) {
                    if (MyApiClient.PROD_FAILURE_FLAG)
                        Toast.makeText(getActivity(), getString(R.string.network_connection_failure_toast), Toast.LENGTH_SHORT).show();
                    else
                        Toast.makeText(getActivity(), throwable.toString(), Toast.LENGTH_SHORT).show();

                    if (prodDialog.isShowing()) {
                        prodDialog.dismiss();
                    }
                }
                Timber.w("Error Koneksi status auto saving:" + throwable.toString());
            }

        });
    }

    private void deleteAutoSaving() {

        prodDialog.show();
        RequestParams params = MyApiClient.getSignatureWithParams(MyApiClient.COMM_ID, MyApiClient.LINK_DELETE_AUTOSAVING,
                userID, accessKey);
        params.put(WebParams.COMM_ID, MyApiClient.COMM_ID);
        params.put(WebParams.MEMBER_ID, member_id);
        params.put(WebParams.USER_ID, userID);
        params.put(WebParams.AUTO_ID, auto_id);

        Timber.d("parameter delete auto saving: " + params.toString());
        MyApiClient.deleteAutoSaving(getActivity(), params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);

                try {
                    Timber.d("respose delete auto saving: " + response.toString());

                    String code = response.getString(WebParams.ERROR_CODE);
                    if (code.equals(WebParams.SUCCESS_CODE)) {
                        getActivity().finish();

                    } else if (code.equals(WebParams.LOGOUT_CODE)) {
                        Timber.d("isi response autologout:" + response.toString());
                        String message = response.getString(WebParams.ERROR_MESSAGE);
                        AlertDialogLogout test = AlertDialogLogout.getInstance();
                        test.showDialoginActivity(getActivity(), message);
                    } else {
                        Timber.d("Error delete auto saving:" + response.toString());
                        code = response.getString(WebParams.ERROR_MESSAGE);
                        Toast.makeText(getActivity(), code, Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if (prodDialog.isShowing()) {
                    prodDialog.dismiss();
                }

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                super.onFailure(statusCode, headers, responseString, throwable);
                failure(throwable);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                failure(throwable);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                failure(throwable);
            }

            private void failure(Throwable throwable) {
                if (getActivity() != null && !getActivity().isFinishing()) {
                    if (MyApiClient.PROD_FAILURE_FLAG)
                        Toast.makeText(getActivity(), getString(R.string.network_connection_failure_toast), Toast.LENGTH_SHORT).show();
                    else
                        Toast.makeText(getActivity(), throwable.toString(), Toast.LENGTH_SHORT).show();

                    if (prodDialog.isShowing()) {
                        prodDialog.dismiss();
                    }
                }
                Timber.w("Error Koneksi delete auto saving:" + throwable.toString());
            }

        });
    }


}
