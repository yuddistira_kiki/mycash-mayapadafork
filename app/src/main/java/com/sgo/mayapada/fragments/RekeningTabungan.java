package com.sgo.mayapada.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.securepreferences.SecurePreferences;
import com.sgo.mayapada.entityRealm.List_Account_Nabung;
import com.sgo.mayapada.R;
import com.sgo.mayapada.activities.TabunganActivity;
import com.sgo.mayapada.adapter.ListRekeningTabunganAdapter;
import com.sgo.mayapada.coreclass.CustomSecurePref;
import com.sgo.mayapada.coreclass.DefineValue;
import com.sgo.mayapada.coreclass.ErrorDefinition;
import com.sgo.mayapada.coreclass.MyApiClient;
import com.sgo.mayapada.coreclass.WebParams;
import com.sgo.mayapada.dialogs.AlertDialogFrag;
import com.sgo.mayapada.dialogs.DefinedDialog;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import in.srain.cube.views.ptr.PtrFrameLayout;
import in.srain.cube.views.ptr.PtrHandler;
import in.srain.cube.views.ptr.header.MaterialHeader;
import io.realm.Realm;
import io.realm.RealmResults;
import timber.log.Timber;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link RekeningTabungan.OnFragmentInteractionListener} interface
 * to handle interaction events.
 */
public class RekeningTabungan extends Fragment implements TambahRekTabungan.OnSuccessAdd, View.OnClickListener {

    public final static String TAG = "com.sgo.mayapada.fragments.RekeningTabungan";

    private OnFragmentInteractionListener mListener;
    private ListView lv;
    private ListRekeningTabunganAdapter listRekeningTabunganAdapter;
    private ArrayList<List_Account_Nabung> rek_tabungan_models;
    private Realm realm;
    private PtrFrameLayout ptrFrameLayout;
    private SecurePreferences sp;
    private String userID;
    private String accessKey;
    private String memberID;
    private ProgressDialog progressDialog;

    public RekeningTabungan() {
        // Required empty public constructor
    }

    private class SavingID{
        public String saving_id;
        public SavingID(String saving_id){
            this.saving_id = saving_id;
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        Timber.d("OncreateView");
        View v = inflater.inflate(R.layout.fragment_rekening_tabungan, container, false);
        lv = (ListView) v.findViewById(R.id.rek_listview);
        View layout_empty = v.findViewById(R.id.empty_layout);
        lv.setEmptyView(layout_empty);
        layout_empty.findViewById(R.id.btnRefresh).setOnClickListener(this);
        ptrFrameLayout = (PtrFrameLayout) v.findViewById(R.id.rotate_header_list_view_frame);

        final MaterialHeader header = new MaterialHeader(getActivity());
        int[] colors = getResources().getIntArray(R.array.google_colors);
        header.setColorSchemeColors(colors);
        header.setLayoutParams(new PtrFrameLayout.LayoutParams(-1, -2));
        header.setPadding(0, 15, 0, 10);
        header.setPtrFrameLayout(ptrFrameLayout);

        ptrFrameLayout.setHeaderView(header);
        ptrFrameLayout.addPtrUIHandler(header);
        ptrFrameLayout.setPtrHandler(new PtrHandler() {
            @Override
            public void onRefreshBegin(PtrFrameLayout frame) {
                retrieveAccountList();
            }

            @Override
            public boolean checkCanDoRefresh(PtrFrameLayout frame, View content, View header) {
                //return PtrDefaultHandler.checkContentCanBePulledDown(frame, content, header);
                return lv != null && (lv.getAdapter().getCount() == 0 || lv.getFirstVisiblePosition() == 0 && lv.getChildAt(0).getTop() == 0);

            }
        });
        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        Timber.d("onActivityCreated");
        setTitle();

        realm = Realm.getDefaultInstance();
        sp = CustomSecurePref.getInstance().getmSecurePrefs();

        userID = sp.getString(DefineValue.USERID_PHONE,"");
        accessKey = sp.getString(DefineValue.ACCESS_KEY,"");
        memberID = sp.getString(DefineValue.MEMBER_ID,"");

        progressDialog = DefinedDialog.CreateProgressDialog(getContext(),"");
        progressDialog.dismiss();

        if(rek_tabungan_models == null) {
            refreshDataList();
        }

        if(listRekeningTabunganAdapter == null) {
            listRekeningTabunganAdapter = new ListRekeningTabunganAdapter(getContext(), R.layout.list_rek_tabungan_item, rek_tabungan_models);
            listRekeningTabunganAdapter.setDeleteListener(new ListRekeningTabunganAdapter.OnDeleteListener() {
                @Override
                public void onCLick(int position, View view) {
                    showDialogDelete(position);
                }
            });
        }

        if(lv.getAdapter() == null) {
            lv.setChoiceMode(AbsListView.CHOICE_MODE_SINGLE);
            lv.setAdapter(listRekeningTabunganAdapter);
            lv.setOnItemClickListener(lvListener);
        }
    }

    private void refreshDataList(){
        RealmResults<List_Account_Nabung> list_account_nabungs = realm.where(List_Account_Nabung.class).findAll();
        if(rek_tabungan_models == null)
            rek_tabungan_models = new ArrayList<>();
        rek_tabungan_models.clear();
        rek_tabungan_models.addAll(realm.copyFromRealm(list_account_nabungs));
        if (listRekeningTabunganAdapter != null) {
            if(rek_tabungan_models.size() == 0) {
                listRekeningTabunganAdapter.setButtonDeleteHide();
            }
            listRekeningTabunganAdapter.notifyDataSetChanged();
        }
    }

    private AdapterView.OnItemClickListener lvListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            RadioButton itemcheck = (RadioButton) view.findViewById(R.id.radio_item_rek);
            Boolean tast = rek_tabungan_models.get(position).isDefault();
            if(!tast){
                itemcheck.setChecked(true);
                defaultAccountList(position, itemcheck);
            }
        }
    };


    private void setTitle(){
        if(getActivity() == null)
            return;

        TabunganActivity ta = (TabunganActivity)getActivity();
        ta.setToolbarTitle(getString(R.string.menu_item_title_manage_savings_acc));
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Timber.d("onattach");
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.rek_tabungan_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId())
        {
            case R.id.menu_item_add:
                changeFragmentAdd();
//                OnSuccessAddAccount(new Rek_Tabungan_model("test","018822814818","wkwkwkwk"));
                return true;
            case R.id.menu_item_delete:
                listRekeningTabunganAdapter.toggleButtonDelete();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void changeFragmentAdd(){
        Fragment fragment = new TambahRekTabungan();
        fragment.setTargetFragment(this,0);

        switchFragment(fragment,"",true, TambahRekTabungan.TAG);
    }

    private void switchFragment(android.support.v4.app.Fragment i, String name, Boolean isBackstack, String tag){
        if (getActivity() == null)
            return;

        TabunganActivity fca = (TabunganActivity ) getActivity();
        fca.switchContent(i,name,isBackstack,tag);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void OnSuccessAddAccount(int status) {
        switch (status){
            case TambahRekTabungan.STATUS_SUCCESS:
                refreshDataList();
                if(rek_tabungan_models.size()>0){
                    List_Account_Nabung tempAccountNabung = realm.where(List_Account_Nabung.class).equalTo(WebParams.IS_DEFAULT,DefineValue.STRING_YES).findFirst();
                    if(tempAccountNabung != null)
                        mListener.onSelectDefaultRek(tempAccountNabung,rek_tabungan_models.size());
                }
                break;
            case TambahRekTabungan.STATUS_ERROR:
                break;
        }
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.btnRefresh:
                ptrFrameLayout.autoRefresh();
                break;
        }
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onSelectDefaultRek(List_Account_Nabung rek_tabungan_model, int sizeList);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Timber.d("oncreate");
    }

    @Override
    public void onDestroy() {
        if(realm!=null)
            realm.close();
        super.onDestroy();
    }

    private void retrieveAccountList(){
        try{
            RequestParams params = MyApiClient.getSignatureWithParams(MyApiClient.COMM_ID,MyApiClient.LINK_SAVING_ACCOUNT_LIST,
                    userID,accessKey);
            params.put(WebParams.MEMBER_ID, memberID);
            params.put(WebParams.USER_ID, userID);
            params.put(WebParams.COMM_ID, MyApiClient.COMM_ID);

            Timber.d("isi params listsavingaccount:" + params.toString());

            MyApiClient.sentListSavingAccount(getActivity(), params, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    try {
                        String code = response.getString(WebParams.ERROR_CODE);
                        Timber.d("Isi response listsavingaccount: "+response.toString());
                        if (code.equals(WebParams.SUCCESS_CODE)) {
                            JSONArray account = response.optJSONArray(WebParams.ACCOUNT);
                            if(account != null && account.length() > 0){

                                realm.beginTransaction();
                                realm.delete(List_Account_Nabung.class);

                                List_Account_Nabung list_account_nabung;

                                for (int i= 0; i < account.length(); i++) {
                                    try {
                                        list_account_nabung = realm.createObjectFromJson(List_Account_Nabung.class, account.getJSONObject(i));
                                        list_account_nabung.insertLastUpdate();

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                        realm.cancelTransaction();
                                    }
                                }
                                realm.commitTransaction();
                                refreshDataList();
                            }
                        }
                        else if(code.equals(ErrorDefinition.NO_TRANSACTION)) {
                            realm.executeTransaction(new Realm.Transaction() {
                                @Override
                                public void execute(Realm realm) {
                                    realm.delete(List_Account_Nabung.class);
                                }
                            });
                            refreshDataList();
                        }else {
                            code = response.getString(WebParams.ERROR_MESSAGE);
                            Toast.makeText(getActivity(),code,Toast.LENGTH_SHORT).show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    finally {
                        ptrFrameLayout.refreshComplete();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    super.onFailure(statusCode, headers, responseString, throwable);
                    failure(throwable);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    failure(throwable);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    failure(throwable);
                }

                private void failure(Throwable throwable) {
                    if(MyApiClient.PROD_FAILURE_FLAG)
                        Toast.makeText(getActivity(), getString(R.string.network_connection_failure_toast), Toast.LENGTH_SHORT).show();
                    else
                        Toast.makeText(getActivity(), throwable.toString(), Toast.LENGTH_SHORT).show();
                    ptrFrameLayout.refreshComplete();
                    Timber.w("Error Koneksi listsavingaccount:"+throwable.toString());
                }
            });
        }catch (Exception e){
            Log.d("httpclient:",e.getMessage());
        }
    }

    private void deleteAccountList(final int position){
        try{
            progressDialog.show();

            List<SavingID> acctIDs = new ArrayList<>();
            acctIDs.add(new SavingID(rek_tabungan_models.get(position).getSaving_id()));
            GsonBuilder gsonBuilder = new GsonBuilder();
            gsonBuilder.setPrettyPrinting();
            Gson gson = gsonBuilder.create();
            String testJson = gson.toJson(acctIDs);

            RequestParams params = MyApiClient.getSignatureWithParams(MyApiClient.COMM_ID,MyApiClient.LINK_DELETE_ACCOUNT_LIST,
                    userID,accessKey);
            params.put(WebParams.MEMBER_ID, memberID);
            params.put(WebParams.USER_ID, userID);
            params.put(WebParams.COMM_ID, MyApiClient.COMM_ID);
            params.put(WebParams.DATA, testJson);


            Timber.d("isi params deleteAccountList:" + params.toString());

            MyApiClient.sentDeleteSavingAccount(getActivity(), params, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    try {
                        String code = response.getString(WebParams.ERROR_CODE);
                        Timber.d("Isi response deleteAccountList: "+response.toString());
                        if (code.equals(WebParams.SUCCESS_CODE)) {
                            List_Account_Nabung test = realm.where(List_Account_Nabung.class)
                                    .equalTo(WebParams.SAVING_ID,rek_tabungan_models.get(position).getSaving_id())
                                    .findFirst();
                            realm.beginTransaction();
                            test.deleteFromRealm();
                            realm.commitTransaction();
                            if(rek_tabungan_models.get(position).isDefault())
                                mListener.onSelectDefaultRek(null,rek_tabungan_models.size());
                            rek_tabungan_models.remove(position);
                            refreshDataList();
                        } else {
                            code = response.getString(WebParams.ERROR_MESSAGE);
                            Toast.makeText(getActivity(),code,Toast.LENGTH_SHORT).show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    finally {
                        progressDialog.dismiss();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    super.onFailure(statusCode, headers, responseString, throwable);
                    failure(throwable);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    failure(throwable);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    failure(throwable);
                }

                private void failure(Throwable throwable) {
                    if(MyApiClient.PROD_FAILURE_FLAG)
                        Toast.makeText(getActivity(), getString(R.string.network_connection_failure_toast), Toast.LENGTH_SHORT).show();
                    else
                        Toast.makeText(getActivity(), throwable.toString(), Toast.LENGTH_SHORT).show();
                    progressDialog.dismiss();
                    Timber.w("Error Koneksi deleteAccountList:"+throwable.toString());
                }
            });
        }catch (Exception e){
            Log.d("httpclient:",e.getMessage());
        }
    }

    private void defaultAccountList(final int position, final RadioButton radioButton){
        try{
            progressDialog.show();

            RealmResults<List_Account_Nabung> list_account_nabungs = realm
                    .where(List_Account_Nabung.class)
                    .findAll();
            if(list_account_nabungs.size() > 0){
                realm.beginTransaction();
                for(List_Account_Nabung list_account_nabung : list_account_nabungs){
                    if(list_account_nabung.getSaving_id().equals(rek_tabungan_models.get(position).getSaving_id()))
                        list_account_nabung.setDefault(true);
                    else
                        list_account_nabung.setDefault(false);
                }

            }
            else {
                Toast.makeText(getContext(),getString(R.string.problem_has_occurred),Toast.LENGTH_SHORT).show();
                return;
            }

            RequestParams params = MyApiClient.getSignatureWithParams(MyApiClient.COMM_ID,MyApiClient.LINK_DEFAULT_ACCOUNT_LIST,
                    userID,accessKey);
            params.put(WebParams.MEMBER_ID, memberID);
            params.put(WebParams.USER_ID, userID);
            params.put(WebParams.COMM_ID, MyApiClient.COMM_ID);
            params.put(WebParams.SAVING_ID, rek_tabungan_models.get(position).getSaving_id());


            Timber.d("isi params defaultAccount:" + params.toString());

            MyApiClient.sentDefaultSavingAccount(getActivity(), params, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    try {
                        String code = response.getString(WebParams.ERROR_CODE);
                        Timber.d("Isi response defaultAccount: "+response.toString());
                        if (code.equals(WebParams.SUCCESS_CODE)) {
                            if(realm.isInTransaction())
                                realm.commitTransaction();
                            refreshDataList();
                            mListener.onSelectDefaultRek(rek_tabungan_models.get(position),rek_tabungan_models.size());
                        } else {
                            code = response.getString(WebParams.ERROR_MESSAGE);
                            Toast.makeText(getActivity(),code,Toast.LENGTH_SHORT).show();
                            radioButton.setChecked(false);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    finally {
                        progressDialog.dismiss();
                        if(realm.isInTransaction())
                            realm.cancelTransaction();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    super.onFailure(statusCode, headers, responseString, throwable);
                    failure(throwable);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    failure(throwable);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    failure(throwable);
                }

                private void failure(Throwable throwable) {
                    if(MyApiClient.PROD_FAILURE_FLAG)
                        Toast.makeText(getActivity(), getString(R.string.network_connection_failure_toast), Toast.LENGTH_SHORT).show();
                    else
                        Toast.makeText(getActivity(), throwable.toString(), Toast.LENGTH_SHORT).show();
                    radioButton.setChecked(false);
                    listRekeningTabunganAdapter.notifyDataSetChanged();
                    if(realm.isInTransaction())
                        realm.cancelTransaction();
                    progressDialog.dismiss();
                    Timber.w("Error Koneksi defaultAccount:"+throwable.toString());
                }
            });
        }catch (Exception e){
            Log.d("httpclient:",e.getMessage());
        }
    }

    private void showDialogDelete(final int position){
        String msg = getString(R.string.dialog_msg_delete_account);
        if(rek_tabungan_models.get(position).isDefault())
            msg = getString(R.string.dialog_msg_is_default_delete) + msg ;

        AlertDialogFrag dialog_frag = AlertDialogFrag.newInstance(getString(R.string.dialog_title_delete_account),
                msg,getString(R.string.yes),getString(R.string.no),false);
        dialog_frag.setOkListener(new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                deleteAccountList(position);
            }
        });
        dialog_frag.setTargetFragment(RekeningTabungan.this, 0);
        dialog_frag.show(getActivity().getSupportFragmentManager(), AlertDialogFrag.TAG);
    }


}
