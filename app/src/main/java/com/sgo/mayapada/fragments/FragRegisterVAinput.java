package com.sgo.mayapada.fragments;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.securepreferences.SecurePreferences;
import com.sgo.mayapada.R;
import com.sgo.mayapada.activities.RegisterVaActivity;
import com.sgo.mayapada.coreclass.CustomSecurePref;
import com.sgo.mayapada.coreclass.DefineValue;
import com.sgo.mayapada.coreclass.MyApiClient;
import com.sgo.mayapada.coreclass.WebParams;
import com.sgo.mayapada.dialogs.DefinedDialog;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import timber.log.Timber;

/**
 * Created by User on 6/13/2017.
 */

public class FragRegisterVAinput extends Fragment {
    private View v;

    private SecurePreferences sp;
    private String user_id;
    private String access_key;
    private String response;
    private String pairing_id_input;
    private ImageView spinning_wheel_comm;
    private Spinner commlist_spinner;
    private ArrayList<String> list_comm_data;
    private ArrayAdapter<String> comm_spinner_adapter;
    private EditText pairing_id_field;
    private JSONArray arrayData;
    private JSONArray list;
    private Button proses_btn;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.frag_register_va_input, container, false);

        spinning_wheel_comm = (ImageView) v.findViewById(R.id.spinning_wheel_community);
        commlist_spinner = (Spinner) v.findViewById(R.id.spinner_commList);
        pairing_id_field = (EditText) v.findViewById(R.id.pairing_id_value);
        proses_btn = (Button) v.findViewById(R.id.btn_submit_register_va);

        return v;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        sp = CustomSecurePref.getInstance().getmSecurePrefs();
        user_id = sp.getString(DefineValue.USERID_PHONE, "");
        access_key = sp.getString(DefineValue.ACCESS_KEY, "");
        response = getArguments().getString("response", "");
        try {
            arrayData = new JSONArray(response);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        list_comm_data = new ArrayList<>();
        list = new JSONArray();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        spinning_wheel_comm.setVisibility(View.VISIBLE);
        commlist_spinner.setVisibility(View.INVISIBLE);
        comm_spinner_adapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, list_comm_data);
        comm_spinner_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        commlist_spinner.setAdapter(comm_spinner_adapter);
        commlist_spinner.setOnItemSelectedListener(comm_spinner_item_selected_listener);

        if (!response.equals("")) {
            try {
                if (list_comm_data.size() > 0)
                    list_comm_data.clear();
                for (int i = 0; i < arrayData.length(); i++) {
                    JSONObject mObj = arrayData.getJSONObject(i);
                    String is_pairing = mObj.optString("is_pairing");
                    if (is_pairing.equals("Y")) {
                        list_comm_data.add(mObj.optString(WebParams.COMM_NAME));
                        list.put(arrayData.get(i));
                    }
                }
                spinning_wheel_comm.setVisibility(View.INVISIBLE);
                commlist_spinner.setVisibility(View.VISIBLE);
                comm_spinner_adapter.notifyDataSetChanged();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        proses_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkInput())
                    pairingInquiry();
            }
        });
    }

    private boolean checkInput(){
        try {
            if (list.getJSONObject(commlist_spinner.getSelectedItemPosition()).optString("pairing_id").equals("null")) {
                if (pairing_id_field.getText().toString().equals("")){
                    pairing_id_field.setError("Isi field pairing id terlebih dahulu!");
                    return false;
                }
                pairing_id_input = pairing_id_field.getText().toString();
            }else pairing_id_input = list.getJSONObject(commlist_spinner.getSelectedItemPosition()).optString("pairing_id");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return true;
    }

    private Spinner.OnItemSelectedListener comm_spinner_item_selected_listener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            try {
//                String pairing_id = arrayData.getJSONObject(position).optString("pairing_id");
                String pairing_id = list.getJSONObject(position).optString("pairing_id");
                if (pairing_id.equals("null")){
                    pairing_id_field.setText("");
                }else {
                    pairing_id_field.setText(pairing_id);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };

    private void pairingInquiry() {
        final ProgressDialog progdialog = DefinedDialog.CreateProgressDialog(getActivity(), "");
        try {
            final RequestParams params = MyApiClient.getSignatureWithParams(MyApiClient.COMM_ID, MyApiClient.LINK_UOB_PAIRING_REQUEST,
                    user_id, access_key);
            params.put(WebParams.COMM_ID, list.getJSONObject(commlist_spinner.getSelectedItemPosition()).optString("comm_id"));
            params.put(WebParams.COMM_ID_EMO,MyApiClient.COMM_ID );
            params.put(WebParams.USER_ID, user_id);
            params.put(WebParams.MEMBER_ID, sp.getString(DefineValue.MEMBER_ID, ""));
            params.put(WebParams.PAIRING_ID, pairing_id_input);

            Timber.d("isi params pairing request:" + params.toString());

            MyApiClient.sentPairingRequest(getActivity(), params, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                    try {
                        Timber.d("isi response pairing request: " + response.toString());
                        String code = response.getString(WebParams.ERROR_CODE);
                        if (code.equals(WebParams.SUCCESS_CODE)) {
                            Fragment frag = new FragRegPairingidDesciption();
                            Bundle bundle = new Bundle();
//                            String pair_id = list.getJSONObject(commlist_spinner.getSelectedItemPosition()).optString("pairing_id");
//                            if (pair_id.equals("null")){
//                                bundle.putString("pairing_id", pairing_id_field.getText().toString());
//                            }else
//                                bundle.putString("pairing_id", pair_id);
                            bundle.putString("pairing_id", pairing_id_input);

                            bundle.putString("data", list.get(commlist_spinner.getSelectedItemPosition()).toString());
                            frag.setArguments(bundle);
                            switchFragment(frag, "regist_pairing_describ", "Register Pairing", true, "regist_pairing_describ");
                        }else {
                            Toast.makeText(getActivity(), response.getString(WebParams.ERROR_MESSAGE), Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (progdialog.isShowing())
                        progdialog.dismiss();
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    super.onFailure(statusCode, headers, responseString, throwable);
                    failure(throwable);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    failure(throwable);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    failure(throwable);
                }

                private void failure(Throwable throwable) {
                    if (MyApiClient.PROD_FAILURE_FLAG)
                        Toast.makeText(getActivity(), getString(R.string.network_connection_failure_toast), Toast.LENGTH_SHORT).show();
                    else
                        Toast.makeText(getActivity(), throwable.toString(), Toast.LENGTH_SHORT).show();

                    Timber.w("Error Koneksi get comm inv:" + throwable.toString());
                    if (progdialog.isShowing())
                        progdialog.dismiss();
                }
            });
        }catch (Exception e){
            Timber.d("httpclient:" + e.getMessage());
            if (progdialog.isShowing())
                progdialog.dismiss();
        }
    }

    private void switchFragment(android.support.v4.app.Fragment i, String name,String next_name, Boolean isBackstack, String tag){
        if (getActivity() == null)
            return;

        RegisterVaActivity fca = (RegisterVaActivity) getActivity();
        fca.switchContent(i,name,next_name,isBackstack,tag);
    }
}
