package com.sgo.mayapada.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import com.securepreferences.SecurePreferences;
import com.sgo.mayapada.R;
import com.sgo.mayapada.activities.CashoutActivity;
import com.sgo.mayapada.activities.InsertPIN;
import com.sgo.mayapada.activities.MainPage;
import com.sgo.mayapada.coreclass.CurrencyFormat;
import com.sgo.mayapada.coreclass.CustomSecurePref;
import com.sgo.mayapada.coreclass.DefineValue;
import com.sgo.mayapada.coreclass.WebParams;
import com.sgo.mayapada.interfaces.OnLoadDataListener;
import com.sgo.mayapada.loader.UtilsLoader;

import java.util.ArrayList;
import java.util.StringTokenizer;

import timber.log.Timber;

/**
 * Created by thinkpad on 8/21/2017.
 */

public class CashOutTarikDana extends Fragment {
    private View v;
    private SecurePreferences sp;
    private String userID, accessKey;
    private int attempt=-1;
    private ArrayList<String> dataNominal;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        sp = CustomSecurePref.getInstance().getmSecurePrefs();
        userID = sp.getString(DefineValue.USERID_PHONE,"");
        accessKey = sp.getString(DefineValue.ACCESS_KEY,"");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.frag_cash_out, container, false);
        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        String minCashout = sp.getString(WebParams.NOMINAL_CASHOUT_ATM,"");
        String maxCashout = sp.getString(WebParams.MAX_CASHOUT_ATM_PERTRX,"");

        Spinner spNominal = (Spinner) v.findViewById(R.id.cashout_spinner_nominal);
        Button btnProses = (Button) v.findViewById(R.id.cashout_btn_buat_kode_kupon);
        btnProses.setOnClickListener(btnProsesListener);

        dataNominal = new ArrayList<>();
        long nominal=0;
        do{
            nominal += Long.parseLong(minCashout);
            dataNominal.add(CurrencyFormat.format(nominal));
        }while(nominal < Long.parseLong(maxCashout));
        ArrayAdapter<String> nominalAdapter = new ArrayAdapter<>(getActivity(),
                android.R.layout.simple_spinner_item, dataNominal);
        nominalAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spNominal.setAdapter(nominalAdapter);
        spNominal.setOnItemSelectedListener(nominalListener);

        new UtilsLoader(getActivity(),sp).getFailedPIN(userID,new OnLoadDataListener() { // get pin attempt
            @Override
            public void onSuccess(Object deData) {
                attempt = (int) deData;
            }

            @Override
            public void onFail(Bundle message) {

            }

            @Override
            public void onFailure(String message) {

            }
        });
    }

    private Spinner.OnItemSelectedListener nominalListener = new Spinner.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
            String nominal="";
            StringTokenizer tokensDecimal = new StringTokenizer(dataNominal.get(position).toString(), ",");
            String currencyNominal = tokensDecimal.nextToken();
            StringTokenizer tokens = new StringTokenizer(currencyNominal, ".");
            for(int i=0 ; i<=tokens.countTokens()+1 ; i++) {
                nominal += tokens.nextToken();
            }
            Toast.makeText(getActivity(),nominal,Toast.LENGTH_LONG).show();
        }

        @Override
        public void onNothingSelected(AdapterView<?> adapterView) {

        }
    };

    private Button.OnClickListener btnProsesListener = new Button.OnClickListener() {
        @Override
        public void onClick(View view) {
            Intent i = new Intent(getActivity(), InsertPIN.class);
            if(attempt != -1 && attempt < 2)
                i.putExtra(DefineValue.ATTEMPT,attempt);
            startActivityForResult(i, MainPage.REQUEST_FINISH);
        }
    };

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
//        Timber.d("onActivity result"+ " Cashout Fragment"+" / "+requestCode+" / "+resultCode);
        if(requestCode == MainPage.REQUEST_FINISH){
//            Timber.d("onActivity result" + "Cashout Fragment masuk request exit");
            if(resultCode == InsertPIN.RESULT_PIN_VALUE){
                String value_pin = data.getStringExtra(DefineValue.PIN_VALUE);
//                Timber.d("onActivity result" + "Cashout Fragment result pin value");
                openCouponCode();
            }
        }
    }

    private void openCouponCode() {
        Fragment newFrag = new CashoutCouponCode();
        switchContent(newFrag, CashoutCouponCode.TAG);
    }

    private void switchContent(Fragment mFrag, String tag){
        if (getActivity() == null)
            return;

        CashoutActivity fca = (CashoutActivity) getActivity();
        fca.switchContent(mFrag, getString(R.string.menu_item_title_cash_out), true, tag);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                getActivity().finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}