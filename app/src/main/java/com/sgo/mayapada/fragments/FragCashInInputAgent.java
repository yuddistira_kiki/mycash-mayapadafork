package com.sgo.mayapada.fragments;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.securepreferences.SecurePreferences;
import com.sgo.mayapada.R;
import com.sgo.mayapada.activities.AgentActivity;
import com.sgo.mayapada.coreclass.CustomSecurePref;
import com.sgo.mayapada.coreclass.DefineValue;
import com.sgo.mayapada.coreclass.InetHandler;
import com.sgo.mayapada.coreclass.MyApiClient;
import com.sgo.mayapada.coreclass.WebParams;
import com.sgo.mayapada.dialogs.AlertDialogLogout;
import com.sgo.mayapada.dialogs.DefinedDialog;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import timber.log.Timber;

/**
 * Created by thinkpad on 7/14/2016.
 */
public class FragCashInInputAgent extends Fragment {
    private View v;
    private SecurePreferences sp;
    private EditText etHpNo;
    private EditText etAmount;
    private EditText etMessage;
    private Button btnProses;
    private ProgressDialog progdialog;
    private String userID;
    private String accessKey;
    private String memberID;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        sp = CustomSecurePref.getInstance().getmSecurePrefs();
        userID = sp.getString(DefineValue.USERID_PHONE, "");
        accessKey = sp.getString(DefineValue.ACCESS_KEY, "");
        memberID = sp.getString(DefineValue.MEMBER_ID,"");

        etHpNo = (EditText) v.findViewById(R.id.cashinagent_value_hp);
        etAmount = (EditText) v.findViewById(R.id.cashinagent_value_amount);
        etMessage = (EditText) v.findViewById(R.id.cashinagent_value_message);
        btnProses = (Button) v.findViewById(R.id.btn_proses);

        btnProses.setOnClickListener(btnProsesListener);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.frag_cash_in_input_agent, container, false);
        return v;
    }

    private Button.OnClickListener btnProsesListener = new Button.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (InetHandler.isNetworkAvailable(getActivity())) {
                if(inputValidation()) {
                    requestCashIn();
                }
            }
            else DefinedDialog.showErrorDialog(getActivity(), getString(R.string.inethandler_dialog_message));
        }
    };

    private void requestCashIn(){
        try{
            progdialog = DefinedDialog.CreateProgressDialog(getActivity(), "");
            progdialog.show();

            RequestParams params;
            params = MyApiClient.getSignatureWithParams(MyApiClient.COMM_ID, MyApiClient.LINK_REQUEST_CASH_IN,
                    userID, accessKey);

            params.put(WebParams.MEMBER_ID, memberID);
            params.put(WebParams.MOBILEPHONE, etHpNo.getText().toString());
            params.put(WebParams.COMM_ID, MyApiClient.COMM_ID);
            params.put(WebParams.AMOUNT, etAmount.getText().toString());
            params.put(WebParams.PAYMENT_REMARK, etMessage.getText().toString());
            params.put(WebParams.USER_ID, userID);
            params.put(WebParams.CCY_ID, MyApiClient.CCY_VALUE);
            Timber.d("isi params sent request cash in:" + params.toString());

            MyApiClient.sentRequestCashIn(getActivity(), params, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    progdialog.dismiss();
                    Timber.d("isi response sent request cash in:" + response.toString());

                    try {
                        String code = response.getString(WebParams.ERROR_CODE);
                        if (code.equals(WebParams.SUCCESS_CODE)) {
                            Bundle args = new Bundle();
                            args.putString(DefineValue.TX_ID, response.optString(WebParams.TX_ID, ""));
                            args.putString(DefineValue.MEMBER_CUST_TO, response.optString(WebParams.MEMBER_CUST_TO, ""));
                            args.putString(DefineValue.MEMBER_NAME_TO, response.optString(WebParams.MEMBER_NAME_TO, ""));
                            args.putString(DefineValue.AMOUNT, response.optString(WebParams.AMOUNT, ""));
                            args.putString(DefineValue.MESSAGE, response.optString(WebParams.PAYMENT_REMARK, "-"));
                            args.putString(DefineValue.FEE, response.optString(WebParams.ADMIN_FEE, ""));
                            args.putString(DefineValue.TOTAL_AMOUNT, response.optString(WebParams.TOTAL, ""));

                            Fragment newFrag = new FragCashInDescriptionAgent();
                            newFrag.setArguments(args);
                            switchFragment(newFrag, getString(R.string.toolbar_title_cashin), true);
                        }
                        else if(code.equals(WebParams.LOGOUT_CODE)){
                            String message = response.getString(WebParams.ERROR_MESSAGE);
                            AlertDialogLogout test = AlertDialogLogout.getInstance();
                            test.showDialoginActivity(getActivity(),message);
                        }
                        else {
                            code = response.getString(WebParams.ERROR_MESSAGE);
                            Toast.makeText(getActivity(), code, Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    super.onFailure(statusCode, headers, responseString, throwable);
                    failure(throwable);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    failure(throwable);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    failure(throwable);
                }

                private void failure(Throwable throwable){
                    if(MyApiClient.PROD_FAILURE_FLAG)
                        Toast.makeText(getActivity(), getString(R.string.network_connection_failure_toast), Toast.LENGTH_SHORT).show();
                    else
                        Toast.makeText(getActivity(), throwable.toString(), Toast.LENGTH_SHORT).show();
                    if (progdialog.isShowing())
                        progdialog.dismiss();

                    Timber.w("Error Koneksi sent request cash in:"+throwable.toString());
                }
            });
        }catch (Exception e){
            Timber.d("httpclient:"+e.getMessage());
        }
    }

    private boolean inputValidation(){
        if(etHpNo.getText().toString().length()==0){
            etHpNo.requestFocus();
            etHpNo.setError(getString(R.string.cashinagent_hp_no_validation));
            return false;
        }
        if(etAmount.getText().toString().length()==0){
            etAmount.requestFocus();
            etAmount.setError(getString(R.string.cashinagent_amount_validation));
            return false;
        } else if(Long.parseLong(etAmount.getText().toString()) < 1){
            etAmount.requestFocus();
            etAmount.setError(getString(R.string.cashinagent_amount_zero));
            return false;
        }
        return true;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                getActivity().finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void switchFragment(Fragment i, String name, Boolean isBackstack){
        if (getActivity() == null)
            return;

        AgentActivity fca = (AgentActivity ) getActivity();
        fca.switchContent(i,name,isBackstack);
    }

    @Override
    public void onResume() {
        super.onResume();
        setTitle(getString(R.string.toolbar_title_cashin));
    }

    private void setTitle(String _title){
        if (getActivity() == null)
            return;

        AgentActivity fca = (AgentActivity) getActivity();
        fca.setTitleFragment(_title);
    }
}
