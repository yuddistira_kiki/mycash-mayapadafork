package com.sgo.mayapada.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.securepreferences.SecurePreferences;
import com.sgo.mayapada.R;
import com.sgo.mayapada.activities.MainPage;
import com.sgo.mayapada.activities.TimelineDetailActivity;
import com.sgo.mayapada.adapter.TimeLineRecycleAdapter;
import com.sgo.mayapada.coreclass.BaseFragmentMainPage;
import com.sgo.mayapada.coreclass.CoreApp;
import com.sgo.mayapada.coreclass.CustomSecurePref;
import com.sgo.mayapada.coreclass.DateTimeFormat;
import com.sgo.mayapada.coreclass.DefineValue;
import com.sgo.mayapada.coreclass.MyApiClient;
import com.sgo.mayapada.coreclass.WebParams;
import com.sgo.mayapada.dialogs.AlertDialogLogout;
import com.sgo.mayapada.entityDAO.DaoSession;
import com.sgo.mayapada.entityDAO.TimelineData;
import com.sgo.mayapada.entityDAO.TimelineDataDao;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import in.srain.cube.views.ptr.PtrFrameLayout;
import timber.log.Timber;

/*
  Created by Administrator on 12/2/2014.
 */
public class TimeLine extends BaseFragmentMainPage {

    private SecurePreferences sp;

    private RecyclerView currentRecyclerView;
    private TimeLineRecycleAdapter currentAdapter;
    private LinearLayoutManager currentLayoutManag;

    private LinearLayout layout_alert, layout_list;
    private Button btnRefresh;
    private TextView txtAlert;
    private ImageView imgAlert;

    private String _ownerID,accessKey;
    private String page = "0";
    private String privacy = "2";
    private String isTimelineNew;

    private List<TimelineData> listTimelineData;
    private int start = 0;
    private RecyclerView mRecyclerView;
    private TimelineDataDao timelineDataDao;

    /*@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.frag_time_line, container, false);
        return v;
    }*/

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DaoSession daoSession = CoreApp.get_instance().getDaoSession();
        timelineDataDao = daoSession.getTimelineDataDao();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        TimeLineRecycleAdapter mAdapter;
        sp = CustomSecurePref.getInstance().getmSecurePrefs();
        _ownerID = sp.getString(DefineValue.USERID_PHONE, "");
        accessKey = sp.getString(DefineValue.ACCESS_KEY, "");
        isTimelineNew = sp.getString(DefineValue.TIMELINE_FIRST_TIME,"");

        layout_alert = (LinearLayout) mView.findViewById(R.id.layout_alert_timeline);
        layout_list = (LinearLayout) mView.findViewById(R.id.layout_list_timeline);
        btnRefresh = (Button) mView.findViewById(R.id.btnRefresh);
        txtAlert = (TextView) mView.findViewById(R.id.txt_alert);
        imgAlert = (ImageView) mView.findViewById(R.id.img_alert);

        listTimelineData = new ArrayList<>();

        mRecyclerView = (RecyclerView)mView.findViewById(R.id.timeline_recycle_list);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity());

        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mLayoutManager.scrollToPosition(0);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setHasFixedSize(true);

        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mAdapter = new TimeLineRecycleAdapter(listTimelineData, R.layout.list_recycle_timeline_item,
                getActivity(),getFragmentManager(),timelineDataDao);
        mRecyclerView.setAdapter(mAdapter);

        mAdapter.SetOnItemClickListener(new TimeLineRecycleAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Intent i = new Intent(getActivity(), TimelineDetailActivity.class);
                i.putExtra(DefineValue.POST_ID, Integer.toString(listTimelineData.get(position).getTimeline_id()));
                switchActivity(i);
            }
        });

        btnRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                page = "0";
                getTimelineList(null, 0);
            }
        });

        setCurrentRecyclerView(mRecyclerView);
        setCurrentLayoutManag(mLayoutManager);
        setCurrentAdapter(mAdapter);

        if(isTimelineNew.equals(DefineValue.NO)){
            initializeDataPost();
        }
        getTimelineList(null,0);

    }

    private void initializeDataPost(){
        listTimelineData.addAll(TimelineData.getAllTimelineData(timelineDataDao));
        if(listTimelineData.size() > 0) {
            layout_alert.setVisibility(View.GONE);
            layout_list.setVisibility(View.VISIBLE);
        }
        else {
            layout_alert.setVisibility(View.VISIBLE);
            layout_list.setVisibility(View.GONE);
            txtAlert.setText("Data not found");
            imgAlert.setImageResource(R.drawable.ic_data_not_found);
        }
        getCurrentAdapter().notifyDataSetChanged();
    }

    private void getTimelineList(final PtrFrameLayout frameLayout, final int mPage) {
        try {

            sp = CustomSecurePref.getInstance().getmSecurePrefs();
            _ownerID = sp.getString(DefineValue.USERID_PHONE,"");
            accessKey = sp.getString(DefineValue.ACCESS_KEY,"");

            RequestParams params = MyApiClient.getSignatureWithParams(MyApiClient.COMM_ID,MyApiClient.LINK_TIMELINE_LIST,
                    _ownerID,accessKey);
            params.put(WebParams.USER_ID, _ownerID);
            params.put(WebParams.PRIVACY, privacy);
            params.put(WebParams.DATETIME, DateTimeFormat.getCurrentDate());
            params.put(WebParams.PAGE, mPage);
            params.put(WebParams.COUNT, DefineValue.COUNT);
            params.put(WebParams.COMM_ID, MyApiClient.COMM_ID);

            Timber.d("isi params get timeline list:" + params.toString());

            MyApiClient.getTimelineList(getActivity(),params, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                    try {
                        String code = response.getString(WebParams.ERROR_CODE);
                        String count = response.getString(WebParams.COUNT);

                        if (code.equals(WebParams.SUCCESS_CODE) && !count.equals("0")) {
                            Timber.d("isi params timeline list:"+response.toString());
//                            Log.d("list listTimeline", Integer.toString(listTimeline.size()));

                            List<TimelineData> mListTimelineData = new ArrayList<>();

                            JSONArray mArrayPost = new JSONArray(response.getString(WebParams.DATA_POSTS));
                            for (int i = 0; i < mArrayPost.length(); i++) {
                                int id = Integer.parseInt(mArrayPost.getJSONObject(i).getString(WebParams.ID));

                                boolean flagSame = false;

                                // cek apakah ada id yang sama.. kalau ada tidak dimasukan ke array
                                if (listTimelineData.size() > 0) {
                                    for (TimelineData aListTimelineData : listTimelineData) {
                                        if (aListTimelineData.getTimeline_id() != id) {
                                            flagSame = false;
                                        } else {
                                            flagSame = true;
                                            if(!aListTimelineData.getNumlikes().equals(mArrayPost.getJSONObject(i).getString(WebParams.NUMLIKES))) {
                                                TimelineData.updateTimelineDataLikes(timelineDataDao,String.valueOf(id),"",
                                                        mArrayPost.getJSONObject(i).optString(WebParams.NUMLIKES,""),"");
                                            }

                                            if(!aListTimelineData.getNumcomments().equals(mArrayPost.getJSONObject(i).getString(WebParams.NUMCOMMENTS))) {
                                                TimelineData timelineData = TimelineData.getTimelineByPostID(timelineDataDao,String.valueOf(id));
                                                timelineData.setNumcomments(mArrayPost.getJSONObject(i).getString(WebParams.NUMCOMMENTS));
                                                if(!mArrayPost.getJSONObject(i).getString(WebParams.COMMENTS).equals("")) {
                                                    timelineData.setComments(mArrayPost.getJSONObject(i).getString(WebParams.COMMENTS));
                                                    JSONArray mArrayComment = new JSONArray(mArrayPost.getJSONObject(i).getString(WebParams.COMMENTS));
                                                    int lengthComment = mArrayComment.length();
                                                    if (lengthComment == 1) {
                                                        for (int index = 0; index < mArrayComment.length(); index++) {
                                                            timelineData.setComment_id_1(mArrayComment.getJSONObject(index).getString(WebParams.COMMENT_ID));
                                                            timelineData.setFrom_name_1(mArrayComment.getJSONObject(index).getString(WebParams.FROM_NAME));
                                                            timelineData.setFrom_profile_picture_1(mArrayComment.getJSONObject(index).getString(WebParams.FROM_PROFILE_PICTURE));
                                                            timelineData.setReply_1(mArrayComment.getJSONObject(index).getString(WebParams.REPLY));

                                                            timelineData.setComment_id_2("");
                                                            timelineData.setFrom_name_2("");
                                                            timelineData.setFrom_profile_picture_2("");
                                                            timelineData.setReply_2("");

                                                        }
                                                    }
                                                    if (lengthComment == 2) {
                                                        for (int index = 0; index < mArrayComment.length(); index++) {
                                                            if (index == 0) {
                                                                timelineData.setComment_id_1(mArrayComment.getJSONObject(index).getString(WebParams.COMMENT_ID));
                                                                timelineData.setFrom_name_1(mArrayComment.getJSONObject(index).getString(WebParams.FROM_NAME));
                                                                timelineData.setFrom_profile_picture_1(mArrayComment.getJSONObject(index).getString(WebParams.FROM_PROFILE_PICTURE));
                                                                timelineData.setReply_1(mArrayComment.getJSONObject(index).getString(WebParams.REPLY));
                                                            }
                                                            if (index == 1) {
                                                                timelineData.setComment_id_2(mArrayComment.getJSONObject(index).getString(WebParams.COMMENT_ID));
                                                                timelineData.setFrom_name_2(mArrayComment.getJSONObject(index).getString(WebParams.FROM_NAME));
                                                                timelineData.setFrom_profile_picture_2(mArrayComment.getJSONObject(index).getString(WebParams.FROM_PROFILE_PICTURE));
                                                                timelineData.setReply_2(mArrayComment.getJSONObject(index).getString(WebParams.REPLY));
                                                            }
                                                        }
                                                    }
                                                }
                                                timelineDataDao.updateInTx(timelineData);
                                            }
                                            break;
                                        }
                                    }

                                }

                                if (!flagSame) {
                                    String post = mArrayPost.getJSONObject(i).getString(WebParams.POST);
                                    String amount = mArrayPost.getJSONObject(i).getString(WebParams.AMOUNT);
                                    String balance = mArrayPost.getJSONObject(i).getString(WebParams.BALANCE);
                                    String ccy_id = mArrayPost.getJSONObject(i).getString(WebParams.CCY_ID);
                                    String datetime = mArrayPost.getJSONObject(i).getString(WebParams.DATETIME);
                                    String owner = mArrayPost.getJSONObject(i).getString(WebParams.OWNER);
                                    String owner_id = mArrayPost.getJSONObject(i).getString(WebParams.OWNER_ID);
                                    String owner_profile_picture = mArrayPost.getJSONObject(i).getString(WebParams.OWNER_PROFILE_PICTURE);
									String owner_profile_picture_large = mArrayPost.getJSONObject(i).getString(WebParams.OWNER_PROFILE_PICTURE_LARGE);                               
                                    String with_id = mArrayPost.getJSONObject(i).getString(WebParams.WITH_ID);
                                    String with = mArrayPost.getJSONObject(i).getString(WebParams.WITH);
                                    String with_profile_picture = mArrayPost.getJSONObject(i).getString(WebParams.WITH_PROFILE_PICTURE);
									String with_profile_picture_large = mArrayPost.getJSONObject(i).getString(WebParams.WITH_PROFILE_PICTURE_LARGE);                                   
                                    String tx_status = mArrayPost.getJSONObject(i).getString(WebParams.TX_STATUS);
                                    String typepost = mArrayPost.getJSONObject(i).getString(WebParams.TYPEPOST);
                                    String typecaption = mArrayPost.getJSONObject(i).getString(WebParams.TYPECAPTION);
                                    String privacy = mArrayPost.getJSONObject(i).getString(WebParams.PRIVACY);
                                    String numcomments = mArrayPost.getJSONObject(i).getString(WebParams.NUMCOMMENTS);
                                    String numviews = mArrayPost.getJSONObject(i).getString(WebParams.NUMVIEWS);
                                    String numlikes = mArrayPost.getJSONObject(i).getString(WebParams.NUMLIKES);
                                    String share = mArrayPost.getJSONObject(i).getString(WebParams.SHARE);
                                    String comments = mArrayPost.getJSONObject(i).getString(WebParams.COMMENTS);
                                    String likes = mArrayPost.getJSONObject(i).getString(WebParams.LIKES);

                                    String isLike = "0";
                                    if(likes.equals("")){
                                        isLike = "0";
                                    }
                                    else {
                                        JSONArray mArrayLike = new JSONArray(likes);
                                        for(int index = 0; index < mArrayLike.length(); index++){
                                            String from = mArrayLike.getJSONObject(index).getString(WebParams.FROM);
                                            if(_ownerID.equals(from)) isLike = "1";
                                        }
                                    }

                                    if(comments.equals("")) {
                                        mListTimelineData.add(new TimelineData(id, post, amount, balance, ccy_id, datetime, owner, owner_id,
                                                owner_profile_picture, owner_profile_picture_large, with_id, with, with_profile_picture, with_profile_picture_large, tx_status, typepost, typecaption,
                                                privacy, numcomments, numviews, numlikes, share, comments, likes,  "","","","","","","","",isLike));
                                    }
                                    else{
                                        JSONArray mArrayComment = new JSONArray(comments);
                                        int lengthComment = mArrayComment.length();
                                        String comment_id_1 = "", from_name_1 = "", from_profile_picture_1 = "", reply_1 = "",
                                                comment_id_2 = "", from_name_2 = "", from_profile_picture_2 = "", reply_2 = "";
                                        if(lengthComment == 1) {
                                            for (int index = 0; index < mArrayComment.length(); index++) {
                                                comment_id_1 = mArrayComment.getJSONObject(index).getString(WebParams.COMMENT_ID);
                                                from_name_1 = mArrayComment.getJSONObject(index).getString(WebParams.FROM_NAME);
                                                from_profile_picture_1 = mArrayComment.getJSONObject(index).getString(WebParams.FROM_PROFILE_PICTURE);
                                                reply_1 = mArrayComment.getJSONObject(index).getString(WebParams.REPLY);
                                            }
                                        }
                                        if(lengthComment == 2) {
                                            for (int index = 0; index < mArrayComment.length(); index++) {
                                                if(index == 0) {
                                                    comment_id_1 = mArrayComment.getJSONObject(index).getString(WebParams.COMMENT_ID);
                                                    from_name_1 = mArrayComment.getJSONObject(index).getString(WebParams.FROM_NAME);
                                                    from_profile_picture_1 = mArrayComment.getJSONObject(index).getString(WebParams.FROM_PROFILE_PICTURE);
                                                    reply_1 = mArrayComment.getJSONObject(index).getString(WebParams.REPLY);
                                                }
                                                if(index == 1) {
                                                    comment_id_2 = mArrayComment.getJSONObject(index).getString(WebParams.COMMENT_ID);
                                                    from_name_2 = mArrayComment.getJSONObject(index).getString(WebParams.FROM_NAME);
                                                    from_profile_picture_2 = mArrayComment.getJSONObject(index).getString(WebParams.FROM_PROFILE_PICTURE);
                                                    reply_2 = mArrayComment.getJSONObject(index).getString(WebParams.REPLY);
                                                }
                                            }
                                        }
                                        mListTimelineData.add(new TimelineData(id, post, amount, balance, ccy_id, datetime, owner, owner_id,
                                                owner_profile_picture, owner_profile_picture_large, with_id, with, with_profile_picture, with_profile_picture_large, tx_status, typepost, typecaption,
                                                privacy, numcomments, numviews, numlikes, share, comments, likes, comment_id_1, from_name_1, from_profile_picture_1,
                                                reply_1, comment_id_2, from_name_2, from_profile_picture_2, reply_2, isLike));
                                    }
                                }

                            }
                            insertPostToDB(mListTimelineData);
                        }
                        else if(code.equals(WebParams.LOGOUT_CODE)){
                            Timber.d("isi response autologout:"+response.toString());
                            String message = response.getString(WebParams.ERROR_MESSAGE);
                            AlertDialogLogout test = AlertDialogLogout.getInstance();
                            test.showDialoginMain(getActivity(),message);
                        }
                        else {
                            Timber.d("isi error timeline list:"+response.toString());
                            if(code.equals("0003")) {
                                TimelineData.deleteAll(timelineDataDao);
                                initializeDataPost();
                            }
                        }
                        if(frameLayout !=null)
                            frameLayout.refreshComplete();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    super.onFailure(statusCode, headers, responseString, throwable);
                    failure(throwable);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    failure(throwable);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    failure(throwable);
                }

                private void failure(Throwable throwable){
                    if(TimeLine.this.isVisible()) {
//                        if (MyApiClient.PROD_FAILURE_FLAG)
//                            Toast.makeText(getActivity(), getString(R.string.network_connection_failure_toast), Toast.LENGTH_SHORT).show();
//                        else
//                            Toast.makeText(getActivity(), throwable.getCause().getMessage(), Toast.LENGTH_SHORT).show();
                    }
                    Timber.w("Error Koneksi Timeline:"+throwable.toString());
                }


            });
        }catch (Exception e){
            Timber.d("httpclient:"+e.getMessage());
        }
    }

    private void insertPostToDB(List<TimelineData> mListTimelineData){
        timelineDataDao.insertOrReplaceInTx(mListTimelineData);
        sp.edit().putString(DefineValue.TIMELINE_FIRST_TIME, DefineValue.NO).apply();
        if(getActivity() != null){
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    listTimelineData.clear();
                    listTimelineData.addAll(TimelineData.getAllTimelineData(timelineDataDao));
                    if(listTimelineData.size() > 0) {
                        layout_alert.setVisibility(View.GONE);
                        layout_list.setVisibility(View.VISIBLE);
                    }
                    else {
                        layout_alert.setVisibility(View.VISIBLE);
                        layout_list.setVisibility(View.GONE);
                        txtAlert.setText(getString(R.string.data_not_found));
                        imgAlert.setImageResource(R.drawable.ic_data_not_found);
                    }
                    getCurrentAdapter().notifyDataSetChanged();
                }
            });
            Timber.d("finish initialize timeline");
        }
    }

    private void switchActivity(Intent mIntent){
        if (getActivity() == null)
            return;

        MainPage fca = (MainPage) getActivity();
        fca.switchActivity(mIntent, MainPage.ACTIVITY_RESULT);
    }

    @Override
    protected int getInflateFragmentLayout() {
        return R.layout.frag_time_line;
    }

    @Override
    public boolean checkCanDoRefresh() {
        try {
            return getCurrentAdapter().getItemCount() == 0 || getCurrentRecyclerView() == null || getCurrentLayoutManag().findFirstCompletelyVisibleItemPosition() == 0 && getCurrentRecyclerView().getChildAt(0).getTop() > 0;
        }
        catch (Exception ex){
            Timber.wtf("Exception checkCandoRefresh:"+ex.getMessage());
        }
        return false;
    }

    @Override
    public void refresh(PtrFrameLayout frameLayout) {
        int p = Integer.parseInt(page) + 1;
        page = Integer.toString(p);
        getTimelineList(frameLayout,0);
    }

    @Override
    public void goToTop() {
        mRecyclerView.smoothScrollToPosition(0);
    }

    private LinearLayoutManager getCurrentLayoutManag() {
        return currentLayoutManag;
    }

    private void setCurrentLayoutManag(LinearLayoutManager currentLayoutManag) {
        this.currentLayoutManag = currentLayoutManag;
    }

    private TimeLineRecycleAdapter getCurrentAdapter() {
        return currentAdapter;
    }

    private void setCurrentAdapter(TimeLineRecycleAdapter currentAdapter) {
        this.currentAdapter = currentAdapter;
    }

    private RecyclerView getCurrentRecyclerView() {
        return currentRecyclerView;
    }

    private void setCurrentRecyclerView(RecyclerView currentRecyclerView) {
        this.currentRecyclerView = currentRecyclerView;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onStart() {
        super.onStart();
        if(start > 0) {
            listTimelineData.clear();
            listTimelineData.addAll(TimelineData.getAllTimelineData(timelineDataDao));
            getCurrentAdapter().notifyDataSetChanged();
        }
        start++;
    }
}