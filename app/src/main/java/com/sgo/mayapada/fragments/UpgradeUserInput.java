package com.sgo.mayapada.fragments;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.securepreferences.SecurePreferences;
import com.sgo.mayapada.R;
import com.sgo.mayapada.activities.MainPage;
import com.sgo.mayapada.activities.UpgradeUserActivity;
import com.sgo.mayapada.coreclass.CustomSecurePref;
import com.sgo.mayapada.coreclass.DefineValue;
import com.sgo.mayapada.coreclass.InetHandler;
import com.sgo.mayapada.coreclass.MyApiClient;
import com.sgo.mayapada.coreclass.WebParams;
import com.sgo.mayapada.dialogs.AlertDialogLogout;
import com.sgo.mayapada.dialogs.DefinedDialog;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import timber.log.Timber;

/**
 * Created by thinkpad on 8/10/2017.
 */

public class UpgradeUserInput extends Fragment {
    private View v;
    private EditText etNoRek, etNoID;
    private ProgressDialog progdialog;
    private String userID, accessKey;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.frag_upgrade_user_input, container, false);
        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        SecurePreferences sp = CustomSecurePref.getInstance().getmSecurePrefs();
        userID = sp.getString(DefineValue.USERID_PHONE,"");
        accessKey = sp.getString(DefineValue.ACCESS_KEY,"");

        etNoID = (EditText) v.findViewById(R.id.upgradeuser_value_id_no);
        etNoRek = (EditText) v.findViewById(R.id.upgradeuser_value_bank_acc_no);
        Button btnProcess = (Button) v.findViewById(R.id.upgrade_user_btn_process);

        btnProcess.setOnClickListener(btnProcessListener);

        ToggleFAB(false);
    }

    private Button.OnClickListener btnProcessListener = new Button.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(InetHandler.isNetworkAvailable(getActivity())) {
                if (inputValidation()) {
                    requestUpgrade();
                }
            }
            else DefinedDialog.showErrorDialog(getActivity(), getString(R.string.inethandler_dialog_message));
        }
    };

    private void requestUpgrade(){
        try {
            progdialog = DefinedDialog.CreateProgressDialog(getActivity(), "");
            progdialog.show();

            RequestParams params = MyApiClient.getSignatureWithParams(MyApiClient.COMM_ID,MyApiClient.LINK_REQUEST_UPGRADE,
                    userID,accessKey);
            params.put(WebParams.COMM_ID, MyApiClient.COMM_ID);
            params.put(WebParams.USER_ID, userID);
            params.put(WebParams.CUST_PHONE, userID);
            params.put(WebParams.CUST_ID_NUMBER, etNoID.getText().toString());
            params.put(WebParams.NO_REK, etNoRek.getText().toString());

            Timber.d("isi params request upgrade:" + params.toString());

            MyApiClient.sentRequestUpgradeUser(getActivity(),params, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    progdialog.dismiss();

                    try {
                        String code = response.getString(WebParams.ERROR_CODE);
                        if (code.equals(WebParams.SUCCESS_CODE)) {
                            Timber.d("isi response request upgrade:"+response.toString());

                            Intent mI = new Intent(getActivity(),UpgradeUserActivity.class);
                            mI.putExtra(DefineValue.CUST_PHONE, response.optString(WebParams.CUST_PHONE));
                            mI.putExtra(DefineValue.CUST_NAME, response.optString(WebParams.CUST_NAME));
                            mI.putExtra(DefineValue.PROFILE_ADDRESS, response.optString(WebParams.CUST_ADDRESS));
                            mI.putExtra(DefineValue.PROFILE_ID_TYPE, response.optString(WebParams.ID_TYPE));
                            mI.putExtra(DefineValue.ID_NUMBER, response.optString(WebParams.CUST_ID_NUMBER));
                            mI.putExtra(DefineValue.PROFILE_BIRTH_PLACE, response.optString(WebParams.CUST_BIRTH_PLACE));
                            mI.putExtra(DefineValue.PROFILE_DOB, response.optString(WebParams.CUST_BIRTH_DATE));
                            mI.putExtra(DefineValue.PROFILE_GENDER, response.optString(WebParams.CUST_GENDER));
                            mI.putExtra(DefineValue.PROFILE_BOM, response.optString(WebParams.CUST_MOTHER_NAME));
                            mI.putExtra(DefineValue.ACCOUNT_NUMBER, response.optString(WebParams.NO_REK));
                            getActivity().startActivityForResult(mI, MainPage.REQUEST_FINISH);
                        } else if (code.equals(WebParams.LOGOUT_CODE)) {
                            Timber.d("isi response autologout:"+response.toString());
                            String message = response.getString(WebParams.ERROR_MESSAGE);
                            AlertDialogLogout test = AlertDialogLogout.getInstance();
                            test.showDialoginMain(getActivity(), message);
                        } else {
                            Timber.d("isi error request upgrade:"+response.toString());
                            String code_msg = response.getString(WebParams.ERROR_MESSAGE);
                            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                            builder.setMessage(code_msg)
                                    .setPositiveButton("Dismiss", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();
                                        }
                                    });
                            AlertDialog dialog = builder.create();
                            dialog.show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    super.onFailure(statusCode, headers, responseString, throwable);
                    failure(throwable);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    failure(throwable);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    failure(throwable);
                }

                private void failure(Throwable throwable){
                    if(MyApiClient.PROD_FAILURE_FLAG)
                        Toast.makeText(getActivity(), getString(R.string.network_connection_failure_toast), Toast.LENGTH_SHORT).show();
                    else
                        Toast.makeText(getActivity(), throwable.toString(), Toast.LENGTH_SHORT).show();

                    if(progdialog.isShowing())
                        progdialog.dismiss();

                    Timber.w("Error Koneksi request upgrade:"+throwable.toString());
                }
            });
        }catch (Exception e){
            Timber.d("httpclient:" + e.getMessage());
        }
    }

    private boolean inputValidation(){
        if(etNoRek.getText().toString().length()==0){
            etNoRek.requestFocus();
            etNoRek.setError(getString(R.string.cashout_accno_validation));
            return false;
        }
        if(etNoID.getText().toString().length()==0){
            etNoID.requestFocus();
            etNoID.setError(getString(R.string.id_no_empty_message));
            return false;
        }
        return true;
    }

    private void ToggleFAB(Boolean isShow){
        if (getActivity() == null)
            return;

        if(getActivity() instanceof MainPage) {
            MainPage fca = (MainPage) getActivity();
            if(isShow)
                fca.materialSheetFab.showFab();
            else
                fca.materialSheetFab.hideSheetThenFab();
        }
    }
}