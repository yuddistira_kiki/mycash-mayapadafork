package com.sgo.mayapada.fragments;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.securepreferences.SecurePreferences;
import com.sgo.mayapada.Beans.KantinManajemenStokModel;
import com.sgo.mayapada.R;
import com.sgo.mayapada.activities.KantinActivity;
import com.sgo.mayapada.adapter.KantinManajemenStokAdapter;
import com.sgo.mayapada.coreclass.CustomSecurePref;
import com.sgo.mayapada.coreclass.DefineValue;
import com.sgo.mayapada.coreclass.MyApiClient;
import com.sgo.mayapada.coreclass.WebParams;
import com.sgo.mayapada.dialogs.AlertDialogLogout;
import com.sgo.mayapada.dialogs.DefinedDialog;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.AppSettingsDialog;
import pub.devrel.easypermissions.EasyPermissions;
import timber.log.Timber;

/**
 * Created by thinkpad on 10/6/2016.
 */

public class FragKantinManajemenStok extends Fragment implements StockBarangAction.SuccessStockBarangListener, EasyPermissions.PermissionCallbacks {

    private static final int RC_CAMERA_GALLERY = 100;
    private SecurePreferences sp;
    private View v;
    private View addLayout;
    private ProgressDialog out;
    private EditText etSearch;
    private ListView lvCatalog;
    private ImageButton btnRefresh;
    private Button btnBack;
    private KantinManajemenStokAdapter adapter;
    private String user_id;
    private String access_key;
    private String subcategory_code;
    private String member_id;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        v = inflater.inflate(R.layout.frag_kantin_manajemen_stok, container, false);
        addLayout = v.findViewById(R.id.add_layout);
        etSearch = (EditText) v.findViewById(R.id.et_search);
        btnRefresh = (ImageButton) v.findViewById(R.id.btn_refresh);
        lvCatalog = (ListView) v.findViewById(R.id.lv_catalog);
        btnBack = (Button) v.findViewById(R.id.btn_back);

        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                adapter.getFilter().filter(s);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        addLayout.setOnClickListener(addListener);
        btnBack.setOnClickListener(backListener);
        btnRefresh.setOnClickListener(refreshListener);
        return v;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle(getString(R.string.kantin_manajemen_stok));
        sp = CustomSecurePref.getInstance().getmSecurePrefs();
        user_id = sp.getString(DefineValue.USERID_PHONE, "");
        access_key = sp.getString(DefineValue.ACCESS_KEY, "");
        if(sp.getBoolean(DefineValue.IS_MERCHANT,false))
            subcategory_code = sp.getString(DefineValue.MERCHANT_SUBCATEGORY,"");
        else
            subcategory_code = sp.getString(DefineValue.SUBCATEGORY_CODE,"");
        member_id = sp.getString(DefineValue.MEMBER_ID,"");
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if(adapter == null) {
            ArrayList<KantinManajemenStokModel> listCatalog = new ArrayList<>();
            adapter = new KantinManajemenStokAdapter(getActivity(), listCatalog, FragKantinManajemenStok.this);
        }

        if(lvCatalog.getAdapter() == null) {
            lvCatalog.setAdapter(adapter);
            lvCatalog.setTextFilterEnabled(true);
        }
        permissionCameraGallery();
    }

    private View.OnClickListener addListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Fragment frag = StockBarangAction.newInstanceTambah();
            frag.setTargetFragment(FragKantinManajemenStok.this, 0);
            switchFragment(frag, getString(R.string.tambahstock_title_tambah), true);
        }
    };

    public void onListItemClick(KantinManajemenStokModel oriData){
        Fragment frag = StockBarangAction.newInstanceUpdate(oriData.getCode());
        frag.setTargetFragment(this, 0);
        switchFragment(frag, getString(R.string.tambahstock_title_update), true);
    }

    private void switchFragment(Fragment i, String name, Boolean isBackstack){
        if (getActivity() == null)
            return;

        KantinActivity fca = (KantinActivity) getActivity();
        fca.switchContent(this, i,name,isBackstack);
        etSearch.setText("");
    }

    private Button.OnClickListener backListener = new Button.OnClickListener() {
        @Override
        public void onClick(View view) {
            getActivity().finish();
        }
    };

    private ImageButton.OnClickListener refreshListener = new ImageButton.OnClickListener() {
        @Override
        public void onClick(View view) {
            etSearch.setText("");
        }
    };

    private void getDataCatalog() {
        try{
            out = DefinedDialog.CreateProgressDialog(getActivity(), null);
            out.show();

            RequestParams params = MyApiClient.getSignatureWithParams(MyApiClient.COMM_ID, MyApiClient.LINK_CANTEEN_CATALOGS,
                    user_id, access_key);

            params.put(WebParams.USER_ID,user_id);
            params.put(WebParams.COMM_ID, MyApiClient.COMM_ID);
            params.put(WebParams.SUBCATEGORY_CODE, subcategory_code);
            params.put(WebParams.MERCHANT_ID, member_id);

            Timber.d("isi params get data catalog:"+params.toString());

            MyApiClient.getCanteenCatalogs(getActivity(), params, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                    try {
                        if (isAdded()) {
                            out.dismiss();

                            String code = response.getString(WebParams.ERROR_CODE);
                            Timber.w("isi response get data catalog:" + response.toString());

                            if (code.equals(WebParams.SUCCESS_CODE) || code.equals("0050")) {

                                String catalog = response.getString(WebParams.CATALOG);
                                if (!catalog.equals("")) {
                                    JSONArray arrayData = new JSONArray(catalog);
                                    JSONObject mObj;
                                    ArrayList<KantinManajemenStokModel> data = new ArrayList<>();
                                    for (int i = 0; i < arrayData.length(); i++) {
                                        mObj = arrayData.getJSONObject(i);

                                        KantinManajemenStokModel menu = new KantinManajemenStokModel();
                                        menu.setCode(mObj.getString(WebParams.ITEM_CODE));
                                        menu.setName(mObj.getString(WebParams.ITEM_NAME));
                                        if(mObj.getString(WebParams.ITEM_STATUS).equalsIgnoreCase("A")) {
                                            menu.setIsStop(false);
                                        }
                                        else
                                            menu.setIsStop(true);
                                        data.add(menu);
                                    }
                                    adapter.swapItems(data);
                                }
                                adapter.notifyDataSetChanged();

                            } else if (code.equals(WebParams.LOGOUT_CODE)) {
                                Timber.d("isi response autologout:" + response.toString());
                                String message = response.getString(WebParams.ERROR_MESSAGE);
                                AlertDialogLogout test = AlertDialogLogout.getInstance();
                                test.showDialoginActivity(getActivity(), message);
                            }
                            else {
                                String message = response.getString(WebParams.ERROR_MESSAGE);
                                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                                builder.setMessage(message)
                                        .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                dialog.dismiss();
                                                getActivity().finish();
                                            }
                                        });
                                AlertDialog dialog = builder.create();
                                dialog.show();
                            }
                        }

                    } catch (JSONException e) {

                        Toast.makeText(getActivity(), getString(R.string.internal_error), Toast.LENGTH_LONG).show();
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    super.onFailure(statusCode, headers, responseString, throwable);
                    failure(throwable);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    failure(throwable);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    failure(throwable);
                }

                private void failure(Throwable throwable) {
                    out.dismiss();
                    if (MyApiClient.PROD_FAILURE_FLAG)
                        Toast.makeText(getActivity(), getString(R.string.network_connection_failure_toast), Toast.LENGTH_SHORT).show();
                    else
                        Toast.makeText(getActivity(), throwable.toString(), Toast.LENGTH_SHORT).show();

                    getActivity().finish();
                    Timber.w("Error Koneksi get data catalog:" + throwable.toString());
                }
            });
        } catch (Exception e) {
            String err = (e.getMessage()==null)?"Connection failed":e.getMessage();
            Timber.e("http err:" + err);
        }
    }

    public void sentActiveDeactive(final int pos) {
        try{
            ArrayList<KantinManajemenStokModel> data = adapter.getFilteredData();

            out = DefinedDialog.CreateProgressDialog(getActivity(), null);
            out.show();

            RequestParams params = MyApiClient.getSignatureWithParams(MyApiClient.COMM_ID, MyApiClient.LINK_ACTIVE_DEACTIVE_ITEM,
                    user_id, access_key);
            params.put(WebParams.MERCHANT_ID, sp.getString(DefineValue.MEMBER_ID,""));
            params.put(WebParams.USER_ID, user_id);
            params.put(WebParams.COMM_ID, MyApiClient.COMM_ID);
            params.put(WebParams.ITEM_CODE, data.get(pos).getCode());
            if(data.get(pos).isStop())
                params.put(WebParams.STATUS, "A");
            else
                params.put(WebParams.STATUS, "I");

            Timber.d("isi params active deactive:" + params.toString());

            MyApiClient.sentActiveDeactiveItem(getActivity(), params, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    try {
                        out.dismiss();
                        String code = response.getString(WebParams.ERROR_CODE);
                        if (code.equals(WebParams.SUCCESS_CODE)) {
                            Timber.d("isi response active deactive:" + response.toString());
                            adapter.setActiveDeactive(response.getString(WebParams.ITEM_STATUS), pos);
                        } else if (code.equals(WebParams.LOGOUT_CODE)) {
                            Timber.d("isi response autologout:" + response.toString());
                            String message = response.getString(WebParams.ERROR_MESSAGE);
                            AlertDialogLogout test = AlertDialogLogout.getInstance();
                            test.showDialoginActivity(getActivity(), message);
                        }
                        else {
                            Timber.d("Error active deactive:" + response.toString());
                            code = response.getString(WebParams.ERROR_CODE) + ":" + response.getString(WebParams.ERROR_MESSAGE);
                            Toast.makeText(getActivity(), code, Toast.LENGTH_LONG).show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    super.onFailure(statusCode, headers, responseString, throwable);
                    failure(throwable);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    failure(throwable);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    failure(throwable);
                }

                private void failure(Throwable throwable) {
                    out.dismiss();
                    if (MyApiClient.PROD_FAILURE_FLAG)
                        Toast.makeText(getActivity(), getString(R.string.network_connection_failure_toast), Toast.LENGTH_SHORT).show();
                    else
                        Toast.makeText(getActivity(), throwable.toString(), Toast.LENGTH_SHORT).show();

                    Timber.w("Error Koneksi active deactive:" + throwable.toString());
                }
            });
        } catch (Exception e) {
            Timber.d("httpclient:" + e.getMessage());
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if(getActivity().getSupportFragmentManager().getBackStackEntryCount() > 0)
                    getActivity().getSupportFragmentManager().popBackStack();
                else
                    getActivity().finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume() {
        super.onResume();
        setTitle(getString(R.string.kantin_manajemen_stok));
        getDataCatalog();
    }

    private void setTitle(String _title){
        if (getActivity() == null)
            return;

        KantinActivity fca = (KantinActivity) getActivity();
        fca.setTitleFragment(_title);
    }

    @Override
    public void onSuccessTambahStock(boolean status) {

    }

    @Override
    public void onSuccessUpdateStock(boolean status) {

    }

    @Override
    public void onSuccessDeleteStock(boolean status) {
        Timber.d("OnSuccessDeleteStock + " + String.valueOf(status));
//        if(status)
//            adapter.notifyDataSetChanged();
    }

    @AfterPermissionGranted(RC_CAMERA_GALLERY)
    private void permissionCameraGallery() {
        String[] perms = {Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE};
        if (EasyPermissions.hasPermissions(getActivity(), perms)) {
            // Already have permission, do the thing
        } else {
            // Do not have permissions, request them now
            EasyPermissions.requestPermissions(this, getString(R.string.cancel_permission_read_contacts),
                    RC_CAMERA_GALLERY, perms);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(requestCode == RC_CAMERA_GALLERY) {
            if (EasyPermissions.hasPermissions(getActivity(), permissions)) {
            }
            else {
                // Forward results to EasyPermissions
                EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
            }
        }
    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {
        if(requestCode == RC_CAMERA_GALLERY) {
            if (EasyPermissions.somePermissionPermanentlyDenied(this, perms)) {
                getActivity().finish();
            }
        }
    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {
        getActivity().finish();
        // (Optional) Check whether the user denied any permissions and checked "NEVER ASK AGAIN."
        // This will display a dialog directing them to enable the permission in app settings.
        if (EasyPermissions.somePermissionPermanentlyDenied(this, perms)) {
            new AppSettingsDialog.Builder(this).build().show();
            }
    }
}
