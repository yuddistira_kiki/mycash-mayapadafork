package com.sgo.mayapada.fragments;

import android.Manifest;
import android.app.Dialog;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.telephony.TelephonyManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.faber.circlestepview.CircleStepView;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.securepreferences.SecurePreferences;
import com.sgo.mayapada.BuildConfig;
import com.sgo.mayapada.R;
import com.sgo.mayapada.activities.LoginActivity;
import com.sgo.mayapada.coreclass.CustomSecurePref;
import com.sgo.mayapada.coreclass.DateTimeFormat;
import com.sgo.mayapada.coreclass.DefineValue;
import com.sgo.mayapada.coreclass.InetHandler;
import com.sgo.mayapada.coreclass.MyApiClient;
import com.sgo.mayapada.coreclass.NoHPFormat;
import com.sgo.mayapada.coreclass.ToggleKeyboard;
import com.sgo.mayapada.coreclass.WebParams;
import com.sgo.mayapada.dialogs.DefinedDialog;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import pub.devrel.easypermissions.EasyPermissions;
import timber.log.Timber;

/*
 Created by Administrator on 7/4/2014.
 */
public class Regist1 extends Fragment implements EasyPermissions.PermissionCallbacks {//implements FacebookFunction.LoginFBListener{


    private final static int RC_READ_SMS = 101;

    private String namaValid = "", emailValid = "", noHPValid = "";
    private EditText namaValue, emailValue, noHPValue;
    private Button btnLanjut;//    btnFacebook;
    private CheckBox cb_terms;
    private View v, layout_regist1;
//	private FacebookFunction mFaceFunction;
    private ProgressDialog progdialog;
	private Boolean isFacebook;
//    private String dataFacebook;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        isFacebook = false;

//        mFaceFunction = new FacebookFunction(getActivity());
//        mFaceFunction.setTargetFragment(this);

        progdialog = DefinedDialog.CreateProgressDialog(getActivity(), "");
        progdialog.dismiss();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.frag_regist1, container, false);
        return v;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onActivityCreated (Bundle savedInstanceState){
        super.onActivityCreated(savedInstanceState);
        initializeLayout();
    }

    private void initializeLayout(){

        getActivity().getWindow().setBackgroundDrawableResource(R.drawable.background_register);

        layout_regist1 = v.findViewById(R.id.layout_regist1);
        layout_regist1.setVisibility(View.VISIBLE);

        CircleStepView mCircleStepView = ((CircleStepView) v.findViewById(R.id.circle_step_view));
        mCircleStepView.setTextBelowCircle("Step 1", "Step 2", "Step 3");
        mCircleStepView.setCurrentCircleIndex(0, true);

        btnLanjut = (Button)getActivity().findViewById(R.id.btn_reg1_verification);
        btnLanjut.setOnClickListener(btnNextClickListener);

//        if(mFaceFunction.isLogin())
//            mFaceFunction.logout();

        namaValue=(EditText)getActivity().findViewById(R.id.name_value);
        emailValue=(EditText)getActivity().findViewById(R.id.email_value);
        noHPValue=(EditText)getActivity().findViewById(R.id.noHP_value);
        cb_terms = (CheckBox) v.findViewById(R.id.cb_termsncondition);

        cb_terms.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked) {
                    btnLanjut.setEnabled(true);
                    btnLanjut.setTextColor(ContextCompat.getColor(getActivity(),R.color.text_color_button));
                } else {
                    btnLanjut.setEnabled(false);
                    btnLanjut.setTextColor(ContextCompat.getColor(getActivity(),R.color.colorSecondary));
                }
            }
        });

        if(EasyPermissions.hasPermissions(getActivity(),Manifest.permission.READ_SMS)) {
            if (isSimExists()) {

                TelephonyManager tm = (TelephonyManager) getActivity().getSystemService(Context.TELEPHONY_SERVICE);
                String Nomor1 = tm.getLine1Number();

                noHPValue.setText(Nomor1);
            }
        }
        else {
            EasyPermissions.requestPermissions(this,
                    getString(R.string.rational_readphonestate),
                    RC_READ_SMS, Manifest.permission.READ_SMS);
        }
        //else Toast.makeText(getActivity(),"tidak ada sim",Toast.LENGTH_LONG).show();

        noHPValue.requestFocus();
        ToggleKeyboard.show_keyboard(getActivity());

        TextView tv_termsnconditions = (TextView) v.findViewById(R.id.tv_termsncondition);
        tv_termsnconditions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TermsNConditionWeb mfrag = new TermsNConditionWeb();
                switchFragment(mfrag,getString(R.string.termsncondition_title),true);
            }
        });
        SecurePreferences sp = CustomSecurePref.getInstance().getmSecurePrefs();
        if(sp.contains(DefineValue.SENDER_ID)) {
            noHPValid = NoHPFormat.editNoHP(sp.getString(DefineValue.SENDER_ID, ""));
            noHPValue.setText(noHPValid);
        }

        if(BuildConfig.DEBUG && BuildConfig.FLAVOR.equals("development")){ //untuk shorcut dari tombol di activity LoginActivity
            Bundle m = getArguments();
            if(m != null && m.containsKey(DefineValue.USER_IS_NEW)) {
//                v.findViewById(R.id.noHP_value).setVisibility(View.VISIBLE);
                noHPValue.setEnabled(true);
            }
        }

//        btnFacebook = (Button) v.findViewById(R.id.btn_register_facebook);
//        btnFacebook.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                mFaceFunction.loginRead(Regist1.this,false);
//            }
//        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(requestCode,permissions,grantResults,this);
    }

    private Button.OnClickListener btnNextClickListener= new Button.OnClickListener(){

        @Override
        public void onClick(View view) {
            if(view == btnLanjut){
                if(InetHandler.isNetworkAvailable(getActivity())){
                    if(inputValidation()){
                        if(BuildConfig.DEBUG && BuildConfig.FLAVOR.equals("development") ){
                            if(getActivity().findViewById(R.id.noHP_value).getVisibility() == View.VISIBLE)
                                noHPValid = NoHPFormat.editNoHP (((EditText)getActivity().findViewById(R.id.noHP_value)).getText().toString());
                        }
                        sentData(noHPValid);
                    }
                }else DefinedDialog.showErrorDialog(getActivity(),getString(R.string.inethandler_dialog_message));
            }
        }
    };

//	@Override
//    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//        mFaceFunction.getmCallBackManager().onActivityResult(requestCode,resultCode,data);
//    }

    private void switchFragment(Fragment i, String name, Boolean isBackstack){
        if (getActivity() == null)
            return;

        LoginActivity fca = (LoginActivity) getActivity();
        fca.switchContent(i, name, isBackstack);
    }

    private void SaveIMEIICCID(){
        if (getActivity() == null)
            return;

        LoginActivity fca = (LoginActivity) getActivity();
        fca.SaveImeiICCIDDevice();
    }

    private void sentData(final String noHP){
        try{
            progdialog.show();

            btnLanjut.setEnabled(false);
            noHPValue.setEnabled(false);
            namaValue.setEnabled(false);
            emailValue.setEnabled(false);

            RequestParams params = new RequestParams();
            params.put(WebParams.COMM_ID, MyApiClient.COMM_ID);
            params.put(WebParams.CUST_PHONE, noHP);
            params.put(WebParams.CUST_NAME,namaValue.getText());
            params.put(WebParams.CUST_EMAIL, emailValue.getText());
            params.put(WebParams.DATE_TIME, DateTimeFormat.getCurrentDateTime());

            Timber.d("isi params reg1:" + params.toString());

//				if(mFaceFunction.isLogin())
//                    mFaceFunction.logout();
            MyApiClient.sentRegStep1(getActivity(),params,new JsonHttpResponseHandler(){
                @Override
                public void onSuccess(int statusCode,Header[] headers, JSONObject response) {
                    btnLanjut.setEnabled(true);
                    noHPValue.setEnabled(true);
                    namaValue.setEnabled(true);
                    emailValue.setEnabled(true);
                    Timber.d("response register:"+response.toString());
                    progdialog.dismiss();
                    try {
                        String code = response.getString(WebParams.ERROR_CODE);
                        if(code.equals(WebParams.SUCCESS_CODE)){

                            namaValid = response.getString(WebParams.CUST_NAME);
                            emailValid = response.getString(WebParams.CUST_EMAIL);
                            noHPValid = response.getString(WebParams.CUST_PHONE);

                            showDialog(code);
                        }
                        else if(code.equals("0002")){
                            showDialog(code);
                        }
                        else {
                            Timber.d("Error Reg1:"+response.toString());
                            code = response.getString(WebParams.ERROR_MESSAGE);
                            Toast.makeText(getActivity(), code, Toast.LENGTH_LONG).show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    super.onFailure(statusCode, headers, responseString, throwable);
                    failure(throwable);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    failure(throwable);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    failure(throwable);
                }

                private void failure(Throwable throwable){
                    if(MyApiClient.PROD_FAILURE_FLAG)
                        Toast.makeText(getActivity(), getString(R.string.network_connection_failure_toast), Toast.LENGTH_SHORT).show();
                    else
                        Toast.makeText(getActivity(), throwable.toString(), Toast.LENGTH_SHORT).show();
                    if(progdialog.isShowing())
                        progdialog.dismiss();
                    btnLanjut.setEnabled(true);
                    noHPValue.setEnabled(true);
                    namaValue.setEnabled(true);
                    emailValue.setEnabled(true);
                    Timber.w("Error Koneksi reg1 proses reg1:"+throwable.toString());
                }
            });
        }catch (Exception e){
            Timber.d("httpclient:"+e.getMessage());
        }
    }

    private void changeActivity(Boolean login){
        if(login){
            DefineValue.NOBACK = false; //fragment selanjutnya tidak bisa menekan tombol BACK
            SaveIMEIICCID();
            getActivity().getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            Fragment newFrag = new Login();
            switchFragment(newFrag,"Login",false);
        }
        else{
            DefineValue.NOBACK = true; //fragment selanjutnya tidak bisa menekan tombol BACK
            Fragment mFragment = new Regist2();
            Bundle mBun = new Bundle();
            mBun.putString(DefineValue.CUST_NAME,namaValid);
            mBun.putString(DefineValue.CUST_PHONE,noHPValid);
            mBun.putString(DefineValue.CUST_EMAIL,emailValid);
			mBun.putBoolean(DefineValue.IS_FACEBOOK, isFacebook);

//            if(isFacebook)
//                mBun.putString(DefineValue.DATA_FACEBOOK, dataFacebook);
            mFragment.setArguments(mBun);
            switchFragment(mFragment, "reg2", true);
        }
    }

    private void showDialog(final String code) {
        // Create custom dialog object
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCanceledOnTouchOutside(false);
        // Include dialog.xml file
        dialog.setContentView(R.layout.dialog_notification);

        // set values for custom dialog components - text, image and button
        Button btnDialogOTP = (Button)dialog.findViewById(R.id.btn_dialog_notification_ok);
        TextView Title = (TextView)dialog.findViewById(R.id.title_dialog);
        TextView Message = (TextView)dialog.findViewById(R.id.message_dialog);

        Message.setVisibility(View.VISIBLE);
        Title.setText(getResources().getString(R.string.regist1_notif_title));
        if(code.equals("0002")){
            Title.setText(getResources().getString(R.string.regist1_notif_title_registered));
            Message.setText(getResources().getString(R.string.regist1_notif_message_registered));
        }
        else if(code.equals(WebParams.SUCCESS_CODE)){
            Title.setText(getResources().getString(R.string.regist1_notif_title_verification));
            Message.setText(getString(R.string.appname)+" "+getString(R.string.regist1_notif_message_email));
        }

        btnDialogOTP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(code.equals("0002")) changeActivity(true);
                else if(code.equals(WebParams.SUCCESS_CODE)) changeActivity(false);

                dialog.dismiss();
            }
        });

        dialog.show();
    }

    //----------------------------------------------------------------------------------------------------------------

    private boolean inputValidation(){
        if(noHPValue.getText().toString().length()==0){
            noHPValue.requestFocus();
            noHPValue.setError(getResources().getString(R.string.regist1_validation_nohp));
            return false;
        }
        else if(namaValue.getText().toString().length()<2){
            namaValue.requestFocus();
            namaValue.setError(getResources().getString(R.string.regist1_validation_nama));
            return false;
        }
        else if(emailValue.getText().toString().length()==0){
            emailValue.requestFocus();
            emailValue.setError(getResources().getString(R.string.regist1_validation_email_length));
            return false;
        }
        else if(emailValue.getText().toString().length()>0 && !isValidEmail(emailValue.getText()) ){
            emailValue.requestFocus();
            emailValue.setError(getString(R.string.regist1_validation_email));
            return false;
        }
        return true;
    }

    private boolean isSimExists()
    {
        TelephonyManager telephonyManager = (TelephonyManager) getActivity().getSystemService(Context.TELEPHONY_SERVICE);
        int SIM_STATE = telephonyManager.getSimState();

        if(SIM_STATE == TelephonyManager.SIM_STATE_READY)
            return true;
        else
        {
            switch(SIM_STATE)
            {
                case TelephonyManager.SIM_STATE_ABSENT: //SimState = "No Sim Found!";
                    break;
                case TelephonyManager.SIM_STATE_NETWORK_LOCKED: //SimState = "Network Locked!";
                    break;
                case TelephonyManager.SIM_STATE_PIN_REQUIRED: //SimState = "PIN Required to access SIM!";
                    break;
                case TelephonyManager.SIM_STATE_PUK_REQUIRED: //SimState = "PUK Required to access SIM!"; // Personal Unblocking Code
                    break;
                case TelephonyManager.SIM_STATE_UNKNOWN: //SimState = "Unknown SIM State!";
                    break;
            }
            return false;
        }
    }

    private static boolean isValidEmail(CharSequence target) {
        return target != null && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {
        switch (requestCode) {
            case RC_READ_SMS:
                if (isSimExists()) {
                    TelephonyManager tm = (TelephonyManager) getActivity().getSystemService(Context.TELEPHONY_SERVICE);
                    String Nomor1 = tm.getLine1Number();

                    noHPValue.setText(Nomor1);
                }
                break;
        }
    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {
        switch (requestCode) {
            case RC_READ_SMS:
                break;
        }
    }

//    @Override
//    public void onDestroy() {
//        super.onDestroy();
//        mFaceFunction.getmATT().stopTracking();
//        mFaceFunction.getmPT().stopTracking();
//    }
//
//    @Override
//    public void OnLoginFBSuccess() {
//        isFacebook = true;
//        progdialog.show();
//        mFaceFunction.getUserData(new GraphRequest.GraphJSONObjectCallback() {
//            @Override
//            public void onCompleted(JSONObject object, GraphResponse response) {
//                try {
//                    final String name = object.getString(WebParams.NAME);
//                    final String email = object.getString(WebParams.EMAIL);
////                            Toast.makeText(getActivity(), "response is: "+object.toString(), Toast.LENGTH_LONG).show();
//                    Timber.d("isi response graph:" + response.toString());
//                    final View layout_btn_fb = v.findViewById(R.id.layout_btn_facebook);
////                            final TextView subTitle = (TextView) v.findViewById(R.id.text_subtitle);
//
//                    getActivity().runOnUiThread(new Runnable() {
//                        @Override
//                        public void run() {
//                            namaValue.setText(name);
//                            emailValue.setText(email);
//                            layout_btn_fb.setVisibility(View.GONE);
////                                    subTitle.setText(getString(R.string.regist1_subtext_facebook));
////                                    subTitle.setVisibility(View.VISIBLE);
//
//                        }
//                    });
//                    dataFacebook = object.toString();
//                    if(progdialog.isShowing())
//                        progdialog.dismiss();
//
//                } catch (Exception e) {
//                    Toast.makeText(getActivity(), "error is: " + e.toString(), Toast.LENGTH_LONG).show();
//                }
//            }
//        });
//
//    }
//
//    @Override
//    public void OnLoginFBFailed(FacebookException error) {
//
//    }
//
//    @Override
//    public void OnProfileChange() {
//
//    }

}