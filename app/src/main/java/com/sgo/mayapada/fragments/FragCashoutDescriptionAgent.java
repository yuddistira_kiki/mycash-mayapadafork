package com.sgo.mayapada.fragments;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.securepreferences.SecurePreferences;
import com.sgo.mayapada.R;
import com.sgo.mayapada.activities.AgentActivity;
import com.sgo.mayapada.coreclass.CurrencyFormat;
import com.sgo.mayapada.coreclass.CustomSecurePref;
import com.sgo.mayapada.coreclass.DefineValue;
import com.sgo.mayapada.coreclass.ErrorDefinition;
import com.sgo.mayapada.coreclass.InetHandler;
import com.sgo.mayapada.coreclass.MyApiClient;
import com.sgo.mayapada.coreclass.WebParams;
import com.sgo.mayapada.dialogs.AlertDialogLogout;
import com.sgo.mayapada.dialogs.DefinedDialog;
import com.sgo.mayapada.securities.Md5;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.security.NoSuchAlgorithmException;

import timber.log.Timber;

/**
 * Created by thinkpad on 7/21/2016.
 */
public class FragCashoutDescriptionAgent extends Fragment {
    private View v;
    private SecurePreferences sp;
    private String userID;
    private String accessKey;
    private String memberID;
    private String txID;
    private String name;
    private String amount;
    private String ccyId;
    private String fee;
    private String total;
    private String member_no;
    private String member_name;
    private TextView tvTxID;
    private TextView tvNoHP;
    private TextView tvNameHP;
    private TextView tvAmount;
    private TextView tvFee;
    private TextView tvTotal;
    private EditText tokenValue;
    private Button btnConfirm;
    private Button btnCancel;
    private ProgressDialog progdialog;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.frag_cash_out_desc_agent, container, false);
        return v;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        sp = CustomSecurePref.getInstance().getmSecurePrefs();
        userID = sp.getString(DefineValue.USERID_PHONE, "");
        accessKey = sp.getString(DefineValue.ACCESS_KEY, "");
        memberID = sp.getString(DefineValue.MEMBER_ID, "");
        name = sp.getString(DefineValue.USER_NAME, "");

        tvTxID = (TextView) v.findViewById(R.id.cashoutagent_tx_id_value);
        tvNoHP = (TextView) v.findViewById(R.id.cashoutagent_no_value);
        tvNameHP  = (TextView) v.findViewById(R.id.cashoutagent_name_value);
        tvAmount  = (TextView) v.findViewById(R.id.cashoutagent_amount_value);
        tvFee  = (TextView) v.findViewById(R.id.cashoutagent_fee_value);
        tvTotal  = (TextView) v.findViewById(R.id.cashoutagent_total_value);
        tokenValue = (EditText) v.findViewById(R.id.cashoutagent_value_otp);
        btnConfirm = (Button) v.findViewById(R.id.cashoutagent_btn_verification);
        btnCancel = (Button) v.findViewById(R.id.cashoutagent_btn_cancel);

        Bundle bundle = getArguments();
        if(bundle != null) {
            txID = bundle.getString(DefineValue.TX_ID);
            ccyId = bundle.getString(DefineValue.CCY_ID);
            amount = bundle.getString(DefineValue.AMOUNT);
            fee = bundle.getString(DefineValue.FEE);
            total = bundle.getString(DefineValue.TOTAL_AMOUNT);
            member_no = bundle.getString(DefineValue.MEMBER_CUST_FROM);
            member_name = bundle.getString(DefineValue.MEMBER_NAME_FROM);

            tvTxID.setText(txID);
            tvNoHP.setText(member_no);
            tvNameHP.setText(member_name);
            tvAmount.setText(ccyId + ". " + CurrencyFormat.format(amount));
            tvFee.setText(ccyId + ". " + CurrencyFormat.format(fee));
            tvTotal.setText(ccyId + ". " + CurrencyFormat.format(total));
        }

        btnConfirm.setOnClickListener(btnConfirmListener);
        btnCancel.setOnClickListener(btnCancelListener);
    }

    private Button.OnClickListener btnConfirmListener = new Button.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(InetHandler.isNetworkAvailable(getActivity())) {
                if(inputValidation()) {
                    try {
                        confirmTokenATC(Md5.hashMd5(tokenValue.getText().toString()));
                    } catch (NoSuchAlgorithmException e) {
                        e.printStackTrace();
                    }
                }
            }
            else DefinedDialog.showErrorDialog(getActivity(), getString(R.string.inethandler_dialog_message));
        }
    };

    private Button.OnClickListener btnCancelListener = new Button.OnClickListener() {
        @Override
        public void onClick(View v) {
            getActivity().getSupportFragmentManager().popBackStack();
        }
    };

    private void confirmTokenATC(String _token) {
        try{
            progdialog = DefinedDialog.CreateProgressDialog(getActivity(), "");
            progdialog.show();

            RequestParams params;
            params = MyApiClient.getSignatureWithParams(MyApiClient.COMM_ID, MyApiClient.LINK_CONFIRM_TOKEN_ATC,
                    userID, accessKey);

            params.put(WebParams.MEMBER_ID, memberID);
            params.put(WebParams.TX_ID, txID);
            params.put(WebParams.COMM_ID, MyApiClient.COMM_ID);
            params.put(WebParams.TOKEN_MEMBER, _token);
            params.put(WebParams.USER_ID, userID);
            Timber.d("isi params sent confirm token ATC:" + params.toString());

            MyApiClient.sentConfirmTokenATC(getActivity(), params, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    progdialog.dismiss();
                    Timber.d("isi response sent confirm token ATC:" + response.toString());

                    try {
                        String code = response.getString(WebParams.ERROR_CODE);
                        if (code.equals(WebParams.SUCCESS_CODE)) {
                            AlertDialog test = DefinedDialog.BuildAlertDialog(getActivity(),
                                    null,
                                    getString(R.string.cashoutagent_success),
                                    0,
                                    false,
                                    getString(R.string.ok), new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();
                                            getActivity().finish();
                                        }
                                    });
                            test.show();
                        } else if (code.equals(WebParams.LOGOUT_CODE)) {
                            String message = response.getString(WebParams.ERROR_MESSAGE);
                            AlertDialogLogout test = AlertDialogLogout.getInstance();
                            test.showDialoginActivity(getActivity(), message);
                        } else if(code.equals(ErrorDefinition.ERROR_CODE_WRONG_TOKEN)) {
                            String message = response.getString(WebParams.ERROR_MESSAGE);
                            Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
                        } else {
                            String message = response.getString(WebParams.ERROR_MESSAGE);
                            Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
                            getActivity().finish();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    super.onFailure(statusCode, headers, responseString, throwable);
                    failure(throwable);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    failure(throwable);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    failure(throwable);
                }

                private void failure(Throwable throwable) {
                    if (MyApiClient.PROD_FAILURE_FLAG)
                        Toast.makeText(getActivity(), getString(R.string.network_connection_failure_toast), Toast.LENGTH_SHORT).show();
                    else
                        Toast.makeText(getActivity(), throwable.toString(), Toast.LENGTH_SHORT).show();
                    if (progdialog.isShowing())
                        progdialog.dismiss();

                    Timber.w("Error Koneksi sent confirm token ATC:" + throwable.toString());
                }
            });
        }catch (Exception e){
            Timber.d("httpclient:"+e.getMessage());
        }
    }

    private boolean inputValidation(){
        if(tokenValue.getText().toString().length()==0){
            tokenValue.requestFocus();
            tokenValue.setError(getString(R.string.cashoutagent_validation_otp));
            return false;
        }
        return true;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                getFragmentManager().popBackStack();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onResume() {
        super.onResume();
        setTitle(getString(R.string.toolbar_title_cashout));
    }

    private void setTitle(String _title){
        if (getActivity() == null)
            return;

        AgentActivity fca = (AgentActivity) getActivity();
        fca.setTitleFragment(_title);
    }
}
