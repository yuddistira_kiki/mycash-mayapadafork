package com.sgo.mayapada.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.sgo.mayapada.Beans.TrxStatusTagihanBean;
import com.sgo.mayapada.R;
import com.sgo.mayapada.adapter.ReportTagihanEspayAdapter;
import com.sgo.mayapada.coreclass.CurrencyFormat;
import com.sgo.mayapada.coreclass.ListViewHeight;
import com.sgo.mayapada.coreclass.WebParams;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;


/**
 * Created by thinkpad on 8/28/2015.
 */
public class FragReportTagihanEspay extends Fragment {

    private Button btnOk;
    private TextView tvPaidTo;
    private TextView tvUserId;
    private TextView tvBankName;
    private TextView tvProductName;
    private TextView tvFeeName;
    private ListView lvTransaksi;
    private ReportTagihanEspayAdapter reportEspayAdapter;
    private ArrayList<TrxStatusTagihanBean> listTransaksi;
    //IB atau SMS
    private String type;
    private String comm_id;
    private String comm_name;
    private String comm_code;

    public static FragReportTagihanEspay newInstance(String data_detail, String fee, String type) {
        FragReportTagihanEspay f = new FragReportTagihanEspay();

        Bundle args = new Bundle();
        args.putString("data_detail", data_detail);
        args.putString("fee", fee);
        args.putString("type", type);
        f.setArguments(args);

        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frag_report_tagihan_espay, container, false);

        listTransaksi = new ArrayList<>();

        tvPaidTo = (TextView) view.findViewById(R.id.paid_to_value);
        tvUserId = (TextView) view.findViewById(R.id.user_id_value);
        tvBankName = (TextView) view.findViewById(R.id.bank_name_value);
        tvProductName = (TextView) view.findViewById(R.id.product_name_value);
        tvFeeName = (TextView) view.findViewById(R.id.fee_value);
        lvTransaksi = (ListView) view.findViewById(R.id.lvTransaksi);
        btnOk = (Button) view.findViewById(R.id.report_btn_ok);

        Bundle bundle = getArguments();
        if(bundle != null) {
            String data_detail = bundle.getString("data_detail");
            String fee = bundle.getString("fee");
            type = bundle.getString("type");

            if(type.equalsIgnoreCase("SMS")) {
                comm_id = bundle.getString("comm_id");
                comm_name = bundle.getString("comm_name");
                comm_code = bundle.getString("comm_code");
            }
            Log.d("detaill", data_detail);
            try {
                JSONArray arr_data_detail = new JSONArray(data_detail);
                for (int i = 0; i < arr_data_detail.length(); i++) {
                    TrxStatusTagihanBean trxStatusBean = new TrxStatusTagihanBean();
                    trxStatusBean.setTx_id(arr_data_detail.getJSONObject(i).getString(WebParams.TX_ID));
                    trxStatusBean.setTx_amount(arr_data_detail.getJSONObject(i).getString(WebParams.TX_AMOUNT));
                    trxStatusBean.setTx_status(arr_data_detail.getJSONObject(i).getString(WebParams.TX_STATUS));
                    trxStatusBean.setTx_date(arr_data_detail.getJSONObject(i).getString(WebParams.TX_DATE));
                    if(arr_data_detail.getJSONObject(i).getString(WebParams.TX_REMARK).equals(null) || arr_data_detail.getJSONObject(i).getString(WebParams.TX_REMARK).equals("null"))
                        trxStatusBean.setTx_remark("");
                    else
                        trxStatusBean.setTx_remark(arr_data_detail.getJSONObject(i).getString(WebParams.TX_REMARK));
                    trxStatusBean.setBank_name(arr_data_detail.getJSONObject(i).getString(WebParams.BANK_NAME));
                    trxStatusBean.setComm_name(arr_data_detail.getJSONObject(i).getString(WebParams.COMM_NAME));
                    trxStatusBean.setMember_code(arr_data_detail.getJSONObject(i).getString(WebParams.MEMBER_CODE));
                    trxStatusBean.setProduct_name(arr_data_detail.getJSONObject(i).getString(WebParams.PRODUCT_NAME));
                    trxStatusBean.setCcy_id(arr_data_detail.getJSONObject(i).getString(WebParams.CCY_ID));
                    trxStatusBean.setPaid_to(arr_data_detail.getJSONObject(i).getString(WebParams.ANCHOR_CUST_NAME));
                    listTransaksi.add(trxStatusBean);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            tvPaidTo.setText(listTransaksi.get(0).getPaid_to());
            tvUserId.setText(listTransaksi.get(0).getMember_code());
            tvBankName.setText(listTransaksi.get(0).getBank_name());
            tvProductName.setText(listTransaksi.get(0).getProduct_name());
            if(listTransaksi.get(0).getCcy_id().equalsIgnoreCase("idr"))
                tvFeeName.setText("Rp. " + CurrencyFormat.format(fee));
            else
                tvFeeName.setText(listTransaksi.get(0).getCcy_id() + " " + fee);
        }

        reportEspayAdapter = new ReportTagihanEspayAdapter(getActivity(), listTransaksi);
        lvTransaksi.setAdapter(reportEspayAdapter);
        lvTransaksi.setOnTouchListener(new View.OnTouchListener() {
            // Setting on Touch Listener for handling the touch inside ScrollView
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                // Disallow the touch request for parent scroll on touch of child view
                v.getParent().requestDisallowInterceptTouchEvent(true);
                return false;
            }
        });

        ListViewHeight.setTotalHeightofListView(lvTransaksi);

        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().finish();
            }
        });
        return view;
    }
}
