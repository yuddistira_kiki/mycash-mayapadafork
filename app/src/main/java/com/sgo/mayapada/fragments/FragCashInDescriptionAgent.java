package com.sgo.mayapada.fragments;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.securepreferences.SecurePreferences;
import com.sgo.mayapada.R;
import com.sgo.mayapada.activities.AgentActivity;
import com.sgo.mayapada.activities.InsertPIN;
import com.sgo.mayapada.activities.MainPage;
import com.sgo.mayapada.coreclass.CurrencyFormat;
import com.sgo.mayapada.coreclass.CustomSecurePref;
import com.sgo.mayapada.coreclass.DateTimeFormat;
import com.sgo.mayapada.coreclass.DefineValue;
import com.sgo.mayapada.coreclass.ErrorDefinition;
import com.sgo.mayapada.coreclass.InetHandler;
import com.sgo.mayapada.coreclass.MyApiClient;
import com.sgo.mayapada.coreclass.WebParams;
import com.sgo.mayapada.dialogs.AlertDialogLogout;
import com.sgo.mayapada.dialogs.DefinedDialog;
import com.sgo.mayapada.dialogs.ReportBillerDialog;
import com.sgo.mayapada.interfaces.OnLoadDataListener;
import com.sgo.mayapada.loader.UtilsLoader;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import timber.log.Timber;

/**
 * Created by thinkpad on 7/14/2016.
 */
public class FragCashInDescriptionAgent extends Fragment implements ReportBillerDialog.OnDialogOkCallback{
    private View v;
    private SecurePreferences sp;
    private String userID;
    private String accessKey;
    private String memberID;
    private String authType;
    private String txID;
    private String name;
    private String amount;
    private String ccyId;
    private String fee;
    private String total;
    private String message;
    private String member_no;
    private String member_name;
    private LinearLayout layoutOTP;
    private TextView tvTxID;
    private TextView tvNoHP;
    private TextView tvNameHP;
    private TextView tvMessage;
    private TextView tvAmount;
    private TextView tvFee;
    private TextView tvTotal;
    private EditText tokenValue;
    private Button btnConfirm;
    private Button btnCancel;
    private Button btnResend;
    private int pin_attempt=-1;
    private boolean isPIN;
    private boolean isOTP;
    private ProgressDialog progdialog;
    private int max_token_resend = 3;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        ccyId = MyApiClient.CCY_VALUE;
        sp = CustomSecurePref.getInstance().getmSecurePrefs();
        userID = sp.getString(DefineValue.USERID_PHONE, "");
        accessKey = sp.getString(DefineValue.ACCESS_KEY, "");
        memberID = sp.getString(DefineValue.MEMBER_ID,"");
        authType = sp.getString(DefineValue.AUTHENTICATION_TYPE,"");
        name = sp.getString(DefineValue.USER_NAME,"");

        isPIN = authType.equalsIgnoreCase(DefineValue.AUTH_TYPE_PIN);
        isOTP = authType.equalsIgnoreCase(DefineValue.AUTH_TYPE_OTP);

        tvTxID = (TextView) v.findViewById(R.id.cashinagent_tx_id_value);
        tvNoHP = (TextView) v.findViewById(R.id.cashinagent_no_value);
        tvNameHP  = (TextView) v.findViewById(R.id.cashinagent_name_value);
        tvMessage  = (TextView) v.findViewById(R.id.cashinagent_message_value);
        tvAmount  = (TextView) v.findViewById(R.id.cashinagent_amount_value);
        tvFee  = (TextView) v.findViewById(R.id.cashinagent_fee_value);
        tvTotal  = (TextView) v.findViewById(R.id.cashinagent_total_value);
        layoutOTP = (LinearLayout) v.findViewById(R.id.cashinagent_layout_OTP);
        tokenValue = (EditText) v.findViewById(R.id.cashinagent_value_otp);
        btnConfirm = (Button) v.findViewById(R.id.cashinagent_btn_verification);
        btnCancel = (Button) v.findViewById(R.id.cashinagent_btn_cancel);

        Bundle bundle = getArguments();
        if(bundle != null) {
            txID = bundle.getString(DefineValue.TX_ID);
            amount = bundle.getString(DefineValue.AMOUNT);
            fee = bundle.getString(DefineValue.FEE);
            total = bundle.getString(DefineValue.TOTAL_AMOUNT);
            message = bundle.getString(DefineValue.MESSAGE);
            member_no = bundle.getString(DefineValue.MEMBER_CUST_TO);
            member_name = bundle.getString(DefineValue.MEMBER_NAME_TO);

            tvTxID.setText(txID);
            tvNoHP.setText(member_no);
            tvNameHP.setText(member_name);
            tvAmount.setText(ccyId + ". " + CurrencyFormat.format(amount));
            if(message.equals(""))
                message = "-";
            tvMessage.setText(message);
            tvFee.setText(ccyId + ". " + CurrencyFormat.format(fee));
            tvTotal.setText(ccyId + ". " + CurrencyFormat.format(total));
        }

        if(isOTP) {
            layoutOTP.setVisibility(View.VISIBLE);
            btnResend = (Button) v.findViewById(R.id.btn_resend_token);

            View layout_resendbtn = v.findViewById(R.id.layout_btn_resend);
            layout_resendbtn.setVisibility(View.VISIBLE);

            btnResend.setOnClickListener(resendListener);
            changeTextBtnSub();
        }
        else {
            layoutOTP.setVisibility(View.GONE);
            new UtilsLoader(getActivity(),sp).getFailedPIN(userID, new OnLoadDataListener() { //get pin attempt
                @Override
                public void onSuccess(Object deData) {
                    pin_attempt = (int) deData;
                }

                @Override
                public void onFail(Bundle message) {

                }

                @Override
                public void onFailure(String message) {

                }
            });
        }

        btnConfirm.setOnClickListener(btnConfirmListener);
        btnCancel.setOnClickListener(btnCancelListener);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.frag_cash_in_desc_agent, container, false);
        return v;
    }

    private Button.OnClickListener btnConfirmListener = new Button.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (InetHandler.isNetworkAvailable(getActivity())) {
                if(isPIN) {
                    Intent i = new Intent(getActivity(), InsertPIN.class);
                    if(pin_attempt != -1 && pin_attempt < 2)
                        i.putExtra(DefineValue.ATTEMPT,pin_attempt);
                    startActivityForResult(i, MainPage.REQUEST_FINISH);
                }
                else if(isOTP) {
                    if (inputValidation())
                        confirmCashin(tokenValue.getText().toString());
                }
                else {
                    Toast.makeText(getActivity(), "Authentication type kosong", Toast.LENGTH_LONG).show();
                }
            }
            else DefinedDialog.showErrorDialog(getActivity(), getString(R.string.inethandler_dialog_message));
        }
    };

    private Button.OnClickListener btnCancelListener = new Button.OnClickListener() {
        @Override
        public void onClick(View v) {
            getActivity().getSupportFragmentManager().popBackStack();
        }
    };

    private Button.OnClickListener resendListener = new Button.OnClickListener() {
        @Override
        public void onClick(View view) {
            if(InetHandler.isNetworkAvailable(getActivity())){
                if(authType.equalsIgnoreCase(DefineValue.AUTH_TYPE_OTP)) {
                    if (max_token_resend != 0)
                        sentResendToken(txID);

                }
            }
            else DefinedDialog.showErrorDialog(getActivity(), getString(R.string.inethandler_dialog_message));

        }
    };

    private void changeTextBtnSub() {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                btnResend.setText(getString(R.string.reg2_btn_text_resend_token_sms) + " (" + max_token_resend + ")");
            }
        });
    }

    private void confirmCashin(String _token){
        try {
            progdialog = DefinedDialog.CreateProgressDialog(getActivity(), "");
            progdialog.show();

            RequestParams params = MyApiClient.getSignatureWithParams(MyApiClient.COMM_ID, MyApiClient.LINK_CONFIRM_CASH_IN,
                    userID, accessKey);
            params.put(WebParams.MEMBER_ID, memberID);
            params.put(WebParams.TX_ID, txID);
            params.put(WebParams.COMM_ID, MyApiClient.COMM_ID);
            params.put(WebParams.USER_ID, userID);
            params.put(WebParams.TOKEN_ID, _token);
            Timber.d("isi param confirm cashin:" + params.toString());

            MyApiClient.sentConfirmCashIn(getActivity(), params, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    progdialog.dismiss();
                    Timber.d("isi response confirm cashin:" + response.toString());

                    try {
                        String code = response.getString(WebParams.ERROR_CODE);
                        if (code.equals(WebParams.SUCCESS_CODE)) {
                            showReportBillerDialog(response.getString(WebParams.TX_STATUS), name, DateTimeFormat.getCurrentDateTime(), userID, txID,
                                    ccyId + " " + CurrencyFormat.format(amount),
                                    ccyId + " " + CurrencyFormat.format(fee), ccyId + " " + CurrencyFormat.format(total));
                        } else if (code.equals(WebParams.LOGOUT_CODE)) {
                            String message = response.getString(WebParams.ERROR_MESSAGE);
                            AlertDialogLogout test = AlertDialogLogout.getInstance();
                            test.showDialoginActivity(getActivity(), message);
                        } else {
                            String code_msg = response.getString(WebParams.ERROR_MESSAGE);
                            if (isPIN && code.equals(ErrorDefinition.WRONG_PIN_CASHOUT)) {
                                Intent i = new Intent(getActivity(), InsertPIN.class);
                                pin_attempt = pin_attempt - 1;
                                if (pin_attempt != -1 && pin_attempt < 2)
                                    i.putExtra(DefineValue.ATTEMPT, pin_attempt);

                                startActivityForResult(i, MainPage.REQUEST_FINISH);
                            } else if(code.equals(ErrorDefinition.ERROR_CODE_WRONG_TOKEN)) {

                            } else {
                                onOkButton();
                            }

                            Toast.makeText(getActivity(), code_msg, Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    super.onFailure(statusCode, headers, responseString, throwable);
                    failure(throwable);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    failure(throwable);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    failure(throwable);
                }

                private void failure(Throwable throwable) {
                    if (MyApiClient.PROD_FAILURE_FLAG)
                        Toast.makeText(getActivity(), getString(R.string.network_connection_failure_toast), Toast.LENGTH_SHORT).show();
                    else
                        Toast.makeText(getActivity(), throwable.toString(), Toast.LENGTH_SHORT).show();

                    if (progdialog.isShowing())
                        progdialog.dismiss();

                    Timber.w("Error Koneksi confirm cashin:" + throwable.toString());
                }

            });
        }catch (Exception e){
            Timber.d("httpclient:"+e.getMessage());
        }
    }

    private void sentResendToken(String _data){
        try{
            progdialog = DefinedDialog.CreateProgressDialog(getActivity(), "");
            progdialog.show();

            RequestParams params = MyApiClient.getSignatureWithParams(MyApiClient.COMM_ID,MyApiClient.LINK_RESEND_TOKEN_LKD,
                    userID,accessKey);
            params.put(WebParams.TX_ID,_data);
            params.put(WebParams.USER_ID, userID);
            params.put(WebParams.COMM_ID, MyApiClient.COMM_ID);

            Timber.d("isi params sent resend token :"+params.toString());

            MyApiClient.sentResendTokenLKD(getActivity(), params, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    progdialog.dismiss();
                    try {
                        String code = response.getString(WebParams.ERROR_CODE);
                        if (code.equals(WebParams.SUCCESS_CODE)) {
                            Timber.d("isi params resend token :" + response.toString());
                            max_token_resend = max_token_resend - 1;
                            changeTextBtnSub();
                            Toast.makeText(getActivity(), getString(R.string.reg2_notif_text_resend_token), Toast.LENGTH_SHORT).show();
                        } else if (code.equals(WebParams.LOGOUT_CODE)) {
                            Timber.d("isi response autologout:" + response.toString());
                            String message = response.getString(WebParams.ERROR_MESSAGE);
                            AlertDialogLogout test = AlertDialogLogout.getInstance();
                            test.showDialoginActivity(getActivity(), message);
                        } else {
                            Timber.d("isi error resend token :" + response.toString());
                            code = response.getString(WebParams.ERROR_MESSAGE);
                            Toast.makeText(getActivity(), code, Toast.LENGTH_SHORT).show();
                        }
                        if (max_token_resend == 0) {
                            btnResend.setEnabled(false);
                            Toast.makeText(getActivity(), getString(R.string.notif_max_resend_token_empty), Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    super.onFailure(statusCode, headers, responseString, throwable);
                    failure(throwable);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    failure(throwable);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    failure(throwable);
                }

                private void failure(Throwable throwable) {
                    if (MyApiClient.PROD_FAILURE_FLAG)
                        Toast.makeText(getActivity(), getString(R.string.network_connection_failure_toast), Toast.LENGTH_SHORT).show();
                    else
                        Toast.makeText(getActivity(), throwable.toString(), Toast.LENGTH_SHORT).show();
                    if (progdialog.isShowing())
                        progdialog.dismiss();

                    Timber.w("Error Koneksi resend token:" + throwable.toString());
                }
            });
        }catch (Exception e){
            Timber.d("httpclient:"+e.getMessage());
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == MainPage.REQUEST_FINISH){
            if(resultCode == InsertPIN.RESULT_PIN_VALUE){
                String value_pin = data.getStringExtra(DefineValue.PIN_VALUE);
                confirmCashin(value_pin);
            }
        }
    }

    private void showReportBillerDialog(String _tx_status, String _name, String _date, String _userId, String _txId,
                                        String _nominal, String _fee, String _totalAmount) {

        Bundle args = new Bundle();
        ReportBillerDialog dialog = new ReportBillerDialog();
        Boolean txStat = false;
        if (_tx_status.equals(DefineValue.SUCCESS)){
            txStat = true;
            args.putString(DefineValue.TRX_MESSAGE, getString(R.string.transaction_success));
        }else if(_tx_status.equals(DefineValue.ONRECONCILED)){
            txStat = true;
            args.putString(DefineValue.TRX_MESSAGE, getString(R.string.transaction_pending));
        }else if(_tx_status.equals(DefineValue.SUSPECT)){
            args.putString(DefineValue.TRX_MESSAGE, getString(R.string.transaction_suspect));
        }
        else if(!_tx_status.equals(DefineValue.FAILED)){
            args.putString(DefineValue.TRX_MESSAGE, getString(R.string.transaction)+" "+_tx_status);
        }
        else {
            args.putString(DefineValue.TRX_MESSAGE, getString(R.string.transaction_failed));
        }
        args.putBoolean(DefineValue.TRX_STATUS, txStat);

        args.putString(DefineValue.USER_NAME,_name);
        args.putString(DefineValue.DATE_TIME,_date);
        args.putString(DefineValue.USERID_PHONE,_userId);
        args.putString(DefineValue.TX_ID,_txId);
        args.putString(DefineValue.AMOUNT,_nominal);
        args.putString(DefineValue.FEE,_fee);
        args.putString(DefineValue.TOTAL_AMOUNT,_totalAmount);
        args.putString(DefineValue.MESSAGE,message);
        args.putString(DefineValue.MEMBER_CUST_TO,member_no);
        args.putString(DefineValue.MEMBER_NAME_TO,member_name);
        args.putString(DefineValue.REPORT_TYPE,DefineValue.CASHINAGENT);

        dialog.setArguments(args);
        dialog.setTargetFragment(this,0);
        dialog.show(getActivity().getSupportFragmentManager(),ReportBillerDialog.TAG);
    }

    private boolean inputValidation(){
        if(tokenValue.getText().toString().length()==0){
            tokenValue.requestFocus();
            tokenValue.setError(getString(R.string.cashinagent_validation_otp));
            return false;
        }
        return true;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                getFragmentManager().popBackStack();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onOkButton() {
        getActivity().setResult(MainPage.RESULT_BALANCE);
        getActivity().finish();
    }

    @Override
    public void onResume() {
        super.onResume();
        setTitle(getString(R.string.toolbar_title_cashin));
    }

    private void setTitle(String _title){
        if (getActivity() == null)
            return;

        AgentActivity fca = (AgentActivity) getActivity();
        fca.setTitleFragment(_title);
    }
}
