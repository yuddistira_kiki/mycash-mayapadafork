package com.sgo.mayapada.fragments;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.securepreferences.SecurePreferences;
import com.sgo.mayapada.entityRealm.Target_Saving_Model;
import com.sgo.mayapada.R;
import com.sgo.mayapada.activities.AskRewardActivity;
import com.sgo.mayapada.activities.MainPage;
import com.sgo.mayapada.activities.TabunganActivity;
import com.sgo.mayapada.coreclass.CurrencyFormat;
import com.sgo.mayapada.coreclass.CustomSecurePref;
import com.sgo.mayapada.coreclass.DateTimeFormat;
import com.sgo.mayapada.coreclass.DefineValue;
import com.sgo.mayapada.coreclass.ErrorDefinition;
import com.sgo.mayapada.coreclass.MyApiClient;
import com.sgo.mayapada.coreclass.WebParams;
import com.sgo.mayapada.dialogs.AlertDialogLogout;
//import com.txusballesteros.widgets.FitChart;
//import com.txusballesteros.widgets.FitChartValue;

import org.apache.http.Header;
import org.joda.time.DateTime;
import org.joda.time.Days;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import io.realm.Realm;
import timber.log.Timber;


public class TabunganMenuInput extends Fragment implements View.OnClickListener{

    private View mView,layout_empty,layout_success;
    private TextView tv_current_day;
    private TextView percentValue;
    private TextView tv_savings_value;
//    private FitChart fitChart;
    private Realm realm;
    private String userID;
    private String accessKey;
    private String memberID;
    private SecurePreferences sp;
    private Target_Saving_Model target_saving_model;
    private DateTime currentDate;
    private DateTime start;
    private DateTime end;
    private float total_temp = 0f;
    private int hariKe;
    private int percent;
    private int jumlahHari;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        realm = Realm.getDefaultInstance();
        sp = CustomSecurePref.getInstance().getmSecurePrefs();

        userID = sp.getString(DefineValue.USERID_PHONE,"");
        accessKey = sp.getString(DefineValue.ACCESS_KEY,"");
        memberID = sp.getString(DefineValue.MEMBER_ID,"");
        currentDate = new DateTime();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.frag_tabungan_menu, container,false);
        percentValue = (TextView) mView.findViewById(R.id.percent_value);
        mView.findViewById(R.id.button_rek_tabungan).setOnClickListener(this);
        mView.findViewById(R.id.button_automat_nabung).setOnClickListener(this);
        mView.findViewById(R.id.button_target_nabung).setOnClickListener(this);
        mView.findViewById(R.id.button_nabung).setOnClickListener(this);

        tv_current_day = (TextView) mView.findViewById(R.id.tv_days_value);
        tv_savings_value = (TextView) mView.findViewById(R.id.savings_value);
        layout_empty = mView.findViewById(R.id.layout_no_saving_target);
        layout_success = mView.findViewById(R.id.layout_success_target);
        layout_success.setOnClickListener(this);

        return mView;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

//        fitChart = (FitChart) mView.findViewById(R.id.fitChart);
//        fitChart.setMinValue(0f);
//        fitChart.setMaxValue(100f);

//        refreshUI();
    }


    private void refreshUI(){
        if (this.isVisible()) {
            if (checkingTarget()) {
                layout_empty.setVisibility(View.GONE);
                tv_current_day.setText(String.valueOf(hariKe) + " / " + String.valueOf(jumlahHari));
                percentValue.setText(String.valueOf(percent) + "%");
                String savings = target_saving_model.getCcy_id() + ". " + CurrencyFormat.format(target_saving_model.getAchieved_amount())
                        + "/" + target_saving_model.getCcy_id() + ". " + CurrencyFormat.format(target_saving_model.getTargeted_amount());
                tv_savings_value.setText(savings);
            } else {
                tv_current_day.setText(getString(R.string.saving_text_default_days));
                percentValue.setText(getString(R.string.saving_text_default_percent));
                total_temp = 0f;
                tv_savings_value.setText(getString(R.string.saving_text_default_savings));
                layout_empty.setVisibility(View.VISIBLE);
                layout_success.setVisibility(View.GONE);
            }

//            setFitChart(total_temp, fitChart);
        }
    }

    private boolean checkingTarget(){
        target_saving_model = realm.where(Target_Saving_Model.class).findFirst();
        if(target_saving_model == null) {
            Timber.d("target saving model kosong");
            total_temp = 0f;
            return false;
        }

        String dateStart = target_saving_model.getTarget_start();
//        Log.i("dateStart",dateStart);
        String dateEnd = target_saving_model.getTarget_end();
        String currentDated = DateTimeFormat.getCurrentDate();
//        String asds = currentDate.toString();
//        Log.i("dateEnd",dateEnd);
//        Log.i("currentDated",currentDated);
//        Log.i("currentDate",asds);
        start = new DateTime(dateStart);
        end = new DateTime(dateEnd);
        Date started = DateTimeFormat.convertStringtoCustomDate(dateStart);
        Date current = DateTimeFormat.convertStringtoCustomDate(currentDated);
//        hariKe = Days.daysBetween(start.toLocalDate(),currentDate.toLocalDate()).getDays();
        Long test = current.getTime() - started.getTime();
        hariKe = (int)TimeUnit.MILLISECONDS.toDays(test) + 1;
//        Log.i("harike",String.valueOf(hariKe));
//        Log.i("harike lain",String.valueOf());

        double achieve_amount = Double.valueOf(target_saving_model.getAchieved_amount());
        double target_amount = Double.valueOf(target_saving_model.getTargeted_amount());
        total_temp = (float)(achieve_amount / target_amount);
        total_temp = total_temp * 100;
        percent = (int)((achieve_amount * 100f) / target_amount);
        jumlahHari = Days.daysBetween(start.toLocalDate(),end.toLocalDate()).getDays();

        if(currentDate.getMillis() > end.getMillis()) {
            Timber.d("currentDate lebih dari tanggal akhir target");
            return false;
        }

        if(percent >= 100){
            layout_success.setVisibility(View.VISIBLE);
            layout_success.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showDialogSuccess();
                }
            });
        }
        else {
            layout_success.setVisibility(View.GONE);
            layout_success.setOnClickListener(null);
        }

        return true;
    }

    @Override
    public void onResume() {
        super.onResume();
        inqSavingTarget();
        refreshUI();
    }


    @Override
    public void onStart() {
        super.onStart();

    }

//    private void setFitChart(float total_temp, FitChart fitChartTemp){
//        Collection<FitChartValue> values = new ArrayList<>();
//        Float stemp;
//        if(total_temp > 99)
//        {
//            values.add(new FitChartValue(25f, getResources().getColor(R.color.amber_900)));
//            values.add(new FitChartValue(25f, getResources().getColor(R.color.amber_700)));
//            values.add(new FitChartValue(25f, getResources().getColor(R.color.amber_500)));
//            values.add(new FitChartValue(25f, getResources().getColor(R.color.amber_300)));
//            fitChartTemp.setValues(values);
//        }
//        else if(total_temp > 75 && total_temp < 100)
//        {
//            stemp = total_temp - 75;
//            values.add(new FitChartValue(25f,getResources().getColor(R.color.amber_900)));
//            values.add(new FitChartValue(25f,getResources().getColor(R.color.amber_700)));
//            values.add(new FitChartValue(25f, getResources().getColor(R.color.amber_500)));
//            values.add(new FitChartValue(stemp, getResources().getColor(R.color.amber_300)));
//            fitChartTemp.setValues(values);
//        }
//        else if(total_temp > 50 && total_temp < 75)
//        {
//            stemp = total_temp - 50;
//            values.add(new FitChartValue(25f, getResources().getColor(R.color.amber_900)));
//            values.add(new FitChartValue(25f, getResources().getColor(R.color.amber_700)));
//            values.add(new FitChartValue(stemp, getResources().getColor(R.color.amber_500)));
//            fitChartTemp.setValues(values);
//        }
//        else if(total_temp > 25 && total_temp < 50)
//        {
//            stemp = total_temp - 25;
//            values.add(new FitChartValue(25f, getResources().getColor(R.color.amber_900)));
//            values.add(new FitChartValue(stemp, getResources().getColor(R.color.amber_700)));
//            fitChartTemp.setValues(values);
//        }
//        else if(total_temp > 0 && total_temp < 25)
//        {
//            stemp = total_temp - 0;
//            values.add(new FitChartValue(stemp, getResources().getColor(R.color.amber_900)));
//            fitChartTemp.setValues(values);
//        }
//        else {
//            fitChartTemp.setValue(total_temp);
//        }
//    }

    private void switchActivity(Intent mIntent, int activity_type){
        if (getActivity() == null)
            return;

        MainPage fca = (MainPage) getActivity();
        fca.switchActivity(mIntent,activity_type);
    }



    @Override
    public void onClick(View v) {
        Intent i;
        switch (v.getId()){
            case R.id.button_rek_tabungan :
                i = new Intent(getActivity(), TabunganActivity.class);
                i.putExtra(DefineValue.MENU_TABUNGAN,TabunganActivity.MENU_ATUR_TABUNGAN);
                switchActivity(i,MainPage.ACTIVITY_RESULT);
                break;
            case R.id.button_automat_nabung :
                i = new Intent(getActivity(), TabunganActivity.class);
                i.putExtra(DefineValue.MENU_TABUNGAN,TabunganActivity.MENU_AUTOMATE_NABUNG);
                switchActivity(i,MainPage.ACTIVITY_RESULT);
                break;
            case R.id.button_target_nabung :
                i = new Intent(getActivity(), TabunganActivity.class);
                i.putExtra(DefineValue.MENU_TABUNGAN,TabunganActivity.MENU_TARGET_NABUNG);
                switchActivity(i,MainPage.ACTIVITY_RESULT);
                break;
            case R.id.button_nabung :
                openNabung();
                break;
        }
    }

    private void openNabung(){
        if (getActivity() == null)
            return;

        MainPage fca = (MainPage) getActivity();
        fca.nabungAction();
    }

    private void showDialogSuccess(){
        final Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_success_target_saving);
        // Include dialog.xml file

        String target = target_saving_model.getCcy_id()+". "+CurrencyFormat.format(target_saving_model.getTargeted_amount());
        ((TextView)dialog.findViewById(R.id.tv_msg)).setText(getString(R.string.dialog_msg_target_success,target));

        dialog.findViewById(R.id.btn_dialog_ok).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(),AskRewardActivity.class);
                switchActivity(i,MainPage.ACTIVITY_RESULT);
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    @Override
    public void onDestroy() {
        if(realm!=null)
            realm.close();
        super.onDestroy();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    private void inqSavingTarget(){
        try {

            RequestParams params = MyApiClient.getSignatureWithParams(MyApiClient.COMM_ID,MyApiClient.LINK_INQ_SAVING_TARGET,
                    userID,accessKey);
            params.put(WebParams.COMM_ID, MyApiClient.COMM_ID);
            params.put(WebParams.USER_ID, userID);
            params.put(WebParams.MEMBER_ID, memberID);

            Timber.d("isi params inqSavingTarget:" + params.toString());

            MyApiClient.sentInqSavingTarget(getActivity(),params,false, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, final JSONObject response) {
                    try {
                        String code = response.getString(WebParams.ERROR_CODE);
                        Timber.d("isi response inqSavingTarget:" + response.toString());
                        if (code.equals(WebParams.SUCCESS_CODE)) {
//                            Target_Saving_Model.insertOrUpdate(response);
                           Target_Saving_Model.insertData(response);
                            Timber.d("inqSavingTarget complete");
                        }
                        else if(code.equals(ErrorDefinition.NO_TRANSACTION)){
                            realm.beginTransaction();
                            realm.delete(Target_Saving_Model.class);
                            realm.commitTransaction();
                        }
                        else if (code.equals(WebParams.LOGOUT_CODE)) {
                            String message = response.getString(WebParams.ERROR_MESSAGE);
                            AlertDialogLogout test = AlertDialogLogout.getInstance();
                            test.showDialoginMain(getActivity(), message);
                        } else {
                            String code_msg = response.getString(WebParams.ERROR_MESSAGE);
                            Timber.d("inqSavingTarget "+ code_msg);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    finally {
                        refreshUI();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    super.onFailure(statusCode, headers, responseString, throwable);
                    failure(throwable);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    failure(throwable);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    failure(throwable);
                }

                private void failure(Throwable throwable){
                    Timber.w("Error Koneksi inqSavingTarget:"+throwable.toString());
                }
            });
        }catch (Exception e){
            Timber.d("httpclient:" + e.getMessage());
        }
    }
}
