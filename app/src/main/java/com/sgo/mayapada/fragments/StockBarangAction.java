package com.sgo.mayapada.fragments;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.securepreferences.SecurePreferences;
import com.sgo.mayapada.R;
import com.sgo.mayapada.activities.KantinActivity;
import com.sgo.mayapada.activities.TabunganActivity;
import com.sgo.mayapada.coreclass.CurrencyFormat;
import com.sgo.mayapada.coreclass.CustomSecurePref;
import com.sgo.mayapada.coreclass.DefineValue;
import com.sgo.mayapada.coreclass.MyApiClient;
import com.sgo.mayapada.coreclass.MyPicasso;
import com.sgo.mayapada.coreclass.WebParams;
import com.sgo.mayapada.dialogs.DefinedDialog;
import com.squareup.picasso.Picasso;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;

import timber.log.Timber;

public class StockBarangAction extends Fragment {

    /**
     * Global variable
     * TAMBAH & UPDATE = variable untuk menentukan mode
     * MODE = sebagai String key di fungsi {@link #newInstanceTambah} dan {@link #newInstanceUpdate(String)}
     * S & D = untuk parameter pengiriman webservice
     */
    public final static String TAG = "com.sgo.mayapada.fragments.StockBarangTambah";
    private static final int TAMBAH = 1;
    private static final int UPDATE = 2;
    private static final String MODE = "mode";
    private static final String N = "N";
    private static final String S = "S";
    private static final String D = "D";

    private SuccessStockBarangListener mListener;
    private EditText et_item_code, et_item_name, et_item_price, et_item_stock;
    private Spinner spin_stock_type;
    private View layout_stock;
    private String userID, accessKey, memberID, merchant_subcategory;
    private ProgressDialog progdialog;
    private int mode = 0;
    private ImageButton img_catalog;
//    private TextView tv_delete;
private Uri mCapturedImageURI;
    private File mFileImageCatalog = null;


    private void setTitle(){
        if(getActivity() == null)
            return;

        TabunganActivity ta = (TabunganActivity)getActivity();
        ta.setToolbarTitle(getString(R.string.menu_item_title_add_account));
    }

    public StockBarangAction() {
        // Required empty public constructor
    }


    /**
     * panggil fungsi ini untuk inisialisasi fragment dengan action Tambah
     * @return fragment StockBarangAction dengan inisialisai action Tambah
     */
    public static StockBarangAction newInstanceTambah() {
        StockBarangAction fragment = new StockBarangAction();
        Bundle args = new Bundle();
        args.putInt(MODE, TAMBAH);
        fragment.setArguments(args);
        return fragment;
    }

    /**
     * panggil fungsi ini untuk inisialisasi fragment dengan action Update
     * @param item_code dari item yg bersangkutan untuk panggil {@link #updateItem(RequestParams)} nanti
     * @return fragment StockBarangAction dengan inisialisai action Tambah
     */
    public static StockBarangAction newInstanceUpdate(String item_code) {
        StockBarangAction fragment = new StockBarangAction();
        Bundle args = new Bundle();
        args.putString(DefineValue.ITEM_CODE,item_code);
        args.putInt(MODE, UPDATE);
        fragment.setArguments(args);
        return fragment;
    }


    /**
     * Inflate menu Delete kalau modenya UPDATE
     * @param menu menu
     * @param inflater inflater
     */
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        if(mode == UPDATE)
            inflater.inflate(R.menu.menu_stock_barang_action,menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId())
        {
            case android.R.id.home:
                getActivity().onBackPressed();
                return true;
            case R.id.menu_item_delete:
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setMessage(getString(R.string.alert_text_delete))
                        .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                deleteItem();
                            }
                        })
                        .setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                AlertDialog dialogDelete = builder.create();
                dialogDelete.show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        if (getArguments() != null) {
            mode = getArguments().getInt(MODE,1);
        }

        SecurePreferences sp = CustomSecurePref.getInstance().getmSecurePrefs();
        progdialog = DefinedDialog.CreateProgressDialog(getActivity(),"");
        progdialog.dismiss();
        userID = sp.getString(DefineValue.USERID_PHONE,"");
        accessKey = sp.getString(DefineValue.ACCESS_KEY,"");
        memberID = sp.getString(DefineValue.MEMBER_ID,"");
        merchant_subcategory = sp.getString(DefineValue.MERCHANT_SUBCATEGORY,"");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_stock_barang_tambah, container, false);
        layout_stock = v.findViewById(R.id.layout_stock);
        et_item_code = (EditText) v.findViewById(R.id.et_item_code);
        et_item_name = (EditText) v.findViewById(R.id.et_item_name);
        et_item_price = (EditText) v.findViewById(R.id.et_item_price);
        et_item_stock = (EditText) v.findViewById(R.id.et_stock);
        spin_stock_type = (Spinner) v.findViewById(R.id.spinner_stock_type);
        img_catalog = (ImageButton) v.findViewById(R.id.img_catalog);
//        tv_delete = (TextView) v.findViewById(R.id.tv_delete_photo);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item,
                getResources().getStringArray(R.array.array_type_stock));
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spin_stock_type.setAdapter(adapter);
        spin_stock_type.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(position == 1)
                    layout_stock.setVisibility(View.VISIBLE);
                else
                    layout_stock.setVisibility(View.GONE);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        v.findViewById(R.id.btn_submit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(inputValidation()){
                    addOrUpdateItem();
                }
            }
        });

        v.findViewById(R.id.btn_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });

        Picasso mPic = MyPicasso.getImageLoader(getActivity());

        String _url = "";
        if(_url != null && _url.isEmpty()){
            mPic.load(R.drawable.icon_no_photo)
                    .fit()
                    .centerInside()
                    .placeholder(R.drawable.progress_animation)
                    .into(img_catalog);
        }
        else {
            mPic.load(_url)
                    .fit()
                    .centerInside()
                    .placeholder(R.drawable.progress_animation)
                    .into(img_catalog);
        }

        img_catalog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                picture_catalog_dialog(v);
            }
        });
        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if(mode == UPDATE)
            getDetailsItem();
    }

    private boolean inputValidation(){
        if(et_item_code.getText().toString().length() == 0){
            et_item_code.requestFocus();
            et_item_code.setError(getString(R.string.tambahstock_item_code_validation));
            return false;
        }
        else if(et_item_name.getText().toString().length() == 0){
            et_item_name.requestFocus();
            et_item_name.setError(getString(R.string.tambahstock_item_name_validation));
            return false;
        }
        else if(et_item_price.getText().toString().length() == 0){
            et_item_price.requestFocus();
            et_item_price.setError(getString(R.string.tambahstock_item_price_validation));
            return false;
        }
        else if(spin_stock_type.getSelectedItemPosition() == 0){
            TextView errorText = (TextView)spin_stock_type.getSelectedView();
            errorText.setError("");
            errorText.setTextColor(Color.RED);//just to highlight that this is an error
            errorText.setText(getString(R.string.tambahstock_stock_type_validation));//changes the selected item text to this
            return false;
        }
        else if(layout_stock.getVisibility() == View.VISIBLE && et_item_stock.getText().toString().length() == 0 ){
            et_item_stock.requestFocus();
            et_item_stock.setError(getString(R.string.tambahstock_stock_validation));
            return false;
        }
        else if(et_item_price.getText().toString().equalsIgnoreCase("0")){
            et_item_price.requestFocus();
            et_item_price.setError(getString(R.string.tambahstock_item_price_validation));
            return false;
        }
        return true;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (getTargetFragment() instanceof SuccessStockBarangListener) {
            mListener = (SuccessStockBarangListener) getTargetFragment();
        } else {
            if(context instanceof SuccessStockBarangListener){
                mListener = (SuccessStockBarangListener) context;
            }
            else {
                throw new RuntimeException(context.toString()
                        + " must implement SuccessTambahStockListener");
            }
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    /**
     * interface kalau {@link #addItem(RequestParams)} sukses bakal panggil onSuccessTambahStock
     * interface kalau {@link #updateItem(RequestParams)} sukses bakal panggil onSuccessUpdateStock
     * interface kalau {@link #deleteItem()} sukses bakal panggil onSuccessDeleteStock
     */
    public interface SuccessStockBarangListener {
        // TODO: Update argument type and name
        void onSuccessTambahStock(boolean status);
        void onSuccessUpdateStock(boolean status);
        void onSuccessDeleteStock(boolean status);
    }


    /**
     * fungsi ini biar gampang misahin panggil {@link #updateItem(RequestParams)} atau {@link #addItem(RequestParams)}
     * karena parameternya sama
     */
    private void addOrUpdateItem(){
        progdialog.show();
        RequestParams params;

        if(mode == UPDATE)
            params = MyApiClient.getSignatureWithParams(MyApiClient.COMM_ID, MyApiClient.LINK_UPDATE_ITEM,
                userID, accessKey);
        else
            params = MyApiClient.getSignatureWithParams(MyApiClient.COMM_ID, MyApiClient.LINK_ADD_ITEM,
                    userID, accessKey);

        params.put(WebParams.MERCHANT_ID, memberID);
        params.put(WebParams.USER_ID, userID);
        params.put(WebParams.COMM_ID, MyApiClient.COMM_ID);
        params.put(WebParams.ITEM_CODE, et_item_code.getText().toString());
        params.put(WebParams.ITEM_NAME, et_item_name.getText().toString());
        params.put(WebParams.PRICE, et_item_price.getText().toString());
        params.put(WebParams.MERCHANT_SUBCATEGORY, merchant_subcategory);
        try {
            if(mFileImageCatalog != null)
                params.put(WebParams.USER_FILE, mFileImageCatalog);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        if(spin_stock_type.getSelectedItemPosition() == 1)
            params.put(WebParams.STOCK_TYPE, S);
        else
            params.put(WebParams.STOCK_TYPE, N);

        if(layout_stock.getVisibility() == View.VISIBLE)
            params.put(WebParams.STOCK, et_item_stock.getText().toString() );

        if(mode == UPDATE){
            updateItem(params);
        }
        else {
            addItem(params);
        }
    }

    private void addItem(RequestParams params){
        try{
            Timber.d("isi params reqAddItem:" + params.toString());
            MyApiClient.sentAddItem(getActivity(), params, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    try {
                        String code = response.getString(WebParams.ERROR_CODE);
                        Timber.d("Isi response reqAddItem: " + response.toString());
                        if (code.equals(WebParams.SUCCESS_CODE)) {
                            Toast.makeText(getActivity(),getString(R.string.tambahstock_toast_add_success),Toast.LENGTH_SHORT).show();
                            mListener.onSuccessTambahStock(true);
                            getFragmentManager().popBackStack();
                        } else {
                            code = response.getString(WebParams.ERROR_MESSAGE);
                            Toast.makeText(getActivity(), code, Toast.LENGTH_SHORT).show();
                            mListener.onSuccessTambahStock(false);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } finally {
                        if (progdialog.isShowing())
                            progdialog.dismiss();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    super.onFailure(statusCode, headers, responseString, throwable);
                    failure(throwable);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    failure(throwable);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    failure(throwable);
                }

                private void failure(Throwable throwable) {
                    if (MyApiClient.PROD_FAILURE_FLAG)
                        Toast.makeText(getActivity(), getString(R.string.network_connection_failure_toast), Toast.LENGTH_SHORT).show();
                    else
                        Toast.makeText(getActivity(), throwable.toString(), Toast.LENGTH_SHORT).show();
                    if (progdialog.isShowing())
                        progdialog.dismiss();
                    mListener.onSuccessTambahStock(false);
                    Timber.w("Error Koneksi reqAddItem :" + throwable.toString());
                }
            });
        }catch (Exception e){
            Log.d("httpclient:",e.getMessage());
        }
    }

    private void updateItem(RequestParams params){
        try{
            Timber.d("isi params update item:" + params.toString());
            MyApiClient.sentUpdateItem(getActivity(), params, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    try {
                        String code = response.getString(WebParams.ERROR_CODE);
                        Timber.d("Isi response update item: " + response.toString());
                        if (code.equals(WebParams.SUCCESS_CODE)) {
                            Toast.makeText(getActivity(),getString(R.string.tambahstock_toast_update_success),Toast.LENGTH_SHORT).show();
                            mListener.onSuccessUpdateStock(true);
                            getFragmentManager().popBackStack();
                        } else {
                            code = response.getString(WebParams.ERROR_MESSAGE);
                            Toast.makeText(getActivity(), code, Toast.LENGTH_SHORT).show();
                            mListener.onSuccessUpdateStock(false);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        mListener.onSuccessUpdateStock(false);
                    } finally {
                        if (progdialog.isShowing())
                            progdialog.dismiss();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    super.onFailure(statusCode, headers, responseString, throwable);
                    failure(throwable);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    failure(throwable);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    failure(throwable);
                }

                private void failure(Throwable throwable) {
                    if (MyApiClient.PROD_FAILURE_FLAG)
                        Toast.makeText(getActivity(), getString(R.string.network_connection_failure_toast), Toast.LENGTH_SHORT).show();
                    else
                        Toast.makeText(getActivity(), throwable.toString(), Toast.LENGTH_SHORT).show();
                    if (progdialog.isShowing())
                        progdialog.dismiss();
                    mListener.onSuccessUpdateStock(false);
                    Timber.w("Error Koneksi update item :" + throwable.toString());
                }
            });
        }catch (Exception e){
            Log.d("httpclient:",e.getMessage());
        }
    }


    private void getDetailsItem(){
        try{
            progdialog.show();
            RequestParams params = MyApiClient.getSignatureWithParams(MyApiClient.COMM_ID, MyApiClient.LINK_DETAILS_ITEM,
                    userID, accessKey);
            params.put(WebParams.MERCHANT_ID, memberID);
            params.put(WebParams.USER_ID, userID);
            params.put(WebParams.COMM_ID, MyApiClient.COMM_ID);
            params.put(WebParams.ITEM_CODE, getArguments().getString(DefineValue.ITEM_CODE));

            Timber.d("isi params getDetailsItem:" + params.toString());
            MyApiClient.sentDetailsItem(getActivity(), params, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    try {
                        String code = response.getString(WebParams.ERROR_CODE);
                        Timber.d("Isi response getDetailsItem: " + response.toString());
                        if (code.equals(WebParams.SUCCESS_CODE)) {
                            et_item_code.setText(getArguments().getString(DefineValue.ITEM_CODE));
                            et_item_code.setEnabled(false);
                            et_item_name.setText(response.optString(WebParams.ITEM_NAME,""));
                            et_item_price.setText(CurrencyFormat.deleteDecimal(response.optString(WebParams.PRICE,"")));
                            if(response.optString(WebParams.STOCK_TYPE).equalsIgnoreCase(N))
                                spin_stock_type.setSelection(2);
                            else {
                                spin_stock_type.setSelection(1);
                                et_item_stock.setText(response.optString(WebParams.STOCK,""));
                            }

                            Picasso mPic = MyPicasso.getImageLoader(getActivity());

                            String _url = response.optString(WebParams.IMG_MEDIUM_URL,"");
                            if(_url != null && _url.isEmpty()){
                                mPic.load(R.drawable.icon_no_photo)
                                        .fit()
                                        .centerInside()
                                        .placeholder(R.drawable.progress_animation)
                                        .into(img_catalog);
                            }
                            else {
                                mPic.load(_url)
                                        .error(R.drawable.icon_no_photo)
                                        .fit()
                                        .centerInside()
                                        .placeholder(R.drawable.progress_animation)
                                        .into(img_catalog);
                            }

                        } else {
                            code = response.getString(WebParams.ERROR_MESSAGE);
                            Toast.makeText(getActivity(), code, Toast.LENGTH_SHORT).show();
                            getActivity().onBackPressed();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } finally {
                        if (progdialog.isShowing())
                            progdialog.dismiss();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    super.onFailure(statusCode, headers, responseString, throwable);
                    failure(throwable);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    failure(throwable);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    failure(throwable);
                }

                private void failure(Throwable throwable) {
                    if (MyApiClient.PROD_FAILURE_FLAG)
                        Toast.makeText(getActivity(), getString(R.string.network_connection_failure_toast), Toast.LENGTH_SHORT).show();
                    else
                        Toast.makeText(getActivity(), throwable.toString(), Toast.LENGTH_SHORT).show();
                    if (progdialog.isShowing())
                        progdialog.dismiss();
                    getActivity().onBackPressed();
                    Timber.w("Error Koneksi getDetailsItem :" + throwable.toString());
                }
            });
        }catch (Exception e){
            Log.d("httpclient:",e.getMessage());
        }
    }

    private void deleteItem(){
        try{
            progdialog.show();
            RequestParams params = MyApiClient.getSignatureWithParams(MyApiClient.COMM_ID, MyApiClient.LINK_DELETE_ITEM,
                    userID, accessKey);
            params.put(WebParams.MERCHANT_ID, memberID);
            params.put(WebParams.USER_ID, userID);
            params.put(WebParams.COMM_ID, MyApiClient.COMM_ID);
            params.put(WebParams.ITEM_CODE, et_item_code.getText().toString());
            params.put(WebParams.MERCHANT_SUBCATEGORY, merchant_subcategory);

            Timber.d("isi params delete item:" + params.toString());
            MyApiClient.sentDeleteItem(getActivity(), params, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    try {
                        String code = response.getString(WebParams.ERROR_CODE);
                        Timber.d("Isi response delete item: " + response.toString());
                        if (code.equals(WebParams.SUCCESS_CODE)) {
                            String stat = response.optString(WebParams.ITEM_STATUS,"");
                            if(stat.equalsIgnoreCase(D)){
                                Toast.makeText(getActivity(),getString(R.string.tambahstock_toast_delete_success),Toast.LENGTH_SHORT).show();
                                mListener.onSuccessDeleteStock(true);
                                getActivity().onBackPressed();
                            }
                            else {
                                Toast.makeText(getActivity(), getString(R.string.tambahstock_toast_delete_failed), Toast.LENGTH_SHORT).show();
                                mListener.onSuccessDeleteStock(false);
                            }
                        } else {
                            code = response.getString(WebParams.ERROR_MESSAGE);
                            Toast.makeText(getActivity(), code, Toast.LENGTH_SHORT).show();
                            mListener.onSuccessDeleteStock(false);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        mListener.onSuccessDeleteStock(false);
                    } finally {
                        if (progdialog.isShowing())
                            progdialog.dismiss();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    super.onFailure(statusCode, headers, responseString, throwable);
                    failure(throwable);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    failure(throwable);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    failure(throwable);
                }

                private void failure(Throwable throwable) {
                    if (MyApiClient.PROD_FAILURE_FLAG)
                        Toast.makeText(getActivity(), getString(R.string.network_connection_failure_toast), Toast.LENGTH_SHORT).show();
                    else
                        Toast.makeText(getActivity(), throwable.toString(), Toast.LENGTH_SHORT).show();
                    if (progdialog.isShowing())
                        progdialog.dismiss();
                    mListener.onSuccessDeleteStock(false);
                    Timber.w("Error Koneksi delete item :" + throwable.toString());
                }
            });
        }catch (Exception e){
            Log.d("httpclient:",e.getMessage());
        }
    }

    private void picture_catalog_dialog(View view){
        final String[] items = {"Choose from Gallery" , "Take Photo"};

        AlertDialog.Builder a = new AlertDialog.Builder(getActivity());
        a.setCancelable(true);
        a.setAdapter(new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, items),
                new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        if (which == 0) {
                            Timber.wtf("masuk gallery");
                            chooseGallery();
                        } else if (which == 1) {
                            runCamera();
                        }

                    }
                }
        );
        a.create();
        a.show();
    }

    private void chooseGallery() {
        Timber.wtf("masuk gallery");
        Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        getActivity().startActivityForResult(intent, KantinActivity.RESULT_GALERY);
    }

    private void runCamera(){
        String fileName = "temp.jpg";

        ContentValues values = new ContentValues();
        values.put(MediaStore.Images.Media.TITLE, fileName);

        mCapturedImageURI = getActivity().getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);

        KantinActivity parent = (KantinActivity) getActivity();
        parent.setmCapturedImageURI(mCapturedImageURI);

        Intent takePictureIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, mCapturedImageURI);
        getActivity().startActivityForResult(takePictureIntent, KantinActivity.RESULT_CAMERA);
    }

    public void setFileImageCatalog(boolean isCamera, File mFile, String imageContent) {
        mFileImageCatalog = mFile;

        Picasso mPic = MyPicasso.getImageLoader(getActivity());

        if(mFileImageCatalog != null){
            if(isCamera) {
                mPic.load(new File(imageContent))
                        .error(R.drawable.icon_no_photo)
                        .placeholder(R.drawable.progress_animation)
                        .fit()
                        .centerInside()
                        .into(img_catalog);
            }else
                mPic.load(imageContent)
                        .error(R.drawable.icon_no_photo)
                        .fit()
                        .centerInside()
                        .placeholder(R.drawable.progress_animation)
                        .into(img_catalog);
        }
    }

}
