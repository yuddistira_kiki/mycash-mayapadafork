package com.sgo.mayapada.fragments;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.securepreferences.SecurePreferences;
import com.sgo.mayapada.entityRealm.List_Account_Nabung;
import com.sgo.mayapada.entityRealm.List_Bank_Nabung;
import com.sgo.mayapada.entityRealm.Target_Saving_Model;
import com.sgo.mayapada.R;
import com.sgo.mayapada.activities.MainPage;
import com.sgo.mayapada.activities.TabunganActivity;
import com.sgo.mayapada.coreclass.CustomSecurePref;
import com.sgo.mayapada.coreclass.DefineValue;
import com.sgo.mayapada.coreclass.MyApiClient;
import com.sgo.mayapada.coreclass.WebParams;
import com.sgo.mayapada.dialogs.AlertDialogLogout;
import com.sgo.mayapada.dialogs.DefinedDialog;
import com.sgo.mayapada.interfaces.TransactionResult;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import io.realm.Realm;
import timber.log.Timber;

public class Nabung extends Fragment implements View.OnClickListener, TransactionResult {

    public final static String TAG = "com.sgo.mayapada.fragments.Nabung";

    private List_Account_Nabung rek_tabungan_model;
    private List_Bank_Nabung list_bank_nabung;
    private TextView tv_rek_value;
    private Realm realm;
    private SecurePreferences sp;
    private String authType;
    private String userID;
    private String accessKey;
    private String memberID;
    private ProgressDialog progressDialog;
    private EditText et_amount;
    private double tempAmount;

    public Nabung() {
        // Required empty public constructor
    }



    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        realm = Realm.getDefaultInstance();
        sp = CustomSecurePref.getInstance().getmSecurePrefs();

        userID = sp.getString(DefineValue.USERID_PHONE,"");
        accessKey = sp.getString(DefineValue.ACCESS_KEY,"");
        memberID = sp.getString(DefineValue.MEMBER_ID,"");
        authType = sp.getString(DefineValue.AUTHENTICATION_TYPE,"");

        List_Account_Nabung list_account_nabungs = realm.where(List_Account_Nabung.class)
                .equalTo(WebParams.IS_DEFAULT,DefineValue.STRING_YES)
                .findFirst();
        if(list_account_nabungs != null)
            rek_tabungan_model = realm.copyFromRealm(list_account_nabungs);

        progressDialog = DefinedDialog.CreateProgressDialog(getContext(),"");
        progressDialog.dismiss();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_nabung, container, false);
        v.findViewById(R.id.btn_rek_nabung).setOnClickListener(this);
        tv_rek_value = (TextView) v.findViewById(R.id.text_rekening_value);
        et_amount = (EditText) v.findViewById(R.id.edittext_nabung_nominal);
        v.findViewById(R.id.btn_submit).setOnClickListener(this);
        v.findViewById(R.id.btn_cancel).setOnClickListener(this);
        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        setTitle();
        showDefaultRek();
    }


    private void setTitle(){
        if(getActivity() == null)
            return;

        TabunganActivity ta = (TabunganActivity)getActivity();
        ta.setToolbarTitle(getString(R.string.savings_title_saving));
    }

    public void setDefaultRek(List_Account_Nabung rek_tabungan_model){
        this.setRek_tabungan_model(rek_tabungan_model);
        showDefaultRek();
    }

    private void showDefaultRek(){
        tv_rek_value.setText(getString(R.string.nabung_text_rekening_value,getRek_tabungan_model().getBank_name(),
                getRek_tabungan_model().getAcct_no()));
        list_bank_nabung = realm.where(List_Bank_Nabung.class).equalTo(WebParams.BANK_CODE,getRek_tabungan_model().getBank_code()).findFirstAsync();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_rek_nabung:
                openRekTabunganFrag();
                break;
            case R.id.btn_submit :
                if(inputValidation()){
                    reqCashout(et_amount.getText().toString());
                }
                break;
            case R.id.btn_cancel :
                getActivity().onBackPressed();
                break;
        }

    }

    private void openRekTabunganFrag(){
        Fragment fragment = new RekeningTabungan();
        switchFragment(fragment,"",true,RekeningTabungan.TAG);
    }

    private void switchFragment(android.support.v4.app.Fragment i, String name, Boolean isBackstack, String tag){
        if (getActivity() == null)
            return;

        TabunganActivity fca = (TabunganActivity ) getActivity();
        fca.switchContent(i,name,isBackstack,tag);
    }

    private List_Account_Nabung getRek_tabungan_model() {
        return rek_tabungan_model;
    }

    private void setRek_tabungan_model(List_Account_Nabung rek_tabungan_model) {
        this.rek_tabungan_model = rek_tabungan_model;
    }

    private boolean inputValidation(){
        if(et_amount.getText().toString().length() == 0){
            et_amount.requestFocus();
            et_amount.setError(getString(R.string.billertoken_validation_payment_input_amount));
            return false;
        }
        else if(Long.parseLong(et_amount.getText().toString()) < 1){
            et_amount.requestFocus();
            et_amount.setError(getString(R.string.payfriends_amount_zero));
            return false;
        }
        return true;
    }

    @Override
    public void onDetach() {
        et_amount.setText("");
        super.onDetach();
    }

    private void reqCashout(final String _amount){
        try {

            progressDialog.show();

            RequestParams params = MyApiClient.getSignatureWithParams(MyApiClient.COMM_ID,MyApiClient.LINK_REQUEST_CASHOUT,
                    userID,accessKey);
            params.put(WebParams.COMM_ID, MyApiClient.COMM_ID);
            params.put(WebParams.USER_ID, userID);
            params.put(WebParams.MEMBER_ID, memberID);
            params.put(WebParams.AMOUNT, _amount);
            params.put(WebParams.CCY_ID, MyApiClient.CCY_VALUE);
            params.put(WebParams.BANK_CODE, rek_tabungan_model.getBank_code());
            params.put(WebParams.ACCT_NO, rek_tabungan_model.getAcct_no());
            params.put(WebParams.IS_TABUNG, DefineValue.STRING_YES);

            if(!list_bank_nabung.isBank_gateway()) {
                params.put(WebParams.ACCT_NAME, rek_tabungan_model.getAcct_name());

            }

            Timber.d("isi params req cashout nabung:" + params.toString());

            MyApiClient.sentReqCashout(getActivity(),params, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    progressDialog.dismiss();

                    try {
                        String code = response.getString(WebParams.ERROR_CODE);
                        if (code.equals(WebParams.SUCCESS_CODE)) {
                            Timber.d("isi response req cashout nabung:"+response.toString());

                            tempAmount = Double.parseDouble(_amount);
                            Fragment i = new CashoutTransferConfirm();
                            Bundle args = new Bundle();
                            args.putString(DefineValue.TX_ID, response.optString(WebParams.TX_ID,""));
                            args.putString(DefineValue.BANK_NAME, rek_tabungan_model.getBank_name());
                            args.putString(DefineValue.ACCOUNT_NUMBER, rek_tabungan_model.getAcct_no());
                            args.putString(DefineValue.CCY_ID, response.optString(WebParams.CCY_ID,""));
                            args.putString(DefineValue.NOMINAL, _amount);
                            args.putString(DefineValue.ACCT_NAME, rek_tabungan_model.getAcct_name());
                            args.putString(DefineValue.FEE, response.optString(WebParams.FEE,""));
                            args.putString(DefineValue.TOTAL_AMOUNT, response.optString(WebParams.TOTAL,""));
                            i.setArguments(args);
                            i.setTargetFragment(Nabung.this,0);
                            switchFragment(i,getString(R.string.savings_title_saving),true, CashoutTransferConfirm.TAG);

                        } else if (code.equals(WebParams.LOGOUT_CODE)) {
                            Timber.d("isi response autologout:"+response.toString());
                            String message = response.getString(WebParams.ERROR_MESSAGE);
                            AlertDialogLogout test = AlertDialogLogout.getInstance();
                            test.showDialoginMain(getActivity(), message);
                        } else {
                            Timber.d("isi error req cashout nabung:"+response.toString());
                            String code_msg = response.getString(WebParams.ERROR_MESSAGE);
                            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                            builder.setMessage(code_msg)
                                    .setPositiveButton("Dismiss", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();
                                        }
                                    });
                            AlertDialog dialog = builder.create();
                            dialog.show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    super.onFailure(statusCode, headers, responseString, throwable);
                    failure(throwable);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    failure(throwable);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    failure(throwable);
                }

                private void failure(Throwable throwable){
                    if(MyApiClient.PROD_FAILURE_FLAG)
                        Toast.makeText(getActivity(), getString(R.string.network_connection_failure_toast), Toast.LENGTH_SHORT).show();
                    else
                        Toast.makeText(getActivity(), throwable.toString(), Toast.LENGTH_SHORT).show();

                    if(progressDialog.isShowing())
                        progressDialog.dismiss();

                    Timber.w("Error Koneksi req cashout nabung:"+throwable.toString());
                }
            });
        }catch (Exception e){
            Timber.d("httpclient:" + e.getMessage());
        }
    }


    @Override
    public void onDestroy() {
        if(realm!=null)
            realm.close();
        super.onDestroy();
    }

    @Override
    public void TransResult(Boolean isSuccess) {
        if(isSuccess) {
            Target_Saving_Model target_saving_model = realm.where(Target_Saving_Model.class).findFirst();
            if(target_saving_model != null){
                realm.beginTransaction();
                double ad = Double.parseDouble(target_saving_model.getAchieved_amount())+ tempAmount;
                String amount = String.format("%.2f", ad);
                target_saving_model.setAchieved_amount(amount);
                realm.commitTransaction();
            }

        }
        getActivity().setResult(MainPage.RESULT_BALANCE);
        getActivity().finish();
    }
}
