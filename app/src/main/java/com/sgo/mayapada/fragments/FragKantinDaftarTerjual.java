package com.sgo.mayapada.fragments;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.securepreferences.SecurePreferences;
import com.sgo.mayapada.Beans.KantinDaftarTerjualModel;
import com.sgo.mayapada.Beans.KantinUntakenModel;
import com.sgo.mayapada.R;
import com.sgo.mayapada.activities.KantinActivity;
import com.sgo.mayapada.adapter.KantinDaftarTerjualAdapter;
import com.sgo.mayapada.coreclass.CurrencyFormat;
import com.sgo.mayapada.coreclass.CustomSecurePref;
import com.sgo.mayapada.coreclass.DefineValue;
import com.sgo.mayapada.coreclass.InetHandler;
import com.sgo.mayapada.coreclass.MyApiClient;
import com.sgo.mayapada.coreclass.WebParams;
import com.sgo.mayapada.dialogs.AlertDialogLogout;
import com.sgo.mayapada.dialogs.CanteenUntakenDialog;
import com.sgo.mayapada.dialogs.DefinedDialog;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import timber.log.Timber;

/**
 * Created by thinkpad on 8/10/2016.
 */
public class FragKantinDaftarTerjual extends ListFragment {

    private SecurePreferences sp;
    private View v;
    private ProgressDialog out;
//    private PtrFrameLayout mPtrFrame;
//    View emptyLayout;
//    Animation frameAnimation;
//    ImageView spining_progress;
//    MaterialRippleLayout btn_loadmore;
//    ViewGroup footerLayout;
private TextView tvTotalPendapatan;
    private ListView lvMenu;
    private KantinDaftarTerjualAdapter adapter;
    private ArrayList<KantinDaftarTerjualModel> listMenu;
//    Button btn_refresh;
private Button btnBack;
//    int page;
private String user_id;
    private String access_key;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        v = inflater.inflate(R.layout.frag_kantin_daftar_terjual, container, false);
        return v;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        sp = CustomSecurePref.getInstance().getmSecurePrefs();
        user_id = sp.getString(DefineValue.USERID_PHONE, "");
        access_key = sp.getString(DefineValue.ACCESS_KEY, "");

        listMenu = new ArrayList<>();

        tvTotalPendapatan = (TextView) v.findViewById(R.id.totalpendapatan_value);
        lvMenu = (ListView) v.findViewById(android.R.id.list);
//        footerLayout = (ViewGroup) getActivity().getLayoutInflater().inflate(R.layout.footer_loadmore, lvMenu, false);
//        footerLayout.setLayoutParams(new ListView.LayoutParams(ListView.LayoutParams.MATCH_PARENT, ListView.LayoutParams.WRAP_CONTENT));
//        spining_progress = (ImageView) footerLayout.findViewById(R.id.image_spinning_wheel);
//        btn_loadmore = (MaterialRippleLayout) footerLayout.findViewById(R.id.btn_loadmore);
//        emptyLayout = v.findViewById(R.id.empty_layout);
//        emptyLayout.setVisibility(View.GONE);
//        btn_refresh = (Button) emptyLayout.findViewById(R.id.btnRefresh);
        btnBack = (Button) v.findViewById(R.id.kantindaftarterjual_back_button);

//        frameAnimation = AnimationUtils.loadAnimation(getActivity(), R.anim.spinner_animation);
//        frameAnimation.setRepeatCount(Animation.INFINITE);
//
//        page = 1;
//        btn_loadmore.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
////                getDataTagihan(page, false, comm_id);
//            }
//        });
//
//        btn_refresh.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                mPtrFrame.autoRefresh();
//            }
//        });

        adapter = new KantinDaftarTerjualAdapter(getActivity(), listMenu, FragKantinDaftarTerjual.this);
        lvMenu.setAdapter(adapter);

//        setLoadMore(false);

//        mPtrFrame = (PtrFrameLayout) v.findViewById(R.id.rotate_header_list_view_frame);
//
//        final MaterialHeader header = new MaterialHeader(getActivity());
//        int[] colors = getResources().getIntArray(R.array.google_colors);
//        header.setColorSchemeColors(colors);
//        header.setLayoutParams(new PtrFrameLayout.LayoutParams(-1, -2));
//        header.setPadding(0, 15, 0, 10);
//        header.setPtrFrameLayout(mPtrFrame);
//
//        mPtrFrame.setHeaderView(header);
//        mPtrFrame.addPtrUIHandler(header);
//        mPtrFrame.setPtrHandler(new PtrHandler() {
//            @Override
//            public void onRefreshBegin(PtrFrameLayout frame) {
//                page = 1;
////                getDataTagihan(0, null, comm_id);
////                jumlah = 0;
////                tv_total_bayar.setText(Long.toString(jumlah));
//            }
//
//            @Override
//            public boolean checkCanDoRefresh(PtrFrameLayout frame, View content, View header) {
//                //return PtrDefaultHandler.checkContentCanBePulledDown(frame, content, header);
//                return canScrollUp();
//            }
//        });
//
//        mPtrFrame.postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                mPtrFrame.autoRefresh(false);
//            }
//        }, 50);

        lvMenu.setOnItemClickListener(menuListener);
        btnBack.setOnClickListener(backListener);

        getDataTodaySales();
    }

    private ListView.OnItemClickListener menuListener = new ListView.OnItemClickListener() {

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            if(!listMenu.get(position).getBelum_terambil().equalsIgnoreCase("0")) {
                if (InetHandler.isNetworkAvailable(getActivity())) {
                    getUntaken(listMenu.get(position).getCode());
                } else
                    DefinedDialog.showErrorDialog(getActivity(), getString(R.string.inethandler_dialog_message));
            }
        }
    };

    private Button.OnClickListener backListener = new Button.OnClickListener() {
        @Override
        public void onClick(View view) {
            getActivity().finish();
        }
    };

//    public boolean canScrollUp() {
//        return lvMenu != null && (lvMenu.getAdapter().getCount() == 0 || lvMenu.getFirstVisiblePosition() == 0 && lvMenu.getChildAt(0).getTop() == 0);
//    }
//
//    private void setLoadMore(boolean isLoading)
//    {
//        if (isLoading) {
//            lvMenu.addFooterView(footerLayout,null,false);
//        }
//        else {
//            lvMenu.removeFooterView(footerLayout);
//        }
//    }

    private void getDataTodaySales() {
        try{
            out = DefinedDialog.CreateProgressDialog(getActivity(), null);
            out.show();

            RequestParams params = MyApiClient.getSignatureWithParams(MyApiClient.COMM_ID, MyApiClient.LINK_TODAY_SALES,
                    user_id, access_key);
            params.put(WebParams.MERCHANT_ID, sp.getString(DefineValue.MEMBER_ID,""));
            params.put(WebParams.USER_ID, user_id);
            params.put(WebParams.COMM_ID, MyApiClient.COMM_ID);

            Timber.d("isi params data today sales:" + params.toString());

            MyApiClient.getTodaySales(getActivity(), params, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    try {
                        out.dismiss();
                        String code = response.getString(WebParams.ERROR_CODE);
                        if (code.equals(WebParams.SUCCESS_CODE)) {
                            Timber.d("isi response data today sales:" + response.toString());

                            String data = response.getString(WebParams.SALES);
                            if(!data.equals("") && !data.equals("null")) {
                                JSONArray array = new JSONArray(data);
                                for (int i = 0; i < array.length(); i++) {
                                    KantinDaftarTerjualModel menu = new KantinDaftarTerjualModel();
                                    menu.setCode(array.getJSONObject(i).getString(WebParams.ITEM_CODE));
                                    menu.setTitle(array.getJSONObject(i).getString(WebParams.ITEM_NAME));
                                    menu.setBelum_terambil(array.getJSONObject(i).getString(WebParams.NOT_YET_TAKEN));
                                    menu.setTerambil(array.getJSONObject(i).getString(WebParams.TAKEN));
                                    menu.setTerjual(array.getJSONObject(i).getString(WebParams.SOLD));
                                    if(array.getJSONObject(i).getString(WebParams.ITEM_STATUS).equalsIgnoreCase("A"))
                                        menu.setIsStop(false);
                                    else
                                        menu.setIsStop(true);
                                    listMenu.add(menu);
                                }
                                adapter.notifyDataSetChanged();
                            }
                            tvTotalPendapatan.setText(response.getString(WebParams.CCY_ID) + " " + CurrencyFormat.format(response.getString(WebParams.TOTAL)));
                        } else if (code.equals(WebParams.LOGOUT_CODE)) {
                            Timber.d("isi response autologout:" + response.toString());
                            String message = response.getString(WebParams.ERROR_MESSAGE);
                            AlertDialogLogout test = AlertDialogLogout.getInstance();
                            test.showDialoginActivity(getActivity(), message);
                        }
                        else {
                            Timber.d("Error data today sales:" + response.toString());
                            code = response.getString(WebParams.ERROR_CODE) + ":" + response.getString(WebParams.ERROR_MESSAGE);
                            Toast.makeText(getActivity(), code, Toast.LENGTH_LONG).show();
                            getActivity().finish();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    super.onFailure(statusCode, headers, responseString, throwable);
                    failure(throwable);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    failure(throwable);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    failure(throwable);
                }

                private void failure(Throwable throwable) {
                    out.dismiss();
                    if (MyApiClient.PROD_FAILURE_FLAG)
                        Toast.makeText(getActivity(), getString(R.string.network_connection_failure_toast), Toast.LENGTH_SHORT).show();
                    else
                        Toast.makeText(getActivity(), throwable.toString(), Toast.LENGTH_SHORT).show();

                    Timber.w("Error Koneksi data today sales:" + throwable.toString());
                }
            });
        } catch (Exception e) {
            Timber.d("httpclient:" + e.getMessage());
        }
    }

//    public void sentActiveDeactive(final int pos) {
//        try{
//            out = DefinedDialog.CreateProgressDialog(getActivity(), null);
//            out.show();
//
//            RequestParams params = MyApiClient.getSignatureWithParams(MyApiClient.COMM_ID, MyApiClient.LINK_ACTIVE_DEACTIVE_ITEM,
//                    user_id, access_key);
//            params.put(WebParams.MERCHANT_ID, sp.getString(DefineValue.MEMBER_ID,""));
//            params.put(WebParams.USER_ID, user_id);
//            params.put(WebParams.COMM_ID, MyApiClient.COMM_ID);
//            params.put(WebParams.ITEM_CODE, listMenu.get(pos).getCode());
//            if(listMenu.get(pos).isStop())
//                params.put(WebParams.STATUS, "A");
//            else
//                params.put(WebParams.STATUS, "I");
//
//            Timber.d("isi params active deactive:" + params.toString());
//
//            MyApiClient.sentActiveDeactiveItem(getActivity(), params, new JsonHttpResponseHandler() {
//                @Override
//                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
//                    try {
//                        out.dismiss();
//                        String code = response.getString(WebParams.ERROR_CODE);
//                        if (code.equals(WebParams.SUCCESS_CODE)) {
//                            Timber.d("isi response active deactive:" + response.toString());
//                            adapter.setActiveDeactive(response.getString(WebParams.ITEM_STATUS), pos);
//                        } else if (code.equals(WebParams.LOGOUT_CODE)) {
//                            Timber.d("isi response autologout:" + response.toString());
//                            String message = response.getString(WebParams.ERROR_MESSAGE);
//                            AlertDialogLogout test = AlertDialogLogout.getInstance();
//                            test.showDialoginActivity(getActivity(), message);
//                        }
//                        else {
//                            Timber.d("Error active deactive:" + response.toString());
//                            code = response.getString(WebParams.ERROR_CODE) + ":" + response.getString(WebParams.ERROR_MESSAGE);
//                            Toast.makeText(getActivity(), code, Toast.LENGTH_LONG).show();
//                        }
//
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                    }
//                }
//
//                @Override
//                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
//                    super.onFailure(statusCode, headers, responseString, throwable);
//                    failure(throwable);
//                }
//
//                @Override
//                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
//                    super.onFailure(statusCode, headers, throwable, errorResponse);
//                    failure(throwable);
//                }
//
//                @Override
//                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
//                    super.onFailure(statusCode, headers, throwable, errorResponse);
//                    failure(throwable);
//                }
//
//                private void failure(Throwable throwable) {
//                    out.dismiss();
//                    if (MyApiClient.PROD_FAILURE_FLAG)
//                        Toast.makeText(getActivity(), getString(R.string.network_connection_failure_toast), Toast.LENGTH_SHORT).show();
//                    else
//                        Toast.makeText(getActivity(), throwable.toString(), Toast.LENGTH_SHORT).show();
//
//                    Timber.w("Error Koneksi active deactive:" + throwable.toString());
//                }
//            });
//        } catch (Exception e) {
//            Timber.d("httpclient:" + e.getMessage());
//        }
//    }

    private void getUntaken(final String item_code) {
        try{
            out = DefinedDialog.CreateProgressDialog(getActivity(), null);
            out.show();

            RequestParams params = MyApiClient.getSignatureWithParams(MyApiClient.COMM_ID, MyApiClient.LINK_UNTAKEN_ITEM,
                    user_id, access_key);
            params.put(WebParams.MERCHANT_ID, sp.getString(DefineValue.MEMBER_ID,""));
            params.put(WebParams.USER_ID, user_id);
            params.put(WebParams.COMM_ID, MyApiClient.COMM_ID);
            params.put(WebParams.ITEM_CODE, item_code);

            Timber.d("isi params data untaken:" + params.toString());

            MyApiClient.getListUntakenItem(getActivity(), params, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    try {
                        out.dismiss();
                        String code = response.getString(WebParams.ERROR_CODE);
                        if (code.equals(WebParams.SUCCESS_CODE)) {
                            Timber.d("isi response data untaken:" + response.toString());

                            String data = response.getString(WebParams.SALES);
                            if(!data.equals("") && !data.equals("null")) {

                                ArrayList<KantinUntakenModel> listUntaken = new ArrayList<>();
                                JSONArray array = new JSONArray(data);
                                for (int i = 0; i < array.length(); i++) {
                                    KantinUntakenModel untaken = new KantinUntakenModel();
                                    untaken.setOrder_id(array.getJSONObject(i).getString(WebParams.ORDER_ID));
                                    untaken.setNo_hp(array.getJSONObject(i).getString(WebParams.NO_HP_MEMBER));
                                    untaken.setItem(array.getJSONObject(i).getString(WebParams.ITEM));
                                    listUntaken.add(untaken);
                                }
                                adapter.notifyDataSetChanged();

                                DialogFragment via = CanteenUntakenDialog.CreateInstance(item_code, listUntaken);
                                FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
                                via.setCancelable(false);
                                via.show(ft, CanteenUntakenDialog.TAG);
                            }

                        } else if (code.equals(WebParams.LOGOUT_CODE)) {
                            Timber.d("isi response autologout:" + response.toString());
                            String message = response.getString(WebParams.ERROR_MESSAGE);
                            AlertDialogLogout test = AlertDialogLogout.getInstance();
                            test.showDialoginActivity(getActivity(), message);
                        }
                        else {
                            Timber.d("Error data untaken:" + response.toString());
                            code = response.getString(WebParams.ERROR_CODE) + ":" + response.getString(WebParams.ERROR_MESSAGE);
                            Toast.makeText(getActivity(), code, Toast.LENGTH_LONG).show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    super.onFailure(statusCode, headers, responseString, throwable);
                    failure(throwable);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    failure(throwable);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    failure(throwable);
                }

                private void failure(Throwable throwable) {
                    out.dismiss();
                    if (MyApiClient.PROD_FAILURE_FLAG)
                        Toast.makeText(getActivity(), getString(R.string.network_connection_failure_toast), Toast.LENGTH_SHORT).show();
                    else
                        Toast.makeText(getActivity(), throwable.toString(), Toast.LENGTH_SHORT).show();

                    Timber.w("Error Koneksi data untaken:" + throwable.toString());
                }
            });
        } catch (Exception e) {
            Timber.d("httpclient:" + e.getMessage());
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if(getActivity().getSupportFragmentManager().getBackStackEntryCount() > 0)
                    getActivity().getSupportFragmentManager().popBackStack();
                else
                    getActivity().finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume() {
        super.onResume();
        setTitle(getString(R.string.kantin_list_sold));
    }

    private void setTitle(String _title){
        if (getActivity() == null)
            return;

        KantinActivity fca = (KantinActivity) getActivity();
        fca.setTitleFragment(_title);
    }
}
