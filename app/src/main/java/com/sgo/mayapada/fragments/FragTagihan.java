package com.sgo.mayapada.fragments;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.balysv.materialripple.MaterialRippleLayout;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.securepreferences.SecurePreferences;
import com.sgo.mayapada.Beans.TagihanListModel;
import com.sgo.mayapada.R;
import com.sgo.mayapada.activities.TagihanActivity;
import com.sgo.mayapada.adapter.TagihanListAdapter;
import com.sgo.mayapada.coreclass.CurrencyFormat;
import com.sgo.mayapada.coreclass.CustomSecurePref;
import com.sgo.mayapada.coreclass.DefineValue;
import com.sgo.mayapada.coreclass.InetHandler;
import com.sgo.mayapada.coreclass.MyApiClient;
import com.sgo.mayapada.coreclass.WebParams;
import com.sgo.mayapada.dialogs.AlertDialogLogout;
import com.sgo.mayapada.dialogs.DefinedDialog;
import com.sgo.mayapada.dialogs.TagihanDetailDialog;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import in.srain.cube.views.ptr.PtrFrameLayout;
import in.srain.cube.views.ptr.PtrHandler;
import in.srain.cube.views.ptr.header.MaterialHeader;
import timber.log.Timber;

/**
 * Created by thinkpad on 3/31/2016.
 */
public class FragTagihan extends ListFragment implements TagihanDetailDialog.OnDialogOkCallback {

    private View v;
    private SecurePreferences sp;
    private ProgressDialog out;
    private TagihanDetailDialog dialogDetail;
    private ListView lv_tagihan;
    private TextView tv_total_bayar;
    private Button btnBayarSaldoUnik;
    private Button btnBayarViaBank;
    private Button btnBatal;
    private int page;
    private PtrFrameLayout mPtrFrame;
    private View emptyLayout;
    private Animation frameAnimation;
    private Button btn_refresh;
    private ImageView spining_progress;
    private MaterialRippleLayout btn_loadmore;
    private ViewGroup footerLayout;
    private ArrayList<TagihanListModel> listTagihan;
    private TagihanListAdapter adapter;
    private long jumlah = 0;
    private String user_id;
    private String access_key;
    private String comm_id;
    private String comm_code;
    private String comm_name;
    private String api_key;
    private String callback_url;
    private String bank_data;
    private String offset = "10";
    private String partial_payment;
    private String cust_name;
    private String pairing_id;

    public static FragTagihan newInstance() {
        return new FragTagihan();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        setV(inflater.inflate(R.layout.frag_sekolahku_tagihan, container, false));
        return getV();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        sp = CustomSecurePref.getInstance().getmSecurePrefs();
        user_id = sp.getString(DefineValue.USERID_PHONE, "");
        access_key = sp.getString(DefineValue.ACCESS_KEY, "");

        Bundle mArgs = getArguments();
        if(mArgs != null && !mArgs.isEmpty()) {
            comm_id = mArgs.getString(DefineValue.COMMUNITY_ID, "");
            comm_code = mArgs.getString(DefineValue.COMMUNITY_CODE, "");
            comm_name = mArgs.getString(DefineValue.COMMUNITY_NAME, "");
            api_key = mArgs.getString(DefineValue.API_KEY, "");
            callback_url = mArgs.getString(DefineValue.CALLBACK_URL, "");
            cust_name = mArgs.getString(DefineValue.CUST_NAME, "");
            pairing_id = mArgs.getString("pairing_id", "");
        }

        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        listTagihan = new ArrayList<>();

        lv_tagihan = (ListView) getV().findViewById(android.R.id.list);
        tv_total_bayar = (TextView) getV().findViewById(R.id.sekolahkutagihan_totalbayar_value);
        btnBayarSaldoUnik = (Button) getV().findViewById(R.id.sekolahkutagihan_bayar_saldo_unik_button);
        btnBayarViaBank = (Button) getV().findViewById(R.id.sekolahkutagihan_bayar_via_bank_button);
        btnBatal = (Button) getV().findViewById(R.id.sekolahkutagihan_batal_button);
        footerLayout = (ViewGroup) getActivity().getLayoutInflater().inflate(R.layout.footer_loadmore, lv_tagihan, false);
        footerLayout.setLayoutParams(new ListView.LayoutParams(ListView.LayoutParams.MATCH_PARENT, ListView.LayoutParams.WRAP_CONTENT));
        spining_progress = (ImageView) footerLayout.findViewById(R.id.image_spinning_wheel);
        btn_loadmore = (MaterialRippleLayout) footerLayout.findViewById(R.id.btn_loadmore);
        emptyLayout = getV().findViewById(R.id.empty_layout);
        emptyLayout.setVisibility(View.GONE);
        btn_refresh = (Button) emptyLayout.findViewById(R.id.btnRefresh);

        frameAnimation = AnimationUtils.loadAnimation(getActivity(), R.anim.spinner_animation);
        frameAnimation.setRepeatCount(Animation.INFINITE);

        page = 1;
        btn_loadmore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDataTagihan(page, false, comm_id);
            }
        });

        btn_refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPtrFrame.autoRefresh();
            }
        });

        setLoadMore(true);
        adapter = new TagihanListAdapter(getActivity(), R.layout.list_sekolahku_tagihan_item, listTagihan)
        {
            @Override
            public View getView(final int position, View convertView, final ViewGroup parent) {
//                return super.getView(position, convertView, parent);

                ViewHolder holder;

                if(convertView == null) {
                    LayoutInflater inflater = (getActivity()).getLayoutInflater();
                    convertView = inflater.inflate(R.layout.list_sekolahku_tagihan_item, parent, false);
                    holder = new ViewHolder();
                    holder.tv_payment_name = (TextView)convertView.findViewById(R.id.txt_payment_name);
                    holder.tv_desc = (TextView)convertView.findViewById(R.id.txt_desc);
                    holder.tv_remain_amount = (TextView)convertView.findViewById(R.id.txt_amount);
                    holder.et_input_amount = (EditText)convertView.findViewById(R.id.et_input_amount);
                    holder.cb_tagihan = (CheckBox)convertView.findViewById(R.id.cb_tagihan);
                    holder.img_arrow = (ImageView)convertView.findViewById(R.id.img_arrow);

                    convertView.setTag(holder);
                }
                else{
                    holder = (ViewHolder) convertView.getTag();
                }

                final TagihanListModel itemnya = listTagihan.get(position);

                holder.tv_payment_name.setText(itemnya.getDoc_name());
                if(itemnya.getDesc().equals("") || itemnya.getDesc().equals(null) || itemnya.getDesc().equals("null"))
                    holder.tv_desc.setText(getString(R.string.desc) + " : -");
                else
                    holder.tv_desc.setText(getString(R.string.desc) + " : " + itemnya.getDesc());
                if(itemnya.getCcy().equalsIgnoreCase("idr")) {
                    holder.tv_remain_amount.setText(getString(R.string.sisa_tagihan) + " : Rp. " + CurrencyFormat.format(itemnya.getRemain_amount()));
                }
                else {
                    holder.tv_remain_amount.setText(getString(R.string.sisa_tagihan) + " : " + itemnya.getRemain_amount());
                }

                if(partial_payment.equalsIgnoreCase("Y")) {
                    holder.et_input_amount.setVisibility(View.VISIBLE);

                    if(itemnya.getInput_amount().equals("0"))
                        holder.et_input_amount.setText("");
                    else
                        holder.et_input_amount.setText(itemnya.getInput_amount());
                }
                else if(partial_payment.equalsIgnoreCase("N"))
                    holder.et_input_amount.setVisibility(View.GONE);


                if(Long.parseLong(itemnya.getInput_amount()) == 0) {
                    itemnya.setCb_checked(false);
                }

                holder.cb_tagihan.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        int getPosition = (Integer) buttonView.getTag();  // Here we get the position that we have set for the checkbox using setTag.
                        listTagihan.get(getPosition).setCb_checked(buttonView.isChecked()); // Set the value of checkbox to maintain its state.
                        if(partial_payment.equalsIgnoreCase("N")) {
                            if(buttonView.isChecked()) {
                                listTagihan.get(getPosition).setInput_amount(listTagihan.get(getPosition).getRemain_amount());
                            }
                            else {
                                listTagihan.get(getPosition).setInput_amount("0");
                            }
                            countTotal(position);
                        }
                        else if(partial_payment.equalsIgnoreCase("Y")) {
                            if (Long.parseLong(itemnya.getInput_amount()) == 0) {
                                buttonView.setChecked(false);
                                itemnya.setCb_checked(false);
                            } else {
                                countTotal(position);
                            }
                        }
                    }
                });
                holder.cb_tagihan.setTag(position);
                holder.cb_tagihan.setChecked(itemnya.isCb_checked());

                holder.et_input_amount.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        showDialog(position);
                    }
                });

                return convertView;
            }

        };

        lv_tagihan.setAdapter(adapter);
        setLoadMore(false);

        mPtrFrame = (PtrFrameLayout) getV().findViewById(R.id.rotate_header_list_view_frame);

        final MaterialHeader header = new MaterialHeader(getActivity());
        int[] colors = getResources().getIntArray(R.array.google_colors);
        header.setColorSchemeColors(colors);
        header.setLayoutParams(new PtrFrameLayout.LayoutParams(-1, -2));
        header.setPadding(0, 15, 0, 10);
        header.setPtrFrameLayout(mPtrFrame);

        mPtrFrame.setHeaderView(header);
        mPtrFrame.addPtrUIHandler(header);
        mPtrFrame.setPtrHandler(new PtrHandler() {
            @Override
            public void onRefreshBegin(PtrFrameLayout frame) {
                page = 1;
                getDataTagihan(0, null, comm_id);
                jumlah = 0;
                tv_total_bayar.setText(Long.toString(jumlah));
            }

            @Override
            public boolean checkCanDoRefresh(PtrFrameLayout frame, View content, View header) {
                //return PtrDefaultHandler.checkContentCanBePulledDown(frame, content, header);
                return canScrollUp();
            }
        });

        mPtrFrame.postDelayed(new Runnable() {
            @Override
            public void run() {
                mPtrFrame.autoRefresh(false);
            }
        }, 50);

        tv_total_bayar.setText("0");
        btnBayarSaldoUnik.setOnClickListener(bayarSaldoUnikListener);
        btnBayarViaBank.setOnClickListener(bayarViaBankListener);
        btnBatal.setOnClickListener(batalListener);

        lv_tagihan.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                showDialog(position);
            }
        });

        getDataTagihan(0, null, comm_id);

//        if (!pairing_id.equals("null")) {
            TagihanActivity fca = (TagihanActivity) getActivity();
            fca.setPairingIdTitle(pairing_id);
//        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if(getActivity().getSupportFragmentManager().getBackStackEntryCount() > 0)
                    getActivity().getSupportFragmentManager().popBackStack();
                else
                    getActivity().finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private Button.OnClickListener bayarSaldoUnikListener = new Button.OnClickListener() {
        @Override
        public void onClick(View v) {
            prosesBayar(true);
        }
    };

    private Button.OnClickListener bayarViaBankListener = new Button.OnClickListener() {
        @Override
        public void onClick(View v) {
            prosesBayar(false);
        }
    };

    private Button.OnClickListener batalListener = new Button.OnClickListener() {
        @Override
        public void onClick(View v) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setMessage(getString(R.string.alert_batal_dialog))
                    .setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            resetInvoice();
                        }
                    })
                    .setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
            AlertDialog alert = builder.create();
            alert.show();
        }
    };

    private void resetInvoice() {
        if(listTagihan.size() > 0 ) {
            for (int i = 0; i < listTagihan.size(); i++) {
                listTagihan.get(i).setCb_checked(false);
                listTagihan.get(i).setInput_amount("0");
            }
            jumlah = 0;
            tv_total_bayar.setText("0");
            adapter.notifyDataSetChanged();
        }
    }

    private void prosesBayar(boolean _isScash) {
        if(InetHandler.isNetworkAvailable(getActivity())) {
            if(inputValidation()) {
                ArrayList<TempObjectData> mTempObjectDataList = new ArrayList<>();
                for(int i=0 ; i<listTagihan.size() ; i++) {
                    if(listTagihan.get(i).isCb_checked()) {
                        mTempObjectDataList.add(new TempObjectData(listTagihan.get(i).getDoc_id(), listTagihan.get(i).getInput_amount()));
                    }
                }

                final GsonBuilder gsonBuilder = new GsonBuilder();
                gsonBuilder.setPrettyPrinting();
                final Gson gson = gsonBuilder.create();
                String invoices = gson.toJson(mTempObjectDataList);

                Fragment mFrag;
                Bundle mArgs = new Bundle();

                if(_isScash) {
                    mFrag = new PreviewTagihanViaScash();
                }
                else {
                    mFrag = new ListTagihanViaBank();
                }
                mArgs.putString(DefineValue.COMMUNITY_ID, comm_id);
                mArgs.putString(DefineValue.COMMUNITY_CODE, comm_code);
                mArgs.putString(DefineValue.COMMUNITY_NAME, comm_name);
                mArgs.putString(DefineValue.API_KEY, api_key);
                mArgs.putString(DefineValue.CALLBACK_URL, callback_url);
                mArgs.putString(DefineValue.AMOUNT_TAGIHAN, Long.toString(jumlah));
                mArgs.putString(DefineValue.BANKLIST_DATA, bank_data);
                mArgs.putString(DefineValue.INVOICES, invoices);

                mFrag.setArguments(mArgs);

                switchFragment(mFrag, getString(R.string.tagihan_ab_title), true);
            }
        }
    }

    private void switchFragment(android.support.v4.app.Fragment i, String name, Boolean isBackstack){
        if (getActivity() == null)
            return;

        TagihanActivity fca = (TagihanActivity) getActivity();
        fca.switchContent(i,name,isBackstack);
    }

    private void countTotal(int position) {
        jumlah = 0;
        for (int i = 0; i < listTagihan.size(); i++) {
            String total = listTagihan.get(i).getInput_amount();
            if (listTagihan.get(i).isCb_checked())
                jumlah += Long.parseLong(total);
        }
        if (jumlah != 0) {
            if(listTagihan.get(position).getCcy().equalsIgnoreCase("idr")) {
                tv_total_bayar.setText("Rp. " + CurrencyFormat.format(Long.toString(jumlah)));
            }
            else {
                tv_total_bayar.setText(Long.toString(jumlah));
            }
        } else {
            tv_total_bayar.setText(Long.toString(jumlah));
        }
    }

    private void showDialog(int _position) {
        TagihanListModel itemnya = listTagihan.get(_position);
        dialogDetail = TagihanDetailDialog.newInstance(getActivity(),_position, itemnya.getInput_amount());

        Bundle args = new Bundle();
        args.putString(DefineValue.DOC_ID, itemnya.getDoc_id());
        args.putString(DefineValue.DOC_NO, itemnya.getDoc_no());
        args.putString(DefineValue.DOC_NAME, itemnya.getDoc_name());
        args.putString(DefineValue.DESCRIPTION, itemnya.getDesc());
        args.putString(DefineValue.DOC_AMOUNT, itemnya.getDoc_amount());
        args.putString(DefineValue.REMAIN_AMOUNT, itemnya.getRemain_amount());
        args.putString(DefineValue.CCY_ID, itemnya.getCcy());
        args.putString(DefineValue.PARTIAL_PAYMENT, partial_payment);
        args.putString(DefineValue.CUST_NAME, cust_name);

        dialogDetail.setArguments(args);
        dialogDetail.setTargetFragment(FragTagihan.this,0);
        dialogDetail.show(getActivity().getSupportFragmentManager(), TagihanDetailDialog.TAG);
    }

    private void getDataTagihan(int _page, final Boolean isRefresh, String _comm_id) {
        try{

            if(isRefresh == null){
                Timber.wtf("masuk ptr");
            }
            else if(isRefresh){
                Timber.wtf("masuk refresh");
                out = DefinedDialog.CreateProgressDialog(getActivity(), null);
                out.show();
                mPtrFrame.setEnabled(true);
                mPtrFrame.setVisibility(View.VISIBLE);
            }
            else {
                Timber.wtf("masuk load more");
                btn_loadmore.setVisibility(View.GONE);
                spining_progress.setVisibility(View.VISIBLE);
                spining_progress.startAnimation(frameAnimation);
            }

            RequestParams params = MyApiClient.getSignatureWithParams(MyApiClient.COMM_ID, MyApiClient.LINK_INVOICE_TAGIHAN,
                    user_id, access_key);
            params.put(WebParams.COMM_ID_INV, _comm_id);
            params.put(WebParams.USER_ID,user_id);
            params.put(WebParams.COMM_ID, MyApiClient.COMM_ID);
            params.put(WebParams.PAGE, _page);
            params.put(WebParams.OFFSET, offset);

            Timber.d("isi params get invoice tagihan:"+params.toString());

            MyApiClient.getInvoiceTagihan(getActivity(), params, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                    try {
                        if (isAdded()) {
                            if (isRefresh != null) {
                                if (isRefresh)
                                    out.dismiss();
                            }

                            String code = response.getString(WebParams.ERROR_CODE);
                            Timber.w("isi response get invoice tagihan:" + response.toString());

                            if (code.equals(WebParams.SUCCESS_CODE)) {
                                if (isRefresh == null) {
                                    mPtrFrame.refreshComplete();
                                    listTagihan.clear();
                                } else if (isRefresh)
                                    listTagihan.clear();
                                else {
                                    btn_loadmore.setVisibility(View.VISIBLE);
                                    spining_progress.setVisibility(View.GONE);
                                    spining_progress.setAnimation(null);
                                }

                                partial_payment = response.getString(WebParams.PARTIAL_PAYMENT);
                                bank_data = response.getString(WebParams.BANK);
                                if(bank_data.equals("") || bank_data.equals("null")) {
                                    btnBayarViaBank.setEnabled(false);
                                    btnBayarViaBank.setTextColor(getResources().getColor(R.color.colorSecondaryDark));
                                }
                                else {
                                    btnBayarViaBank.setEnabled(true);
                                    btnBayarViaBank.setTextColor(getResources().getColor(R.color.black));
                                }
                                String invoice = response.getString(WebParams.INVOICE);
                                if (!invoice.equals("")) {
                                    JSONArray arrayData = new JSONArray(invoice);
                                    JSONObject mObj;
                                    for (int i = 0; i < arrayData.length(); i++) {
                                        mObj = arrayData.getJSONObject(i);

                                        TagihanListModel tagihanListModel = new TagihanListModel();
                                        tagihanListModel.setCb_checked(false);
                                        tagihanListModel.setDoc_id(mObj.optString(WebParams.DOC_ID, ""));
                                        tagihanListModel.setDoc_no(mObj.optString(WebParams.DOC_NO, ""));
                                        tagihanListModel.setDoc_name(mObj.optString(WebParams.DOC_NAME, ""));
                                        tagihanListModel.setDoc_amount(mObj.optString(WebParams.DOC_AMOUNT, ""));
                                        tagihanListModel.setRemain_amount(mObj.optString(WebParams.REMAIN_AMOUNT, ""));
                                        tagihanListModel.setInput_amount("0");
                                        tagihanListModel.setCcy(mObj.optString(WebParams.CCY_ID, ""));
                                        tagihanListModel.setDesc(mObj.optString(WebParams.DOC_DESC, ""));
                                        listTagihan.add(tagihanListModel);
                                    }
                                }
                                else if(invoice.equals("") && page == 1) {
                                    mPtrFrame.refreshComplete();
                                    setLoadMore(false);
                                    lv_tagihan.setVisibility(View.GONE);
                                    emptyLayout.setVisibility(View.VISIBLE);
                                    listTagihan.clear();
                                    adapter.notifyDataSetChanged();
                                    String message = getActivity().getResources().getString(R.string.no_invoice_data_info);
                                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                                    builder.setMessage(message)
                                            .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    dialog.dismiss();
                                                    getActivity().finish();
                                                }
                                            });
                                    AlertDialog dialog = builder.create();
                                    dialog.show();
                                }

                                int _page = response.optInt(WebParams.NEXT, 0);
                                if (_page != 0) {
                                    page++;
                                    setLoadMore(false);
                                    setLoadMore(true);
                                } else {
                                    setLoadMore(false);
                                }
                                adapter.notifyDataSetChanged();

                                if (isRefresh == null || isRefresh) {
                                    lv_tagihan.setSelection(0);
                                    lv_tagihan.smoothScrollToPosition(0);
                                    lv_tagihan.setSelectionAfterHeaderView();
                                }

                            } else if (code.equals(WebParams.LOGOUT_CODE)) {
                                Timber.d("isi response autologout:" + response.toString());
                                String message = response.getString(WebParams.ERROR_MESSAGE);
                                AlertDialogLogout test = AlertDialogLogout.getInstance();
                                test.showDialoginActivity(getActivity(), message);
                            }
                            else {
//                                String message = response.getString(WebParams.ERROR_MESSAGE);
                                String message = getActivity().getResources().getString(R.string.no_invoice_data_info);
                                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                                builder.setMessage(message)
                                        .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                dialog.dismiss();
                                                getActivity().finish();
                                            }
                                        });
                                AlertDialog dialog = builder.create();
                                dialog.show();
                            }
                        }

                    } catch (JSONException e) {

                        Toast.makeText(getActivity(), getString(R.string.internal_error), Toast.LENGTH_LONG).show();
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    super.onFailure(statusCode, headers, responseString, throwable);
                    failure(throwable);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    failure(throwable);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    failure(throwable);
                }

                private void failure(Throwable throwable) {
                    if (MyApiClient.PROD_FAILURE_FLAG)
                        Toast.makeText(getActivity(), getString(R.string.network_connection_failure_toast), Toast.LENGTH_SHORT).show();
                    else
                        Toast.makeText(getActivity(), throwable.toString(), Toast.LENGTH_SHORT).show();

                    mPtrFrame.refreshComplete();
                    setLoadMore(false);
                    lv_tagihan.setVisibility(View.GONE);
                    emptyLayout.setVisibility(View.VISIBLE);
                    listTagihan.clear();
                    adapter.notifyDataSetChanged();

                    Timber.w("Error Koneksi get invoice tagihan:" + throwable.toString());
                }
            });
        } catch (Exception e) {
            String err = (e.getMessage()==null)?"Connection failed":e.getMessage();
            Timber.e("http err:" + err);
        }
    }

    private boolean inputValidation() {
        if(jumlah == 0){
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setMessage(getString(R.string.alert_total_bayar))
                    .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
            AlertDialog alert = builder.create();
            alert.show();
            return false;
        }
        return true;
    }

    private boolean canScrollUp() {
        return lv_tagihan != null && (lv_tagihan.getAdapter().getCount() == 0 || lv_tagihan.getFirstVisiblePosition() == 0 && lv_tagihan.getChildAt(0).getTop() == 0);
    }

    private void setLoadMore(boolean isLoading)
    {
        if (isLoading) {
            lv_tagihan.addFooterView(footerLayout,null,false);
        }
        else {
            lv_tagihan.removeFooterView(footerLayout);
        }
    }

    private View getV() {
        return v;
    }

    private void setV(View v) {
        this.v = v;
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        setTitle(getString(R.string.tagihan_ab_title));
    }

    @Override
    public void onOkButton(String input_amount, int position) {
        if(Long.parseLong(input_amount) == 0)
            listTagihan.get(position).setCb_checked(false);
        else
            listTagihan.get(position).setCb_checked(true);
        listTagihan.get(position).setInput_amount(input_amount);
        adapter.notifyDataSetChanged();
        countTotal(position);
    }

    private void setTitle(String _title){
        if (getActivity() == null)
            return;

        TagihanActivity fca = (TagihanActivity) getActivity();
        fca.setTitleFragment(_title);
    }

    static class ViewHolder
    {
        TextView tv_payment_name,tv_remain_amount, tv_desc;
        EditText et_input_amount;
        CheckBox cb_tagihan;
        ImageView img_arrow;
    }

    private class TempObjectData {
        private String doc_id;
        private String payment_amount;

        public TempObjectData(String _doc_id, String _payment_amount) {
            this.setDoc_id(_doc_id);
            this.setPayment_amount(_payment_amount);
        }

        public String getDoc_id() {
            return doc_id;
        }

        public void setDoc_id(String doc_id) {
            this.doc_id = doc_id;
        }

        public String getPayment_amount() {
            return payment_amount;
        }

        public void setPayment_amount(String payment_amount) {
            this.payment_amount = payment_amount;
        }
    }

}


