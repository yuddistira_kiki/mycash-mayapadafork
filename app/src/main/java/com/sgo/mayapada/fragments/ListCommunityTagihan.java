package com.sgo.mayapada.fragments;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.securepreferences.SecurePreferences;
import com.sgo.mayapada.Beans.CommunityMySchoolModel;
import com.sgo.mayapada.R;
import com.sgo.mayapada.activities.MainPage;
import com.sgo.mayapada.activities.RegisterVaActivity;
import com.sgo.mayapada.activities.TagihanActivity;
import com.sgo.mayapada.adapter.EasyAdapter;
import com.sgo.mayapada.coreclass.CustomSecurePref;
import com.sgo.mayapada.coreclass.DefineValue;
import com.sgo.mayapada.coreclass.MyApiClient;
import com.sgo.mayapada.coreclass.WebParams;
import com.sgo.mayapada.dialogs.AlertDialogLogout;
import com.sgo.mayapada.dialogs.DefinedDialog;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import timber.log.Timber;

/**
 * Created by thinkpad on 5/24/2016.
 */
public class ListCommunityTagihan extends ListFragment{

    private View v;
    private SecurePreferences sp;
    private ProgressDialog out;
    private EasyAdapter mAdapter;
    private ArrayList<String> listCommName;
    private ArrayList<String> listPairID;
    private ArrayList<CommunityMySchoolModel> listComm;
    private String user_id;
    private String access_key;
    String comm_id;
    String comm_code;
    String comm_name;
    String api_key;
    String callback_url;
    private String response_comm_list;
    private Button register_pair_id_btn;

    public static ListCommunityTagihan newInstance() {
        return new ListCommunityTagihan();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        v = inflater.inflate(R.layout.frag_list_community_myschool, container, false);
        register_pair_id_btn = (Button) v.findViewById(R.id.register_pairing_id_button);
        return v;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        sp = CustomSecurePref.getInstance().getmSecurePrefs();
        user_id = sp.getString(DefineValue.USERID_PHONE, "");
        access_key = sp.getString(DefineValue.ACCESS_KEY, "");

        listComm = new ArrayList<>();
        listCommName = new ArrayList<>();
        listPairID = new ArrayList<>();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


        mAdapter = new EasyAdapter(getActivity(),R.layout.list_view_item_with_arrow_komunitasku, listCommName, listPairID);

        ListView listView = (ListView) v.findViewById(android.R.id.list);
        listView.setAdapter(mAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent mI = new Intent(getActivity(),TagihanActivity.class);
                mI.putExtra(DefineValue.COMMUNITY_ID, listComm.get(position).getComm_id());
                mI.putExtra(DefineValue.COMMUNITY_CODE, listComm.get(position).getComm_code());
                mI.putExtra(DefineValue.COMMUNITY_NAME, listComm.get(position).getComm_name());
                mI.putExtra(DefineValue.API_KEY, listComm.get(position).getApi_key());
                mI.putExtra(DefineValue.CALLBACK_URL, listComm.get(position).getCallback_url());
                mI.putExtra(DefineValue.CUST_NAME, listComm.get(position).getCust_name());
                if (!listComm.get(position).getPairing_id().equals("null"))
                    mI.putExtra("pairing_id", listComm.get(position).getPairing_id());

                getActivity().startActivityForResult(mI, MainPage.ACTIVITY_RESULT);
            }
        });
        register_pair_id_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startPairingIDActivity();
            }
        });

        getCommInv();
    }

    private void getCommInv() {
        try {
            out = DefinedDialog.CreateProgressDialog(getActivity(), null);
            out.show();

            RequestParams params = MyApiClient.getSignatureWithParams(MyApiClient.COMM_ID, MyApiClient.LINK_COMM_INV,
                    user_id, access_key);
            params.put(WebParams.COMM_ID, MyApiClient.COMM_ID);
            params.put(WebParams.USER_ID, user_id);
            params.put(WebParams.MEMBER_ID, sp.getString(DefineValue.MEMBER_ID, ""));

            Timber.d("isi params get comm inv:" + params.toString());

            MyApiClient.getCommInv(getActivity(), params, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    out.dismiss();

                    try {
                        String code = response.getString(WebParams.ERROR_CODE);
                        if (code.equals(WebParams.SUCCESS_CODE)) {
                            Timber.d("isi response get comm inv:" + response.toString());
                            JSONArray arrayData = new JSONArray(response.getString(WebParams.COMMUNITY));
                            response_comm_list = response.getString(WebParams.COMMUNITY);
                            for(int i = 0 ; i < arrayData.length() ; i++) {
                                JSONObject mObj = arrayData.getJSONObject(i);
                                CommunityMySchoolModel commModel = new CommunityMySchoolModel();
                                commModel.setComm_id(mObj.optString(WebParams.COMM_ID, ""));
                                commModel.setComm_name(mObj.optString(WebParams.COMM_NAME, ""));
                                commModel.setComm_code(mObj.optString(WebParams.COMM_CODE, ""));
                                commModel.setApi_key(mObj.optString(WebParams.API_KEY, ""));
                                commModel.setCallback_url(mObj.optString(WebParams.CALLBACK_URL, ""));
                                commModel.setCust_name(mObj.optString(WebParams.CUST_NAME, ""));
                                commModel.setPairing_id(mObj.optString(WebParams.PAIRING_ID, ""));
                                sp.edit().putString("comm_id_emo_sp", mObj.optString(WebParams.COMM_ID, "")).apply();
                                listComm.add(commModel);
                                listCommName.add(mObj.optString(WebParams.COMM_NAME, ""));
                                listPairID.add(mObj.optString(WebParams.PAIRING_ID, ""));
                            }
                            mAdapter.notifyDataSetChanged();
                        } else if (code.equals(WebParams.LOGOUT_CODE)) {
                            Timber.d("isi response autologout:" + response.toString());
                            String message = response.getString(WebParams.ERROR_MESSAGE);
                            AlertDialogLogout test = AlertDialogLogout.getInstance();
                            test.showDialoginMain(getActivity(), message);
                        } else {
                            Timber.d("isi error get comm inv:" + response.toString());
                            String code_msg = response.getString(WebParams.ERROR_MESSAGE);
                            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                            builder.setMessage(code_msg)
                                    .setPositiveButton("Dismiss", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();
                                        }
                                    });
                            AlertDialog dialog = builder.create();
                            dialog.show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    super.onFailure(statusCode, headers, responseString, throwable);
                    failure(throwable);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    failure(throwable);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    failure(throwable);
                }

                private void failure(Throwable throwable) {
                    if (MyApiClient.PROD_FAILURE_FLAG)
                        Toast.makeText(getActivity(), getString(R.string.network_connection_failure_toast), Toast.LENGTH_SHORT).show();
                    else
                        Toast.makeText(getActivity(), throwable.toString(), Toast.LENGTH_SHORT).show();

                    if (out.isShowing())
                        out.dismiss();

                    Timber.w("Error Koneksi get comm inv:" + throwable.toString());
                }
            });
        }catch (Exception e){
            Timber.d("httpclient:" + e.getMessage());
        }
    }

    private void startPairingIDActivity(){
        Intent i = new Intent(getActivity(), RegisterVaActivity.class);
        i.putExtra("response", response_comm_list);
        startActivityForResult(i, 100);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 100){
            switch (resultCode){
                case 101:
                    if (listCommName.size() > 0)
                        listCommName.clear();
                    if (listComm.size() > 0)
                        listComm.clear();
                    if (listPairID.size() > 0)
                        listPairID.clear();
                    getCommInv();
            }
        }
    }
}
