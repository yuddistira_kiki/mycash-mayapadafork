package com.sgo.mayapada.fragments;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.securepreferences.SecurePreferences;
import com.sgo.mayapada.Beans.KantinStudentDetailModel;
import com.sgo.mayapada.R;
import com.sgo.mayapada.activities.KantinActivity;
import com.sgo.mayapada.adapter.KantinStudentDetailAdapter;
import com.sgo.mayapada.coreclass.CurrencyFormat;
import com.sgo.mayapada.coreclass.CustomSecurePref;
import com.sgo.mayapada.coreclass.DefineValue;
import com.sgo.mayapada.coreclass.InetHandler;
import com.sgo.mayapada.coreclass.ListViewHeight;
import com.sgo.mayapada.coreclass.MyApiClient;
import com.sgo.mayapada.coreclass.WebParams;
import com.sgo.mayapada.dialogs.AlertDialogLogout;
import com.sgo.mayapada.dialogs.DefinedDialog;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import timber.log.Timber;

/**
 * Created by thinkpad on 8/9/2016.
 */
public class FragKantinDetailBelanja extends Fragment {
    private SecurePreferences sp;
    private View v;
    private View btn_confirm_layout;
    private View coupon_layout;
    private View coupon_row;
    private View kadaluarsa_layout;
    private ProgressDialog out;
    private ListView lvBelanja;
    private TextView tvDatetime;
    private TextView tvName;
    private TextView tvHp;
    private TextView tvStatus;
    private TextView tvCoupon;
    private TextView tvTotal;
    private TextView tvKadaluarsa;
    private TextView tvTglKadaluarsa;
    private KantinStudentDetailAdapter adapter;
    private ArrayList<KantinStudentDetailModel> listBelanja;
    private Button btnBack;
    private Button btnConfirm;
    private boolean confirm = false;
    private String coupon_code;
    private String user_id;
    private String access_key;
    private String member_id;
    private String username;
    private String order;
    private String member_name;
    private String no_hp_member;
    private String amount;
    private String ccy_id;
    private String tx_datetime;
    private String kadaluarsa;
    private String status;
    private String merchant_subcategory;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        v = inflater.inflate(R.layout.frag_kantin_detail_belanja, container, false);
        return v;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        sp = CustomSecurePref.getInstance().getmSecurePrefs();
        user_id = sp.getString(DefineValue.USERID_PHONE, "");
        username = sp.getString(DefineValue.USER_NAME, "");
        access_key = sp.getString(DefineValue.ACCESS_KEY, "");
        member_id = sp.getString(DefineValue.MEMBER_ID, "");
        merchant_subcategory = sp.getString(DefineValue.MERCHANT_SUBCATEGORY, "");

        listBelanja = new ArrayList<>();

        Bundle bundle = getArguments();
        if(bundle != null) {
            confirm = bundle.getBoolean(DefineValue.CONFIRM);
            coupon_code = bundle.getString(DefineValue.COUPON_CODE);
            order = bundle.getString(DefineValue.LIST_ORDER);
            member_name = bundle.getString(DefineValue.MEMBER_NAME);
            no_hp_member = bundle.getString(DefineValue.NO_HP_MEMBER);
            amount = bundle.getString(DefineValue.AMOUNT);
            ccy_id = bundle.getString(DefineValue.CCY_ID);
            tx_datetime = bundle.getString(DefineValue.TX_DATETIME);
            kadaluarsa = bundle.getString(DefineValue.KADALUARSA);
            status = bundle.getString(DefineValue.STATUS);
        }

        tvCoupon = (TextView) v.findViewById(R.id.txt_kode_kupon);
        tvDatetime = (TextView) v.findViewById(R.id.txt_datetime);
        tvName = (TextView) v.findViewById(R.id.txt_name);
        tvHp = (TextView) v.findViewById(R.id.txt_no_hp);
        tvTotal = (TextView) v.findViewById(R.id.txt_total_jumlah);
        tvKadaluarsa = (TextView) v.findViewById(R.id.txt_berlaku_sampai);
        tvStatus = (TextView) v.findViewById(R.id.txt_status_barang);
        btn_confirm_layout = v.findViewById(R.id.btn_confirm_layout);
        kadaluarsa_layout = v.findViewById(R.id.kadaluarsa_layout);
        tvTglKadaluarsa = (TextView) v.findViewById(R.id.txt_datetime_kl);
        coupon_layout = v.findViewById(R.id.coupon_layout);
        coupon_row = v.findViewById(R.id.coupon_row);
        lvBelanja = (ListView) v.findViewById(R.id.lv_belanja);
        btnBack = (Button) v.findViewById(R.id.kantindetailbelanja_back_button);
        btnConfirm = (Button) v.findViewById(R.id.kantindetailbelanja_confirm_button);

        tvCoupon.setText(coupon_code);
        tvDatetime.setText(tx_datetime);
        tvName.setText(member_name);
        tvHp.setText(no_hp_member);
        tvTotal.setText(ccy_id + " " + CurrencyFormat.format(amount));
        tvKadaluarsa.setText(kadaluarsa);
        tvStatus.setText(status);

        if(status.equalsIgnoreCase("sudah diambil")) {
            tvStatus.setTextColor(ContextCompat.getColor(getActivity(),R.color.green_500));
            kadaluarsa_layout.setVisibility(View.GONE);
        }
        else if(status.equalsIgnoreCase("belum diambil")) {
            tvStatus.setTextColor(ContextCompat.getColor(getActivity(), R.color.blue_800));
            kadaluarsa_layout.setVisibility(View.GONE);
        }
        else if(status.equalsIgnoreCase("kadaluarsa")) {
            tvStatus.setTextColor(ContextCompat.getColor(getActivity(), R.color.red_500));
            kadaluarsa_layout.setVisibility(View.VISIBLE);
            tvTglKadaluarsa.setText(kadaluarsa);
        }

        try {
            JSONArray arr = new JSONArray(order);
            for(int i = 0 ; i < arr.length() ; i++) {
                KantinStudentDetailModel detail = new KantinStudentDetailModel();
                detail.setName(arr.getJSONObject(i).getString(WebParams.ITEM_NAME));
                detail.setCount(arr.getJSONObject(i).getString(WebParams.QTY));
                detail.setAmount(arr.getJSONObject(i).getString(WebParams.TOTAL_AMOUNT_THIS_ITEM));
                listBelanja.add(detail);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if(confirm) {
            coupon_row.setVisibility(View.VISIBLE);
            coupon_layout.setVisibility(View.VISIBLE);
            if(status.equalsIgnoreCase("kadaluarsa") || status.equalsIgnoreCase("sudah diambil"))
                btn_confirm_layout.setVisibility(View.GONE);
            else
                btn_confirm_layout.setVisibility(View.VISIBLE);
        }
        else {
            coupon_row.setVisibility(View.GONE);
            coupon_layout.setVisibility(View.GONE);
            btn_confirm_layout.setVisibility(View.GONE);
        }

        adapter = new KantinStudentDetailAdapter(getActivity(), listBelanja, ccy_id);
        lvBelanja.setAdapter(adapter);

        lvBelanja.setOnTouchListener(new View.OnTouchListener() {
            // Setting on Touch Listener for handling the touch inside ScrollView
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                // Disallow the touch request for parent scroll on touch of child view
                v.getParent().requestDisallowInterceptTouchEvent(true);
                return false;
            }
        });

        ListViewHeight.setTotalHeightofListView(lvBelanja);

        btnConfirm.setOnClickListener(confirmListener);
        btnBack.setOnClickListener(backListener);
    }

    private Button.OnClickListener backListener = new Button.OnClickListener() {
        @Override
        public void onClick(View view) {
            getActivity().getSupportFragmentManager().popBackStack();
        }
    };

    private Button.OnClickListener confirmListener = new Button.OnClickListener() {
        @Override
        public void onClick(View view) {
            if(InetHandler.isNetworkAvailable(getActivity())) {
                sentConfirmCouponCode();
            }
            else DefinedDialog.showErrorDialog(getActivity(), getString(R.string.inethandler_dialog_message));
        }
    };

    private void sentConfirmCouponCode() {
        try{
            out = DefinedDialog.CreateProgressDialog(getActivity(), null);
            out.show();

            RequestParams params = MyApiClient.getSignatureWithParams(MyApiClient.COMM_ID, MyApiClient.LINK_CONFIRM_COUPON_CODE,
                    user_id, access_key);
            params.put(WebParams.USER_ID,user_id);
            params.put(WebParams.COMM_ID, MyApiClient.COMM_ID);
            params.put(WebParams.MERCHANT_ID, member_id);
            params.put(WebParams.KUPON, coupon_code);
            params.put(WebParams.MERCHANT_SUBCATEGORY, merchant_subcategory);

            Timber.d("isi params confirm coupon code:"+params.toString());

            MyApiClient.sentConfirmCouponCode(getActivity(), params, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                    try {
                        out.dismiss();

                        String code = response.getString(WebParams.ERROR_CODE);
                        Timber.w("isi response confirm coupon code:" + response.toString());

                        if (code.equals(WebParams.SUCCESS_CODE)) {
                            tvStatus.setText(getResources().getString(R.string.kupon_sudah_ambil));
                            tvStatus.setTextColor(getResources().getColor(R.color.red));
                            btn_confirm_layout.setVisibility(View.GONE);

                        } else if (code.equals(WebParams.LOGOUT_CODE)) {
                            Timber.d("isi response autologout:" + response.toString());
                            String message = response.getString(WebParams.ERROR_MESSAGE);
                            AlertDialogLogout test = AlertDialogLogout.getInstance();
                            test.showDialoginActivity(getActivity(), message);
                        }
                        else {
                            String message = response.getString(WebParams.ERROR_MESSAGE);
                            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                            builder.setMessage(message)
                                    .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();
                                        }
                                    });
                            AlertDialog dialog = builder.create();
                            dialog.show();
                        }

                    } catch (JSONException e) {
                        Toast.makeText(getActivity(), getString(R.string.internal_error), Toast.LENGTH_LONG).show();
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    super.onFailure(statusCode, headers, responseString, throwable);
                    failure(throwable);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    failure(throwable);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    failure(throwable);
                }

                private void failure(Throwable throwable) {
                    out.dismiss();
                    if (MyApiClient.PROD_FAILURE_FLAG)
                        Toast.makeText(getActivity(), getString(R.string.network_connection_failure_toast), Toast.LENGTH_SHORT).show();
                    else
                        Toast.makeText(getActivity(), throwable.toString(), Toast.LENGTH_SHORT).show();

                    Timber.w("Error Koneksi confirm coupon code:" + throwable.toString());
                }
            });
        } catch (Exception e) {
            String err = (e.getMessage()==null)?"Connection failed":e.getMessage();
            Timber.e("http err:" + err);
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if(getActivity().getSupportFragmentManager().getBackStackEntryCount() > 0)
                    getActivity().getSupportFragmentManager().popBackStack();
                else
                    getActivity().finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume() {
        super.onResume();
        setTitle(getString(R.string.kantin_detail_title));
    }

    private void setTitle(String _title){
        if (getActivity() == null)
            return;

        KantinActivity fca = (KantinActivity) getActivity();
        fca.setTitleFragment(_title);
    }
}
