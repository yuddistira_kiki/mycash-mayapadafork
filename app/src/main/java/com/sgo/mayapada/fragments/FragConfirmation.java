package com.sgo.mayapada.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.sgo.mayapada.R;
import com.sgo.mayapada.activities.InsertPIN;
import com.sgo.mayapada.activities.MainPage;
import com.sgo.mayapada.coreclass.DefineValue;
import com.sgo.mayapada.dialogs.ReportBillerDialog;

import timber.log.Timber;

/**
 * Created by Denny on 9/22/2016.
 */

public class FragConfirmation extends Fragment implements ReportBillerDialog.OnDialogOkCallback{

    private int flag;
    private String type;
    private String userid;
    private String merchant;
    private String fee;
    private String total;
    private String price;
    private String item;

    private Button one_btn, two_btn, three_btn, four_btn, five_btn, six_btn, seven_btn, eight_btn,
            nine_btn, zero_btn, back_btn, dot_btn;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Bundle bundle = this.getArguments();
        type = bundle.getString("type");
        userid = bundle.getString("userid");
        merchant = bundle.getString("merchantname");


        if(type.equalsIgnoreCase("1")) {
            return inflater.inflate(R.layout.frag_qr_confirmation_personal, container, false);
        }
        else if(type.equalsIgnoreCase("2"))
        {
            item = bundle.getString("item");
            fee = bundle.getString("fee");
            total = bundle.getString("total");
            price = bundle.getString("price");
            return inflater.inflate(R.layout.frag_qr_confirmation, container, false);
        }
        else
        {
            fee = bundle.getString("fee");
            total = bundle.getString("total");
            price = bundle.getString("price");
            return inflater.inflate(R.layout.frag_qr_confirmation, container, false);
        }

    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if(type.equalsIgnoreCase("1")) {
            final TextView total_harga_personal = (TextView) getActivity().findViewById(R.id.total_harga_personal);
            TextView merchant_name = (TextView) getActivity().findViewById(R.id.merchant_name);
            merchant_name.setText(merchant);

            one_btn=(Button) getActivity().findViewById(R.id.one_btn);
            two_btn=(Button) getActivity().findViewById(R.id.two_btn);
            three_btn=(Button) getActivity().findViewById(R.id.three_btn);
            four_btn=(Button) getActivity().findViewById(R.id.four_btn);
            five_btn=(Button) getActivity().findViewById(R.id.five_btn);
            six_btn=(Button) getActivity().findViewById(R.id.six_btn);
            seven_btn=(Button) getActivity().findViewById(R.id.seven_btn);
            eight_btn=(Button) getActivity().findViewById(R.id.eight_btn);
            nine_btn=(Button) getActivity().findViewById(R.id.nine_btn);
            zero_btn=(Button) getActivity().findViewById(R.id.zero_btn);
            back_btn=(Button) getActivity().findViewById(R.id.back_btn);
            dot_btn=(Button) getActivity().findViewById(R.id.dot_btn);

            //        ------------------------------------------------------

            // button listener
            one_btn.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
//                    if(mlistener != null){
                    total_harga_personal.append("1");
                    Log.d("denny", "1");
//                        mlistener.add("1");
//                    }
                }
            });
            two_btn.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
//                    if(mlistener != null){
                    total_harga_personal.append("2");
//                        mlistener.add("2");
//                    }
                }
            });
            three_btn.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
//                    if(mlistener != null){
                    total_harga_personal.append("3");
//                        mlistener.add("3");
//                    }
                }
            });
            four_btn.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
//                    if(mlistener != null){
                    total_harga_personal.append("4");
//                        mlistener.add("4");
//                    }
                }
            });
            five_btn.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
//                    if(mlistener != null){
                    total_harga_personal.append("5");
//                        mlistener.add("5");
//                    }
                }
            });
            six_btn.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
//                    if(mlistener != null){
                    total_harga_personal.append("6");
//                        mlistener.add("6");
//                    }
                }
            });
            seven_btn.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
//                    if(mlistener != null){
                    total_harga_personal.append("7");
//                        mlistener.add("7");
//                    }
                }
            });
            eight_btn.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
//                    if(mlistener != null){
                    total_harga_personal.append("8");
//                        mlistener.add("8");
//                    }
                }
            });
            nine_btn.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
//                    if(mlistener != null){
                    total_harga_personal.append("9");
//                        mlistener.add("9");
//                    }
                }
            });
            zero_btn.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
//                    if(mlistener != null){
                    Timber.d("nol");
                    total_harga_personal.append("0");
//                        mlistener.add("0");
//                    }
                }
            });
            back_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    if(mlistener != null){
//                        mlistener.subtract();

                    if (total_harga_personal.length() > 0) {
                        String asd = total_harga_personal.getText().toString();
                        String as = asd.substring(0, total_harga_personal.getText().length() - 1);
                        total_harga_personal.setText(as);
                        if (total_harga_personal.length() == 0) {
                            total_harga_personal.setText("0");
                        }
                    }
//                    }
                }
            });

//        ------------------------------------------------------
        }
        else
        {
            TextView merchant_name = (TextView) getActivity().findViewById(R.id.merchant_name);
            TextView total_harga = (TextView) getActivity().findViewById(R.id.total_harga);
            TextView txt_total = (TextView) getActivity().findViewById(R.id.txt_total);
            TextView txt_fee = (TextView) getActivity().findViewById(R.id.txt_fee);
            TextView txt_merchant = (TextView) getActivity().findViewById(R.id.txt_merchant);
            merchant_name.setText(merchant);
            total_harga.setText(total);
            txt_total.setText(total);
            txt_fee.setText(total);
            txt_fee.setText(fee);
            txt_merchant.setText(merchant);

        }

        Button process = (Button) getActivity().findViewById(R.id.proses);
        process.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), InsertPIN.class);
                startActivityForResult(i, MainPage.REQUEST_FINISH);
                flag = 1;
            }
        });


    }

    @Override
    public void onResume() {
        super.onResume();
        if(flag == 1)
        {
            showReportBillerDialog(userid, merchant, price, fee, total);
            flag = 0;
        }
    }

    private void showReportBillerDialog(String userid, String merchantname,String price, String fee, String total) {

        if(type.equalsIgnoreCase("1")) {
            Bundle args = new Bundle();
            ReportBillerDialog dialogcm = new ReportBillerDialog();
            args.putString(DefineValue.REPORT_TYPE, DefineValue.PAYBYQR_PM);
//            args.putString(DefineValue.USERID_PHONE, userid);
//            args.putString(DefineValue.MERCHANT, merchantname);
//            args.putString(DefineValue.FEE, MyApiClient.CCY_VALUE + ". " + CurrencyFormat.format(fee));
//            args.putString(DefineValue.AMOUNT, MyApiClient.CCY_VALUE + ". " + CurrencyFormat.format(price));
//            args.putString(DefineValue.TOTAL_AMOUNT, MyApiClient.CCY_VALUE + ". " + CurrencyFormat.format(total));

            dialogcm.setArguments(args);
            dialogcm.setTargetFragment(this,0);
            dialogcm.show(getActivity().getSupportFragmentManager(), ReportBillerDialog.TAG);
        }
        else if(type.equalsIgnoreCase("2"))
        {
            Bundle args = new Bundle();
            ReportBillerDialog dialogpm = new ReportBillerDialog();
            args.putString(DefineValue.REPORT_TYPE, DefineValue.PAYBYQR_CM);
//            args.putString(DefineValue.USERID_PHONE, userid);
//            args.putString(DefineValue.MERCHANT, merchantname);
//            args.putString(DefineValue.ITEM, item);
//            args.putString(DefineValue.FEE, MyApiClient.CCY_VALUE + ". " + CurrencyFormat.format(fee));
//            args.putString(DefineValue.AMOUNT, MyApiClient.CCY_VALUE + ". " + CurrencyFormat.format(price));
//            args.putString(DefineValue.TOTAL_AMOUNT, MyApiClient.CCY_VALUE + ". " + CurrencyFormat.format(total));

            dialogpm.setArguments(args);
            dialogpm.setTargetFragment(this,0);
            dialogpm.show(getActivity().getSupportFragmentManager(), ReportBillerDialog.TAG);
        }
        else
        {
            Bundle args = new Bundle();
            ReportBillerDialog dialogpm = new ReportBillerDialog();
            args.putString(DefineValue.REPORT_TYPE, DefineValue.PAYBYQR_CM);
//            args.putString(DefineValue.USERID_PHONE, userid);
//            args.putString(DefineValue.MERCHANT, merchantname);
//            args.putString(DefineValue.FEE, MyApiClient.CCY_VALUE + ". " + CurrencyFormat.format(fee));
//            args.putString(DefineValue.AMOUNT, MyApiClient.CCY_VALUE + ". " + CurrencyFormat.format(price));
//            args.putString(DefineValue.TOTAL_AMOUNT, MyApiClient.CCY_VALUE + ". " + CurrencyFormat.format(total));

            dialogpm.setArguments(args);
            dialogpm.setTargetFragment(this,0);
            dialogpm.show(getActivity().getSupportFragmentManager(), ReportBillerDialog.TAG);
        }
    }

    @Override
    public void onOkButton() {
        getActivity().setResult(MainPage.RESULT_BALANCE);
//        getActivity().finish();
    }
}
