package com.sgo.mayapada.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.securepreferences.SecurePreferences;
import com.sgo.mayapada.Beans.BuyCanteenModel;
import com.sgo.mayapada.Beans.CanteenMerchantModel;
import com.sgo.mayapada.Beans.CanteenPurchaseModel;
import com.sgo.mayapada.R;
import com.sgo.mayapada.activities.KantinActivity;
import com.sgo.mayapada.activities.MainPage;
import com.sgo.mayapada.adapter.EasyAdapter;
import com.sgo.mayapada.adapter.BuyCanteenAdapter;
import com.sgo.mayapada.coreclass.CustomSecurePref;
import com.sgo.mayapada.coreclass.DefineValue;
import com.sgo.mayapada.coreclass.ErrorDefinition;
import com.sgo.mayapada.coreclass.MyApiClient;
import com.sgo.mayapada.coreclass.WebParams;
import com.sgo.mayapada.dialogs.AlertDialogLogout;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import timber.log.Timber;

/**
 * Created by thinkpad on 3/31/2016.
 */
public class FragKantin extends Fragment {

    private SecurePreferences sp;
    private View v;
    private ProgressBar prgBelanja;
//    ProgressDialog out;
//    private PtrFrameLayout mPtrFrame;
//    View emptyLayout;
//    Animation frameAnimation;
//    ImageView spining_progress;
private ImageView image_empty_kantin;
    private ImageView image_empty_belanja;
//    MaterialRippleLayout btn_loadmore;
//    ViewGroup footerLayout;
//    Button btn_refresh;
private Button btnCheckKupon;
    private Button btnDaftarTerjual;
    private Button btnDaftarPembeli;
    private Button btnManajemenStok;
    private LinearLayout owner_layout;
    private ListView lvKantin;
    private ListView lvBelanja;
    private EasyAdapter adapterKantin;
    private BuyCanteenAdapter adapterBelanja;
    private ArrayList<BuyCanteenModel> listBuy;
    private ArrayList<CanteenMerchantModel> dataMerchant;
    private ArrayList<String> listKantin;
    private ArrayList<CanteenPurchaseModel> dataPurchase;
//    int page;
private String user_id;
    private String access_key;
    private String member_id;
    private int start = 0;

    public static FragKantin newInstance() {
        return new FragKantin();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        v = inflater.inflate(R.layout.frag_kantin, container, false);
        return v;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        sp = CustomSecurePref.getInstance().getmSecurePrefs();
        user_id = sp.getString(DefineValue.USERID_PHONE, "");
        access_key = sp.getString(DefineValue.ACCESS_KEY, "");
        Boolean is_merchant = sp.getBoolean(DefineValue.IS_MERCHANT,false);
        member_id = sp.getString(DefineValue.MEMBER_ID, "");

        listKantin = new ArrayList<>();
        listBuy = new ArrayList<>();
        dataMerchant = new ArrayList<>();
        dataPurchase = new ArrayList<>();

        image_empty_kantin = (ImageView) v.findViewById(R.id.image_empty_kantin);
        image_empty_belanja = (ImageView) v.findViewById(R.id.image_empty_belanja);
        lvKantin = (ListView) v.findViewById(R.id.lv_kantin);
        lvBelanja  = (ListView) v.findViewById(R.id.lv_belanja);
        prgBelanja = (ProgressBar) v.findViewById(R.id.prgBelanja);
//        footerLayout = (ViewGroup) getActivity().getLayoutInflater().inflate(R.layout.footer_loadmore, lvBelanja, false);
//        footerLayout.setLayoutParams(new ListView.LayoutParams(ListView.LayoutParams.MATCH_PARENT, ListView.LayoutParams.WRAP_CONTENT));
//        spining_progress = (ImageView) footerLayout.findViewById(R.id.image_spinning_wheel);
//        btn_loadmore = (MaterialRippleLayout) footerLayout.findViewById(R.id.btn_loadmore);
//        emptyLayout = v.findViewById(R.id.empty_layout);
//        emptyLayout.setVisibility(View.GONE);
//        btn_refresh = (Button) emptyLayout.findViewById(R.id.btnRefresh);
        owner_layout = (LinearLayout) v.findViewById(R.id.owner_layout);
        btnCheckKupon = (Button) v.findViewById(R.id.btn_check_kupon);
        btnDaftarTerjual = (Button) v.findViewById(R.id.btn_daftar_terjual);
        btnDaftarPembeli = (Button) v.findViewById(R.id.btn_daftar_pembeli);
        btnManajemenStok = (Button) v.findViewById(R.id.btn_manajemen_stok);
        if(is_merchant) {
            owner_layout.setVisibility(View.VISIBLE);
        }
        else
            owner_layout.setVisibility(View.GONE);

        btnCheckKupon.setOnClickListener(checkKuponListener);
        btnDaftarTerjual.setOnClickListener(daftarTerjualListener);
        btnDaftarPembeli.setOnClickListener(daftarPembeliListener);
        btnManajemenStok.setOnClickListener(manajemenStokListener);

//        frameAnimation = AnimationUtils.loadAnimation(getActivity(), R.anim.spinner_animation);
//        frameAnimation.setRepeatCount(Animation.INFINITE);
//
//        page = 1;
//        btn_loadmore.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
////                getDataBelanja(page, false, comm_id);
//            }
//        });
//
//        btn_refresh.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                mPtrFrame.autoRefresh();
//            }
//        });

        adapterKantin = new EasyAdapter(getActivity(),R.layout.list_kantin_item, listKantin);
        adapterBelanja = new BuyCanteenAdapter(getActivity(), listBuy);
        lvKantin.setAdapter(adapterKantin);
        lvBelanja.setAdapter(adapterBelanja);

//        setLoadMore(false);

//        mPtrFrame = (PtrFrameLayout) v.findViewById(R.id.rotate_header_list_view_frame);
//
//        final MaterialHeader header = new MaterialHeader(getActivity());
//        int[] colors = getResources().getIntArray(R.array.google_colors);
//        header.setColorSchemeColors(colors);
//        header.setLayoutParams(new PtrFrameLayout.LayoutParams(-1, -2));
//        header.setPadding(0, 15, 0, 10);
//        header.setPtrFrameLayout(mPtrFrame);
//
//        mPtrFrame.setHeaderView(header);
//        mPtrFrame.addPtrUIHandler(header);
//        mPtrFrame.setPtrHandler(new PtrHandler() {
//            @Override
//            public void onRefreshBegin(PtrFrameLayout frame) {
//                page = 1;
////                getDataBelanja(0, null);
//            }
//
//            @Override
//            public boolean checkCanDoRefresh(PtrFrameLayout frame, View content, View header) {
//                //return PtrDefaultHandler.checkContentCanBePulledDown(frame, content, header);
//                return canScrollUp();
//            }
//        });
//
//        mPtrFrame.postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                mPtrFrame.autoRefresh(false);
//            }
//        }, 50);

        lvKantin.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Intent mI = new Intent(getActivity(),KantinActivity.class);
                mI.putExtra(DefineValue.KANTIN, DefineValue.KANTIN_STUDENT_ORDER);
                mI.putExtra(DefineValue.KANTIN_ID, dataMerchant.get(position).getId());
                mI.putExtra(DefineValue.KANTIN_NAME, dataMerchant.get(position).getName());
                getActivity().startActivityForResult(mI, MainPage.REQUEST_FINISH);
            }
        });

        lvBelanja.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Intent mI = new Intent(getActivity(),KantinActivity.class);
                mI.putExtra(DefineValue.KANTIN, DefineValue.KANTIN_STUDENT_DETAIL);
                mI.putExtra(DefineValue.COUPON_CODE, listBuy.get(position).getCoupon());
                mI.putExtra(DefineValue.TX_DATETIME, dataPurchase.get(position).getTx_datetime());
                mI.putExtra(DefineValue.LIST_ORDER, dataPurchase.get(position).getItem());
                mI.putExtra(DefineValue.STATUS, dataPurchase.get(position).getStatus());
                mI.putExtra(DefineValue.KADALUARSA, dataPurchase.get(position).getKadaluarsa());
                getActivity().startActivityForResult(mI, MainPage.REQUEST_FINISH);
            }
        });
//        ToggleFAB(false);

        getDataKantin();
    }

    private Button.OnClickListener checkKuponListener = new Button.OnClickListener() {
        @Override
        public void onClick(View view) {
            Intent mI = new Intent(getActivity(),KantinActivity.class);
            mI.putExtra(DefineValue.KANTIN, DefineValue.KANTIN_OWNER_CEK_KUPON);
            getActivity().startActivityForResult(mI, MainPage.ACTIVITY_RESULT);
        }
    };

    private Button.OnClickListener daftarTerjualListener = new Button.OnClickListener() {
        @Override
        public void onClick(View view) {
            Intent mI = new Intent(getActivity(),KantinActivity.class);
            mI.putExtra(DefineValue.KANTIN, DefineValue.KANTIN_OWNER_DAFTAR_TERJUAL);
            getActivity().startActivityForResult(mI, MainPage.ACTIVITY_RESULT);
        }
    };

    private Button.OnClickListener daftarPembeliListener = new Button.OnClickListener() {
        @Override
        public void onClick(View view) {
            Intent mI = new Intent(getActivity(),KantinActivity.class);
            mI.putExtra(DefineValue.KANTIN, DefineValue.KANTIN_OWNER_DAFTAR_PEMBELI);
            getActivity().startActivityForResult(mI, MainPage.ACTIVITY_RESULT);
        }
    };

    private Button.OnClickListener manajemenStokListener = new Button.OnClickListener() {
        @Override
        public void onClick(View view) {
            Intent mI = new Intent(getActivity(),KantinActivity.class);
            mI.putExtra(DefineValue.KANTIN, DefineValue.KANTIN_OWNER_MANAJEMEN_STOK);
            getActivity().startActivityForResult(mI, MainPage.ACTIVITY_RESULT);
        }
    };

    private void getDataKantin() {
        try{
            RequestParams params = MyApiClient.getSignatureWithParams(MyApiClient.COMM_ID, MyApiClient.LINK_CANTEEN_MERCHANTS,
                    user_id, access_key);
            String subcategoryCode;
            if(sp.getBoolean(DefineValue.IS_MERCHANT,false))
                subcategoryCode = sp.getString(DefineValue.MERCHANT_SUBCATEGORY,"");
            else
                subcategoryCode = sp.getString(DefineValue.SUBCATEGORY_CODE,"");

            params.put(WebParams.SUBCATEGORY_CODE,subcategoryCode);
            params.put(WebParams.USER_ID, user_id);
            params.put(WebParams.COMM_ID, MyApiClient.COMM_ID);
            params.put(WebParams.MEMBER_ID, member_id);

            Timber.d("isi params data merchant:" + params.toString());

            MyApiClient.getCanteenMerchants(getActivity(), params, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    try {
                        String code = response.getString(WebParams.ERROR_CODE);
                        if (code.equals(WebParams.SUCCESS_CODE)) {
                            Timber.d("isi response data merchant:" + response.toString());

                            String data = response.getString(WebParams.MERCHANT);
                            if(!data.equals("") && !data.equals("null")) {
                                image_empty_kantin.setVisibility(View.GONE);
                                lvKantin.setVisibility(View.VISIBLE);
                                JSONArray arrayMerchant = new JSONArray(data);
                                for (int i = 0; i < arrayMerchant.length(); i++) {
                                    if(!member_id.equalsIgnoreCase(arrayMerchant.getJSONObject(i).getString(WebParams.MERCHANT_ID)))
                                    {
                                        CanteenMerchantModel merchant = new CanteenMerchantModel();
                                        merchant.setId(arrayMerchant.getJSONObject(i).getString(WebParams.MERCHANT_ID));
                                        merchant.setName(arrayMerchant.getJSONObject(i).getString(WebParams.MERCHANT_NAME));
                                        merchant.setCategory(arrayMerchant.getJSONObject(i).getString(WebParams.MERCHANT_CATEGORY));
                                        dataMerchant.add(merchant);
                                        listKantin.add(arrayMerchant.getJSONObject(i).getString(WebParams.MERCHANT_NAME));
                                    }
                                }
                                adapterKantin.notifyDataSetChanged();

                                if(listKantin.size() == 0) {
                                    image_empty_kantin.setVisibility(View.VISIBLE);
                                    lvKantin.setVisibility(View.GONE);
                                }
                            }
                            else {
                                image_empty_kantin.setVisibility(View.VISIBLE);
                                lvKantin.setVisibility(View.GONE);
                            }
                        } else if (code.equals(WebParams.LOGOUT_CODE)) {
                            Timber.d("isi response autologout:" + response.toString());
                            String message = response.getString(WebParams.ERROR_MESSAGE);
                            AlertDialogLogout test = AlertDialogLogout.getInstance();
                            test.showDialoginMain(getActivity(), message);
                        }
                        else if(code.equals(ErrorDefinition.NO_TRANSACTION)) {
                            image_empty_kantin.setVisibility(View.VISIBLE);
                            lvKantin.setVisibility(View.GONE);
                        }
                        else {
                            Timber.d("Error data merchant:" + response.toString());
                            code = response.getString(WebParams.ERROR_CODE) + ":" + response.getString(WebParams.ERROR_MESSAGE);
                            Toast.makeText(getActivity(), code, Toast.LENGTH_LONG).show();
                        }
                        getDataBelanja();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    super.onFailure(statusCode, headers, responseString, throwable);
                    failure(throwable);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    failure(throwable);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    failure(throwable);
                }

                private void failure(Throwable throwable) {
                    if (MyApiClient.PROD_FAILURE_FLAG)
                        Toast.makeText(getActivity(), getString(R.string.network_connection_failure_toast), Toast.LENGTH_SHORT).show();
                    else
                        Toast.makeText(getActivity(), throwable.toString(), Toast.LENGTH_SHORT).show();

                    Timber.w("Error Koneksi data merchant:" + throwable.toString());
                }
            });
        } catch (Exception e) {
            Timber.d("httpclient:" + e.getMessage());
        }
    }

    public void getDataBelanja() {
        try{
            image_empty_belanja.setVisibility(View.GONE);
            prgBelanja.setVisibility(View.VISIBLE);
            lvBelanja.setVisibility(View.GONE);

            if(listBuy.size() > 0) {
                listBuy.clear();
                dataPurchase.clear();
            }
            RequestParams params = MyApiClient.getSignatureWithParams(MyApiClient.COMM_ID, MyApiClient.LINK_PURCHASE_LIST,
                    user_id, access_key);
            params.put(WebParams.MEMBER_ID, sp.getString(DefineValue.MEMBER_ID,""));
            params.put(WebParams.USER_ID, user_id);
            params.put(WebParams.COMM_ID, MyApiClient.COMM_ID);

            Timber.d("isi params data purchase:" + params.toString());

            MyApiClient.getPurchaseList(getActivity(), params, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    try {
                        prgBelanja.setVisibility(View.GONE);
                        String code = response.getString(WebParams.ERROR_CODE);
                        if (code.equals(WebParams.SUCCESS_CODE)) {
                            Timber.d("isi response data purchase:" + response.toString());

                            String data = response.getString(WebParams.ORDER);
                            if(!data.equals("") && !data.equals("null")) {
                                image_empty_belanja.setVisibility(View.GONE);
                                lvBelanja.setVisibility(View.VISIBLE);
                                JSONArray array = new JSONArray(data);
                                for (int i = 0; i < array.length(); i++) {
                                    BuyCanteenModel buyCanteenModel = new BuyCanteenModel();
                                    buyCanteenModel.setCoupon(array.getJSONObject(i).getString(WebParams.KUPON));
                                    buyCanteenModel.setCanteen(array.getJSONObject(i).getString(WebParams.MERCHANT_NAME));
                                    listBuy.add(buyCanteenModel);
                                    CanteenPurchaseModel purchase = new CanteenPurchaseModel(array.getJSONObject(i).getString(WebParams.ORDER_ID),
                                            array.getJSONObject(i).getString(WebParams.MERCHANT_ID), array.getJSONObject(i).getString(WebParams.MERCHANT_NAME),
                                            array.getJSONObject(i).getString(WebParams.KUPON), array.getJSONObject(i).getString(WebParams.TX_DATETIME),
                                            array.getJSONObject(i).getString(WebParams.KADALUARSA), array.getJSONObject(i).getString(WebParams.CCY_ID),
                                            array.getJSONObject(i).getString(WebParams.AMOUNT), array.getJSONObject(i).getString(WebParams.STATUS),
                                            array.getJSONObject(i).getString(WebParams.ITEM));
                                    dataPurchase.add(purchase);
                                }
                                adapterBelanja.notifyDataSetChanged();
                            }
                            else {
                                image_empty_belanja.setVisibility(View.VISIBLE);
                                lvBelanja.setVisibility(View.GONE);
                            }
                        } else if (code.equals(WebParams.LOGOUT_CODE)) {
                            Timber.d("isi response autologout:" + response.toString());
                            String message = response.getString(WebParams.ERROR_MESSAGE);
                            AlertDialogLogout test = AlertDialogLogout.getInstance();
                            test.showDialoginMain(getActivity(), message);
                        }
                        else if(code.equals(ErrorDefinition.NO_TRANSACTION)) {
                            image_empty_belanja.setVisibility(View.VISIBLE);
                            lvBelanja.setVisibility(View.GONE);
                        }
                        else {
                            image_empty_belanja.setVisibility(View.VISIBLE);
                            Timber.d("Error data purchase:" + response.toString());
                            code = response.getString(WebParams.ERROR_CODE) + ":" + response.getString(WebParams.ERROR_MESSAGE);
                            Toast.makeText(getActivity(), code, Toast.LENGTH_LONG).show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    super.onFailure(statusCode, headers, responseString, throwable);
                    failure(throwable);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    failure(throwable);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    failure(throwable);
                }

                private void failure(Throwable throwable) {
                    prgBelanja.setVisibility(View.GONE);
                    image_empty_belanja.setVisibility(View.VISIBLE);
                    lvBelanja.setVisibility(View.GONE);


                    if (isVisible() && MyApiClient.PROD_FAILURE_FLAG)
                        Toast.makeText(getActivity(), getString(R.string.network_connection_failure_toast), Toast.LENGTH_SHORT).show();
                    else
                        Toast.makeText(getActivity(), throwable.toString(), Toast.LENGTH_SHORT).show();

                    Timber.w("Error Koneksi data purchase:" + throwable.toString());
                }
            });
        } catch (Exception e) {
            Timber.d("httpclient:" + e.getMessage());
        }
    }
//    public void getDataBelanja(int _page, final Boolean isRefresh) {
//        try{
//
//            if(isRefresh == null){
//                Timber.wtf("masuk ptr");
//            }
//            else if(isRefresh){
//                Timber.wtf("masuk refresh");
//                out = DefinedDialog.CreateProgressDialog(getActivity(), null);
//                out.show();
//                mPtrFrame.setEnabled(true);
//                mPtrFrame.setVisibility(View.VISIBLE);
//            }
//            else {
//                Timber.wtf("masuk load more");
//                btn_loadmore.setVisibility(View.GONE);
//                spining_progress.setVisibility(View.VISIBLE);
//                spining_progress.startAnimation(frameAnimation);
//            }
//
//            RequestParams params = MyApiClient.getSignatureWithParams(MyApiClient.COMM_ID, MyApiClient.LINK_INVOICE_TAGIHAN,
//                    user_id, access_key);
//            params.put(WebParams.USER_ID,user_id);
//            params.put(WebParams.COMM_ID, MyApiClient.COMM_ID);
//            params.put(WebParams.MEMBER_ID, _page);
//            params.put(WebParams.OFFSET, offset);
//
//            Timber.d("isi params get canteen catalogs:"+params.toString());
//
//            MyApiClient.getInvoiceTagihan(getActivity(), params, new JsonHttpResponseHandler() {
//                @Override
//                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
//
//                    try {
//                        if (isAdded()) {
//                            if (isRefresh != null) {
//                                if (isRefresh)
//                                    out.dismiss();
//                            }
//
//                            String code = response.getString(WebParams.ERROR_CODE);
//                            Timber.w("isi response get canteen catalogs:" + response.toString());
//
//                            if (code.equals(WebParams.SUCCESS_CODE)) {
//                                if (isRefresh == null) {
//                                    mPtrFrame.refreshComplete();
//                                    dataBuy.clear();
//                                } else if (isRefresh)
//                                    dataBuy.clear();
//                                else {
//                                    btn_loadmore.setVisibility(View.VISIBLE);
//                                    spining_progress.setVisibility(View.GONE);
//                                    spining_progress.setAnimation(null);
//                                }
//
//                                String invoice = response.getString(WebParams.INVOICE);
//                                if (!invoice.equals("")) {
//                                    JSONArray arrayData = new JSONArray(invoice);
//                                    JSONObject mObj;
//                                    for (int i = 0; i < arrayData.length(); i++) {
//                                        mObj = arrayData.getJSONObject(i);
//
//                                        BuyCanteenModel tagihanListModel = new BuyCanteenModel();
//                                        tagihanListModel.setDoc_id(mObj.optString(WebParams.DOC_ID, ""));
//                                        tagihanListModel.setDoc_no(mObj.optString(WebParams.DOC_NO, ""));
//                                        dataBuy.add(tagihanListModel);
//                                    }
//                                }
//                                else if(invoice.equals("") && page == 1) {
//                                    mPtrFrame.refreshComplete();
//                                    setLoadMore(false);
//                                    lvBelanja.setVisibility(View.GONE);
//                                    emptyLayout.setVisibility(View.VISIBLE);
//                                    dataBuy.clear();
//                                    adapterBelanja.notifyDataSetChanged();
//                                }
//
//                                int _page = response.optInt(WebParams.NEXT, 0);
//                                if (_page != 0) {
//                                    page++;
//                                    setLoadMore(false);
//                                    setLoadMore(true);
//                                } else {
//                                    setLoadMore(false);
//                                }
//                                adapterBelanja.notifyDataSetChanged();
//
//                                if (isRefresh == null || isRefresh) {
//                                    lvBelanja.setSelection(0);
//                                    lvBelanja.smoothScrollToPosition(0);
//                                    lvBelanja.setSelectionAfterHeaderView();
//                                }
//
//                            } else if (code.equals(WebParams.LOGOUT_CODE)) {
//                                Timber.d("isi response autologout:" + response.toString());
//                                String message = response.getString(WebParams.ERROR_MESSAGE);
//                                AlertDialogLogout test = AlertDialogLogout.getInstance();
//                                test.showDialoginActivity(getActivity(), message);
//                            }
//                            else {
//                                String message = response.getString(WebParams.ERROR_MESSAGE);
//                                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
//                                builder.setMessage(message)
//                                        .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
//                                            @Override
//                                            public void onClick(DialogInterface dialog, int which) {
//                                                dialog.dismiss();
//                                            }
//                                        });
//                                AlertDialog dialog = builder.create();
//                                dialog.show();
//                            }
//                        }
//
//                    } catch (JSONException e) {
//
//                        Toast.makeText(getActivity(), getString(R.string.internal_error), Toast.LENGTH_LONG).show();
//                        e.printStackTrace();
//                    }
//                }
//
//                @Override
//                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
//                    super.onFailure(statusCode, headers, responseString, throwable);
//                    failure(throwable);
//                }
//
//                @Override
//                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
//                    super.onFailure(statusCode, headers, throwable, errorResponse);
//                    failure(throwable);
//                }
//
//                @Override
//                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
//                    super.onFailure(statusCode, headers, throwable, errorResponse);
//                    failure(throwable);
//                }
//
//                private void failure(Throwable throwable) {
//                    if (MyApiClient.PROD_FAILURE_FLAG)
//                        Toast.makeText(getActivity(), getString(R.string.network_connection_failure_toast), Toast.LENGTH_SHORT).show();
//                    else
//                        Toast.makeText(getActivity(), throwable.toString(), Toast.LENGTH_SHORT).show();
//
//                    mPtrFrame.refreshComplete();
//                    setLoadMore(false);
//                    lvBelanja.setVisibility(View.GONE);
//                    emptyLayout.setVisibility(View.VISIBLE);
//                    dataBuy.clear();
//                    adapterBelanja.notifyDataSetChanged();
//
//                    Timber.w("Error Koneksi get canteen catalogs:" + throwable.toString());
//                }
//            });
//        } catch (Exception e) {
//            String err = (e.getMessage()==null)?"Connection failed":e.getMessage();
//            Timber.e("http err:" + err);
//        }
//    }

//    public boolean canScrollUp() {
//        return lvBelanja != null && (lvBelanja.getAdapter().getCount() == 0 || lvBelanja.getFirstVisiblePosition() == 0 && lvBelanja.getChildAt(0).getTop() == 0);
//    }
//
//    private void setLoadMore(boolean isLoading)
//    {
//        if (isLoading) {
//            lvBelanja.addFooterView(footerLayout,null,false);
//        }
//        else {
//            lvBelanja.removeFooterView(footerLayout);
//        }
//    }

    private void ToggleFAB(Boolean isShow){
        if (getActivity() == null)
            return;

        if(getActivity() instanceof MainPage) {
            MainPage fca = (MainPage) getActivity();
            if(isShow)
                fca.materialSheetFab.showFab();
            else
                fca.materialSheetFab.hideSheetThenFab();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if(start == 0)
            start++;
        else if(listBuy.size() > 0)
            getDataBelanja();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        MyApiClient.CancelRequestWS(getActivity(),true);
    }
}