package com.sgo.mayapada.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sgo.mayapada.R;
import com.sgo.mayapada.activities.InsertPIN;
import com.sgo.mayapada.activities.MainPage;
import com.sgo.mayapada.activities.PayStoreActivity;
import com.sgo.mayapada.coreclass.DefineValue;
import com.sgo.mayapada.dialogs.DefinedDialog;

public class PayStoreInput extends Fragment implements View.OnClickListener {

    private View v;
    private TextView tv_paystore_input_text;
    private ProgressDialog progdialog;
    private int mAttempt;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v = inflater.inflate(R.layout.fragment_pay_store_input, container, false);
        tv_paystore_input_text = (TextView) v.findViewById(R.id.paystore_input_message);
        tv_paystore_input_text.setText(getString(R.string.paystore_text_input,getString(R.string.paystore_btn_input), getString(R.string.appname)));
        v.findViewById(R.id.btn_create_voucher).setOnClickListener(this);
        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mAttempt = 3;
    }

    private void CallPINinput(int _attempt){
        Intent i = new Intent(getActivity(), InsertPIN.class);
        if(_attempt == 1)
            i.putExtra(DefineValue.ATTEMPT,_attempt);
        startActivityForResult(i, MainPage.REQUEST_FINISH);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == MainPage.REQUEST_FINISH){
            if(resultCode == InsertPIN.RESULT_PIN_VALUE) {
                if(progdialog == null)
                    progdialog = DefinedDialog.CreateProgressDialog(getActivity(), "");
                else
                    progdialog.show();

                Handler hand = new Handler();
                hand.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        progdialog.dismiss();
                        Intent i = new Intent(getActivity(), PayStoreActivity.class);
                        switchActivity(i);
                    }
                },2000);

            }
        }
    }

    private void switchActivity(Intent mIntent){
        if (getActivity() == null)
            return;

        MainPage fca = (MainPage) getActivity();
        fca.switchActivity(mIntent, MainPage.ACTIVITY_RESULT);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id){
            case R.id.btn_create_voucher:
                CallPINinput(mAttempt);
                break;
        }
    }
}
