package com.sgo.mayapada.fragments;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.securepreferences.SecurePreferences;
import com.sgo.mayapada.R;
import com.sgo.mayapada.activities.MainPage;
import com.sgo.mayapada.coreclass.CustomSecurePref;
import com.sgo.mayapada.coreclass.DefineValue;
import com.sgo.mayapada.coreclass.InetHandler;
import com.sgo.mayapada.coreclass.MyApiClient;
import com.sgo.mayapada.coreclass.WebParams;
import com.sgo.mayapada.dialogs.AlertDialogLogout;
import com.sgo.mayapada.dialogs.DefinedDialog;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import timber.log.Timber;

/**
 * Created by thinkpad on 8/14/2017.
 */

public class UpgradeUserConfirm extends Fragment {
    private View v;
    private TextView tvName, tvAddress, tvNoID, tvDOB, tvBirthMother, tvGender;
    private EditText etOTP;
    private ProgressDialog progdialog;
    private String userID, accessKey, cust_id_number, no_rek;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.frag_upgrade_user_confirm, container, false);
        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        Bundle bundle = getArguments();
        if(bundle != null) {
            SecurePreferences sp = CustomSecurePref.getInstance().getmSecurePrefs();
            userID = sp.getString(DefineValue.USERID_PHONE, "");
            accessKey = sp.getString(DefineValue.ACCESS_KEY, "");

            tvName = (TextView) v.findViewById(R.id.upgradeuser_name_value);
            tvAddress = (TextView) v.findViewById(R.id.upgradeuser_address_value);
            tvNoID = (TextView) v.findViewById(R.id.upgradeuser_no_id_value);
            tvDOB = (TextView) v.findViewById(R.id.upgradeuser_dob_value);
            tvBirthMother = (TextView) v.findViewById(R.id.upgradeuser_birth_mother_value);
            tvGender = (TextView) v.findViewById(R.id.upgradeuser_gender_value);
            etOTP = (EditText) v.findViewById(R.id.upgradeuser_token_value);
            Button btnUpgrade = (Button) v.findViewById(R.id.upgradeuser_btn_verification);
            Button btnCancel = (Button) v.findViewById(R.id.upgradeuser_btn_cancel);

            cust_id_number = bundle.getString(DefineValue.ID_NUMBER);
            no_rek = bundle.getString(DefineValue.ACCOUNT_NUMBER);
            tvName.setText(bundle.getString(DefineValue.CUST_NAME));
            tvAddress.setText(bundle.getString(DefineValue.PROFILE_ADDRESS));
            tvNoID.setText(bundle.getString(DefineValue.ID_NUMBER));
            tvDOB.setText(bundle.getString(DefineValue.PROFILE_BIRTH_PLACE) + ", " + bundle.getString(DefineValue.PROFILE_DOB));
            tvBirthMother.setText(bundle.getString(DefineValue.PROFILE_BOM));
            tvGender.setText(bundle.getString(DefineValue.PROFILE_GENDER));

            btnUpgrade.setOnClickListener(btnUpgradeListener);
            btnCancel.setOnClickListener(btnCancelListener);
        }
        else {
            getActivity().finish();
        }
    }

    private Button.OnClickListener btnUpgradeListener = new Button.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(InetHandler.isNetworkAvailable(getActivity())) {
                if(inputValidation())
                    confirmUpgrade();
            }
            else DefinedDialog.showErrorDialog(getActivity(), getString(R.string.inethandler_dialog_message));
        }
    };

    private Button.OnClickListener btnCancelListener = new Button.OnClickListener() {
        @Override
        public void onClick(View v) {
            getActivity().finish();
        }
    };

    private void confirmUpgrade(){
        try {
            progdialog = DefinedDialog.CreateProgressDialog(getActivity(), "");
            progdialog.show();

            RequestParams params = MyApiClient.getSignatureWithParams(MyApiClient.COMM_ID,MyApiClient.LINK_CONFIRM_UPGRADE,
                    userID,accessKey);
            params.put(WebParams.COMM_ID, MyApiClient.COMM_ID);
            params.put(WebParams.USER_ID, userID);
            params.put(WebParams.CUST_PHONE, userID);
            params.put(WebParams.CUST_ID_NUMBER, cust_id_number);
            params.put(WebParams.NO_REK, no_rek);
            params.put(WebParams.TOKEN_ID, etOTP.getText().toString());

            Timber.d("isi params confirm upgrade:" + params.toString());

            MyApiClient.sentConfirmUpgradeUser(getActivity(),params, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    progdialog.dismiss();

                    try {
                        String code = response.getString(WebParams.ERROR_CODE);
                        if (code.equals(WebParams.SUCCESS_CODE)) {
                            Timber.d("isi response confirm upgrade:"+response.toString());

                            SecurePreferences sp = CustomSecurePref.getInstance().getmSecurePrefs();
                            SecurePreferences.Editor mEdit = sp.edit();
                            mEdit.putInt(DefineValue.LEVEL_VALUE, 2);
                            mEdit.apply();

                            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                            builder.setMessage(getString(R.string.upgrade_user_success_message))
                                    .setCancelable(false)
                                    .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            getActivity().setResult(MainPage.RESULT_UPDATE_MENU_UPGRADE_USER);
                                            getActivity().finish();
                                        }
                                    });

                            AlertDialog dialogSuccess = builder.create();
                            dialogSuccess.show();
                        } else if (code.equals(WebParams.LOGOUT_CODE)) {
                            Timber.d("isi response autologout:"+response.toString());
                            String message = response.getString(WebParams.ERROR_MESSAGE);
                            AlertDialogLogout test = AlertDialogLogout.getInstance();
                            test.showDialoginMain(getActivity(), message);
                        } else {
                            Timber.d("isi error confirm upgrade:"+response.toString());
                            String code_msg = response.getString(WebParams.ERROR_MESSAGE);
                            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                            builder.setMessage(code_msg)
                                    .setPositiveButton(getString(R.string.dismiss), new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();
                                        }
                                    });
                            AlertDialog dialog = builder.create();
                            dialog.show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    super.onFailure(statusCode, headers, responseString, throwable);
                    failure(throwable);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    failure(throwable);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    failure(throwable);
                }

                private void failure(Throwable throwable){
                    if(MyApiClient.PROD_FAILURE_FLAG)
                        Toast.makeText(getActivity(), getString(R.string.network_connection_failure_toast), Toast.LENGTH_SHORT).show();
                    else
                        Toast.makeText(getActivity(), throwable.toString(), Toast.LENGTH_SHORT).show();

                    if(progdialog.isShowing())
                        progdialog.dismiss();

                    Timber.w("Error Koneksi confirm upgrade:"+throwable.toString());
                }
            });
        }catch (Exception e){
            Timber.d("httpclient:" + e.getMessage());
        }
    }

    private boolean inputValidation(){
        if(etOTP.getText().toString().length()==0){
            etOTP.requestFocus();
            etOTP.setError(this.getString(R.string.validation_otp));
            return false;
        }
        return true;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                getActivity().finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}