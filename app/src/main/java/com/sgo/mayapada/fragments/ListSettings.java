package com.sgo.mayapada.fragments;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.securepreferences.SecurePreferences;
import com.sgo.mayapada.R;
import com.sgo.mayapada.activities.AboutAppsActivity;
import com.sgo.mayapada.activities.ChangePIN;
import com.sgo.mayapada.activities.ChangePassword;
import com.sgo.mayapada.activities.MainPage;
import com.sgo.mayapada.activities.RegisterSMSBankingActivity;
import com.sgo.mayapada.activities.SharingOptionActivity;
import com.sgo.mayapada.adapter.EasyAdapter;
import com.sgo.mayapada.coreclass.CustomSecurePref;
import com.sgo.mayapada.coreclass.DefineValue;
import com.sgo.mayapada.coreclass.LevelClass;
import com.sgo.mayapada.dialogs.InformationDialog;

/**
 * Created by thinkpad on 6/11/2015.
 */
public class ListSettings extends ListFragment implements InformationDialog.OnDialogOkCallback {
    private View v;
    private SecurePreferences sp;
//    private InformationDialog dialogI;
    ProgressDialog progdialog;
    private String userID, accessKey, memberID;
    private Boolean agent;
//    static boolean successUpgrade = false;
//    private LevelClass levelClass;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        v = inflater.inflate(R.layout.frag_list_settings, container, false);
        return v;
    }

//    @Override
//    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
//        inflater.inflate(R.menu.information, menu);
//        super.onCreateOptionsMenu(menu, inflater);
//    }

//    @Override
//    public boolean onOptionsItemSelected(android.view.MenuItem item) {
//        switch(item.getItemId())
//        {
//            case R.id.action_information:
//                if(!dialogI.isAdded())
//                    dialogI.show(getActivity().getSupportFragmentManager(), InformationDialog.TAG);
//                return true;
//            default:
//                return super.onOptionsItemSelected(item);
//        }
//    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
//        successUpgrade = false;
        sp = CustomSecurePref.getInstance().getmSecurePrefs();
//        levelClass = new LevelClass(getActivity(),sp);
        userID = sp.getString(DefineValue.USERID_PHONE, "");
        accessKey = sp.getString(DefineValue.ACCESS_KEY,"");
        memberID = sp.getString(DefineValue.MEMBER_ID, "");
        agent = sp.getBoolean(DefineValue.IS_AGENT,false);
//        levelClass.refreshData();
//        dialogI = InformationDialog.newInstance(this,11);
        String[] _data;
        _data = getResources().getStringArray(R.array.settings_list_pin);

        EasyAdapter adapter = new EasyAdapter(getActivity(),R.layout.list_view_item_with_arrow, _data);

        ListView listView1 = (ListView) v.findViewById(android.R.id.list);
        listView1.setAdapter(adapter);
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        Intent i;
//        Fragment f;
//        Boolean masuk = agent;
//        if(!masuk)
//            masuk =!levelClass.isLevel1QAC();
        switch (position) {
//            case 0:
//                if(masuk){
//                    i = new Intent(getActivity(), RegisterSMSBankingActivity.class);
//                    switchActivity(i,MainPage.ACTIVITY_RESULT);
//                }else {
//                    levelClass.showDialogLevel();
//                }
//                break;
//            case 1:
//                i = new Intent(getActivity(), SharingOptionActivity.class);
//                switchActivity(i,MainPage.ACTIVITY_RESULT);
//                break;
            case 0:
                i = new Intent(getActivity(), ChangePassword.class);
                switchActivity(i,MainPage.ACTIVITY_RESULT);
                break;

            case 1:
                i = new Intent(getActivity(), ChangePIN.class);
                switchActivity(i,MainPage.ACTIVITY_RESULT);
                break;

//            case 4:
//                i = new Intent(getActivity(), AboutAppsActivity.class);
//                switchActivity(i,MainPage.ACTIVITY_RESULT);
//                break;
        }

    }

//    @Override
//    public void onResume() {
//        super.onResume();
//        if(successUpgrade) {
//            if(levelClass != null)
//                levelClass.refreshData();
//        }
//    }

    private void switchFragment(android.support.v4.app.Fragment i, String name, Boolean isBackstack){
        if (getActivity() == null)
            return;

        MainPage fca = (MainPage) getActivity();
        fca.switchContent(i,name,isBackstack);
    }

    private void switchActivity(Intent mIntent, int j){
        if (getActivity() == null)
            return;

        MainPage fca = (MainPage) getActivity();
        fca.switchActivity(mIntent,j);
    }

    @Override
    public void onOkButton() {

    }
}
