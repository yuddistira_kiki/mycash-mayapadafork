package com.sgo.mayapada.fragments;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.TextView;
import com.securepreferences.SecurePreferences;
import com.sgo.mayapada.R;
import com.sgo.mayapada.activities.BillerActivity;
import com.sgo.mayapada.activities.MainPage;
import com.sgo.mayapada.adapter.GridHome;
import com.sgo.mayapada.coreclass.BaseFragmentMainPage;
import com.sgo.mayapada.coreclass.CurrencyFormat;
import com.sgo.mayapada.coreclass.CustomSecurePref;
import com.sgo.mayapada.coreclass.DefineValue;
import com.sgo.mayapada.coreclass.LevelClass;
import com.sgo.mayapada.services.BalanceService;

import java.util.ArrayList;
import java.util.Collections;

import in.srain.cube.views.ptr.PtrFrameLayout;
import timber.log.Timber;

/**
 * Created by Lenovo Thinkpad on 5/10/2017.
 */
public class Home extends BaseFragmentMainPage {
    GridView GridHome;
    Button btn_beli;
    TextView tv_saldo;
    EditText input;
    TextView tv_pulsa;
    TextView tv_bpjs;
    TextView tv_listrikPLN;
    View view_pulsa;
    View view_bpjs;
    View view_listrikPLN;
    View v;
    private LevelClass levelClass;
    private SecurePreferences sp;
    ProgressDialog progdialog;

    public Home() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v = inflater.inflate(R.layout.frag_home, container, false);
        GridHome=(GridView)v.findViewById(R.id.grid);
        tv_saldo = (TextView)v.findViewById(R.id.tv_saldo);
        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        sp = CustomSecurePref.getInstance().getmSecurePrefs();
        levelClass = new LevelClass(getActivity(),sp);

        btn_beli = (Button) v.findViewById(R.id.btn_beli);
        input = (EditText) v.findViewById(R.id.input);
        tv_pulsa = (TextView) v.findViewById(R.id.tv_pulsa);
        tv_bpjs =(TextView) v.findViewById(R.id.tv_bpjs);
        tv_listrikPLN = (TextView) v.findViewById(R.id.tv_listrikPLN);
        view_pulsa = v.findViewById(R.id.view_pulsa);
        view_bpjs = v.findViewById(R.id.view_bpjs);
        view_listrikPLN = v.findViewById(R.id.view_listrikPLN);

        GridHome adapter = new GridHome(getActivity(), SetupListMenu(), SetupListMenuIcons());
        GridHome.setAdapter(adapter);

        btn_beli.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                if(view_pulsa.getVisibility()==View.VISIBLE)
                {
                    Intent intent = new Intent(getActivity(), BillerActivity.class);
                    intent.putExtra(DefineValue.BILLER_TYPE, "PLS");
                    intent.putExtra(DefineValue.BILLER_ID_NUMBER, input.getText().toString());
                    intent.putExtra(DefineValue.BILLER_NAME, "Pulsa");
                    startActivity(intent);
                }
                if(view_bpjs.getVisibility()==View.VISIBLE)
                {
                    Intent intent = new Intent(getActivity(), BillerActivity.class);
                    intent.putExtra(DefineValue.BILLER_TYPE, "BPJS");
                    intent.putExtra(DefineValue.BILLER_ID_NUMBER, input.getText().toString());
                    intent.putExtra(DefineValue.BILLER_NAME, "BPJS");
                    startActivity(intent);
                }
                if (view_listrikPLN.getVisibility()==View.VISIBLE)
                {
                    Intent intent = new Intent(getActivity(), BillerActivity.class);
                    intent.putExtra(DefineValue.BILLER_TYPE, "TKN");
                    intent.putExtra(DefineValue.BILLER_ID_NUMBER, input.getText().toString());
                    intent.putExtra(DefineValue.BILLER_NAME, "Voucher Token Listrik");
                    startActivity(intent);
                }
            }
        });
        tv_pulsa.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                tv_pulsa.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorPrimaryDark));
                tv_pulsa.setTypeface(null, Typeface.BOLD);
                tv_bpjs.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorSecondaryDark));
                tv_bpjs.setTypeface(null, Typeface.NORMAL);
                tv_listrikPLN.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorSecondaryDark));
                tv_listrikPLN.setTypeface(null, Typeface.NORMAL);
                input.setText("");
                view_pulsa.setVisibility(View.VISIBLE);
                view_bpjs.setVisibility(View.INVISIBLE);
                view_listrikPLN.setVisibility(View.INVISIBLE);
                input.setHint("Masukkan No. Hp");
            }
        });
        tv_bpjs.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                tv_bpjs.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorPrimaryDark));
                tv_bpjs.setTypeface(null, Typeface.BOLD);
                tv_pulsa.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorSecondaryDark));
                tv_pulsa.setTypeface(null, Typeface.NORMAL);
                tv_listrikPLN.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorSecondaryDark));
                tv_listrikPLN.setTypeface(null, Typeface.NORMAL);
                input.setText("");
                view_pulsa.setVisibility(View.INVISIBLE);
                view_bpjs.setVisibility(View.VISIBLE);
                view_listrikPLN.setVisibility(View.INVISIBLE);
                input.setHint("Masukkan No. BPJS");
            }
        });
        tv_listrikPLN.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                tv_listrikPLN.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorPrimaryDark));
                tv_listrikPLN.setTypeface(null, Typeface.BOLD);
                tv_pulsa.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorSecondaryDark));
                tv_pulsa.setTypeface(null, Typeface.NORMAL);
                tv_bpjs.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorSecondaryDark));
                tv_bpjs.setTypeface(null, Typeface.NORMAL);
                input.setText("");
                view_pulsa.setVisibility(View.INVISIBLE);
                view_bpjs.setVisibility(View.INVISIBLE);
                view_listrikPLN.setVisibility(View.VISIBLE);
                input.setHint("Masukkan No. Listrik");
            }
        });

        GridHome.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                String menuItemName = ((TextView) view.findViewById(R.id.grid_text)).getText().toString();

                if ( menuItemName.equals(getString(R.string.menu_item_title_topup)) ) {
                    switchMenu(NavigationDrawMenu.MTOPUP, null);
                } else if ( menuItemName.equals(getString(R.string.menu_item_title_pay_friends)) ) {
                    if (levelClass.isLevel1QAC()) {
                        levelClass.showDialogLevel();
                    } else switchMenu(NavigationDrawMenu.MPAYFRIENDS, null);
                } else if ( menuItemName.equals(getString(R.string.menu_item_title_ask_for_money)) ) {
                    if (levelClass.isLevel1QAC()) {
                        levelClass.showDialogLevel();
                    } else switchMenu(NavigationDrawMenu.MASK4MONEY, null);
                } else if ( menuItemName.equals(getString(R.string.menu_item_title_buy)) ) {
                    switchMenu(NavigationDrawMenu.MBUY, null);
                } else if ( menuItemName.equals(getString(R.string.menu_item_title_report)) ) {
                    switchMenu(NavigationDrawMenu.MREPORT, null);
                } else if ( menuItemName.equals(getString(R.string.menu_item_title_cash_out)) ) {
                    switchMenu(NavigationDrawMenu.MCASHOUT, null);
                }else if ( menuItemName.equals(getString(R.string.menu_item_title_split_bill)) ) {
                    switchMenu(NavigationDrawMenu.MSPLITBILL, null);
                }
            }

        });

        RefreshSaldo();
        if(levelClass != null)
            levelClass.refreshData();

        ToggleFAB(false);
    }

    private ArrayList<String> SetupListMenu(){
        String[] _data = getResources().getStringArray(R.array.list_menu_frag_home);
        ArrayList<String> data = new ArrayList<>() ;
        Collections.addAll(data,_data);
        return data;
    }

    private int[] SetupListMenuIcons(){
        TypedArray ta     = getResources().obtainTypedArray(R.array.list_menu_icon_frag_home);
        int totalIdx      = ta.length();
        int[] data        = new int[totalIdx];

        for( int j = 0; j < ta.length(); j++) {
            data[j] = ta.getResourceId(j, -1);
        }

        return data;
    }

    private void switchMenu(int idx_menu,Bundle data){
        if (getActivity() == null)
            return;

        MainPage fca = (MainPage) getActivity();
        fca.switchMenu(idx_menu, data);
    }

    private void RefreshSaldo(){
        String balance = sp.getString(DefineValue.BALANCE_AMOUNT,"0");
        tv_saldo.setText(CurrencyFormat.format(balance));
    }


    @Override
    protected int getInflateFragmentLayout() {
        return 0;
    }

    @Override
    public boolean checkCanDoRefresh() {
        return false;
    }

    @Override
    public void refresh(PtrFrameLayout frameLayout) {

    }

    @Override
    public void goToTop() {

    }

    private IntentFilter filter = new IntentFilter(BalanceService.INTENT_ACTION_BALANCE);
    private BroadcastReceiver receiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            Timber.d("receiver service balance");
            RefreshSaldo();
        }
    };
    @Override
    public void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(receiver, filter);
    }

    @Override
    public void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(receiver);
    }

    private void switchActivity(Intent mIntent, int j){
        if (getActivity() == null)
            return;

        MainPage fca = (MainPage) getActivity();
        fca.switchActivity(mIntent,j);
    }

    private void ToggleFAB(Boolean isShow){
        if (getActivity() == null)
            return;

        if(getActivity() instanceof MainPage) {
            MainPage fca = (MainPage) getActivity();
            if(isShow)
                fca.materialSheetFab.showFab();
            else
                fca.materialSheetFab.hideSheetThenFab();
        }
    }
}
