package com.sgo.mayapada.fragments;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.securepreferences.SecurePreferences;
import com.sgo.mayapada.R;
import com.sgo.mayapada.activities.KantinActivity;
import com.sgo.mayapada.activities.KantinScanCouponActivity;
import com.sgo.mayapada.activities.MainPage;
import com.sgo.mayapada.coreclass.CustomSecurePref;
import com.sgo.mayapada.coreclass.DefineValue;
import com.sgo.mayapada.coreclass.InetHandler;
import com.sgo.mayapada.coreclass.MyApiClient;
import com.sgo.mayapada.coreclass.WebParams;
import com.sgo.mayapada.dialogs.AlertDialogLogout;
import com.sgo.mayapada.dialogs.DefinedDialog;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import pub.devrel.easypermissions.EasyPermissions;
import timber.log.Timber;

/**
 * Created by thinkpad on 8/10/2016.
 */
public class FragKantinCheckKupon extends Fragment implements EasyPermissions.PermissionCallbacks {

    private final static int RC_CAMERA_WRITESTORAGE = 101;
    private SecurePreferences sp;
    private View v;
    private EditText etKupon;
    private Button btnCheck;
    private Button btnScan;
    private ProgressDialog out;
    private String user_id;
    private String access_key;
    private String member_id;
    private String merchant_subcategory;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        sp = CustomSecurePref.getInstance().getmSecurePrefs();
        user_id = sp.getString(DefineValue.USERID_PHONE, "");
        access_key = sp.getString(DefineValue.ACCESS_KEY, "");
        member_id = sp.getString(DefineValue.MEMBER_ID, "");
        merchant_subcategory = sp.getString(DefineValue.MERCHANT_SUBCATEGORY, "");

        etKupon = (EditText) v.findViewById(R.id.kode_kupon_value);
        btnCheck = (Button) v.findViewById(R.id.check_button);
        btnScan = (Button) v.findViewById(R.id.scan_button);

        btnCheck.setOnClickListener(checkKuponListener);
        btnScan.setOnClickListener(scanListener);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        v = inflater.inflate(R.layout.frag_kantin_check_kupon, container, false);
        return v;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
    }

    private Button.OnClickListener checkKuponListener = new Button.OnClickListener() {
        @Override
        public void onClick(View view) {
            if(InetHandler.isNetworkAvailable(getActivity())) {
                if (inputValidation()) {
                    sentCheckCouponCode();
                }
            }
            else DefinedDialog.showErrorDialog(getActivity(), getString(R.string.inethandler_dialog_message));
        }
    };

    private Button.OnClickListener scanListener = new Button.OnClickListener() {
        @Override
        public void onClick(View view) {
            chooseScan();
        }
    };

    private void sentCheckCouponCode() {
        try{
            out = DefinedDialog.CreateProgressDialog(getActivity(), null);
            out.show();

            RequestParams params = MyApiClient.getSignatureWithParams(MyApiClient.COMM_ID, MyApiClient.LINK_CHECK_COUPON_CODE,
                    user_id, access_key);
            params.put(WebParams.USER_ID,user_id);
            params.put(WebParams.COMM_ID, MyApiClient.COMM_ID);
            params.put(WebParams.MERCHANT_ID, member_id);
            params.put(WebParams.KUPON, etKupon.getText().toString());
            params.put(WebParams.MERCHANT_SUBCATEGORY, merchant_subcategory);

            Timber.d("isi params check coupon code:"+params.toString());

            MyApiClient.sentCheckCouponCode(getActivity(), params, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    try {
                        out.dismiss();

                        String code = response.getString(WebParams.ERROR_CODE);
                        Timber.w("isi response check coupon code:" + response.toString());

                        if (code.equals(WebParams.SUCCESS_CODE)) {
                            JSONObject obj = new JSONObject(response.getString(WebParams.ORDER));
                            Fragment newFrag = new FragKantinDetailBelanja();
                            Bundle args = new Bundle();
                            args.putBoolean(DefineValue.CONFIRM, true);
                            args.putString(DefineValue.LIST_ORDER, obj.getString(WebParams.ITEM));
                            args.putString(DefineValue.COUPON_CODE, obj.getString(WebParams.KUPON));
                            args.putString(DefineValue.MEMBER_NAME, obj.getString(WebParams.MEMBER_NAME));
                            args.putString(DefineValue.NO_HP_MEMBER, obj.getString(WebParams.NO_HP_MEMBER));
                            args.putString(DefineValue.AMOUNT, obj.getString(WebParams.AMOUNT));
                            args.putString(DefineValue.CCY_ID, obj.getString(WebParams.CCY_ID));
                            args.putString(DefineValue.TX_DATETIME, obj.getString(WebParams.TX_DATETIME));
                            args.putString(DefineValue.KADALUARSA, obj.getString(WebParams.KADALUARSA));
                            args.putString(DefineValue.STATUS, obj.getString(WebParams.STATUS));
                            newFrag.setArguments(args);
                            etKupon.setText("");
                            switchFragment(newFrag, "FragDetail", true);

                        } else if (code.equals(WebParams.LOGOUT_CODE)) {
                            Timber.d("isi response autologout:" + response.toString());
                            String message = response.getString(WebParams.ERROR_MESSAGE);
                            AlertDialogLogout test = AlertDialogLogout.getInstance();
                            test.showDialoginActivity(getActivity(), message);
                        }
                        else {
                            String message = response.getString(WebParams.ERROR_MESSAGE);
                            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                            builder.setMessage(message)
                                    .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();
                                        }
                                    });
                            AlertDialog dialog = builder.create();
                            dialog.show();
                        }

                    } catch (JSONException e) {
                        Toast.makeText(getActivity(), getString(R.string.internal_error), Toast.LENGTH_LONG).show();
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    super.onFailure(statusCode, headers, responseString, throwable);
                    failure(throwable);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    failure(throwable);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    failure(throwable);
                }

                private void failure(Throwable throwable) {
                    out.dismiss();
                    if (MyApiClient.PROD_FAILURE_FLAG)
                        Toast.makeText(getActivity(), getString(R.string.network_connection_failure_toast), Toast.LENGTH_SHORT).show();
                    else
                        Toast.makeText(getActivity(), throwable.toString(), Toast.LENGTH_SHORT).show();

                    Timber.w("Error Koneksi check coupon code:" + throwable.toString());
                }
            });
        } catch (Exception e) {
            String err = (e.getMessage()==null)?"Connection failed":e.getMessage();
            Timber.e("http err:" + err);
        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(requestCode,permissions,grantResults,this);
    }


    private void chooseScan() {
        String[] perms = {Manifest.permission.CAMERA,Manifest.permission.WRITE_EXTERNAL_STORAGE};
        if (EasyPermissions.hasPermissions(getActivity(),perms)) {
            Intent i = new Intent(getActivity(), KantinScanCouponActivity.class);
            switchActivity(i);
        }
        else {
            EasyPermissions.requestPermissions(this,
                    getString(R.string.rational_camera_write_external),
                    RC_CAMERA_WRITESTORAGE,
                    perms);
        }
    }

    public void setResultFromScanQR(String coupon) {
        etKupon.setText(coupon);
        sentCheckCouponCode();
    }

    private boolean inputValidation() {
        if (etKupon.getText().toString().length() == 0) {
            etKupon.requestFocus();
            etKupon.setError(getString(R.string.coupon_code_validation));
            return false;
        }
        return true;
    }

    private void switchFragment(android.support.v4.app.Fragment i, String name, Boolean isBackstack){
        if (getActivity() == null)
            return;

        KantinActivity fca = (KantinActivity ) getActivity();
        fca.switchContent(FragKantinCheckKupon.this, i,name,isBackstack);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if(getActivity().getSupportFragmentManager().getBackStackEntryCount() > 0)
                    getActivity().getSupportFragmentManager().popBackStack();
                else
                    getActivity().finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume() {
        super.onResume();
        setTitle(getString(R.string.kantin_check_coupon));
    }

    private void setTitle(String _title){
        if (getActivity() == null)
            return;

        KantinActivity fca = (KantinActivity) getActivity();
        fca.setTitleFragment(_title);
    }

    private void switchActivity(Intent mIntent){
        if (getActivity() == null)
            return;

        KantinActivity fca = (KantinActivity) getActivity();
        fca.switchActivity(mIntent,MainPage.ACTIVITY_RESULT);
    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {
        switch (requestCode){
            case RC_CAMERA_WRITESTORAGE :
                chooseScan();
                break;
        }
    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {
        switch (requestCode){
            case RC_CAMERA_WRITESTORAGE :
                Toast.makeText(getActivity(), getString(R.string.cancel_permission_read_contacts), Toast.LENGTH_SHORT).show();
                break;
        }
    }
}
