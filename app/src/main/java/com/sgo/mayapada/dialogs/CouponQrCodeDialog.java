package com.sgo.mayapada.dialogs;

import android.graphics.Bitmap;
import android.graphics.Point;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.sgo.mayapada.R;
import com.sgo.mayapada.coreclass.Contents;
import com.sgo.mayapada.coreclass.DefineValue;
import com.sgo.mayapada.coreclass.QRCodeEncoder;

import org.apache.commons.codec.binary.Hex;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

import static android.content.Context.WINDOW_SERVICE;

/**
 * Created by thinkpad on 9/30/2016.
 */
public class CouponQrCodeDialog extends DialogFragment {

    public final static String TAG = "com.sgo.mayapada.dialogs.CouponQrCodeDialog";
    private ImageView qrcode;
    private TextView btnOk;
    private TextView tvCouponCode;
    private String coupon;

    public static CouponQrCodeDialog CreateInstance(String _coupon){
        CouponQrCodeDialog dialog = new CouponQrCodeDialog();
        Bundle args = new Bundle();
        args.putString(DefineValue.COUPON_CODE, _coupon);
        dialog.setArguments(args);
        return dialog;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_coupon_qrcode, container);

        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        Bundle bundle = getArguments();
        if(bundle != null) {
            coupon = bundle.getString(DefineValue.COUPON_CODE);
        }

        qrcode = (ImageView) view.findViewById(R.id.img_qrcode);
        tvCouponCode = (TextView) view.findViewById(R.id.txt_coupon_code);
        btnOk = (TextView) view.findViewById(R.id.btnOK);

        tvCouponCode.setText(coupon);

//        Picasso mPic;
//        if(MyApiClient.PROD_FLAG_ADDRESS)
//            mPic = MyPicasso.getImageLoader(getActivity());
//        else
//            mPic= Picasso.with(getActivity());

//        String _url = "http://116.90.162.173:59088/static/img/qrcode/qrcouponTRX28YFUV3937696.png?uid=7806963334";
//        if(_url != null && _url.isEmpty()){
//            mPic.load(R.drawable.ic_data_not_found)
//                    .fit()
//                    .placeholder(R.anim.progress_animation)
//                    .into(qrcode);
//        }
//        else {
//            mPic.load(_url)
//                    .fit()
//                    .placeholder(R.anim.progress_animation)
//                    .into(qrcode);
//        }

        generateQRCode();

        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        return view;
    }

    private void generateQRCode() {
        String qrInputText = coupon;

        final Cipher decryptCipher;

        String keyPassEncrypt = "scanCoupon";

        final Cipher encryptCipher;
        try {
            encryptCipher = Cipher.getInstance("AES");
            encryptCipher.init(Cipher.ENCRYPT_MODE, generateMySQLAESKey(keyPassEncrypt, "UTF-8"));
            String encryptPass = new String(Hex.encodeHex(encryptCipher.doFinal(qrInputText.getBytes("UTF-8"))));
            qrInputText = encryptPass;
//            Log.d("denny_qr", qrInputText);

            decryptCipher = Cipher.getInstance("AES");
            decryptCipher.init(Cipher.DECRYPT_MODE, generateMySQLAESKey(keyPassEncrypt, "UTF-8"));
//            String _result = new String(decryptCipher.doFinal(Hex.decodeHex(qrInputText.toCharArray())));
//            Log.d("denny_qr", _result);


        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
//        catch (DecoderException e) {
//            e.printStackTrace();
//        }


        //Find screen size
        WindowManager manager = (WindowManager) getActivity().getSystemService(WINDOW_SERVICE);
        Display display = manager.getDefaultDisplay();
        Point point = new Point();
        display.getSize(point);
        int width = point.x;
        int height = point.y;
        int smallerDimension = width < height ? width : height;
        smallerDimension = smallerDimension * 3 / 4;


        Bundle bundle = new Bundle();

        //Encode with a QR Code image
        QRCodeEncoder qrCodeEncoder = new QRCodeEncoder(qrInputText,
                bundle,
                Contents.Type.TEXT,
                BarcodeFormat.QR_CODE.toString(),
                smallerDimension);
        try {
            Bitmap bitmap = qrCodeEncoder.encodeAsBitmap();
            qrcode.setImageBitmap(bitmap);

        } catch (WriterException e) {
            e.printStackTrace();
        }
    }


    private static SecretKeySpec generateMySQLAESKey(final String key, final String encoding) {
        try {
            final byte[] finalKey = new byte[16];
            int i = 0;
            for(byte b : key.getBytes(encoding))
                finalKey[i++%16] ^= b;
            return new SecretKeySpec(finalKey, "AES");
        } catch(UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }
}
