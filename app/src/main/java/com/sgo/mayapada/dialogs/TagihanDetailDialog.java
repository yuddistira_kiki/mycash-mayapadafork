package com.sgo.mayapada.dialogs;

import android.app.Activity;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sgo.mayapada.R;
import com.sgo.mayapada.coreclass.CurrencyFormat;
import com.sgo.mayapada.coreclass.DefineValue;

/**
 * Created by thinkpad on 4/4/2016.
 */
public class TagihanDetailDialog extends DialogFragment implements View.OnClickListener {

    public static final String TAG = "Tagihan Detail Dialog";

    private View v;
    private LinearLayout layout_input_amount;
    private TextView txt_id_tagihan;
    private TextView txt_no_tagihan;
    private TextView txt_nama_tagihan;
    private TextView txt_desc;
    private TextView txt_cust_name;
    private TextView txt_jumlah_tagihan;
    private TextView txt_sisa_tagihan;
    private EditText et_input_amount;

    private Activity mContext;
    private OnDialogOkCallback callback;
    private Boolean isActivty = false;
    private int position;
    private String inputvalue, doc_id, doc_no, doc_name, desc, doc_amount, remain_amount, ccy, partial_payment, cust_name;

    public interface OnDialogOkCallback {
        void onOkButton(String input_amount, int position);
    }

    public static TagihanDetailDialog newInstance(Activity _context, int position, String inputvalue) {
        TagihanDetailDialog f = new TagihanDetailDialog();
        f.mContext = _context;
        f.position = position;
        f.inputvalue = inputvalue;
        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        try {
            if(isActivty)
                callback = (OnDialogOkCallback) getActivity();
            else
                callback = (OnDialogOkCallback) getTargetFragment();
        } catch (ClassCastException e) {
            throw new ClassCastException("Calling fragment must implement DialogClickListener interface");
        }
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);
        this.dismiss();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.sekolahku_tagihan_detail_dialog, container);

        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);

        return v;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        Bundle bundle = getArguments();
        if(bundle != null) {
            doc_id = bundle.getString(DefineValue.DOC_ID);
            doc_no = bundle.getString(DefineValue.DOC_NO);
            doc_name = bundle.getString(DefineValue.DOC_NAME);
            desc = bundle.getString(DefineValue.DESCRIPTION);
            doc_amount = bundle.getString(DefineValue.DOC_AMOUNT);
            remain_amount = bundle.getString(DefineValue.REMAIN_AMOUNT);
            ccy = bundle.getString(DefineValue.CCY_ID);
            partial_payment = bundle.getString(DefineValue.PARTIAL_PAYMENT);
            cust_name = bundle.getString(DefineValue.CUST_NAME);
        }

        layout_input_amount = (LinearLayout) v.findViewById(R.id.tagihandetail_layout_input_amount);
        txt_id_tagihan = (TextView) v.findViewById(R.id.tagihandetail_idtagihan_value);
        txt_no_tagihan = (TextView) v.findViewById(R.id.tagihandetail_notagihan_value);
        txt_nama_tagihan = (TextView) v.findViewById(R.id.tagihandetail_namatagihan_value);
        txt_desc = (TextView) v.findViewById(R.id.tagihandetail_desc_value);
        txt_cust_name = (TextView) v.findViewById(R.id.tagihandetail_ccy_value);
        txt_jumlah_tagihan = (TextView) v.findViewById(R.id.tagihandetail_jumlah_tagihan_value);
        txt_sisa_tagihan = (TextView) v.findViewById(R.id.tagihandetail_sisa_tagihan_value);
        et_input_amount = (EditText) v.findViewById(R.id.tagihandetail_input_amount_value);
        Button btn_back = (Button) v.findViewById(R.id.tagihandetail_back_button);

        txt_id_tagihan.setText(doc_id);
        txt_no_tagihan.setText(doc_no);
        txt_nama_tagihan.setText(doc_name);
        if(desc.equals(null) || desc.equals("") || desc.equals("null"))
            txt_desc.setText("-");
        else
            txt_desc.setText(desc);
        txt_cust_name.setText(cust_name);
        if(ccy.equalsIgnoreCase("IDR")) {
            txt_jumlah_tagihan.setText("Rp. " + CurrencyFormat.format(doc_amount));
            txt_sisa_tagihan.setText("Rp. " + CurrencyFormat.format(remain_amount));
        }
        else {
            txt_jumlah_tagihan.setText(doc_amount);
            txt_sisa_tagihan.setText(remain_amount);
        }

        if(partial_payment.equalsIgnoreCase("Y"))
            layout_input_amount.setVisibility(View.VISIBLE);
        else if(partial_payment.equalsIgnoreCase("N"))
            layout_input_amount.setVisibility(View.GONE);

        if(inputvalue.equals("0"))
            et_input_amount.setText("");
        else
            et_input_amount.setText(inputvalue);

        btn_back.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if(partial_payment.equalsIgnoreCase("Y")) {
            String input_amount = et_input_amount.getText().toString();
            if (input_amount.equals("") || Long.parseLong(input_amount) == 0) {
                this.dismiss();
                callback.onOkButton("0", position);
            } else if (inputValidation()) {
                this.dismiss();
                callback.onOkButton(input_amount, position);
            }
        }
        else if(partial_payment.equalsIgnoreCase("N"))
            this.dismiss();
    }

    private boolean inputValidation(){
        long input_amount = Long.parseLong(et_input_amount.getText().toString());
        long total = Long.parseLong(remain_amount);

        if(input_amount > total){
            et_input_amount.requestFocus();
            et_input_amount.setError(this.getString(R.string.jumlah_yang_dibayar_error));
            return false;
        }
        return true;
    }

}
