package com.sgo.mayapada.dialogs;

import android.app.Activity;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ListView;

import com.sgo.mayapada.Beans.ResultTagihanModel;
import com.sgo.mayapada.R;
import com.sgo.mayapada.adapter.PreviewTagihanAdapter;
import com.sgo.mayapada.coreclass.DefineValue;
import com.sgo.mayapada.coreclass.ListViewHeight;
import com.sgo.mayapada.coreclass.WebParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by thinkpad on 5/24/2016.
 */
public class PreviewDetailTagihanDialog extends DialogFragment implements View.OnClickListener {

    public static final String TAG = "Tagihan Detail Dialog";

    private View v;
    private ListView lvTagihan;
    private PreviewTagihanAdapter mAdapter;

    private ArrayList<ResultTagihanModel> listResult;
    private ArrayList<String> listDocId;
    private Activity mContext;
    private String invoices, results, ccy;

    @Override
    public void onClick(View v) {
        this.dismiss();
    }

    public static PreviewDetailTagihanDialog newInstance(Activity _context) {
        PreviewDetailTagihanDialog f = new PreviewDetailTagihanDialog();
        f.mContext = _context;
        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);
        this.dismiss();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.dialog_preview_detil_tagihan, container);

        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        getDialog().setCancelable(true);

        return v;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        listResult = new ArrayList<>();
        listDocId = new ArrayList<>();

        Bundle bundle = getArguments();
        if(bundle != null) {
            invoices = bundle.getString(DefineValue.INVOICES, "");
            results = bundle.getString(DefineValue.RESULTS, "");
            ccy = bundle.getString(DefineValue.CCY_ID, "");

            try {
                if (!results.equals("")) {
                    JSONArray mArrayData = new JSONArray(invoices);

                    for(int i=0 ; i<mArrayData.length() ; i++) {
                        listDocId.add(mArrayData.getJSONObject(i).getString(WebParams.DOC_ID));
                    }

                    JSONObject obj;
                    obj = new JSONObject(results);
                    for (int i = 0; i < obj.length(); i++) {
                        JSONObject doc = new JSONObject(obj.getString(listDocId.get(i)));
                        ResultTagihanModel resultTagihanModel = new ResultTagihanModel(doc.getString(WebParams.DOC_ID),
                                doc.getString(WebParams.DOC_NAME), ccy, doc.getString(WebParams.PAYMENT_AMOUNT),
                                doc.getString(WebParams.STATUS), doc.getString(WebParams.REMARK));
                        listResult.add(resultTagihanModel);
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        lvTagihan = (ListView) v.findViewById(R.id.list_tagihan);
        Button btn_ok = (Button) v.findViewById(R.id.btn_ok);
        mAdapter = new PreviewTagihanAdapter(getActivity(), listResult);
        lvTagihan.setAdapter(mAdapter);
        ListViewHeight.setTotalHeightofListView(lvTagihan);

        btn_ok.setOnClickListener(this);
    }

}
