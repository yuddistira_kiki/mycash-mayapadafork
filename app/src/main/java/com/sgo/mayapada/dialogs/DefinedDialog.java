package com.sgo.mayapada.dialogs;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.sgo.mayapada.R;

import timber.log.Timber;

public class DefinedDialog {

    public interface DialogButtonListener{
        void onClickButton(View v, boolean isLongClick);
    }

    public static ProgressDialog CreateProgressDialog(Context context, String message) {
        ProgressDialog dialog = new ProgressDialog(context);
        try {
            dialog.show();
        } catch (WindowManager.BadTokenException e) {
            Timber.w("define dialog error:" + e.getMessage());
        }
        dialog.setContentView(R.layout.dialog_progress);
        TextView text1 = (TextView) dialog.findViewById(R.id.progressText1);
        text1.setText(message);
        dialog.setIndeterminate(true);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        return dialog;
    }

    public static void showErrorDialog(Context context, String message) {
        showErrorDialog(context,message,null);
    }

    public static void showErrorDialog(Context context, String message, View.OnClickListener onClickListener) {
        // Create custom dialog object
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCanceledOnTouchOutside(false);
        // Include dialog.xml file
        dialog.setContentView(R.layout.dialog_error);

        // set values for custom dialog components - text, image and button
        Button btnDialogOTP = (Button)dialog.findViewById(R.id.btn_dialog_error_ok);
        TextView Message = (TextView)dialog.findViewById(R.id.message_dialog_error);

        Message.setText(message);
        if(onClickListener == null) {
            btnDialogOTP.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });
        }
        else
            btnDialogOTP.setOnClickListener(onClickListener);
        dialog.show();
    }


    public static Dialog MessageDialog(Context context, String _title, String _message, final DialogButtonListener _dialogListener ){
        return MessageDialog(context,_title,_message,"",_dialogListener);
    }

    public static Dialog MessageDialog(Context context, String _title, String _message,String _btn_text,
                                       final DialogButtonListener _dialogListener){
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        // Include dialog.xml file
        dialog.setContentView(R.layout.dialog_notification);

        // set values for custom dialog components - text, image and button
        Button btnDialogOTP = (Button)dialog.findViewById(R.id.btn_dialog_notification_ok);
        TextView Title = (TextView)dialog.findViewById(R.id.title_dialog);
        TextView Message = (TextView)dialog.findViewById(R.id.message_dialog);

        Message.setVisibility(View.VISIBLE);
        Title.setText(_title);
        Message.setText(_message);

        if(!_btn_text.isEmpty())
            btnDialogOTP.setText(_btn_text);

        btnDialogOTP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _dialogListener.onClickButton(v,false);
                dialog.dismiss();
            }
        });
//        dialog.show();
        return dialog;
    }

    public static Dialog MessageP2P(Context context){
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_p2p_color_help);
        // Include dialog.xml file
        dialog.findViewById(R.id.btn_dialog_ok).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        return dialog;
    }


    public static AlertDialog BuildAlertDialog(Context context, String title, String msg, int icon, Boolean isCancelable,
                                               String okbtn, DialogInterface.OnClickListener ok){
        AlertDialog.Builder builder = new AlertDialog.Builder(context)
                .setMessage(msg)
                .setCancelable(isCancelable)
                .setPositiveButton(okbtn, ok);

        if(title != null)
            builder.setTitle(title);

        if(icon != 0)
            builder.setIcon(icon);
        return builder.create();
    }

}