package com.sgo.mayapada.dialogs;/*
  Created by Administrator on 4/19/2016.
 */

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import com.sgo.mayapada.R;
import com.squareup.picasso.Picasso;
import uk.co.senab.photoview.PhotoView;

public class ViewImageDialog extends DialogFragment {

    public final static String TAG = "com.sgo.mayapada.dialogs.ViewImageDialog";

    private TextView title;
    private ImageButton back;
    private String name;
    private String url;

    public static ViewImageDialog CreateInstance(String _name, String _url){
        ViewImageDialog Vid = new ViewImageDialog();
        Vid.setName(_name);
        Vid.setUrl(_url);
        return Vid;
    }

    public ViewImageDialog() {}

    @NonNull
    @Override
    public Dialog onCreateDialog(final Bundle savedInstanceState) {

        final Dialog dialog = new Dialog(getActivity(), android.R.style.Theme_Black_NoTitleBar);
        dialog.setContentView(R.layout.dialogimage);

        title = (TextView) dialog.findViewById(R.id.dialog_image_title);
        back = (ImageButton) dialog.findViewById(R.id.imageicon);

        title.setText(getName());

        PhotoView photoview = (PhotoView) dialog.findViewById(R.id.dialog_image);
            Picasso.with(getActivity()).
                    load(getUrl()).
                    fit().
                    centerInside().
                    into(photoview);


        back.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        getDialog().dismiss();
      }
    });

        return dialog;
    }

    private String getName() {
        return name;
    }

    private void setName(String name) {
        this.name = name;
    }

    private String getUrl() {
        return url;
    }

    private void setUrl(String url) {
        this.url = url;
    }
}