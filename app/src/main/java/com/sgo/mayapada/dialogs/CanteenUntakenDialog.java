package com.sgo.mayapada.dialogs;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ListView;

import com.securepreferences.SecurePreferences;
import com.sgo.mayapada.Beans.KantinUntakenModel;
import com.sgo.mayapada.R;
import com.sgo.mayapada.adapter.KantinUntakenAdapter;
import com.sgo.mayapada.coreclass.CustomSecurePref;
import com.sgo.mayapada.coreclass.DefineValue;

import java.util.ArrayList;

/**
 * Created by thinkpad on 10/3/2016.
 */

public class CanteenUntakenDialog extends DialogFragment {
    public final static String TAG = "com.sgo.mayapada.dialogs.CanteenUntakenDialog";
    private SecurePreferences sp;
    private View view;
    private ListView lvUntaken;
    private Button btnClose;
    private String item_code;
    private ArrayList<KantinUntakenModel> listUntaken;
    private KantinUntakenAdapter adapter;
    private String user_id;
    private String access_key;

    public static CanteenUntakenDialog CreateInstance(String _item_code, ArrayList<KantinUntakenModel> _listUntaken){
        CanteenUntakenDialog dialog = new CanteenUntakenDialog();
        Bundle args = new Bundle();
        args.putString(DefineValue.ITEM_ID, _item_code);
        args.putParcelableArrayList(DefineValue.LIST_UNTAKEN, _listUntaken);
        dialog.setArguments(args);
        return dialog;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.dialog_canteen_untaken, container);

        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        sp = CustomSecurePref.getInstance().getmSecurePrefs();
        user_id = sp.getString(DefineValue.USERID_PHONE, "");
        access_key = sp.getString(DefineValue.ACCESS_KEY, "");

        Bundle bundle = getArguments();
        if(bundle != null) {
            item_code = bundle.getString(DefineValue.ITEM_ID);
            listUntaken = bundle.getParcelableArrayList(DefineValue.LIST_UNTAKEN);
        }

        lvUntaken = (ListView) view.findViewById(R.id.lv_untaken);
        btnClose = (Button) view.findViewById(R.id.btn_close);

        adapter = new KantinUntakenAdapter(getActivity(), listUntaken, R.layout.list_kantin_untaken);
        lvUntaken.setAdapter(adapter);

        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        getDialog().getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    }
}
