package com.sgo.mayapada.dialogs;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ListView;

import com.securepreferences.SecurePreferences;
import com.sgo.mayapada.Beans.KantinStudentOrderModel;
import com.sgo.mayapada.R;
import com.sgo.mayapada.adapter.KantinConfirmOrderAdapter;
import com.sgo.mayapada.coreclass.CustomSecurePref;
import com.sgo.mayapada.coreclass.DefineValue;

import java.util.ArrayList;

/**
 * Created by thinkpad on 10/12/2016.
 */

public class CanteenConfirmOrderDialog extends DialogFragment implements View.OnClickListener{
    public final static String TAG = "com.sgo.mayapada.dialogs.CanteenConfirmOrderDialog";
    private SecurePreferences sp;
    private View view;
    private ListView lvOrder;
    private Button btnCancel;
    private Button btnConfirm;
    private ArrayList<KantinStudentOrderModel> listMenu;
    private KantinConfirmOrderAdapter adapter;
    private String user_id;
    private String access_key;

    private Activity mContext;
    private OnDialogConfirmCallback callback;
    private Boolean isActivty = false;

    @Override
    public void onClick(View v) {
        dismiss();
        callback.onConfirmButton();
    }

    public interface OnDialogConfirmCallback {
        void onConfirmButton();
        void onCancelButton();
    }

    public static CanteenConfirmOrderDialog CreateInstance(ArrayList<KantinStudentOrderModel> _listMenu){
        CanteenConfirmOrderDialog dialog = new CanteenConfirmOrderDialog();
        Bundle args = new Bundle();
        args.putParcelableArrayList(DefineValue.LIST_ORDER, _listMenu);
        dialog.setArguments(args);
        return dialog;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        try {
            if(isActivty)
                callback = (OnDialogConfirmCallback) getActivity();
            else
                callback = (OnDialogConfirmCallback) getTargetFragment();
        } catch (ClassCastException e) {
            throw new ClassCastException("Calling fragment must implement DialogClickListener interface");
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.dialog_canteen_confirm_order, container);

        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        sp = CustomSecurePref.getInstance().getmSecurePrefs();
        user_id = sp.getString(DefineValue.USERID_PHONE, "");
        access_key = sp.getString(DefineValue.ACCESS_KEY, "");

        Bundle bundle = getArguments();
        if(bundle != null) {
            listMenu = bundle.getParcelableArrayList(DefineValue.LIST_ORDER);
        }

        lvOrder = (ListView) view.findViewById(R.id.lv_order);
        btnCancel = (Button) view.findViewById(R.id.btn_cancel);
        btnConfirm = (Button) view.findViewById(R.id.btn_confirm);

        ArrayList<KantinStudentOrderModel> listOrder = new ArrayList<>();
        for(int i = 0 ; i < listMenu.size() ; i++) {
            if (!listMenu.get(i).getCount().equals("") && Integer.parseInt(listMenu.get(i).getCount()) > 0) {
                listOrder.add(listMenu.get(i));
            }
        }
        adapter = new KantinConfirmOrderAdapter(getActivity(), listOrder);
        lvOrder.setAdapter(adapter);

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                callback.onCancelButton();
            }
        });

        btnConfirm.setOnClickListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        getDialog().getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    }
}
