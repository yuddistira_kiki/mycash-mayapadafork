package com.sgo.mayapada.coreclass;

import android.app.Application;
import android.content.Context;
import android.database.ContentObserver;
import android.os.Handler;
import android.provider.ContactsContract;

/**
 * Created by yuddistirakiki on 1/5/17.
 */

class ContactsSync {

    private static ContactsSync self;
    private Context mContext;
    private DeContentObserver contentObserver = new DeContentObserver(new Handler());

    private ContactsSync(Application application){
        this.mContext = application;
    }


    public void initialize(Application application){
        self = new ContactsSync(application);
        application.getContentResolver().registerContentObserver(ContactsContract.Contacts.CONTENT_URI, true, self.contentObserver);

    }

    class DeContentObserver extends ContentObserver{

        /**
         * Creates a content observer.
         *
         * @param handler The handler to run {@link #onChange} on, or null if none.
         */
        public DeContentObserver(Handler handler) {
            super(handler);
        }

        @Override
        public void onChange(boolean selfChange) {
            super.onChange(selfChange);
        }
    }

    /*
    private void readPhonebook() {
        ContentResolver localContentResolver = this.mContext.getContentResolver();
        String select = "((" + ContactsContract.Contacts.DISPLAY_NAME + " NOTNULL) AND ("
                + ContactsContract.Contacts.HAS_PHONE_NUMBER + "=1) AND ("
                + ContactsContract.Contacts.DISPLAY_NAME + " != '' ))";

        Cursor localCursor = localContentResolver.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, select, null, ContactsContract.Contacts.DISPLAY_NAME);
        SecurePreferences sp = CustomSecurePref.getInstance().getmSecurePrefs();
        Boolean isSyncContact = sp.getBoolean(DefineValue.SYNC_CONTACTS,false);

        if (localCursor != null) {
            if (localCursor.getCount() > 0) {
                Timber.wtf("isi size cursorReadPhoneBook:"+String.valueOf(localCursor.getCount()));
                ActiveAndroid.initialize(this.mContext);
                List<friendModel> mListFriendModel = new ArrayList<>();
                String contact_name;
                String contact_id;
                while (!localCursor.isClosed() && localCursor.moveToNext()) {
                    contact_id = localCursor.getString(localCursor.getColumnIndex(ContactsContract.Contacts._ID));
                    contact_name = localCursor.getString(localCursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                    if (Integer.parseInt(localCursor.getString(localCursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {
                        Cursor pCur = localContentResolver.query(
                                    ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                                    null,
                                    ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                                    new String[]{contact_id}, null);

                        String _phone1 = null, _phone2 = null, _phone3 = null, _phoneTemp;
                        int idx = 0;

                        if(pCur != null) {
                            while (pCur.moveToNext()) {
                                _phoneTemp = NoHPFormat.editNoHP(pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER)));
                                switch (idx) {
                                    case 0:
                                        _phone1 = _phoneTemp;
                                        break;
                                    case 1:
                                        _phone2 = _phoneTemp;
                                        break;
                                    case 2:
                                        _phone3 = _phoneTemp;
                                        break;
                                }
                                idx++;
                            }
                        }
                        if (pCur != null) {
                            pCur.close();
                        }

                        pCur = localContentResolver.query(
                                ContactsContract.CommonDataKinds.Email.CONTENT_URI,
                                null,
                                ContactsContract.CommonDataKinds.Email.CONTACT_ID +" = ?",
                                new String[]{contact_id}, null);

                        String _email=null;

                        if (pCur != null) {
                            while (pCur.moveToNext()) {
                                _email = pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));
                            }
                        }
                        if (pCur != null) {
                            pCur.close();
                        }

                        Timber.wtf("isi contact yg disimpen:"+contact_name+" / "+_phone1+" / "+_phone2+" / "+_phone3+" / "+_email+" / "+);

                        mListFriendModel.add(new friendModel(contact_name, _phone1, _phone2, _phone3, _email, ""));
                    }
                    localCursor.close();
                    if(mListFriendModel.size() > 0)
                        insertFriendToDB(mListFriendModel);
                }

            }else {

            }
        }
    }

    public void insertFriendToDB(List<friendModel> friendModels){
        try {
            ActiveAndroid.beginTransaction();
            friendModel mFm;
            friendModel.

            Timber.d("arrayfriend lenght:"+String.valueOf(arrayFriend.length()));
            if(arrayFriend.length()>0){
                for (int i = 0; i < arrayFriend.length(); i++) {
                    mFm = new friendModel();
                    mFm.setContact_id(arrayFriend.getJSONObject(i).getInt(friendModel.CONTACT_ID));
                    mFm.setFull_name(arrayFriend.getJSONObject(i).getString(friendModel.FULL_NAME));
                    mFm.setMobile_number(arrayFriend.getJSONObject(i).getString(friendModel.MOBILE_NUMBER));
                    mFm.setMobile_number2(arrayFriend.getJSONObject(i).getString(friendModel.MOBILE_NUMBER2));
                    mFm.setMobile_number3(arrayFriend.getJSONObject(i).getString(friendModel.MOBILE_NUMBER3));
                    mFm.setEmail(arrayFriend.getJSONObject(i).getString(friendModel.EMAIL));
                    mFm.setOwner_id(arrayFriend.getJSONObject(i).getString(friendModel.OWNER_ID));

                    bucket = arrayFriend.getJSONObject(i).getString(friendModel.IS_FRIEND);
                    if(!bucket.equals(""))mFm.setIs_friend(Integer.parseInt(bucket));

                    mFm.setCreated_date(DateTimeFormat.convertStringtoCustomDate(arrayFriend.getJSONObject(i).getString(friendModel.CREATED_DATE)));
                    if(isContactNew.equals(DefineValue.NO) && !arrayFriend.getJSONObject(i).getString(friendModel.UPDATED_DATE).isEmpty()){
                        mFm.setUpdate_date(DateTimeFormat.convertStringtoCustomDate(arrayFriend.getJSONObject(i).getString(friendModel.UPDATED_DATE)));
                    }
                    mFm.save();
                    Timber.d("idx array friend:"+String.valueOf(i));
                    if(layout_loading_contact.getVisibility() == View.VISIBLE)
                        loadingCircle.setProgress((int) ((i+1)* (25.0/(double)arrayFriend.length()))+50);
                }
            }
            else {
                if(layout_loading_contact.getVisibility() == View.VISIBLE)
                    loadingCircle.setProgress(75);
            }

            Timber.d("arrayMyfriend lenght:"+String.valueOf(arrayMyfriend.length()));
            if(arrayMyfriend.length()>0){
                for (int i = 0; i < arrayMyfriend.length(); i++) {
                    mMfm = new myFriendModel();
                    mMfm.setContact_id(arrayMyfriend.getJSONObject(i).getInt(myFriendModel.CONTACT_ID));
                    mMfm.setFull_name(arrayMyfriend.getJSONObject(i).getString(myFriendModel.FULL_NAME));
                    mMfm.setFriend_number(arrayMyfriend.getJSONObject(i).getString(myFriendModel.FRIEND_NUMBER));
                    mMfm.setEmail(arrayMyfriend.getJSONObject(i).getString(myFriendModel.EMAIL));
                    mMfm.setUser_id(arrayMyfriend.getJSONObject(i).getString(myFriendModel.USER_ID));
                    mMfm.setImg_url(arrayMyfriend.getJSONObject(i).optString(myFriendModel.IMG_URL,""));
                    mMfm.save();
                    Timber.d("idx array my friend:"+String.valueOf((int) ((i + 1) * (25.0 / (double) arrayMyfriend.length())) + 75));
                    if(layout_loading_contact.getVisibility() == View.VISIBLE)
                        loadingCircle.setProgress((int) ((i+1)* (25.0/(double)arrayMyfriend.length()))+75);
                }
            }
            else {
                if(layout_loading_contact.getVisibility() == View.VISIBLE)
                    loadingCircle.setProgress(100);
            }

            ActiveAndroid.setTransactionSuccessful();
            ActiveAndroid.endTransaction();
            sp.edit().putString(DefineValue.CONTACT_FIRST_TIME, DefineValue.NO).apply();

            if (getActivity() != null) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (isAdded()) {
                            if (isContactNew.equals(DefineValue.NO)) {
                                mMFM.clear();
                                mMFM.addAll(myFriendModel.getAll());
                                mAdapter.notifyDataSetChanged();
                                crossfadingView(layout_loading_contact, layout_list_contact);
                            } else {
                                mMFM.clear();
                                mMFM.addAll(myFriendModel.getAll());
                                mAdapter.notifyDataSetChanged();
                                crossfadingView(layout_loading_contact, layout_list_contact);
                            }
                            Timber.d("finish initialize my friend");
                            mState = SHOW_MENU;
                        }
                    }
                });
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }
        finally {
            if(ActiveAndroid.inTransaction())
                ActiveAndroid.endTransaction();
        }
    }
    */
}
