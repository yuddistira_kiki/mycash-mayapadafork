package com.sgo.mayapada.contactsEngine;
/*
  Created by Administrator on 1/9/2017.
 */

import android.net.Uri;

import timber.log.Timber;

public class ContactObserver extends android.database.ContentObserver {

    private ContentObserverCallback COCallback;

    public interface ContentObserverCallback{
        void update(Uri uri);
    }

    public ContactObserver(ContentObserverCallback callback) {
        super(null);
        this.COCallback = callback;
    }

    @Override
    public void onChange(boolean selfChange) {
        super.onChange(selfChange);
    }

    @Override
    public void onChange(boolean selfChange, Uri uri) {
        super.onChange(selfChange, uri);
        Timber.d("contact service called from content oberserver");
        COCallback.update(uri);
    }
}
