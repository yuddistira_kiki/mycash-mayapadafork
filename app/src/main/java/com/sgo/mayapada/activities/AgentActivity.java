package com.sgo.mayapada.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.Menu;

import com.sgo.mayapada.R;
import com.sgo.mayapada.coreclass.BaseActivity;
import com.sgo.mayapada.coreclass.DefineValue;
import com.sgo.mayapada.coreclass.ToggleKeyboard;
import com.sgo.mayapada.fragments.FragCashInInputAgent;
import com.sgo.mayapada.fragments.FragCashoutInputAgent;
import com.sgo.mayapada.fragments.FragReportCommission;

import timber.log.Timber;

/**
 * Created by thinkpad on 7/13/2016.
 */
public class AgentActivity extends BaseActivity {

    private FragmentManager fragmentManager;
    public final static String FRAG_CI_INPUT = "ciInput";
    public final static String FRAG_CI_DESC = "ciDesc";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        InitializeToolbar();

        if (findViewById(R.id.agent_content) != null) {
            if (savedInstanceState != null) {
                return;
            }

            Intent intent    = getIntent();
            int index = intent.getIntExtra(DefineValue.INDEX,0);

            Fragment newFragment = null;
            switch (index) {
                case 0 :
                    newFragment = new FragCashInInputAgent();
                    break;
                case 1 :
                    newFragment = new FragCashoutInputAgent();
                    break;
                case 2 :
                    newFragment = new FragReportCommission();
                    break;
            }

            fragmentManager = getSupportFragmentManager();
            android.support.v4.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.add(R.id.agent_content, newFragment, "agent");
            fragmentTransaction.commit();
            setResult(MainPage.RESULT_NORMAL);
        }
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_agent;
    }

    private void InitializeToolbar(){
        setActionBarIcon(R.drawable.ic_arrow_left);
        setActionBarTitle(getString(R.string.menu_item_title_agent));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        return true;
    }

    public void switchContent(Fragment mFragment,String fragName,Boolean isBackstack) {
        ToggleKeyboard.hide_keyboard(this);
        if(isBackstack){
            Timber.d("backstack");
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.agent_content, mFragment, fragName)
                    .addToBackStack(null)
                    .commitAllowingStateLoss();
        }
        else {
            Timber.d("bukan backstack");
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.agent_content, mFragment, fragName)
                    .commitAllowingStateLoss();

        }
        setActionBarTitle(fragName);
    }

    public void switchActivity(Intent mIntent, int j) {
        ToggleKeyboard.hide_keyboard(this);
        switch (j){
            case MainPage.ACTIVITY_RESULT:
                startActivityForResult(mIntent,MainPage.REQUEST_FINISH);
                break;
            case 2:
                break;
        }
    }

    public void setTitleFragment(String _title) {
        setActionBarTitle(_title);
    }
}
