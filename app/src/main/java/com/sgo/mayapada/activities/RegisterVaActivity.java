package com.sgo.mayapada.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.MenuItem;

import com.sgo.mayapada.R;
import com.sgo.mayapada.coreclass.BaseActivity;
import com.sgo.mayapada.fragments.FragRegPairingidDesciption;
import com.sgo.mayapada.fragments.FragRegisterVAinput;

/**
 * Created by User on 6/13/2017.
 */

public class RegisterVaActivity extends BaseActivity {
    @Override
    protected int getLayoutResource() {
        return R.layout.register_va_activity;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Fragment frag = new FragRegisterVAinput();
        Intent intent = getIntent();
        Bundle bundle = new Bundle();
        bundle.putString("response", intent.getStringExtra("response"));
        frag.setArguments(bundle);
        switchContent(frag, "input_no_va", "Register Community", false, "input_no_va");
    }

    public void switchContent(Fragment mFragment,String fragName,String next_frag_title,Boolean isBackstack,String tag) {

        if(isBackstack){
//            Timber.d("backstack:"+ "masuk");
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.RegVAActivityContent, mFragment, tag)
                    .addToBackStack(fragName)
                    .commitAllowingStateLoss();
        }
        else {
//            Timber.d("bukan backstack:"+"masuk");
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.RegVAActivityContent, mFragment, tag)
                    .commitAllowingStateLoss();

        }
        if(next_frag_title!=null)setActionBarTitle(next_frag_title);
//        ToggleKeyboard.hide_keyboard(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home){
            if (getSupportFragmentManager().getBackStackEntryCount() > 0){
                Fragment frag = getSupportFragmentManager().findFragmentById(R.id.RegVAActivityContent);
                if (frag instanceof FragRegPairingidDesciption){
                    ((FragRegPairingidDesciption)frag).dialog_Cancel();
                }
            }else
                finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            if (getSupportFragmentManager().getBackStackEntryCount() > 0){
                Fragment frag = getSupportFragmentManager().findFragmentById(R.id.RegVAActivityContent);
                if (frag instanceof FragRegPairingidDesciption){
                    ((FragRegPairingidDesciption)frag).dialog_Cancel();
                }
            }else
                finish();
        }
        return super.onKeyDown(keyCode, event);
    }
}
