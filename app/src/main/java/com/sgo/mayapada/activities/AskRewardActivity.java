package com.sgo.mayapada.activities;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.util.Rfc822Tokenizer;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.MultiAutoCompleteTextView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.ex.chips.BaseRecipientAdapter;
import com.android.ex.chips.RecipientEditTextView;
import com.android.ex.chips.recipientchip.DrawableRecipientChip;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.securepreferences.SecurePreferences;
import com.sgo.mayapada.R;
import com.sgo.mayapada.coreclass.BaseActivity;
import com.sgo.mayapada.coreclass.CustomSecurePref;
import com.sgo.mayapada.coreclass.DateTimeFormat;
import com.sgo.mayapada.coreclass.DefineValue;
import com.sgo.mayapada.coreclass.InetHandler;
import com.sgo.mayapada.coreclass.MyApiClient;
import com.sgo.mayapada.coreclass.MyPicasso;
import com.sgo.mayapada.coreclass.NoHPFormat;
import com.sgo.mayapada.coreclass.RoundImageTransformation;
import com.sgo.mayapada.coreclass.WebParams;
import com.sgo.mayapada.dialogs.AlertDialogLogout;
import com.sgo.mayapada.dialogs.DefinedDialog;
import com.squareup.picasso.Picasso;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import timber.log.Timber;

/**
 * Created by thinkpad on 8/18/2016.
 */
public class AskRewardActivity extends BaseActivity {

    private ImageView imgProfile;
    private ImageView imgRecipients;
    private TextView txtName;
    private TextView txtNumberRecipients;
    private RecipientEditTextView phoneRetv;
    private Spinner sp_privacy;
    private Button btnCancel;
    private Button btnShare;
    private EditText etMessage;
    private String _memberId;
    private String _userid;
    private String accessKey;
    private ProgressDialog progdialog;

    private int privacy;
    private int max_member_trans;

    private SecurePreferences sp;
    private DrawableRecipientChip[] chips;

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_ask_reward;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sp = CustomSecurePref.getInstance().getmSecurePrefs();
        max_member_trans = sp.getInt(DefineValue.MAX_MEMBER_TRANS, 5);

        InitializeToolbar();

        View v = this.findViewById(android.R.id.content);

        imgProfile = (ImageView) v.findViewById(R.id.img_profile);
        imgRecipients = (ImageView) v.findViewById(R.id.img_recipients);
        txtName = (TextView) v.findViewById(R.id.txtName);
        phoneRetv = (RecipientEditTextView) v.findViewById(R.id.phone_retv);
        etMessage = (EditText) v.findViewById(R.id.askreward_value_message);
        txtNumberRecipients = (TextView) v.findViewById(R.id.askreward_value_number_recipients);
        btnCancel = (Button) v.findViewById(R.id.btn_cancel);
        btnShare = (Button) v.findViewById(R.id.btn_share);
        sp_privacy = (Spinner) v.findViewById(R.id.askreward_privacy_spinner);

        ArrayAdapter<CharSequence> spinAdapter = ArrayAdapter.createFromResource(this,
                R.array.privacy_list, android.R.layout.simple_spinner_item);
        spinAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_privacy.setAdapter(spinAdapter);
        sp_privacy.setOnItemSelectedListener(spinnerPrivacy);

        Bitmap bmRecipients = BitmapFactory.decodeResource(getResources(), R.drawable.grey_background);
        RoundImageTransformation roundedImageRecipients = new RoundImageTransformation(bmRecipients);
        imgRecipients.setImageDrawable(roundedImageRecipients);

        _memberId = sp.getString(DefineValue.MEMBER_ID,"");
        _userid = sp.getString(DefineValue.USERID_PHONE,"");
        accessKey = sp.getString(DefineValue.ACCESS_KEY,"");
        setImageProfPic();

        txtName.setText(sp.getString(DefineValue.USER_NAME, ""));

        phoneRetv.setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());
        BaseRecipientAdapter adapter = new BaseRecipientAdapter(BaseRecipientAdapter.QUERY_TYPE_PHONE, getApplicationContext());
        phoneRetv.setAdapter(adapter);
        phoneRetv.dismissDropDownOnItemSelected(true);
        phoneRetv.setThreshold(1);

        btnCancel.setOnClickListener(btnCancelListener);
        btnShare.setOnClickListener(btnShareListener);

        etMessage.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    setNumberRecipients();
                }
            }
        });


        phoneRetv.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                Timber.d("on Text Change:" + s.toString());
                boolean retval = s.toString().contains("@");

                if(retval)
                {
                    Timber.d("denny benarrrr");

                    phoneRetv.setTokenizer(new Rfc822Tokenizer());
                    final BaseRecipientAdapter adapter =new BaseRecipientAdapter(getApplicationContext());
                    phoneRetv.setAdapter(adapter);
                    phoneRetv.dismissDropDownOnItemSelected(true);
                    phoneRetv.setThreshold(1);
                }
                else
                {
                    phoneRetv.setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());
                    final BaseRecipientAdapter adapter = new BaseRecipientAdapter(BaseRecipientAdapter.QUERY_TYPE_PHONE, getApplicationContext());
                    phoneRetv.setAdapter(adapter);
                    phoneRetv.dismissDropDownOnItemSelected(true);
                    phoneRetv.setThreshold(1);
                }

                if (phoneRetv.hasFocus()) {
                    if (phoneRetv.getSortedRecipients().length == 0) {
                        txtNumberRecipients.setTextColor(getResources().getColor(R.color.colorSecondaryDark));
                    } else {
                        txtNumberRecipients.setTextColor(getResources().getColor(R.color.colorPrimaryBlueDark));
                    }
                    txtNumberRecipients.setText(String.valueOf(phoneRetv.getSortedRecipients().length));
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }

    private void InitializeToolbar(){
        setActionBarIcon(R.drawable.ic_arrow_left);
        setActionBarTitle(getString(R.string.share_achievement));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    private void setNumberRecipients(){
        if (phoneRetv.getSortedRecipients().length == 0) {
            txtNumberRecipients.setTextColor(getResources().getColor(R.color.colorSecondaryDark));
        } else {
            txtNumberRecipients.setTextColor(getResources().getColor(R.color.colorPrimaryBlueDark));
        }

        if(phoneRetv.length() == 0)
            txtNumberRecipients.setText(String.valueOf(phoneRetv.getSortedRecipients().length));
        else
            txtNumberRecipients.setText(String.valueOf(phoneRetv.getRecipients().length));

        Timber.d("isi length recipients:"+String.valueOf(phoneRetv.getRecipients().length));
    }

    private Spinner.OnItemSelectedListener spinnerPrivacy = new Spinner.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
            privacy = i+1;
            if(phoneRetv.hasFocus())
                phoneRetv.clearFocus();
            setNumberRecipients();

            if(phoneRetv.length() == 0)
                txtNumberRecipients.setText(String.valueOf(phoneRetv.getSortedRecipients().length));
            else
                txtNumberRecipients.setText(String.valueOf(phoneRetv.getRecipients().length));
        }

        @Override
        public void onNothingSelected(AdapterView<?> adapterView) {
            if(phoneRetv.hasFocus())
                phoneRetv.clearFocus();
            setNumberRecipients();

            if(phoneRetv.length() == 0)
                txtNumberRecipients.setText(String.valueOf(phoneRetv.getSortedRecipients().length));
            else
                txtNumberRecipients.setText(String.valueOf(phoneRetv.getRecipients().length));
        }
    };

    private class TempObjectData{

        private String send_to;
        private String ccy_id;
        private String amount;
        private String recipient_name;

        public TempObjectData(String _send_to, String _ccy_id, String _amount,String _recipient_name){
            this.send_to = _send_to;
            this.ccy_id = _ccy_id;
            this.amount = _amount;
            this.recipient_name = _recipient_name;
        }

    }
    private Button.OnClickListener btnCancelListener = new Button.OnClickListener() {
        @Override
        public void onClick(View v) {
            finish();
        }
    };

    private Button.OnClickListener btnShareListener = new Button.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (InetHandler.isNetworkAvailable(AskRewardActivity.this)) {
                if (inputValidation()) {
                    phoneRetv.requestFocus();
                    String amount = "0";
                    String finalNumber;
                    Boolean recipientValidation = true;
                    String message = etMessage.getText().toString();
                    ArrayList<TempObjectData> mTempObjectDataList = new ArrayList<>();

                    String check = phoneRetv.getText().toString();
                    if ((!check.isEmpty()) && check.substring(check.length() - 1).equals(","))
                        phoneRetv.setText(check.substring(0, check.length() - 1));

                    chips = new DrawableRecipientChip[phoneRetv.getSortedRecipients().length];
                    chips = phoneRetv.getSortedRecipients();
                    phoneRetv.clearFocus();
                    if (chips.length <= max_member_trans) {
                        for (DrawableRecipientChip chip : chips) {
                            Timber.v("DrawableChip:" + chip.getEntry().getDisplayName() + " " + chip.getEntry().getDestination());
                            finalNumber = chip.getEntry().getDestination();
                            if (isAlpha(finalNumber) || finalNumber.length() < getResources().getInteger(R.integer.lenght_phone_number)) {
                                recipientValidation = false;
                                break;
                            }
                            finalNumber = NoHPFormat.editNoHP(finalNumber);
                            Timber.v("final number:" + finalNumber);
                            mTempObjectDataList.add(new TempObjectData(finalNumber, DefineValue.IDR, amount, chip.getEntry().getDisplayName()));
                        }


                        if (recipientValidation) {
                            GsonBuilder gsonBuilder = new GsonBuilder();
                            gsonBuilder.setPrettyPrinting();
                            Gson gson = gsonBuilder.create();
                            String data = gson.toJson(mTempObjectDataList);
                            preDialog(message, data);
                        } else {
                            phoneRetv.requestFocus();
                            phoneRetv.setError(getString(R.string.payfriends_recipients_alpha_validation));
                        }
                    } else {
                        phoneRetv.requestFocus();
                        phoneRetv.setError(getString(R.string.payfriends_recipients_max_validation1) + " " +
                                max_member_trans + " " + getString(R.string.payfriends_recipients_max_validation2));
                    }

                }
            }
            else DefinedDialog.showErrorDialog(AskRewardActivity.this, getString(R.string.inethandler_dialog_message));
        }
    };

    private boolean isAlpha(String name) {
        Pattern p = Pattern.compile("[a-zA-Z]");
        Matcher m = p.matcher(name);
        return m.find();
    }


    private void sentData(final String _message, final String _data){
        try{
            progdialog = DefinedDialog.CreateProgressDialog(this, "");
            progdialog.show();

            RequestParams params = MyApiClient.getSignatureWithParams(MyApiClient.COMM_ID, MyApiClient.LINK_ASK4REWARD_SUBMIT,
                    _userid, accessKey);
            params.put(WebParams.MEMBER_ID,_memberId);
            params.put(WebParams.COMM_ID, MyApiClient.COMM_ID);
            params.put(WebParams.DATE_TIME, DateTimeFormat.getCurrentDateTime());
            params.put(WebParams.USER_ID,_userid);
            params.put(WebParams.DESC,_message);
            params.put(WebParams.DATA, _data);
            params.put(WebParams.PRIVACY, privacy);

            Timber.d("isi params sent ask4reward:"+params.toString());

            MyApiClient.sentAsk4RewardSubmit(this, params, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    progdialog.dismiss();
                    Timber.d("isi params response ask4reward:" + response.toString());
                    try {
                        String code = response.getString(WebParams.ERROR_CODE);
                        if (code.equals(WebParams.SUCCESS_CODE)) {

                            JSONArray mArrayData;
                            String messageDialog = null, recipient = "", recipient_name = "";
                            try {
                                mArrayData = new JSONArray(_data);
                                for (int i = 0; i < mArrayData.length(); i++) {
                                    recipient = recipient + mArrayData.getJSONObject(i).getString(WebParams.SEND_TO);
                                    recipient_name = recipient_name + mArrayData.getJSONObject(i).getString(WebParams.RECIPIENT_NAME);
                                    if ((i + 1) < mArrayData.length()) {
                                        recipient = recipient + ", ";
                                        recipient_name = recipient_name + ", ";
                                    }
                                }
                                messageDialog = getString(R.string.askreward_share_to) + " : " + recipient_name + "\n" +
                                        getString(R.string.askreward_text_message) + " : " + _message + "\n";
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            showDialog(messageDialog);
                        } else if (code.equals(WebParams.LOGOUT_CODE)) {
                            Timber.d("isi response autologout:" + response.toString());
                            String message = response.getString(WebParams.ERROR_MESSAGE);
                            AlertDialogLogout test = AlertDialogLogout.getInstance();
                            test.showDialoginActivity(AskRewardActivity.this, message);
                        } else {
                            if (code.equals("0998")) {
                                phoneRetv.requestFocus();
                                phoneRetv.setError(getString(R.string.payfriends_recipients_duplicate_validation));
                            }
                            code = response.getString(WebParams.ERROR_MESSAGE);
                            Toast.makeText(AskRewardActivity.this, code, Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    super.onFailure(statusCode, headers, responseString, throwable);
                    failure(throwable);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    failure(throwable);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    failure(throwable);
                }

                private void failure(Throwable throwable) {
                    if (MyApiClient.PROD_FAILURE_FLAG)
                        Toast.makeText(AskRewardActivity.this, getString(R.string.network_connection_failure_toast), Toast.LENGTH_SHORT).show();
                    else
                        Toast.makeText(AskRewardActivity.this, throwable.toString(), Toast.LENGTH_SHORT).show();

                    if (progdialog.isShowing())
                        progdialog.dismiss();

                    Timber.w("Error Koneksi sent proses ask4reward:" + throwable.toString());
                }
            });
        }catch (Exception e){
            Timber.d("httpclient:"+e.getMessage());
        }
    }

    private void preDialog(final String _message, final String _data){
        String message = getString(R.string.askreward_predialog_msg1)+" "+chips.length+" "+getString(R.string.askreward_predialog_msg2);
        new AlertDialog.Builder(AskRewardActivity.this)
                .setTitle(getString(R.string.askreward_predialog_title))
                .setMessage(message)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        sentData(_message,_data);
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .show();
    }


    private void showDialog(String messageDialog) {
        // Create custom dialog object
        final Dialog dialog = new Dialog(AskRewardActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCanceledOnTouchOutside(false);
        // Include dialog.xml file
        dialog.setContentView(R.layout.dialog_notification);

        // set values for custom dialog components - text, image and button
        Button btnDialogOTP = (Button)dialog.findViewById(R.id.btn_dialog_notification_ok);
        TextView Title = (TextView)dialog.findViewById(R.id.title_dialog);
        TextView Message = (TextView)dialog.findViewById(R.id.message_dialog);
        TextView Message_Detail = (TextView)dialog.findViewById(R.id.message_dialog3);

        //clear data in edit text
        phoneRetv.setText("");
        etMessage.setText("");
        sp_privacy.setSelection(0);
        txtNumberRecipients.setText(getString(R.string.Zero));
        setNumberRecipients();
        txtNumberRecipients.setText(String.valueOf(phoneRetv.getSortedRecipients().length));




        Message.setVisibility(View.VISIBLE);
        Title.setText(getString(R.string.askreward_dialog_title));
        Message.setText(getResources().getString(R.string.askreward_dialog_msg));
        Message_Detail.setVisibility(View.VISIBLE);
        Message_Detail.setGravity(Gravity.LEFT);
        Message_Detail.setText(messageDialog);

        btnDialogOTP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    private boolean inputValidation(){
        if(phoneRetv.getText().toString().length()==0){
            phoneRetv.requestFocus();
            phoneRetv.setError(getString(R.string.payfriends_recipients_validation));
            return false;
        }
        if(phoneRetv.isFocused()){
            phoneRetv.clearFocus();
        }
        if(phoneRetv.getText().toString().charAt(0) == ' '){
            phoneRetv.requestFocus();
            phoneRetv.setError(getString(R.string.payfriends_recipients_validation));
            return false;
        }
        return true;
    }

    private void setImageProfPic(){
        float density = getResources().getDisplayMetrics().density;
        String _url_profpic;

        if(density <= 1) _url_profpic = sp.getString(DefineValue.IMG_SMALL_URL, null);
        else if(density < 2) _url_profpic = sp.getString(DefineValue.IMG_MEDIUM_URL, null);
        else _url_profpic = sp.getString(DefineValue.IMG_LARGE_URL, null);

        Timber.wtf("url prof pic:"+_url_profpic);

        Bitmap bm = BitmapFactory.decodeResource(getResources(), R.drawable.user_unknown_menu);
        RoundImageTransformation roundedImage = new RoundImageTransformation(bm);

        Picasso mPic = MyPicasso.getImageLoader(this);

        if(_url_profpic != null && _url_profpic.isEmpty()){
            mPic.load(R.drawable.user_unknown_menu)
                    .error(roundedImage)
                    .fit().centerInside()
                    .placeholder(R.drawable.progress_animation)
                    .transform(new RoundImageTransformation())
                    .into(imgProfile);
        }
        else {
            mPic.load(_url_profpic)
                    .error(roundedImage)
                    .fit().centerInside()
                    .placeholder(R.drawable.progress_animation)
                    .transform(new RoundImageTransformation())
                    .into(imgProfile);
        }
    }

}
