package com.sgo.mayapada.activities;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.Menu;

import com.sgo.mayapada.R;
import com.sgo.mayapada.coreclass.BaseActivity;
import com.sgo.mayapada.coreclass.DefineValue;
import com.sgo.mayapada.fragments.ListMyFriends;

import timber.log.Timber;

/**
 * Created by thinkpad on 8/25/2017.
 */

public class SplitBillActivity extends BaseActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        InitializeToolbar();

        if (findViewById(R.id.fragment_content) != null) {
            if (savedInstanceState != null) {
                return;
            }

            ListMyFriends listMyfriends = new ListMyFriends();
            Bundle args = new Bundle();
            args.putBoolean(DefineValue.SPLIT_BILL, getIntent().getBooleanExtra(DefineValue.SPLIT_BILL, false));
            if(getIntent().hasExtra(DefineValue.RECIPIENTS))
                args.putString(DefineValue.RECIPIENTS, getIntent().getStringExtra(DefineValue.RECIPIENTS));

            listMyfriends.setArguments(args);
            FragmentManager fragmentManager = getSupportFragmentManager();
            android.support.v4.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.add(R.id.fragment_content, listMyfriends,"listMyfriends");
            fragmentTransaction.commitAllowingStateLoss();
            setResult(MainPage.RESULT_NORMAL);
        }
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_fragment_content;
    }

    public void switchContent(Fragment mFragment, String fragName, Boolean isBackstack) {

        if(isBackstack){
            Timber.d("backstack");
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fragment_content, mFragment, fragName)
                    .addToBackStack(null)
                    .commit();
        }
        else {
            Timber.d("bukan backstack");
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fragment_content, mFragment, fragName)
                    .commit();
        }
        setActionBarTitle(fragName);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        return true;
    }

    private void InitializeToolbar(){
        setActionBarIcon(R.drawable.ic_arrow_left);
        setActionBarTitle(getString(R.string.list_contact_title));
    }
}
