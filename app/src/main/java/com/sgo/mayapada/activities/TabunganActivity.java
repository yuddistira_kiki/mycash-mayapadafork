package com.sgo.mayapada.activities;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.securepreferences.SecurePreferences;
import com.sgo.mayapada.entityRealm.List_Account_Nabung;
import com.sgo.mayapada.R;
import com.sgo.mayapada.coreclass.BaseActivityOTP;
import com.sgo.mayapada.coreclass.CustomSecurePref;
import com.sgo.mayapada.coreclass.DefineValue;
import com.sgo.mayapada.coreclass.ErrorDefinition;
import com.sgo.mayapada.coreclass.MyApiClient;
import com.sgo.mayapada.coreclass.ToggleKeyboard;
import com.sgo.mayapada.coreclass.WebParams;
import com.sgo.mayapada.dialogs.AlertDialogFrag;
import com.sgo.mayapada.dialogs.AlertDialogLogout;
import com.sgo.mayapada.dialogs.DefinedDialog;
import com.sgo.mayapada.fragments.FragAutoDeposit;
import com.sgo.mayapada.fragments.Nabung;
import com.sgo.mayapada.fragments.On_Going_autodeposit;
import com.sgo.mayapada.fragments.RekeningTabungan;
import com.sgo.mayapada.fragments.TambahRekTabungan;
import com.sgo.mayapada.fragments.TargetNabung;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import io.realm.Realm;
import timber.log.Timber;

/**
 * Created by thinkpad on 11/20/2015.
 */
public class TabunganActivity extends BaseActivityOTP implements RekeningTabungan.OnFragmentInteractionListener,
        TambahRekTabungan.OnSuccessAdd{

    final public static int MENU_ATUR_TABUNGAN = 1;
    final public static int MENU_AUTOMATE_NABUNG = 2;
    final public static int MENU_TARGET_NABUNG = 3;
    final public static int MENU_NABUNG = 4;
    private final static int MENU_TAMBAH_ACCOUNT = 5;

    private FragmentManager fragmentManager;
    private List_Account_Nabung defaultModel = null;
    private long sizeRekSavings = 0;
    private ProgressDialog progressDialog;
    private String userID,accessKey, member_id;
    private SecurePreferences sp;
    private int currentMenu;
    private Realm realm ;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        InitializeToolbar();
        sp = CustomSecurePref.getInstance().getmSecurePrefs();
        userID = sp.getString(DefineValue.USERID_PHONE,"");
        accessKey = sp.getString(DefineValue.ACCESS_KEY,"");
        member_id = sp.getString(DefineValue.MEMBER_ID, "");

        progressDialog = DefinedDialog.CreateProgressDialog(this, "");
        progressDialog.dismiss();

        realm = Realm.getDefaultInstance();

        defaultModel = realm.where(List_Account_Nabung.class).equalTo(WebParams.IS_DEFAULT,DefineValue.STRING_YES).findFirstAsync();
        sizeRekSavings = realm.where(List_Account_Nabung.class).count();

        if (findViewById(R.id.tabungan_content) != null) {
            if (savedInstanceState != null) {
                return;
            }
            fragmentManager = getSupportFragmentManager();
            Fragment newFragment = null;
            String tag = null;
            currentMenu = getIntent().getIntExtra(DefineValue.MENU_TABUNGAN,1);

            switch (currentMenu){
                case MENU_ATUR_TABUNGAN :
                    newFragment = new RekeningTabungan();
                    tag = RekeningTabungan.TAG;
                    break;
                case MENU_AUTOMATE_NABUNG :
                    inqAutoSaving();
                    break;
                case MENU_TARGET_NABUNG :
                    newFragment = new TargetNabung();
                    tag=TargetNabung.TAG;
                    break;
                case MENU_NABUNG :
                    newFragment = new Nabung();
                    tag = Nabung.TAG;
                    break;
                case MENU_TAMBAH_ACCOUNT :
                    newFragment = new TambahRekTabungan();
                    tag = TambahRekTabungan.TAG;
                    break;
                default:
                    this.finish();
                    break;
            }

            if(newFragment != null)
                addFragment(newFragment,tag);

        }
    }

    private void addFragment(Fragment newFragment, String tag){
        android.support.v4.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.tabungan_content, newFragment,tag);
        fragmentTransaction.commit();
        setResult(MainPage.RESULT_NORMAL);
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_tabungan;
    }

    private void InitializeToolbar(){
        setActionBarIcon(R.drawable.ic_arrow_left);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        return true;
    }

    public void switchContent(Fragment mFragment,String fragName,Boolean isBackstack, String tag) {
        ToggleKeyboard.hide_keyboard(this);
        if(isBackstack){
            Timber.d("backstack");
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.tabungan_content, mFragment, tag)
                    .addToBackStack(null)
                    .commitAllowingStateLoss();
        }
        else {
            Timber.d("bukan backstack");
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.tabungan_content, mFragment, tag)
                    .commitAllowingStateLoss();

        }
        if(!fragName.isEmpty())
            setToolbarTitle(fragName);
    }

    public void setToolbarTitle(String title){
        setActionBarTitle(title);
    }

    public void switchActivity(Intent mIntent, int j) {
        ToggleKeyboard.hide_keyboard(this);
        switch (j){
            case MainPage.ACTIVITY_RESULT:
                startActivityForResult(mIntent,MainPage.REQUEST_FINISH);
                break;
            case 2:
                break;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    @Override
    protected void onDestroy() {
        realm.close();
        super.onDestroy();
        MyApiClient.CancelRequestWS(this, true);
    }

    @Override
    public void onSelectDefaultRek(List_Account_Nabung rek_tabungan_model, int size) {
        defaultModel = rek_tabungan_model;
        sizeRekSavings = size;

        if(defaultModel != null) {
            Timber.d("selected object : " + rek_tabungan_model.getBank_name() + "/" +
                    rek_tabungan_model.getAcct_name() + "/" + rek_tabungan_model.getAcct_no() + "/" +
                    String.valueOf(rek_tabungan_model.isDefault()));
        }

        Fragment frag = fragmentManager.findFragmentByTag(Nabung.TAG);
        if (frag instanceof Nabung) {
            if(defaultModel == null)
                finish();
            else {
                fragmentManager.popBackStack();
                ((Nabung) frag).setDefaultRek(rek_tabungan_model);
            }
        }
    }

    @Override
    public void onBackPressed() {
        if(currentMenu != MENU_TARGET_NABUNG && currentMenu != MENU_AUTOMATE_NABUNG) {
            if (fragmentManager.getBackStackEntryCount() > 0) {
                Fragment frag = fragmentManager.findFragmentByTag(TambahRekTabungan.TAG);
                if (frag instanceof TambahRekTabungan) {
                    if (((TambahRekTabungan) frag).OnBackPressed()) {
                        super.onBackPressed();
                    }
                } else {
                        super.onBackPressed();
                }
            }
            else {
                if(defaultModel == null || sizeRekSavings == 0)
                    showDialogNoDefault();
                else
                    super.onBackPressed();
            }


        } else {
            super.onBackPressed();
        }
    }

    private void showDialogNoDefault(){
        AlertDialogFrag dialog_frag = AlertDialogFrag.newInstance(getString(R.string.dialog_title_no_default),
                getString(R.string.dialog_msg_no_default),getString(R.string.yes),getString(R.string.no),false);
        dialog_frag.setOkListener(new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        dialog_frag.show(getSupportFragmentManager(), AlertDialogFrag.TAG);
    }

    @Override
    public void OnSuccessAddAccount(int status) {
        switch (status){
            case TambahRekTabungan.STATUS_SUCCESS:
                finish();
                break;
            case TambahRekTabungan.STATUS_ERROR:
                break;
        }
    }

    private void inqAutoSaving() {

        progressDialog.show();
        RequestParams params = MyApiClient.getSignatureWithParams(MyApiClient.COMM_ID, MyApiClient.LINK_INQ_AUTOSAVING,
                userID, accessKey);
        params.put(WebParams.COMM_ID, MyApiClient.COMM_ID);
        params.put(WebParams.MEMBER_ID, member_id);
        params.put(WebParams.USER_ID, userID);

        Timber.d("parameter inqAutoSaving: " + params.toString());
        MyApiClient.getInqAutoSaving(this, params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                Fragment frag;
                try {
                    Timber.d("respose inqAutoSaving: " + response.toString());
                    String code = response.getString(WebParams.ERROR_CODE);
                    if (code.equals(WebParams.SUCCESS_CODE)) {
                        Bundle bundle = new Bundle();
                        frag = new On_Going_autodeposit();
                        bundle.putString(DefineValue.AUTO_ID, response.optString(WebParams.AUTO_ID,""));
                        bundle.putString(DefineValue.AMOUNT_TYPE, response.optString(WebParams.AMOUNT_TYPE,""));
                        bundle.putString(DefineValue.AUTO_VALUE, response.optString(WebParams.AUTO_VALUE,""));
                        bundle.putString(DefineValue.CCY_ID, response.optString(WebParams.CCY_ID,""));
                        bundle.putString(DefineValue.DATE_FROM, response.optString(WebParams.SAVING_START,""));
                        bundle.putString(DefineValue.DATE_TO, response.optString(WebParams.SAVING_END,""));
                        bundle.putInt(DefineValue.REPETITION, response.optInt(WebParams.REPETITION,0));
                        bundle.putString(DefineValue.STATUS, response.optString(WebParams.STATUS,""));
                        bundle.putString(DefineValue.LAST_EXECUTION, response.optString(WebParams.LAST_EXECUTION,""));
                        bundle.putString(DefineValue.FAILED_REASON, response.optString(WebParams.FAILED_REASON,""));
                        frag.setArguments(bundle);
                        addFragment(frag,On_Going_autodeposit.TAG);
                        setToolbarTitle(getString(R.string.savings_title_automate_savings));

                    } else if(code.equals(ErrorDefinition.NO_TRANSACTION)) {
                        frag = new FragAutoDeposit();
                        addFragment(frag,FragAutoDeposit.TAG);
                        setToolbarTitle(getString(R.string.savings_title_automate_savings));
                    } else if (code.equals(WebParams.LOGOUT_CODE)) {
                        Timber.d("isi response inqAutoSaving:" + response.toString());
                        String message = response.getString(WebParams.ERROR_MESSAGE);
                        AlertDialogLogout test = AlertDialogLogout.getInstance();
                        test.showDialoginActivity(TabunganActivity.this, message);
                    } else {
                        Timber.d("Error inqAutoSaving:" + response.toString());
                        code = response.getString(WebParams.ERROR_MESSAGE);
                        Toast.makeText(TabunganActivity.this, code, Toast.LENGTH_LONG).show();
                        finish();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                super.onFailure(statusCode, headers, responseString, throwable);
                failure(throwable);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                failure(throwable);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                failure(throwable);
            }

            private void failure(Throwable throwable) {
                if (MyApiClient.PROD_FAILURE_FLAG)
                    Toast.makeText(getApplicationContext(), getString(R.string.network_connection_failure_toast), Toast.LENGTH_SHORT).show();
                else
                    Toast.makeText(getApplicationContext(), throwable.toString(), Toast.LENGTH_SHORT).show();

                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                finish();
                Timber.w("Error Koneksi inqAutoSaving:" + throwable.toString());
            }

        });
    }

}
