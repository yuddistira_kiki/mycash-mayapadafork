package com.sgo.mayapada.activities;

import android.app.AlertDialog;
import android.content.ContentProviderOperation;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.graphics.PointF;
import android.os.Bundle;
import android.provider.Contacts;
import android.provider.ContactsContract;
import android.view.Display;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.dlazaro66.qrcodereaderview.QRCodeReaderView;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.securepreferences.SecurePreferences;
import com.sgo.mayapada.R;
import com.sgo.mayapada.coreclass.BaseActivity;
import com.sgo.mayapada.coreclass.Contents;
import com.sgo.mayapada.coreclass.CoreApp;
import com.sgo.mayapada.coreclass.CustomSecurePref;
import com.sgo.mayapada.coreclass.DateTimeFormat;
import com.sgo.mayapada.coreclass.DefineValue;
import com.sgo.mayapada.coreclass.MyApiClient;
import com.sgo.mayapada.coreclass.QRCodeEncoder;
import com.sgo.mayapada.coreclass.WebParams;
import com.sgo.mayapada.dialogs.AlertDialogLogout;
import com.sgo.mayapada.entityDAO.DaoSession;
import com.sgo.mayapada.entityDAO.Friend;
import com.sgo.mayapada.entityDAO.FriendDao;
import com.sgo.mayapada.entityDAO.MyFriend;
import com.sgo.mayapada.entityDAO.MyFriendDao;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import timber.log.Timber;

/*
  Created by thinkpad on 4/24/2015.
 */
public class AddByQRCodeActivity extends BaseActivity implements QRCodeReaderView.OnQRCodeReadListener {

    private SecurePreferences sp;
    private int RESULT;

    private QRCodeReaderView mydecoderview;
    private ImageView imageBarcode;
    private TextView tvIDBarcode;
    private AlertDialog dialogContact;
    private FrameLayout flScanner;
    private String _ownerID;
    private String custName;
    private String custPhone;
    private String custEmail;
    private String accessKey;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        InitializeToolbar();
        sp = CustomSecurePref.getInstance().getmSecurePrefs();
        _ownerID = sp.getString(DefineValue.USERID_PHONE,"");
        custName = sp.getString(DefineValue.CUST_NAME,"");
        custPhone = sp.getString(DefineValue.USERID_PHONE,"");
        custEmail = sp.getString(DefineValue.PROFILE_EMAIL,"");
        accessKey = sp.getString(DefineValue.ACCESS_KEY,"");

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x/2;
        int height = size.y/3;

        mydecoderview = (QRCodeReaderView) findViewById(R.id.qrdecoderview);
        mydecoderview.setOnQRCodeReadListener(this);

        imageBarcode = (ImageView) findViewById(R.id.image_barcode);
        tvIDBarcode = (TextView) findViewById(R.id.idBarcode);

        flScanner = (FrameLayout) findViewById(R.id.llQRCodeScanner);
        mydecoderview.getLayoutParams().width = width;
        flScanner.getLayoutParams().height = height;

        imageBarcode.getLayoutParams().width = width;
        imageBarcode.getLayoutParams().height = height;

        tvIDBarcode.setText("ID : " + _ownerID);

        generateQRCode();

        RESULT = MainPage.RESULT_NORMAL;
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_addbyqrcode;
    }

    private void InitializeToolbar(){
        setActionBarIcon(R.drawable.ic_arrow_left);
        setActionBarTitle(getString(R.string.title_add_friends));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                setResult(RESULT);
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void insertContact(List<Friend> mfriendModel){
        try{

            RequestParams params = MyApiClient.getSignatureWithParams(MyApiClient.COMM_ID,MyApiClient.LINK_USER_CONTACT_INSERT,
                    _ownerID,accessKey);
            GsonBuilder gsonBuilder = new GsonBuilder();
            Gson gson = gsonBuilder.registerTypeAdapter(Friend.class, new friendAdapter()).create();
            params.put(WebParams.USER_ID, _ownerID);
            params.put(WebParams.DATE_TIME, DateTimeFormat.getCurrentDateTime());
            params.put(WebParams.CONTACTS, gson.toJson(mfriendModel));
            params.put(WebParams.COMM_ID, MyApiClient.COMM_ID);

            Timber.i("request "+params.toString());

            JsonHttpResponseHandler mHandler = new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    try {
                        String code = response.getString(WebParams.ERROR_CODE);

                        Timber.i("code "+code);
                        if (code.equals(WebParams.SUCCESS_CODE)) {
                            String arrayFriend = response.getString(WebParams.DATA_CONTACT);
                            String arrayMyFriend = response.getString(WebParams.DATA_FRIEND);
                            insertFriendToDB(new JSONArray(arrayFriend), new JSONArray(arrayMyFriend));

                        }
                        else if(code.equals(WebParams.LOGOUT_CODE)){
                            Timber.d("isi response autologout "+response.toString());
                            String message = response.getString(WebParams.ERROR_MESSAGE);
                            AlertDialogLogout test = AlertDialogLogout.getInstance();
                            test.showDialoginActivity(AddByQRCodeActivity.this,message);
                        }
                        else {
                            code = response.getString(WebParams.ERROR_MESSAGE);
                            Toast.makeText(getApplicationContext(),code,Toast.LENGTH_SHORT).show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onProgress(long bytesWritten, long totalSize) {
                    super.onProgress(bytesWritten, totalSize);
                    Timber.d("onProgress insert contact:"+bytesWritten+" / "+totalSize);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    super.onFailure(statusCode, headers, responseString, throwable);
                    failure(throwable);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    failure(throwable);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    failure(throwable);
                }


                private void failure(Throwable throwable){
                    if(MyApiClient.PROD_FAILURE_FLAG)
                        Toast.makeText(AddByQRCodeActivity.this, getString(R.string.network_connection_failure_toast), Toast.LENGTH_SHORT).show();
                    else
                        Toast.makeText(AddByQRCodeActivity.this, throwable.toString(), Toast.LENGTH_SHORT).show();
                    Timber.w("Error Koneksi User contact insert "+throwable.toString());
                }

            };
            MyApiClient.sentInsertContact(this,params, false, mHandler);
        }catch (Exception e){
            Timber.d("httpclient"+e.getMessage());
        }
    }

    private void insertFriendToDB(JSONArray arrayFriend, JSONArray arrayMyfriend){
        DaoSession daoSession = CoreApp.get_instance().getDaoSession();
        FriendDao friendDao = daoSession.getFriendDao();
        MyFriendDao myFriendDao = daoSession.getMyFriendDao();
        Friend mFm;
        MyFriend mMfm;
        String bucket;
        try {
            Friend.deleteAllnCache(friendDao);
            MyFriend.deleteAllnCache(myFriendDao);

            Timber.d("arrayfriend lenght "+String.valueOf(arrayFriend.length()));
            if(arrayFriend.length()>0){
                List<Friend> tempListFriend = new ArrayList<>();
                mFm = new Friend();
                for (int i = 0; i < arrayFriend.length(); i++) {
                    mFm.setContact_id(arrayFriend.getJSONObject(i).getInt(WebParams.CONTACT_ID));
                    mFm.setFull_name(arrayFriend.getJSONObject(i).getString(WebParams.FULL_NAME));
                    mFm.setMobile_number(arrayFriend.getJSONObject(i).getString(WebParams.MOBILE_NUMBER));
                    mFm.setMobile_number2(arrayFriend.getJSONObject(i).getString(WebParams.MOBILE_NUMBER2));
                    mFm.setMobile_number3(arrayFriend.getJSONObject(i).getString(WebParams.MOBILE_NUMBER3));
                    mFm.setEmail(arrayFriend.getJSONObject(i).getString(WebParams.EMAIL));
                    mFm.setOwner_id(arrayFriend.getJSONObject(i).getString(WebParams.OWNER_ID));
                    bucket = arrayFriend.getJSONObject(i).getString(WebParams.IS_FRIEND);
                    if(!bucket.equals(""))
                        mFm.setIs_friend(Integer.parseInt(bucket));
                    mFm.setCreated_date(DateTimeFormat.convertStringtoCustomDate(arrayFriend.getJSONObject(i).getString(WebParams.CREATED_DATE)));
                    tempListFriend.add(mFm);
                    Timber.d("idx array friend"+String.valueOf(i));
                }
                friendDao.insertInTx(tempListFriend);
            }

            Timber.d("arrayMyfriend lenght "+String.valueOf(arrayMyfriend.length()));
            if(arrayMyfriend.length()>0){
                List<MyFriend> tempListMyFriend = new ArrayList<>();
                mMfm = new MyFriend();
                for (int i = 0; i < arrayMyfriend.length(); i++) {
                    mMfm.setContact_id(arrayMyfriend.getJSONObject(i).getInt(WebParams.CONTACT_ID));
                    mMfm.setFull_name(arrayMyfriend.getJSONObject(i).getString(WebParams.FULL_NAME));
                    mMfm.setFriend_number(arrayMyfriend.getJSONObject(i).getString(WebParams.FRIEND_NUMBER));
                    mMfm.setEmail(arrayMyfriend.getJSONObject(i).getString(WebParams.EMAIL));
                    mMfm.setUser_id(arrayMyfriend.getJSONObject(i).getString(WebParams.USER_ID));
                    mMfm.setImg_url(arrayMyfriend.getJSONObject(i).optString(WebParams.IMG_URL,""));
                    tempListMyFriend.add(mMfm);
                    Timber.d("idx array my friend:"+String.valueOf((int) ((i + 1) * (25.0 / (double) arrayMyfriend.length())) + 75));
                }
                myFriendDao.insertInTx(tempListMyFriend);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        } finally {
            sp.edit().putString(DefineValue.CONTACT_FIRST_TIME, DefineValue.NO).apply();
        }

    }

    private class friendAdapter implements JsonSerializer<Friend> {

        @Override
        public JsonElement serialize(Friend _friend, Type type, JsonSerializationContext jsonSerializationContext) {
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty(WebParams.FULL_NAME, _friend.getFull_name());
            jsonObject.addProperty(WebParams.MOBILE_NUMBER, _friend.getMobile_number());
            jsonObject.addProperty(WebParams.MOBILE_NUMBER2, _friend.getMobile_number2());
            jsonObject.addProperty(WebParams.MOBILE_NUMBER3, _friend.getMobile_number3());
            jsonObject.addProperty(WebParams.EMAIL, _friend.getEmail());
            jsonObject.addProperty(WebParams.OWNER_ID, _friend.getOwner_id());
            return jsonObject;
        }
    }

    private void generateQRCode() {
        String qrInputText = "Name : " + custName + "\n" +
                "No HP : " + custPhone + "\n" +
                "Email : " + custEmail;

        //Find screen size
        WindowManager manager = (WindowManager) getSystemService(WINDOW_SERVICE);
        Display display = manager.getDefaultDisplay();
        Point point = new Point();
        display.getSize(point);
        int width = point.x;
        int height = point.y;
        int smallerDimension = width < height ? width : height;
        smallerDimension = smallerDimension * 3 / 4;

        Bundle bundle = new Bundle();
        bundle.putString(Contacts.Intents.Insert.NAME, custName);
        bundle.putString(Contacts.Intents.Insert.PHONE, custPhone);
        bundle.putString(Contacts.Intents.Insert.EMAIL, custEmail);

        //Encode with a QR Code image
        QRCodeEncoder qrCodeEncoder = new QRCodeEncoder(qrInputText,
                bundle,
                Contents.Type.CONTACT,
                BarcodeFormat.QR_CODE.toString(),
                smallerDimension);
        try {
            Bitmap bitmap = qrCodeEncoder.encodeAsBitmap();
            imageBarcode.setImageBitmap(bitmap);

        } catch (WriterException e) {
            e.printStackTrace();
        }
    }

    // Called when a QR is decoded
    // "text" : the text encoded in QR
    // "points" : points where QR control points are placed
    @Override
    public void onQRCodeRead(final String text, PointF[] points) {
        Timber.d("Result "+text);

        if( dialogContact != null && dialogContact.isShowing() ) return;

        if(text.contains("MECARD:")) {

            String[] separatedComa = text.split(";");
            String[] first = separatedComa[0].split(":");
            final String name = first[2];

            String[] second = separatedComa[1].split(":");
            final String phone = second[1];

            String[] third = separatedComa[2].split(":");
            final String email = third[1];

            String qrText = "Name : " + name + "\n" + "Phone : " + phone + "\n" + "Email : " + email;

            final AlertDialog.Builder builderDialog = new AlertDialog.Builder(this)
                    .setTitle("Add to Contacts")
                    .setMessage(qrText)
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            //masukin kontak ke hp
                            addContactToPhone(name, phone, email);
                            //kirim kontak ke server
                            List<Friend> mfriendModel = new ArrayList<>();
                            mfriendModel.add(new Friend(name, phone, "", "", email, _ownerID));
                            insertContact(mfriendModel);

                        }
                    })
                    .setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    })
                    .setIcon(android.R.drawable.ic_dialog_alert);

            dialogContact = builderDialog.create();
            dialogContact.show();
        }
        else {
            final AlertDialog.Builder builderDialog = new AlertDialog.Builder(this)
                    .setTitle("Alert")
                    .setMessage("QR Code Invalid")
                    .setNegativeButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    })
                    .setIcon(android.R.drawable.ic_dialog_alert);

            dialogContact = builderDialog.create();
            dialogContact.show();
        }
    }


    // Called when your device have no camera
    @Override
    public void cameraNotFound() {

    }

    // Called when there's no QR codes in the camera preview image
    @Override
    public void QRCodeNotFoundOnCamImage() {

    }

    @Override
    protected void onResume() {
        super.onResume();
        mydecoderview.getCameraManager().startPreview();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mydecoderview.getCameraManager().stopPreview();
    }

    private void addContactToPhone(String names,
                                   String phoneNumbers,
                                   String emails) {


        ArrayList<ContentProviderOperation> ops = new ArrayList<>();

        ops.add(ContentProviderOperation.newInsert(
                ContactsContract.RawContacts.CONTENT_URI)
                .withValue(ContactsContract.RawContacts.ACCOUNT_TYPE, null)
                .withValue(ContactsContract.RawContacts.ACCOUNT_NAME, null)
                .build());

        //------------------------------------------------------ Names
        if (names != null) {
            ops.add(ContentProviderOperation.newInsert(
                    ContactsContract.Data.CONTENT_URI)
                    .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)
                    .withValue(ContactsContract.Data.MIMETYPE,
                            ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE)
                    .withValue(ContactsContract.CommonDataKinds.StructuredName.DISPLAY_NAME,
                            names).build());
        }

        //------------------------------------------------------ Mobile Number
        if (phoneNumbers != null) {
            ops.add(ContentProviderOperation.
                    newInsert(ContactsContract.Data.CONTENT_URI)
                    .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)
                    .withValue(ContactsContract.Data.MIMETYPE,
                            ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE)
                    .withValue(ContactsContract.CommonDataKinds.Phone.NUMBER, phoneNumbers)
                    .withValue(ContactsContract.CommonDataKinds.Phone.TYPE,
                            ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE)
                    .build());
        }

        //------------------------------------------------------ Email
        if (emails != null) {
            ops.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
                    .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)
                    .withValue(ContactsContract.Data.MIMETYPE,
                            ContactsContract.CommonDataKinds.Email.CONTENT_ITEM_TYPE)
                    .withValue(ContactsContract.CommonDataKinds.Email.DATA, emails)
                    .withValue(ContactsContract.CommonDataKinds.Email.TYPE, ContactsContract.CommonDataKinds.Email.TYPE_WORK)
                    .build());
        }

        // Asking the Contact provider to create a new contact
        try {
            this.getContentResolver().applyBatch(ContactsContract.AUTHORITY, ops);
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(this, "Exception: " + e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }
}

