package com.sgo.mayapada.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.balysv.materialripple.MaterialRippleLayout;
import com.securepreferences.SecurePreferences;
import com.sgo.mayapada.R;
import com.sgo.mayapada.coreclass.BaseActivity;
import com.sgo.mayapada.coreclass.CustomSecurePref;
import com.sgo.mayapada.coreclass.DefineValue;

import java.util.List;
import java.util.Random;

import pub.devrel.easypermissions.EasyPermissions;

/**
 * Created by thinkpad on 2/11/2016.
 */
public class PayStoreActivity extends BaseActivity implements EasyPermissions.PermissionCallbacks {


    private static final int recCodeSaveImage = 12;
    private Button btn_save_img, btn_vouch_redeemed;
    private MaterialRippleLayout btn_redeem, btn_redeemed;
    private View layout_voucher;
    private String code_voucher;
    private TextView tv_voucher_ins, warning;

    private SecurePreferences.Editor mEditor;
    private SecurePreferences sp;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        sp = CustomSecurePref.getInstance().getmSecurePrefs();

        InitializeToolbar();

        if(sp.contains(DefineValue.REDEEM_CODE)){
            code_voucher = sp.getString(DefineValue.REDEEM_CODE,"");
        }
        else {
            Random r = new Random();
            final int i1 = r.nextInt(999999999 - 111111111) + 111111111;
            code_voucher = String.valueOf(i1);
            mEditor = sp.edit();
            mEditor.putString(DefineValue.REDEEM_CODE, code_voucher);
            mEditor.apply();
        }

        TextView tv_voucher_value = (TextView) this.findViewById(R.id.paystore_voucher_text);
        TextView tv_voucher_exp = (TextView) this.findViewById(R.id.paystore_voucher_exp_teks);
        tv_voucher_ins = (TextView) this.findViewById(R.id.paystore_voucher_message);
        warning = (TextView) this.findViewById(R.id.paystore_voucher_message_warning);
//        TextView tv_voucher_balance = (TextView) this.findViewById(R.id.paystore_voucher_balance_value);
        ImageView img_v = (ImageView) this.findViewById(R.id.paystore_voucher_img_zigzag);

//        if (tv_voucher_balance != null) {
//            String balance_value = tv_voucher_balance.getText() + " " + MyApiClient.CCY_VALUE + " " + CurrencyFormat.format(sp.getString(DefineValue.BALANCE, "0"));
//            tv_voucher_balance.setText(balance_value);
//        }

        layout_voucher = this.findViewById(R.id.layout_voucher_view);

        int color = getResources().getColor(R.color.colorPrimaryDark); //The color u want
        if (img_v != null) {
            img_v.setColorFilter(color);
        }
        if (tv_voucher_value != null) {
            tv_voucher_value.setText(code_voucher);
            code_voucher = tv_voucher_value.getText().toString();
        }
//        String date = DateTimeFormat.getCurrentDateTimePlusHour(1);
        if (tv_voucher_exp != null) {
            tv_voucher_exp.setText("Voucher berlaku selama pameran 29 - 30 Agustus 2016");
        }


        btn_save_img = (Button) this.findViewById(R.id.btn_redeem_voucher);
        btn_vouch_redeemed = (Button) this.findViewById(R.id.btn_redeemed_voucher);
        btn_redeem = (MaterialRippleLayout) this.findViewById(R.id.btn_redeem_layout);
        btn_redeemed = (MaterialRippleLayout) this.findViewById(R.id.btn_redeemed_layout);

        setVoucherBtn();

//        final ViewToBitmap viewToBitmap = new ViewToBitmap(this, new ViewToBitmap.ConvertHandler() {
//            @Override
//            public void ConvertResponse(Boolean isSuccess) {
//                if(!isSuccess)
//                    btn_save_img.setEnabled(true);
//
//            }
//        });

        if (btn_save_img != null) {
            btn_save_img.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    AlertDialog.Builder alertbox = new AlertDialog.Builder(PayStoreActivity.this);
                    alertbox.setTitle(getString(R.string.warning));
                    alertbox.setMessage("Apakah anda yakin akan melakukan Redeem kupon?");
                    alertbox.setPositiveButton(getString(R.string.ok), new
                            DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface arg0, int arg1) {
                                    mEditor = sp.edit();
                                    mEditor.putBoolean(DefineValue.IS_REDEEM, false);
                                    mEditor.putString(DefineValue.REDEEM_CODE, code_voucher);
                                    mEditor.apply();
                                    setVoucherBtn();
                                }
                            });
                    alertbox.setNegativeButton(getString(R.string.cancel), new
                            DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface arg0, int arg1) {
                                }
                            });
                    alertbox.show();
//                    String filename = "VPIN_"+String.valueOf(i1);
//                    if(reqPermissionClass.checkPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE,
//                            ReqPermissionClass.PERMISSIONS_REQ_WRITEEXTERNALSTORAGE+recCodeSaveImage)) {
//                        btn_save_img.setEnabled(false);
//                        viewToBitmap.ConvertAsync(layout_voucher, filename);
//                    }

                }
            });
        }
    }

    private void setVoucherBtn(){
        Boolean isRedeem = sp.getBoolean(DefineValue.IS_REDEEM, true);
        if (!isRedeem){
            if (tv_voucher_ins != null) {
                tv_voucher_ins.setText(getString(R.string.paystore_voucher_redemeed_text, getString(R.string.appname)));
            }
//            layout_voucher.setVisibility(View.VISIBLE);
            warning.setVisibility(View.GONE);
            btn_redeem.setVisibility(View.GONE);
            btn_redeemed.setVisibility(View.VISIBLE);
        }else {
            if (tv_voucher_ins != null) {
                tv_voucher_ins.setText(getString(R.string.paystore_voucher_text, getString(R.string.appname)));
            }
            warning.setVisibility(View.VISIBLE);
            btn_redeem.setVisibility(View.VISIBLE);
//            layout_voucher.setVisibility(View.GONE);
            btn_redeemed.setVisibility(View.GONE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(requestCode,permissions,grantResults);
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_pay_store;
    }

    private void InitializeToolbar(){
        setActionBarIcon(R.drawable.ic_arrow_left);
        setActionBarTitle("Voucher");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                setResult(MainPage.RESULT_NORMAL);
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }



    public void finishChild(){
        setResult(MainPage.RESULT_NORMAL);
        this.finish();
    }


    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {
        switch (requestCode){
            case recCodeSaveImage:
                btn_save_img.performClick();
                break;
        }
    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {

    }
}
