package com.sgo.mayapada.activities;

import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.Menu;
import android.widget.Toast;

import com.sgo.mayapada.R;
import com.sgo.mayapada.coreclass.BaseActivity;
import com.sgo.mayapada.coreclass.DefineValue;
import com.sgo.mayapada.coreclass.GeneralizeImage;
import com.sgo.mayapada.coreclass.ToggleKeyboard;
import com.sgo.mayapada.fragments.FragKantinCheckKupon;
import com.sgo.mayapada.fragments.FragKantinDaftarPembeli;
import com.sgo.mayapada.fragments.FragKantinDaftarTerjual;
import com.sgo.mayapada.fragments.FragKantinManajemenStok;
import com.sgo.mayapada.fragments.FragKantinStudentDetail;
import com.sgo.mayapada.fragments.FragKantinStudentOrder;
import com.sgo.mayapada.fragments.StockBarangAction;

import java.io.IOException;

import timber.log.Timber;

/**
 * Created by thinkpad on 8/3/2016.
 */
public class KantinActivity extends BaseActivity {

    public static final int RESULT_GALERY = 110;
    public static final int RESULT_CAMERA = 120;

    public static final int RESULT_SCAN_COUPON = 101;
    private FragmentManager fragmentManager;
    private Fragment mContent;
    private String kantin_name;
    private String toFragment;
    private Uri mCapturedImageURI;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent i = getIntent();
        toFragment = i.getStringExtra(DefineValue.KANTIN);

        InitializeToolbar();

        if (findViewById(R.id.kantinActivityContent) != null) {
            if (savedInstanceState != null) {
                return;
            }

            Fragment mFrag = null;
            Bundle mArgs = i.getExtras();
            if(toFragment.equals(DefineValue.KANTIN_STUDENT_DETAIL)) {
                mArgs.putString(DefineValue.COUPON_CODE, i.getStringExtra(DefineValue.COUPON_CODE));
                mFrag = new FragKantinStudentDetail();
            }
            else if(toFragment.equals(DefineValue.KANTIN_STUDENT_ORDER)) {
                kantin_name = i.getStringExtra(DefineValue.KANTIN_NAME);
                mArgs.putString(DefineValue.KANTIN_ID, i.getStringExtra(DefineValue.KANTIN_ID));
                mArgs.putString(DefineValue.KANTIN_NAME, kantin_name);
                mFrag = new FragKantinStudentOrder();
            }
            else if(toFragment.equals(DefineValue.KANTIN_OWNER_CEK_KUPON))
                mFrag = new FragKantinCheckKupon();
            else if(toFragment.equals(DefineValue.KANTIN_OWNER_DAFTAR_TERJUAL))
                mFrag = new FragKantinDaftarTerjual();
            else if(toFragment.equals(DefineValue.KANTIN_OWNER_DAFTAR_PEMBELI))
                mFrag = new FragKantinDaftarPembeli();
            else if(toFragment.equals(DefineValue.KANTIN_OWNER_MANAJEMEN_STOK))
                mFrag = new FragKantinManajemenStok();

            mContent = mFrag;
            mFrag.setArguments(mArgs);
            fragmentManager = getSupportFragmentManager();
            android.support.v4.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.add(R.id.kantinActivityContent, mFrag, "fragKantin");
            fragmentTransaction.commitAllowingStateLoss();
            setResult(MainPage.RESULT_NORMAL);
        }
    }

    private void InitializeToolbar(){
        setActionBarIcon(R.drawable.ic_arrow_left);
        setActionBarTitle(getString(R.string.kantin_ab_title));
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_kantin;
    }

    public void switchContent(Fragment curr, Fragment mFragment,String fragName,Boolean isBackstack) {
        ToggleKeyboard.hide_keyboard(this);
        if(isBackstack){
            Timber.d("backstack");
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.kantinActivityContent, mFragment, fragName)
                    .addToBackStack(null)
                    .commitAllowingStateLoss();
        }
        else {
            Timber.d("bukan backstack");
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.kantinActivityContent, mFragment, fragName)
                    .commitAllowingStateLoss();

        }
        setActionBarTitle(fragName);
    }

    public void switchActivity(Intent mIntent, int j) {
        ToggleKeyboard.hide_keyboard(this);
        switch (j){
            case MainPage.ACTIVITY_RESULT:
                startActivityForResult(mIntent,MainPage.REQUEST_FINISH);
                break;
            case 2:
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Timber.d("isi request code:" + String.valueOf(requestCode));
        Timber.d("isi result Code:"+ String.valueOf(resultCode));
        super.onActivityResult(requestCode, resultCode, data);

        switch(requestCode) {
            case MainPage.REQUEST_FINISH:
                if(resultCode == RESULT_SCAN_COUPON){
                    if(mContent instanceof FragKantinCheckKupon){
                        FragKantinCheckKupon mFrag = (FragKantinCheckKupon) mContent;
                        mFrag.setResultFromScanQR(data.getStringExtra(DefineValue.COUPON_CODE));
                    }
                }
                break;
            case RESULT_GALERY:
                if(resultCode == RESULT_OK){
                    Fragment currentFrag = getSupportFragmentManager().findFragmentById(R.id.kantinActivityContent);
                    if(currentFrag instanceof StockBarangAction){
                        Bitmap photo = null;
                        Uri _urinya = data.getData();
                        if(data.getData() == null) {
                            photo = (Bitmap)data.getExtras().get("data");
                        } else {
                            try {
                                photo = MediaStore.Images.Media.getBitmap(getContentResolver(), data.getData());
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                        GeneralizeImage mGI = new GeneralizeImage(this,photo,_urinya);
                        StockBarangAction mFrag = (StockBarangAction) currentFrag;
                        mFrag.setFileImageCatalog(false, mGI.Convert(), _urinya.toString());
                    }

                }
                break;
            case RESULT_CAMERA:
                if(resultCode == RESULT_OK && mCapturedImageURI!=null){
                    Fragment currentFrag = getSupportFragmentManager().findFragmentById(R.id.kantinActivityContent);
                    if(currentFrag instanceof StockBarangAction){
                        String[] projection = {MediaStore.Images.Media.DATA};
                        Cursor cursor = getContentResolver().query(mCapturedImageURI, projection, null, null, null);
                        String filePath;
                        if (cursor != null) {
                            cursor.moveToFirst();
                            int column_index_data = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                            filePath = cursor.getString(column_index_data);
                        }
                        else
                            filePath = data.getData().getPath();
                        GeneralizeImage mGI = new GeneralizeImage(this,filePath);
                        StockBarangAction mFrag = (StockBarangAction) currentFrag;
                        mFrag.setFileImageCatalog(true, mGI.Convert(), filePath);
                        if (cursor != null) {
                            cursor.close();
                        }
                    }
                }
                else{
                    Toast.makeText(this, "Try Again", Toast.LENGTH_LONG).show();
                }
                break;
            default:
                break;
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (mCapturedImageURI != null) {
            outState.putString("cameraImageUri", String.valueOf(mCapturedImageURI));
        }
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (savedInstanceState.containsKey("cameraImageUri")) {
            mCapturedImageURI = Uri.parse(savedInstanceState.getString("cameraImageUri"));
        }
    }

    public void togglerBroadcastReceiver(Boolean _on, BroadcastReceiver _myreceiver){
        Timber.wtf("masuk turnOnBR");
        if(_on){
            IntentFilter filter = new IntentFilter("android.provider.Telephony.SMS_RECEIVED");
            registerReceiver(_myreceiver,filter);
            filter.setPriority(999);
            filter.addCategory("android.intent.category.DEFAULT");
        }
        else unregisterReceiver(_myreceiver);

    }

    public void setResultActivity(int result){
        setResult(result);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        return true;
    }

    public void setTitleFragment(String _title) {
        setActionBarTitle(_title);
    }

    public void setmCapturedImageURI(Uri _uri) {
        mCapturedImageURI = _uri;
    }

}