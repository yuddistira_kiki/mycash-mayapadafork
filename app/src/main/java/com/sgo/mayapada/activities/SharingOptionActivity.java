package com.sgo.mayapada.activities;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.facebook.FacebookException;
import com.facebook.Profile;
import com.securepreferences.SecurePreferences;
import com.sgo.mayapada.R;
import com.sgo.mayapada.coreclass.BaseActivity;
import com.sgo.mayapada.coreclass.CustomSecurePref;
import com.sgo.mayapada.coreclass.DefineValue;
import com.sgo.mayapada.coreclass.FacebookFunction;
import com.sgo.mayapada.dialogs.AlertDialogFrag;
import com.sgo.mayapada.dialogs.DefinedDialog;

import timber.log.Timber;

/*
  Created by Administrator on 10/2/2015.
 */
public class SharingOptionActivity extends BaseActivity implements FacebookFunction.LoginFBListener {

    private FacebookFunction mFaceFunction;

    private ToggleButton tbFacebook;
    private ProgressDialog progdialog;
    private SecurePreferences sp;
    private TextView tvFaceName;
    private String userID;
    private String accessKey;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        InitializeToolbar();
        sp = CustomSecurePref.getInstance().getmSecurePrefs();
        userID = sp.getString(DefineValue.USERID_PHONE,"");
        accessKey = sp.getString(DefineValue.ACCESS_KEY,"");
        progdialog = DefinedDialog.CreateProgressDialog(this,"");
        progdialog.dismiss();

        mFaceFunction = new FacebookFunction(this);

        tvFaceName = (TextView) this.findViewById(R.id.facebook_user_name);
        tbFacebook = (ToggleButton) this.findViewById(R.id.facebook_toggle_btn);


        if(mFaceFunction.getFinalAT() == null) {
            tbFacebook.setChecked(false);
        }
        else {
            tbFacebook.setChecked(true);
        }

        Timber.d("Tracking profile onCreate : "+String.valueOf(mFaceFunction.getmPT().isTracking()));
        Timber.d("Tracking AccessToken onCreate : "+String.valueOf(mFaceFunction.getmATT().isTracking()));

        if(mFaceFunction.isLogin()&&mFaceFunction.getFinalProf() != null){
            Timber.d("isi profil,"+ "isi get name");
            tvFaceName.setText(mFaceFunction.getFinalProf().getName());
        }
        else
            tvFaceName.setText("");

        tbFacebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(tbFacebook.isChecked()){
                    mFaceFunction.loginRead(SharingOptionActivity.this,updateDataFBListener,true);
                    tbFacebook.setEnabled(false);
                }
                else{
                    showLogoutDialog();
                }
            }
        });

        setResult(MainPage.RESULT_NORMAL);
    }

    private void showLogoutDialog(){

        AlertDialogFrag alertDialogFrag = AlertDialogFrag.newInstance(getString(R.string.logout),getString(R.string.shareoption_dialog_msg_logout),
                getString(R.string.ok),getString(R.string.cancel),false);
        alertDialogFrag.setOkListener(new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                tvFaceName.setText("");
                mFaceFunction.logoutWithData(null);
                tbFacebook.setChecked(false);
            }
        });
        alertDialogFrag.setCancelListener(new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                tbFacebook.setChecked(true);
            }
        });

        if(mFaceFunction.isLogin()) {
            alertDialogFrag.show(getSupportFragmentManager(), AlertDialogFrag.TAG);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mFaceFunction.getmCallBackManager().onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_sharing_option;
    }

    private void InitializeToolbar(){
        setActionBarIcon(R.drawable.ic_arrow_left);
        setActionBarTitle(getString(R.string.shareoption_title));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        super.onStop();
        mFaceFunction.getmATT().stopTracking();
        mFaceFunction.getmPT().stopTracking();
    }

    @Override
    public void OnLoginFBSuccess() {
        Timber.d("Tracking profile : "+String.valueOf(mFaceFunction.getmPT().isTracking()));
        Timber.d("Tracking AccessToken : "+String.valueOf(mFaceFunction.getmATT().isTracking()));
        tbFacebook.setEnabled(true);
        progdialog.show();
    }

    @Override
    public void OnLoginFBFailed(FacebookException error) {
        tbFacebook.setEnabled(true);
        tbFacebook.setChecked(false);
        Timber.d("Tracking profile onLoginFbFail : "+String.valueOf(mFaceFunction.getmPT().isTracking()));
        Timber.d("Tracking AccessToken onLoginFbFail : "+String.valueOf(mFaceFunction.getmATT().isTracking()));
        if(error != null) {
            Toast.makeText(SharingOptionActivity.this, error.getMessage(), Toast.LENGTH_LONG).show();
            Log.d("FacebookException", error.toString());
        }
    }

    @Override
    public void OnProfileChange() {
        tvFaceName.setText(Profile.getCurrentProfile().getName());
    }

    private FacebookFunction.UpdateDataFBListener updateDataFBListener =  new FacebookFunction.UpdateDataFBListener() {
        @Override
        public void OnUpdateDataFBSuccess() {
            SharingOptionActivity.this.setResult(MainPage.RESULT_REFRESH_NAVDRAW);
            if(progdialog.isShowing())
                progdialog.dismiss();
        }

        @Override
        public void OnUpdateDataFBFailed() {
            if(progdialog.isShowing())
                progdialog.dismiss();
        }
    };

}