package com.sgo.mayapada.activities;

import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.Menu;
import android.widget.TextView;

import com.sgo.mayapada.R;
import com.sgo.mayapada.coreclass.BaseActivity;
import com.sgo.mayapada.coreclass.DefineValue;
import com.sgo.mayapada.coreclass.ToggleKeyboard;
import com.sgo.mayapada.fragments.FragTagihan;

import timber.log.Timber;

/**
 * Created by thinkpad on 5/12/2016.
 */
public class TagihanActivity extends BaseActivity {

    private FragmentManager fragmentManager;
    private String comm_id;
    private String comm_name;
    private String comm_code;
    private String api_key;
    private String callback_url;
    private String cust_name;
    private TextView pairing_id_title_toolbar;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        pairing_id_title_toolbar = (TextView) findViewById(R.id.pairing_id_info_toolbar);

        Intent i = getIntent();
        comm_id = i.getStringExtra(DefineValue.COMMUNITY_ID);
        comm_code = i.getStringExtra(DefineValue.COMMUNITY_CODE);
        comm_name = i.getStringExtra(DefineValue.COMMUNITY_NAME);
        api_key = i.getStringExtra(DefineValue.API_KEY);
        callback_url = i.getStringExtra(DefineValue.CALLBACK_URL);
        cust_name = i.getStringExtra(DefineValue.CUST_NAME);

        InitializeToolbar();

        if (findViewById(R.id.tagihanActivityContent) != null) {
            if (savedInstanceState != null) {
                return;
            }

            Fragment mFrag = new FragTagihan();
            Bundle mArgs = i.getExtras();
            mArgs.putString(DefineValue.COMMUNITY_ID, comm_id);
            mArgs.putString(DefineValue.COMMUNITY_CODE, comm_code);
            mArgs.putString(DefineValue.COMMUNITY_NAME, comm_name);
            mArgs.putString(DefineValue.API_KEY, api_key);
            mArgs.putString(DefineValue.CALLBACK_URL, callback_url);
            mArgs.putString(DefineValue.CUST_NAME, cust_name);
            mArgs.putString("pairing_id", i.getStringExtra("pairing_id"));

            mFrag.setArguments(mArgs);
            fragmentManager = getSupportFragmentManager();
            android.support.v4.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.add(R.id.tagihanActivityContent, mFrag, "fragTagihan");
            fragmentTransaction.commitAllowingStateLoss();
            setResult(MainPage.RESULT_NORMAL);
        }
    }

    private void InitializeToolbar(){
        setActionBarIcon(R.drawable.ic_arrow_left);
        setActionBarTitle(getString(R.string.tagihan_ab_title));
    }

    public void setToolbarTitle(String _title) {
        setActionBarTitle(_title);
    }

    public void setPairingIdTitle(String _title) {
        pairing_id_title_toolbar.setText(_title);
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_tagihan;
    }

    public void switchContent(Fragment mFragment,String fragName,Boolean isBackstack) {
        ToggleKeyboard.hide_keyboard(this);
        if(isBackstack){
            Timber.d("backstack");
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.tagihanActivityContent, mFragment, fragName)
                    .addToBackStack(null)
                    .commitAllowingStateLoss();
        }
        else {
            Timber.d("bukan backstack");
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.tagihanActivityContent, mFragment, fragName)
                    .commitAllowingStateLoss();

        }
        setActionBarTitle(fragName);
    }

    public void switchActivity(Intent mIntent, int j) {
        ToggleKeyboard.hide_keyboard(this);
        switch (j){
            case MainPage.ACTIVITY_RESULT:
                startActivityForResult(mIntent,MainPage.REQUEST_FINISH);
                break;
            case 2:
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Timber.d("isi request code:" + String.valueOf(requestCode));
        Timber.d("isi result Code:"+ String.valueOf(resultCode));
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == MainPage.REQUEST_FINISH) {
            if (resultCode == MainPage.RESULT_BALANCE) {
                setResult(MainPage.RESULT_BALANCE);
                finish();
            }
            else if(resultCode == MainPage.RESULT_NORMAL){
                getSupportFragmentManager().popBackStack();
            }
            else if (resultCode == MainPage.RESULT_LOGOUT) {
                setResult(MainPage.RESULT_LOGOUT);
                finish();
            }

        }
    }

    public void togglerBroadcastReceiver(Boolean _on, BroadcastReceiver _myreceiver){
        Timber.wtf("masuk turnOnBR");
        if(_on){
            IntentFilter filter = new IntentFilter("android.provider.Telephony.SMS_RECEIVED");
            registerReceiver(_myreceiver,filter);
            filter.setPriority(999);
            filter.addCategory("android.intent.category.DEFAULT");
        }
        else unregisterReceiver(_myreceiver);

    }

    public void setResultActivity(int result){
        setResult(MainPage.RESULT_BALANCE);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        return true;
    }

    public void setTitleFragment(String _title) {
        setActionBarTitle(_title);
    }
}