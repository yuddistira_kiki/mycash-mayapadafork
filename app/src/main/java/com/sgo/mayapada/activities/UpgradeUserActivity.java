package com.sgo.mayapada.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import com.sgo.mayapada.R;
import com.sgo.mayapada.coreclass.BaseActivity;
import com.sgo.mayapada.coreclass.DefineValue;
import com.sgo.mayapada.coreclass.ToggleKeyboard;
import com.sgo.mayapada.fragments.UpgradeUserConfirm;

import timber.log.Timber;

/**
 * Created by thinkpad on 8/14/2017.
 */

public class UpgradeUserActivity extends BaseActivity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        InitializeToolbar();

        if (findViewById(R.id.levelFragmentContent) != null) {
            if (savedInstanceState != null) {
                return;
            }

            Fragment mFrag = new UpgradeUserConfirm();
            Bundle args = new Bundle();
            args.putString(DefineValue.CUST_PHONE, getIntent().getStringExtra(DefineValue.CUST_PHONE));
            args.putString(DefineValue.CUST_NAME, getIntent().getStringExtra(DefineValue.CUST_NAME));
            args.putString(DefineValue.PROFILE_ADDRESS, getIntent().getStringExtra(DefineValue.PROFILE_ADDRESS));
            args.putString(DefineValue.PROFILE_ID_TYPE, getIntent().getStringExtra(DefineValue.PROFILE_ID_TYPE));
            args.putString(DefineValue.ID_NUMBER, getIntent().getStringExtra(DefineValue.ID_NUMBER));
            args.putString(DefineValue.PROFILE_BIRTH_PLACE, getIntent().getStringExtra(DefineValue.PROFILE_BIRTH_PLACE));
            args.putString(DefineValue.PROFILE_DOB, getIntent().getStringExtra(DefineValue.PROFILE_DOB));
            args.putString(DefineValue.PROFILE_GENDER, getIntent().getStringExtra(DefineValue.PROFILE_GENDER));
            args.putString(DefineValue.PROFILE_BOM, getIntent().getStringExtra(DefineValue.PROFILE_BOM));
            args.putString(DefineValue.ACCOUNT_NUMBER, getIntent().getStringExtra(DefineValue.ACCOUNT_NUMBER));
            mFrag.setArguments(args);
            FragmentManager fragmentManager = getSupportFragmentManager();
            android.support.v4.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.add(R.id.levelFragmentContent, mFrag, getString(R.string.toolbar_title_upgrade_user));
            fragmentTransaction.commitAllowingStateLoss();
            setResult(MainPage.RESULT_NORMAL);
        }
    }

    private void InitializeToolbar() {
        setActionBarIcon(R.drawable.ic_arrow_left);
        setActionBarTitle(getString(R.string.toolbar_title_upgrade_user));
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_level_form_register;
    }

    public void switchContent(Fragment mFragment, String fragName, Boolean isBackstack) {
        ToggleKeyboard.hide_keyboard(this);
        if (isBackstack) {
            Timber.d("backstack");
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.levelFragmentContent, mFragment, fragName)
                    .addToBackStack(null)
                    .commitAllowingStateLoss();
        } else {
            Timber.d("bukan backstack");
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.levelFragmentContent, mFragment, fragName)
                    .commitAllowingStateLoss();

        }
        setActionBarTitle(fragName);
    }

    public void switchActivity(Intent mIntent, int j) {
        ToggleKeyboard.hide_keyboard(this);
        switch (j) {
            case MainPage.ACTIVITY_RESULT:
                startActivityForResult(mIntent, MainPage.REQUEST_FINISH);
                break;
            case 2:
                break;
        }
    }
}