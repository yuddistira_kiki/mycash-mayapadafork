package com.sgo.mayapada.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.sgo.mayapada.Beans.KantinStudentDetailModel;
import com.sgo.mayapada.R;
import com.sgo.mayapada.coreclass.CurrencyFormat;

import java.util.ArrayList;

/**
 * Created by thinkpad on 8/8/2016.
 */
public class KantinStudentDetailAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<KantinStudentDetailModel> data;
    private String ccy_id;

    public KantinStudentDetailAdapter(Context context, ArrayList<KantinStudentDetailModel> data, String ccy_id) {
        this.context = context;
        this.data = data;
        this.ccy_id = ccy_id;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        if(data != null && data.size() != 0){
            return data.size();
        }
        return 0;
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final ViewHolder holder;
        if (convertView == null) {

            holder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(context);
            convertView = inflater.inflate(R.layout.list_kantin_student_detail_item, parent, false);
            holder.title = (TextView) convertView.findViewById(R.id.txt_title);
            holder.amount = (TextView) convertView.findViewById(R.id.txt_amount);

            convertView.setTag(holder);

        } else {

            holder = (ViewHolder) convertView.getTag();
        }

        holder.title.setText(data.get(position).getName() + " x " + data.get(position).getCount());
//        double total = Double.parseDouble(data.get(position).getCount()) * Double.parseDouble(data.get(position).getAmount());
        holder.amount.setText(ccy_id + " " + CurrencyFormat.format(data.get(position).getAmount()));


        return convertView;
    }

    private class ViewHolder {
        TextView title, amount;
    }
}
