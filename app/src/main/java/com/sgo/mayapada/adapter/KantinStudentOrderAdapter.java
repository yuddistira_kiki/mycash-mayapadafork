package com.sgo.mayapada.adapter;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.sgo.mayapada.Beans.KantinStudentOrderModel;
import com.sgo.mayapada.R;
import com.sgo.mayapada.coreclass.CurrencyFormat;
import com.sgo.mayapada.coreclass.MyApiClient;
import com.sgo.mayapada.coreclass.MyPicasso;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by thinkpad on 8/4/2016.
 */
public class KantinStudentOrderAdapter extends BaseAdapter {
    private View v;
    private Context context;
    private ArrayList<KantinStudentOrderModel> data;
    private AdapterInterface listener;
    private int total = 0 ;

    public interface AdapterInterface
    {
        void onChange(String value);
    }

    public KantinStudentOrderAdapter(View v, AdapterInterface listener, Context context, ArrayList<KantinStudentOrderModel> data) {
        this.v = v;
        this.listener = listener;
        this.context = context;
        this.data = data;
    }
    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        if(data != null && data.size() != 0){
            return data.size();
        }
        return 0;
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final ViewHolder holder;
        if (convertView == null) {

            holder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(context);
            convertView = inflater.inflate(R.layout.list_kantin_student_order_item, parent, false);
            holder.img = (ImageView) convertView.findViewById(R.id.img_catalog);
            holder.title = (TextView) convertView.findViewById(R.id.txt_title);
            holder.price = (TextView) convertView.findViewById(R.id.txt_price);
            holder.stock = (TextView) convertView.findViewById(R.id.txt_stock);
            holder.order = (EditText) convertView.findViewById(R.id.et_input_order);

            convertView.setTag(holder);

        } else {

            holder = (ViewHolder) convertView.getTag();
        }

        holder.ref = position;

        Picasso mPic = MyPicasso.getImageLoader(context);

        String _url = data.get(position).getImg_url();
        if(_url != null && _url.isEmpty()){
            mPic.load(R.drawable.icon_no_photo)
                    .fit().centerInside()
                    .placeholder(R.drawable.progress_animation)
                    .into(holder.img);
        }
        else {
            mPic.load(_url)
                    .error(R.drawable.icon_no_photo)
                    .fit().centerInside()
                    .placeholder(R.drawable.progress_animation)
                    .into(holder.img);
        }

        holder.title.setText(data.get(position).getTitle());
        holder.price.setText(data.get(position).getCcy_id() + ". " + CurrencyFormat.format(data.get(position).getPrice()));
        if(data.get(position).getStock_type().equalsIgnoreCase("S")) {
            holder.stock.setVisibility(View.VISIBLE);
            holder.stock.setText("Stok : " + data.get(position).getQty());
        }
        else {
            holder.stock.setVisibility(View.GONE);
        }
        holder.order.setText(data.get(position).getCount());
        holder.order.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
                // TODO Auto-generated method stub

            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                          int arg3) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

                if(holder.order.hasFocus()) {
//                    if (s.toString().equals("") || Integer.parseInt(s.toString()) < 1) {
//                        data.get(holder.ref).setCount(s.toString());
//                    } else {
                        data.get(holder.ref).setCount(s.toString());
                        total = 0;
                        for (int i = 0; i < data.size(); i++) {
                            if (!data.get(i).getCount().equals("") && !data.get(i).getCount().equals("0"))
                                total += Integer.parseInt(data.get(i).getCount()) * Integer.parseInt(data.get(i).getPrice());
                        }
                        if (listener != null)
                            listener.onChange(Integer.toString(total));
//                    }
                }

            }
        });

        return convertView;
    }

    private class ViewHolder {
        ImageView img;
        TextView title, price, stock;
        EditText order;
        int ref;
    }
}
