package com.sgo.mayapada.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.sgo.mayapada.Beans.ResultTagihanModel;
import com.sgo.mayapada.R;
import com.sgo.mayapada.coreclass.CurrencyFormat;

import java.util.ArrayList;

/**
 * Created by thinkpad on 5/16/2016.
 */
public class PreviewTagihanAdapter extends BaseAdapter {

    Context context;
    private ArrayList<ResultTagihanModel> data = null;
    private LayoutInflater mInflater;

    public PreviewTagihanAdapter(Context context, ArrayList<ResultTagihanModel> objects) {
//        super(context, resource, objects);
        mInflater = LayoutInflater.from(context);
        this.data = objects;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        ViewHolder holder;
        if(convertView == null) {
            view = mInflater.inflate(R.layout.list_preview_tagihan_item, parent, false);
            holder = new ViewHolder();
            holder.name = (TextView)view.findViewById(R.id.txt_name);
            holder.amount = (TextView)view.findViewById(R.id.txt_amount);
            holder.remark = (TextView)view.findViewById(R.id.txt_remark);
            view.setTag(holder);
        } else {
            view = convertView;
            holder = (ViewHolder)view.getTag();
        }

        holder.name.setText("Nama Tagihan : " + data.get(position).getDoc_name());
        if(data.get(position).getCcy_id().equalsIgnoreCase("idr"))
            holder.amount.setText("Jumlah : Rp. " + CurrencyFormat.format(data.get(position).getPayment_amount()));
        else
            holder.amount.setText("Jumlah : " + data.get(position).getCcy_id() + " " + data.get(position).getPayment_amount());
        if(data.get(position).getRemark().equals(""))
            holder.remark.setText("Keterangan : -");
        else
            holder.remark.setText("Keterangan : " + data.get(position).getRemark());

        return view;
    }

    private class ViewHolder {
        public TextView name, amount, remark;
    }
}