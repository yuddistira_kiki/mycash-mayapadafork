package com.sgo.mayapada.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.sgo.mayapada.R;
import com.sgo.mayapada.fragments.PayByQR;

/**
 * Created by Lenovo Thinkpad on 9/20/2016.
 */
public class PayByQRTypeAdapter extends BaseAdapter {
    private Activity activity;

    public PayByQRTypeAdapter(Activity act) {
        this.activity = act;
    }

    @Override
    public int getCount() {
        return PayByQR.paybyqr_type.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ListHolder holder;

        if(convertView == null){
            LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.list_payqrtype, parent, false);
            holder = new ListHolder();

            convertView.setTag(holder);
        }else{
            holder = (ListHolder) convertView.getTag();
        }

        holder.qrtype            = (TextView) convertView.findViewById(R.id.qrtype);
        holder.qrtype.setText(PayByQR.paybyqr_type.get(position));

        convertView.setTag(holder);

        return convertView;
    }

    class ListHolder
    {
        TextView qrtype;
    }
}
