package com.sgo.mayapada.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.sgo.mayapada.Beans.KantinStudentOrderModel;
import com.sgo.mayapada.R;
import com.sgo.mayapada.coreclass.CurrencyFormat;

import java.util.ArrayList;

/**
 * Created by thinkpad on 10/12/2016.
 */

public class KantinConfirmOrderAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<KantinStudentOrderModel> data;

    public KantinConfirmOrderAdapter(Context context, ArrayList<KantinStudentOrderModel> data) {
        this.context = context;
        this.data = data;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        if (data != null && data.size() != 0) {
            return data.size();
        }
        return 0;
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final ViewHolder holder;
        if (convertView == null) {

            holder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(context);
            convertView = inflater.inflate(R.layout.list_kantin_confirm_order_item, parent, false);
            holder.title = (TextView) convertView.findViewById(R.id.txt_title);
            holder.amount = (TextView) convertView.findViewById(R.id.txt_amount);
            holder.total = (TextView) convertView.findViewById(R.id.txt_total);

            convertView.setTag(holder);
        } else {

            holder = (ViewHolder) convertView.getTag();
        }

        holder.title.setText(data.get(position).getTitle() + " x " + data.get(position).getCount());
        double total = Double.parseDouble(data.get(position).getCount()) * Double.parseDouble(data.get(position).getPrice());
        holder.amount.setText(data.get(position).getCcy_id() + " " + CurrencyFormat.format(data.get(position).getPrice()));
        holder.total.setText(data.get(position).getCcy_id() + " " + CurrencyFormat.format(total));

        return convertView;
    }

    private class ViewHolder {
        TextView title, amount, total;
    }
}