package com.sgo.mayapada.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import com.sgo.mayapada.Beans.SplitbillRecipient;
import com.sgo.mayapada.R;
import com.sgo.mayapada.coreclass.MyApiClient;
import com.sgo.mayapada.coreclass.MyPicasso;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by thinkpad on 7/31/2017.
 */

public class SplitbillRecipientAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<SplitbillRecipient> data;
    private AdapterInterface listener;
    private long total = 0;
    private long totalAmountListRecipient = 0;
    private AlertDialog dialogOver = null;
    private String amountBeforeChange;

    public interface AdapterInterface
    {
        void onDelete(String value);
        void onChangeMyAmount(String value);
    }

    public SplitbillRecipientAdapter(AdapterInterface listener, Context context, ArrayList<SplitbillRecipient> data) {
        this.listener = listener;
        this.context = context;
        this.data = data;
    }
    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        if(data != null && data.size() != 0){
            return data.size();
        }
        return 0;
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(context);
            convertView = inflater.inflate(R.layout.list_splitbill_recipient_item, parent, false);
            holder.img = (ImageView) convertView.findViewById(R.id.splitbill_recipient_img);
            holder.delete = (ImageView) convertView.findViewById(R.id.splitbill_recipient_delete);
            holder.name = (TextView) convertView.findViewById(R.id.splitbill_recipient_name);
            holder.hp = (TextView) convertView.findViewById(R.id.splitbill_recipient_hp);
            holder.amount = (EditText) convertView.findViewById(R.id.splitbill_recipient_amount);

            convertView.setTag(holder);

        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.ref = position;

        Picasso mPic = MyPicasso.getImageLoader(context);

        String _url = data.get(position).getImg_url();
        if(_url != null && _url.isEmpty()){
            mPic.load(R.drawable.icon_no_photo)
                    .fit().centerInside()
                    .placeholder(R.drawable.progress_animation)
                    .into(holder.img);
        }
        else {
            mPic.load(_url)
                    .error(R.drawable.icon_no_photo)
                    .fit().centerInside()
                    .placeholder(R.drawable.progress_animation)
                    .into(holder.img);
        }

        holder.name.setText(data.get(position).getName());
        holder.hp.setText(data.get(position).getHp_number());
        if(data.get(position).getAmount().equals("0"))
            holder.amount.setText("");
        else
            holder.amount.setText(data.get(position).getAmount());

        holder.amount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                amountBeforeChange = new StringBuilder(charSequence).toString();
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if(holder.amount.hasFocus()) {
                    if (editable.toString().equals("") || Integer.parseInt(editable.toString()) < 1) {
                        data.get(holder.ref).setAmount("0");
                    } else {
                        data.get(holder.ref).setAmount(editable.toString());
                    }

                    countTotalAmountListRecipient();

                    if(totalAmountListRecipient > total) {
                        showDialogTotalTagihanOver();
                        data.get(holder.ref).setAmount(amountBeforeChange);
                        notifyDataSetChanged();
                    }
                    else {
                        if (listener != null)
                            listener.onChangeMyAmount(Long.toString(total - totalAmountListRecipient));
                    }
                }
            }
        });

        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                data.remove(holder.ref);
                notifyDataSetChanged();
                countTotalAmountListRecipient();
                if (listener != null)
                    listener.onDelete(Long.toString(totalAmountListRecipient));
            }
        });

        return convertView;
    }

    private void countTotalAmountListRecipient() {
        totalAmountListRecipient = 0;
        for(int i = 0 ; i < data.size() ; i++) {
            totalAmountListRecipient += Long.parseLong(data.get(i).getAmount());
        }
    }

    public void setTotal(String total){
        if(!total.equals(""))
            this.total = Long.parseLong(total);
        else
            this.total = 0;
    }

    private void showDialogTotalTagihanOver() {
        if(dialogOver == null) {
            AlertDialog.Builder builder = new AlertDialog.Builder(context)
                    .setMessage(context.getString(R.string.splitbill_total_tagihan_minus_message))
                    .setCancelable(false)
                    .setPositiveButton(context.getString(R.string.ok), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    });
            dialogOver = builder.create();
            dialogOver.show();
        }
        else if(!dialogOver.isShowing())
            dialogOver.show();
    }

    private class ViewHolder {
        ImageView img, delete;
        TextView name, hp;
        EditText amount;
        int ref;
    }
}
