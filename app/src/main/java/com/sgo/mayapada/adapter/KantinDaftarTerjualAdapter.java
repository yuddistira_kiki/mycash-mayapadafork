package com.sgo.mayapada.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sgo.mayapada.Beans.KantinDaftarTerjualModel;
import com.sgo.mayapada.R;
import com.sgo.mayapada.fragments.FragKantinDaftarTerjual;

import java.util.ArrayList;

/**
 * Created by thinkpad on 8/10/2016.
 */
public class KantinDaftarTerjualAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<KantinDaftarTerjualModel> data;
    private ViewHolder holder;
    private FragKantinDaftarTerjual fragment;

    public KantinDaftarTerjualAdapter(Context context, ArrayList<KantinDaftarTerjualModel> data, FragKantinDaftarTerjual fragment) {
        this.fragment = fragment;
        this.context = context;
        this.data = data;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        if(data != null && data.size() != 0){
            return data.size();
        }
        return 0;
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            holder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(context);
            convertView = inflater.inflate(R.layout.list_kantin_daftar_terjual_item, parent, false);
            holder.item_layout = (RelativeLayout) convertView.findViewById(R.id.list_item_layout);
//            holder.img_stop = (ImageView) convertView.findViewById(R.id.img_stop);
            holder.name = (TextView) convertView.findViewById(R.id.name_value);
            holder.terjual = (TextView) convertView.findViewById(R.id.terjual_value);
            holder.terambil = (TextView) convertView.findViewById(R.id.terambil_value);
            holder.belum_terambil = (TextView) convertView.findViewById(R.id.belum_terambil_value);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.name.setText(data.get(position).getTitle());
        holder.terambil.setText(data.get(position).getTerambil());
        holder.terjual.setText(data.get(position).getTerjual());
        holder.belum_terambil.setText(data.get(position).getBelum_terambil());

        if(data.get(position).isStop()) {
            holder.item_layout.setBackgroundColor(Color.parseColor("#bdbdbd"));
//            holder.img_stop.setImageResource(R.drawable.ic_dollar);
        }
        else {
            holder.item_layout.setBackgroundColor(Color.parseColor("#F1F1F1"));
//            holder.img_stop.setImageResource(R.drawable.ic_stop);
        }
//        holder.img_stop.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                fragment.sentActiveDeactive(position);
//            }
//        });

        return convertView;
    }

//    public void setActiveDeactive(String status, int position){
//        if(status.equalsIgnoreCase("A")) {
//            data.get(position).setIsStop(false);
//            holder.item_layout.setBackgroundColor(Color.parseColor("#F1F1F1"));
//            holder.img_stop.setImageResource(R.drawable.ic_stop);
//        }
//        else {
//            data.get(position).setIsStop(true);
//            holder.item_layout.setBackgroundColor(Color.parseColor("#bdbdbd"));
//            holder.img_stop.setImageResource(R.drawable.ic_dollar);
//        }
//        notifyDataSetChanged();
//    }

    private class ViewHolder {
        RelativeLayout item_layout;
        TextView name, terjual, terambil, belum_terambil;
//        ImageView img_stop;
    }
}
