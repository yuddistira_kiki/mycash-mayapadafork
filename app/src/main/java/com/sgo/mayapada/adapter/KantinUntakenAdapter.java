package com.sgo.mayapada.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.sgo.mayapada.Beans.KantinUntakenModel;
import com.sgo.mayapada.R;
import com.sgo.mayapada.coreclass.ListViewHeight;
import com.sgo.mayapada.coreclass.WebParams;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

/**
 * Created by thinkpad on 10/3/2016.
 */

public class KantinUntakenAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<KantinUntakenModel> data;
    private ArrayList<String> items = new ArrayList<>();
    private KantinUntakenItemAdapter adapter;
    private int rowLayout;

    public KantinUntakenAdapter(Context context, ArrayList<KantinUntakenModel> data, int _rowLayout) {
        this.context = context;
        this.data = data;
        rowLayout = _rowLayout;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        if(data != null && data.size() != 0){
            return data.size();
        }
        return 0;
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(context);
            convertView = inflater.inflate(rowLayout, null);

            holder.no_hp = (TextView) convertView.findViewById(R.id.tv_no_hp);
            holder.lv_order = (ListView) convertView.findViewById(R.id.lv_order);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        KantinUntakenModel _data = data.get(position);

        holder.no_hp.setText(_data.getNo_hp());

        String item = _data.getItem();
        try {
            items = new ArrayList<>();
            JSONArray array = new JSONArray(item);
            for(int i = 0 ; i < array.length() ; i++) {
                items.add(array.getJSONObject(i).getString(WebParams.ITEM_NAME) + " x " +
                        array.getJSONObject(i).getString(WebParams.QTY));
            }

            adapter = new KantinUntakenItemAdapter(items, context);
            holder.lv_order.setAdapter(adapter);
            ListViewHeight.setTotalHeightofListView(holder.lv_order);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return convertView;
    }

    private class ViewHolder {
        TextView no_hp;
        ListView lv_order;
    }
}

