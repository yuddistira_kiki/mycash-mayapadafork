package com.sgo.mayapada.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.sgo.mayapada.Beans.ReportListLKDModel;
import com.sgo.mayapada.R;
import com.sgo.mayapada.coreclass.CurrencyFormat;
import com.sgo.mayapada.coreclass.DateTimeFormat;

import java.util.ArrayList;

/**
 * Created by thinkpad on 7/25/2016.
 */
public class ReportListLKDAdapter extends ArrayAdapter<ReportListLKDModel> {

    private Context context;
    private int layoutResourceId;
    private ArrayList<ReportListLKDModel> data = null;

    public ReportListLKDAdapter(Context context, int resource, ArrayList<ReportListLKDModel> objects) {
        super(context, resource, objects);
        this.layoutResourceId = resource;
        this.context = context;
        this.data = objects;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        View row = convertView;
        ListHolder holder;

        if(row == null)
        {
            LayoutInflater inflater = ((Activity)context).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);

            holder = new ListHolder();
            holder.tv_date = (TextView)row.findViewById(R.id.text_tgl_trans);
            holder.tv_bbs_type = (TextView)row.findViewById(R.id.text_bbs_type);
            holder.tv_comm_name = (TextView)row.findViewById(R.id.text_comm_name);
            holder.tv_ccy = (TextView)row.findViewById(R.id.text_ccyID);
            holder.tv_comm_fee = (TextView)row.findViewById(R.id.text_comm_fee);
            holder.tv_tx_status = (TextView)row.findViewById(R.id.text_tx_status);

            row.setTag(holder);
        }
        else
        {
            holder = (ListHolder)row.getTag();
        }

        ReportListLKDModel itemnya = data.get(position);

        holder.tv_date.setText(DateTimeFormat.formatToID(itemnya.getDatetime()));
        holder.tv_bbs_type.setText(itemnya.getBbs_type());
        holder.tv_comm_name.setText(itemnya.getComm_name());
        holder.tv_ccy.setText(itemnya.getCcy_id());

//        Double total = Double.parseDouble(itemnya.getAmount()) + Double.parseDouble(itemnya.getAdmin_fee());

        holder.tv_comm_fee.setText(CurrencyFormat.format(itemnya.getComm_fee()));
        holder.tv_tx_status.setText(itemnya.getTx_status());

        return row;
    }

    class ListHolder
    {
        TextView tv_bbs_type,tv_date,tv_comm_name,tv_ccy,tv_comm_fee, tv_tx_status;
    }

}

