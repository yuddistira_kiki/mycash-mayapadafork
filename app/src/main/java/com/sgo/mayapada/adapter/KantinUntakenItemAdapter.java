package com.sgo.mayapada.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.sgo.mayapada.R;

import java.util.ArrayList;

/**
 * Created by thinkpad on 10/4/2016.
 */

class KantinUntakenItemAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<String> data;

    public KantinUntakenItemAdapter(ArrayList<String> data, Context context) {
        this.context = context;
        this.data = data;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        if(data != null && data.size() != 0){
            return data.size();
        }
        return 0;
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            holder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(context);
            convertView = inflater.inflate(R.layout.list_kantin_untaken_item, parent, false);

            holder.text = (TextView) convertView.findViewById(R.id.tv_item);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.text.setText(data.get(position));

        return convertView;
    }

    private class ViewHolder {
        TextView text;
    }
}
