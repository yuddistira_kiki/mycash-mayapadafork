package com.sgo.mayapada.adapter;/*
  Created by Administrator on 1/18/2015.
 */

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.sgo.mayapada.entityRealm.List_Account_Nabung;
import com.sgo.mayapada.R;

import java.util.ArrayList;

public class ListRekeningTabunganAdapter extends ArrayAdapter<List_Account_Nabung> {

    public interface OnDeleteListener{
        void onCLick(int position, View view);
    }

    private Context context;
    private int layoutResourceId;
    int selectedRadio = 0;
    private ArrayList<List_Account_Nabung> adata;
    private Boolean showDelete = false;
    private OnDeleteListener onDeleteListener;

    public ListRekeningTabunganAdapter(Context context, int layoutResourceId, ArrayList<List_Account_Nabung> data) {
        super(context, layoutResourceId, data);
        this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.adata = data;
    }

    @Override
    public int getCount() {
        return this.adata.size();
    }

    @NonNull
    @Override
    public View getView(final int position, View convertView, @NonNull ViewGroup parent) {

        View row = convertView;
        ListHolder holder;
        parent.setClickable(true);

        if(row == null) {
            LayoutInflater inflater = ((Activity)context).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);

            holder = new ListHolder();
            holder.txtBankName = (TextView)row.findViewById(R.id.bank_name_value);
            holder.txtAccountId = (TextView)row.findViewById(R.id.account_id_value);
            holder.txtAccountName = (TextView)row.findViewById(R.id.account_name_value);
            holder.radioDefault = (RadioButton) row.findViewById(R.id.radio_item_rek);
            holder.btn_delete = (Button) row.findViewById(R.id.btn_delete);

            holder.btn_delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onDeleteListener.onCLick(position,v);
                }
            });

            row.setTag(holder);
        }
        else {
            holder = (ListHolder)row.getTag();
        }

        holder.txtBankName.setText(adata.get(position).getBank_name());
        holder.txtAccountId.setText(adata.get(position).getAcct_no());
        holder.txtAccountName.setText(adata.get(position).getAcct_name());
        Boolean test = adata.get(position).isDefault();
        ((ListView) parent).setItemChecked(position, test);


        if(showDelete)
            holder.btn_delete.setVisibility(View.VISIBLE);
        else
            holder.btn_delete.setVisibility(View.GONE);

        return row;
    }

    public void setDeleteListener(OnDeleteListener _deleteListener){
        this.onDeleteListener = _deleteListener;
    }


    public void toggleButtonDelete(){
        showDelete = !showDelete;
        notifyDataSetChanged();
    }

    public void setButtonDeleteHide(){
        showDelete = false;
    }

    class ListHolder
    {
        TextView txtBankName,txtAccountId, txtAccountName;
        RadioButton radioDefault;
        Button btn_delete;
    }
}
