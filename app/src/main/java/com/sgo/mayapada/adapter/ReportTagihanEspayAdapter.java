package com.sgo.mayapada.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.sgo.mayapada.Beans.TrxStatusTagihanBean;
import com.sgo.mayapada.R;

import java.util.ArrayList;

/**
 * Created by thinkpad on 8/28/2015.
 */
public class ReportTagihanEspayAdapter extends BaseAdapter{
    private Activity activity;
    private ArrayList<TrxStatusTagihanBean> data;

    public ReportTagihanEspayAdapter(Activity activity, ArrayList<TrxStatusTagihanBean> data){
        this.activity = activity;
        this.data = data;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        ViewHolder holder;

        if(convertView == null){
            LayoutInflater inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.list_report_tx_tagihan_item, parent, false);
            holder = new ViewHolder();

            convertView.setTag(holder);
        }else{
            holder = (ViewHolder) convertView.getTag();
        }
        holder.tvTxId    = (TextView) convertView.findViewById(R.id.reportitem_tx_id);
        holder.tvTxDate = (TextView) convertView.findViewById(R.id.reportitem_tx_date);
        holder.tvTxAmount = (TextView) convertView.findViewById(R.id.reportitem_tx_amount);
        holder.tvTxRemark = (TextView) convertView.findViewById(R.id.reportitem_tx_remark);
        holder.tvTxStatus = (TextView) convertView.findViewById(R.id.reportitem_tx_status);

        holder.tvTxId.setText(data.get(position).getTx_id());
        holder.tvTxDate.setText(data.get(position).getTx_date());
        holder.tvTxAmount.setText(data.get(position).getCcy_id() + " " + data.get(position).getTx_amount());
        holder.tvTxRemark.setText(data.get(position).getTx_remark());
        if(data.get(position).getTx_status().equalsIgnoreCase("S"))
            holder.tvTxStatus.setText("Success");
        else if(data.get(position).getTx_status().equalsIgnoreCase("F"))
            holder.tvTxStatus.setText("Failed");
        else if(data.get(position).getTx_status().equalsIgnoreCase("SP"))
            holder.tvTxStatus.setText("Suspect");
        else
            holder.tvTxStatus.setText("");

        return convertView;
    }

    static class ViewHolder {
        TextView tvTxId, tvTxDate, tvTxAmount, tvTxRemark, tvTxStatus;
    }
}
