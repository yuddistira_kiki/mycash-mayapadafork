package com.sgo.mayapada.adapter;/*
  Created by Administrator on 1/18/2015.
 */

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.sgo.mayapada.R;

import java.util.ArrayList;
import java.util.Arrays;

public class EasyAdapter extends ArrayAdapter<String> {

    private Context context;
    private int layoutResourceId;
    private ArrayList<String> adata, list_pairid;

    public EasyAdapter(Context context, int layoutResourceId, String[] data) {
        super(context, layoutResourceId, data);
        this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.adata =  new ArrayList<>();
        this.adata.addAll(Arrays.asList(data));
    }

    public EasyAdapter(Context context, int layoutResourceId, ArrayList<String> data) {
        super(context, layoutResourceId, data);
        this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.adata = data;
    }

    public EasyAdapter(Context context, int layoutResourceId, ArrayList<String> data, ArrayList<String> pair_id) {
        super(context, layoutResourceId, data);
        this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.adata = data;
        this.list_pairid = pair_id;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        View row = convertView;
        ListHolder holder;

        if(row == null)
        {
            LayoutInflater inflater = ((Activity)context).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);

            holder = new ListHolder();
            holder.txtTitle = (TextView)row.findViewById(R.id.txtTitleList);
            holder.txtPairID = (TextView) row.findViewById(R.id.pairing_id_info_inv_adapter);

        row.setTag(holder);
        } else {
            holder = (ListHolder)row.getTag();
        }

        holder.txtTitle.setText(adata.get(position));
        if (list_pairid != null) {
            if (list_pairid.get(position).equals("null")){
                holder.txtPairID.setText("");
                holder.txtPairID.setVisibility(View.INVISIBLE);
            }else {
                String pair_id_string = context.getResources().getString(R.string.pairing_id_info_adapter) + " " + list_pairid.get(position);
                holder.txtPairID.setText(pair_id_string);
                holder.txtPairID.setVisibility(View.VISIBLE);
            }
        }

        return row;
    }

    class ListHolder
    {
      TextView txtTitle, txtPairID,iconArrow;
    }
}
