package com.sgo.mayapada.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.sgo.mayapada.Beans.BuyCanteenModel;
import com.sgo.mayapada.R;

import java.util.ArrayList;

/**
 * Created by thinkpad on 8/3/2016.
 */
public class BuyCanteenAdapter extends BaseAdapter {

    private LayoutInflater mInflater;
    private ArrayList<BuyCanteenModel> adata;


    public BuyCanteenAdapter(Context context, ArrayList<BuyCanteenModel> data) {
        mInflater = LayoutInflater.from(context);
        this.adata = data;
    }

    @Override
    public int getCount() {
        return adata.size();
    }

    @Override
    public Object getItem(int position) {
        return adata.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        ViewHolder holder;
        if(convertView == null) {
            view = mInflater.inflate(R.layout.list_buy_canteen_item, parent, false);
            holder = new ViewHolder();
            holder.coupon = (TextView)view.findViewById(R.id.txtCoupon);
            holder.canteen = (TextView)view.findViewById(R.id.txtKantin);
            view.setTag(holder);
        } else {
            view = convertView;
            holder = (ViewHolder)view.getTag();
        }

        holder.coupon.setText(adata.get(position).getCoupon());
        holder.canteen.setText(adata.get(position).getCanteen());

        return view;
    }

    private class ViewHolder {
        public TextView coupon, canteen;
    }
}

