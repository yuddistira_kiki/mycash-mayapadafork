package com.sgo.mayapada.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.sgo.mayapada.Beans.KantinManajemenStokModel;
import com.sgo.mayapada.R;
import com.sgo.mayapada.fragments.FragKantinManajemenStok;

import java.util.ArrayList;

/**
 * Created by thinkpad on 10/6/2016.
 */

public class KantinManajemenStokAdapter extends BaseAdapter implements Filterable {

    private Context context;
    private FragKantinManajemenStok fragment;
    private ArrayList<KantinManajemenStokModel> data = null;
    private ArrayList<KantinManajemenStokModel> originalData = null;
    private ViewHolder holder;
    private ItemFilter mFilter;

    public KantinManajemenStokAdapter(Context context, ArrayList<KantinManajemenStokModel> data, FragKantinManajemenStok fragment) {
        this.fragment = fragment;
        this.context = context;
        this.data = data;
        this.originalData = data;
    }
    @Override
    public int getCount() {
        if(data != null && data.size() != 0){
            return data.size();
        }
        return 0;
    }

    public void swapItems(ArrayList<KantinManajemenStokModel> items) {
        this.originalData = items;
        this.data = items;
        notifyDataSetChanged();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            holder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(context);
            convertView = inflater.inflate(R.layout.list_kantin_manajemen_stok_item, parent, false);
            holder.img_stop = (ImageView) convertView.findViewById(R.id.img_stop);
            holder.name = (TextView) convertView.findViewById(R.id.name_value);
            holder.edit = (TextView) convertView.findViewById(R.id.txt_edit);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.name.setText(data.get(position).getName());

        if(data.get(position).isStop()) {
            holder.edit.setEnabled(true);
            holder.img_stop.setImageResource(R.drawable.ic_tick);
        }
        else {
            holder.edit.setEnabled(false);
            holder.img_stop.setImageResource(R.drawable.ic_stop_x);
        }

        holder.img_stop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragment.sentActiveDeactive(position);
            }
        });

        holder.edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragment.onListItemClick(data.get(position));
            }
        });

        return convertView;
    }

    public void setActiveDeactive(String status, int position){
        if(status.equalsIgnoreCase("A")) {
            data.get(position).setIsStop(false);
            holder.img_stop.setImageResource(R.drawable.ic_stop_x);
        }
        else {
            data.get(position).setIsStop(true);
            holder.img_stop.setImageResource(R.drawable.ic_tick);
        }
        notifyDataSetChanged();
    }

    public ArrayList<KantinManajemenStokModel> getFilteredData() {
        return data;
    }

    @Override
    public Filter getFilter() {
        if (mFilter == null) {
            mFilter = new ItemFilter();
        }
        return mFilter;
    }

    private class ItemFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            String filterString = constraint.toString().toLowerCase();

            FilterResults results = new FilterResults();

            int count = originalData.size();
            final ArrayList<KantinManajemenStokModel> nlist = new ArrayList<>();

            String filterableName;

            for (int i = 0; i < count; i++) {
                KantinManajemenStokModel stok = originalData.get(i);
                filterableName = stok.getName();

                if (filterableName.toLowerCase().contains(filterString)) {
                    nlist.add(stok);
                }
            }

            results.values = nlist;
            results.count = nlist.size();

            return results;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            data = (ArrayList<KantinManajemenStokModel>) results.values;
            notifyDataSetChanged();
        }
    }

    private class ViewHolder {
        TextView name, edit;
        ImageView img_stop;
    }
}
