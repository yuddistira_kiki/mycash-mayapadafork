package com.sgo.mayapada.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.sgo.mayapada.Beans.TagihanListModel;

import java.util.ArrayList;

/**
 * Created by thinkpad on 3/31/2016.
 */
public class TagihanListAdapter extends BaseAdapter {

    private Context context;
    private int layoutResourceId;
    private ArrayList<TagihanListModel> data = null;

    public TagihanListAdapter(Context context, int resource, ArrayList<TagihanListModel> objects) {
//        super(context, resource, objects);
        this.layoutResourceId = resource;
        this.context = context;
        this.data = objects;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        return row;
    }

}

