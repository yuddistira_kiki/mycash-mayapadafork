package com.sgo.mayapada.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.sgo.mayapada.Beans.KantinDaftarPembeliModel;
import com.sgo.mayapada.R;

import java.util.ArrayList;

/**
 * Created by thinkpad on 8/10/2016.
 */
public class KantinDaftarPembeliAdapter extends BaseAdapter implements Filterable {

    private Context context;
    private int layoutResourceId;
    private ArrayList<KantinDaftarPembeliModel> data = null;
    private ArrayList<KantinDaftarPembeliModel> originalData = null;
    private ItemFilter mFilter;

    public KantinDaftarPembeliAdapter(Context context, int resource, ArrayList<KantinDaftarPembeliModel> objects) {
        this.layoutResourceId = resource;
        this.context = context;
        this.data = objects;
        this.originalData = objects;
    }

    public void swapItems(ArrayList<KantinDaftarPembeliModel> items) {
        this.data = items;
        this.originalData = items;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        ListHolder holder;

        if (row == null) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);

            holder = new ListHolder();
            holder.txt_no_hp = (TextView) row.findViewById(R.id.buyer_value);
            holder.txt_status = (TextView) row.findViewById(R.id.status_value);
            holder.txt_tgl = (TextView) row.findViewById(R.id.tgl_value);

            row.setTag(holder);
        } else {
            holder = (ListHolder) row.getTag();
        }

        KantinDaftarPembeliModel itemnya = data.get(position);

        holder.txt_no_hp.setText(itemnya.getNoHp());
        holder.txt_status.setText(itemnya.getStatus());
        holder.txt_tgl.setText(itemnya.getTgl());

        return row;
    }

    public void refreshAdapter() {
        this.notifyDataSetChanged();
    }

    public ArrayList<KantinDaftarPembeliModel> getFilteredData() {
        return data;
    }

    class ListHolder {
        TextView txt_no_hp, txt_status, txt_tgl;
    }

    @Override
    public Filter getFilter() {
        if (mFilter == null) {
            mFilter = new ItemFilter();
        }
        return mFilter;
    }

    private class ItemFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            String filterString = constraint.toString().toLowerCase();

            FilterResults results = new FilterResults();

            int count = originalData.size();
            final ArrayList<KantinDaftarPembeliModel> nlist = new ArrayList<>();

            String filterableName;

            for (int i = 0; i < count; i++) {
                KantinDaftarPembeliModel buyerModel = originalData.get(i);
                filterableName = buyerModel.getNoHp();

                if (filterableName.toLowerCase().contains(filterString)) {
                    nlist.add(buyerModel);
                }
            }

            results.values = nlist;
            results.count = nlist.size();

            return results;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            data = (ArrayList<KantinDaftarPembeliModel>) results.values;
            notifyDataSetChanged();
        }
    }
}