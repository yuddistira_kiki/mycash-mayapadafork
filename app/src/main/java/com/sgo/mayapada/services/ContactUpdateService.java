package com.sgo.mayapada.services;
/*
  Created by Administrator on 1/10/2017.
 */

import android.app.IntentService;
import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.os.Looper;
import android.provider.ContactsContract;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.securepreferences.SecurePreferences;
import com.sgo.mayapada.coreclass.CoreApp;
import com.sgo.mayapada.coreclass.CustomSecurePref;
import com.sgo.mayapada.coreclass.DateTimeFormat;
import com.sgo.mayapada.coreclass.DefineValue;
import com.sgo.mayapada.coreclass.MyApiClient;
import com.sgo.mayapada.coreclass.NoHPFormat;
import com.sgo.mayapada.coreclass.WebParams;
import com.sgo.mayapada.entityDAO.DaoSession;
import com.sgo.mayapada.entityDAO.Friend;
import com.sgo.mayapada.entityDAO.FriendDao;
import com.sgo.mayapada.entityDAO.MyFriend;
import com.sgo.mayapada.entityDAO.MyFriendDao;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import timber.log.Timber;

public class ContactUpdateService extends IntentService{

    public static final String INTENT_CONTACT_UPDATE = "com.sgo.mayapada.INTENT_CONTACT_UPDATE";
    private static final int LOADER_LISTENER = 10;

    private String isContactNew;
    private String _ownerID;
    private String accessKey;
    private boolean isContactwsRunning;
    private SecurePreferences sp;
    private ContentResolver mCR;
    private MyFriendDao myFriendDao;

    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     *
     * @param name Used to name the worker thread, important only for debugging.
     */
    public ContactUpdateService(String name) {
        super(name);
    }
    public  ContactUpdateService (){
        super(null);

    }

    @Override
    protected void onHandleIntent(Intent intent) {
        sp = CustomSecurePref.getInstance().getmSecurePrefs();

        _ownerID = sp.getString(DefineValue.USERID_PHONE,"");
        accessKey = sp.getString(DefineValue.ACCESS_KEY,"");
        isContactwsRunning = sp.getBoolean(DefineValue.CONTACT_WS, false);
        isContactNew = sp.getString(DefineValue.CONTACT_FIRST_TIME,"");

        DaoSession daoSession = CoreApp.get_instance().getDaoSession();
        myFriendDao = daoSession.getMyFriendDao();

        if(isContactNew.equals(DefineValue.NO) && !isContactwsRunning){

            Log.d("contact service", "contact service called from service. iscontacnew: "+isContactNew);

            mCR = this.getContentResolver();

            String select = "((" + ContactsContract.Contacts.DISPLAY_NAME + " NOTNULL) AND ("
                    + ContactsContract.Contacts.HAS_PHONE_NUMBER + "=1) AND ("
                    + ContactsContract.Contacts.DISPLAY_NAME + " != '' ))";

            Cursor cur = mCR.query(ContactsContract.Contacts.CONTENT_URI, null, select, null, ContactsContract.Contacts.DISPLAY_NAME);
            StartInsert(cur);
            sp.edit().putBoolean(DefineValue.CONTACT_WS, true).apply();
//            if(loader == null) {
//                if(!is_Contactws_running) {
//                    loader = new CursorLoader(this,
//                            //ContactsContract.Contacts.CONTENT_URI, // URI
//                            ContactsContract.Contacts.CONTENT_URI,
//                            null,  // projection fields
//                            select, // the selection criteria
//                            null, // the selection args
//                            ContactsContract.Contacts.DISPLAY_NAME // the sort order
//                    );
//
//                    loader.registerListener(LOADER_LISTENER, this);
//                    loader.startLoading();
//                }
            }
        }

    private void StartInsert(final Cursor cursor){
        Log.d("contact service", "contact service loader complete.");

        if (cursor.getCount() > 0) {
            Timber.wtf("isi size cursor:" + String.valueOf(cursor.getCount()));
            //mAdapter.swapCursor(cursor);
            Thread getContactBackground = new Thread() {
                @Override
                public void run() {
                    Looper.prepare();
                    List<Friend> mListFriend = new ArrayList<>();
                    String _name;
                    String _id;

                    int finalI = 0;
                    while (!cursor.isClosed() && cursor.moveToNext()) {
                        _id = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));
                        _name = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                        if (Integer.parseInt(cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {
                            Cursor pCur = mCR.query(
                                    ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                                    null,
                                    ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                                    new String[]{_id}, null);

                            String _phone1 = null, _phone2 = null, _phone3 = null, _phoneTemp;
                            int idx = 0;

                            if (pCur != null) {
                                while (pCur.moveToNext()) {
                                    _phoneTemp = NoHPFormat.editNoHP(pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER)));
                                    switch (idx) {
                                        case 0:
                                            _phone1 = _phoneTemp;
                                            break;
                                        case 1:
                                            _phone2 = _phoneTemp;
                                            break;
                                        case 2:
                                            _phone3 = _phoneTemp;
                                            break;
                                    }
                                    idx++;
                                }
                            }
                            if (pCur != null) {
                                pCur.close();
                            }

                            pCur = mCR.query(
                                    ContactsContract.CommonDataKinds.Email.CONTENT_URI,
                                    null,
                                    ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = ?",
                                    new String[]{_id}, null);

                            String _email = null;

                            if (pCur != null) {
                                while (pCur.moveToNext()) {
                                    _email = pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));
                                }
                            }
                            if (pCur != null) {
                                pCur.close();
                            }

                            Timber.wtf("isi contact yg disimpen:" + _name + " / " + _phone1 + " / " + _phone2 + " / " + _phone3 + " / " + _email + " / " + _ownerID);

                            mListFriend.add(new Friend(_name, _phone1, _phone2, _phone3, _email, _ownerID));

                            finalI++;
                        }

                    }
                    insertContact(mListFriend);
                    Looper.loop();
                    //getLoaderManager().destroyLoader(CONTACTS_LOADER);
                }
            };
            getContactBackground.start();
        } else {
//                Toast.makeText(getActivity(),getString(R.string.error_message),Toast.LENGTH_SHORT).show();
            sp.edit().putBoolean(DefineValue.CONTACT_WS, false).apply();
        }


    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

        private class friendAdapter implements JsonSerializer<Friend> {

            @Override
            public JsonElement serialize(Friend _friend, Type type, JsonSerializationContext jsonSerializationContext) {
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty(WebParams.FULL_NAME, _friend.getFull_name());
                jsonObject.addProperty(WebParams.MOBILE_NUMBER, _friend.getMobile_number());
                jsonObject.addProperty(WebParams.MOBILE_NUMBER2, _friend.getMobile_number2());
                jsonObject.addProperty(WebParams.MOBILE_NUMBER3, _friend.getMobile_number3());
                jsonObject.addProperty(WebParams.EMAIL, _friend.getEmail());
                jsonObject.addProperty(WebParams.OWNER_ID, _friend.getOwner_id());
                return jsonObject;
            }
        }

    private void insertContact(List<Friend> mfriendModel){
        try{

            RequestParams params;
            if(isContactNew.equals(DefineValue.NO)){
                params = MyApiClient.getSignatureWithParams(MyApiClient.COMM_ID,MyApiClient.LINK_USER_CONTACT_UPDATE,
                        _ownerID,accessKey);
            }
            else
                params = MyApiClient.getSignatureWithParams(MyApiClient.COMM_ID,MyApiClient.LINK_USER_CONTACT_INSERT,
                        _ownerID,accessKey);

            GsonBuilder gsonBuilder = new GsonBuilder();
            Gson gson = gsonBuilder.registerTypeAdapter(Friend.class, new friendAdapter()).create();
            params.put(WebParams.USER_ID, _ownerID);
            params.put(WebParams.DATE_TIME, DateTimeFormat.getCurrentDateTime());
            params.put(WebParams.CONTACTS, gson.toJson(mfriendModel));
            params.put(WebParams.COMM_ID, MyApiClient.COMM_ID);

            JsonHttpResponseHandler mHandler = new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    try {
                        String code = response.getString(WebParams.ERROR_CODE);

//                        if(isContactNew.equals(DefineValue.NO))
//                            Timber.d("isi response UpdateContact:" + response.toString());
//                        else
                            Timber.d("isi response InsertContact:"+response.toString());

                        if (code.equals(WebParams.SUCCESS_CODE)) {
                            String arrayFriend = response.getString(WebParams.DATA_CONTACT);
                            String arrayMyFriend = response.getString(WebParams.DATA_FRIEND);
                            insertFriendToDB(new JSONArray(arrayFriend), new JSONArray(arrayMyFriend));
                        }
                        else if(code.equals(WebParams.LOGOUT_CODE)){
                            Timber.d("isi response autologout:"+response.toString());
                        }
                        else {
                            code = response.getString(WebParams.ERROR_MESSAGE);
                            Log.d("contact update service", "error message: "+code);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onProgress(long bytesWritten, long totalSize) {
                    super.onProgress(bytesWritten, totalSize);
                    Timber.d("onProgress insert contact:"+bytesWritten + " / " + totalSize);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    super.onFailure(statusCode, headers, responseString, throwable);
                    failure(throwable);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    failure(throwable);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    failure(throwable);
                }

                private void failure(Throwable throwable){
                    if(MyApiClient.PROD_FAILURE_FLAG)
//                        Toast.makeText(this, getString(R.string.network_connection_failure_toast), Toast.LENGTH_SHORT).show();
//                    else
//                        Toast.makeText(getActivity(), throwable.toString(), Toast.LENGTH_SHORT).show();


                    Timber.w("Error Koneksi insert contact myfriend:"+throwable.toString());
                }
            };
            MyApiClient.sentInsertContact(this,params,false, mHandler);
            sp.edit().putBoolean(DefineValue.CONTACT_WS, false).apply();
        }catch (Exception e){
            Timber.d("httpclient:"+ e.getMessage());
            sp.edit().putBoolean(DefineValue.CONTACT_WS, false).apply();
        }
    }

    private void insertFriendToDB(JSONArray arrayFriend, JSONArray arrayMyfriend){
        try {
            DaoSession daoSession = CoreApp.get_instance().getDaoSession();
            FriendDao friendDao = daoSession.getFriendDao();
            Friend mFm;
            MyFriend mMfm;
            final List<MyFriend> tempListMyFriend = new ArrayList<>();
            String bucket;

            Friend.deleteAllnCache(friendDao);
            MyFriend.deleteAllnCache(myFriendDao);

            Timber.d("arrayfriend lenght:"+String.valueOf(arrayFriend.length()));
            if(arrayFriend.length()>0){
                List<Friend> tempListFriend = new ArrayList<>();

                for (int i = 0; i < arrayFriend.length(); i++) {
                    mFm = new Friend();
                    mFm.setContact_id(arrayFriend.getJSONObject(i).getInt(WebParams.CONTACT_ID));
                    mFm.setFull_name(arrayFriend.getJSONObject(i).getString(WebParams.FULL_NAME));
                    mFm.setMobile_number(arrayFriend.getJSONObject(i).getString(WebParams.MOBILE_NUMBER));
                    mFm.setMobile_number2(arrayFriend.getJSONObject(i).getString(WebParams.MOBILE_NUMBER2));
                    mFm.setMobile_number3(arrayFriend.getJSONObject(i).getString(WebParams.MOBILE_NUMBER3));
                    mFm.setEmail(arrayFriend.getJSONObject(i).getString(WebParams.EMAIL));
                    mFm.setOwner_id(arrayFriend.getJSONObject(i).getString(WebParams.OWNER_ID));

                    bucket = arrayFriend.getJSONObject(i).getString(WebParams.IS_FRIEND);
                    if(!bucket.equals(""))mFm.setIs_friend(Integer.parseInt(bucket));

                    mFm.setCreated_date(DateTimeFormat.convertStringtoCustomDate(arrayFriend.getJSONObject(i).getString(WebParams.CREATED_DATE)));
                    if(isContactNew.equals(DefineValue.NO) && !arrayFriend.getJSONObject(i).getString(WebParams.UPDATED_DATE).isEmpty()){
                        mFm.setUpdate_date(DateTimeFormat.convertStringtoCustomDate(arrayFriend.getJSONObject(i).getString(WebParams.UPDATED_DATE)));
                    }
                    tempListFriend.add(mFm);
                    Timber.d("idx array friend:"+String.valueOf(i));

                }
                friendDao.insertOrReplaceInTx(tempListFriend);
            }

            Timber.d("arrayMyfriend lenght:"+String.valueOf(arrayMyfriend.length()));
            if(arrayMyfriend.length()>0){

                for (int i = 0; i < arrayMyfriend.length(); i++) {
                    mMfm = new MyFriend();
                    mMfm.setContact_id(arrayMyfriend.getJSONObject(i).getInt(WebParams.CONTACT_ID));
                    mMfm.setFull_name(arrayMyfriend.getJSONObject(i).getString(WebParams.FULL_NAME));
                    mMfm.setFriend_number(arrayMyfriend.getJSONObject(i).getString(WebParams.FRIEND_NUMBER));
                    mMfm.setEmail(arrayMyfriend.getJSONObject(i).getString(WebParams.EMAIL));
                    mMfm.setUser_id(arrayMyfriend.getJSONObject(i).getString(WebParams.USER_ID));
                    mMfm.setImg_url(arrayMyfriend.getJSONObject(i).optString(WebParams.IMG_URL,""));
                    tempListMyFriend.add(mMfm);
                    Timber.d("idx array my friend:"+String.valueOf((int) ((i + 1) * (25.0 / (double) arrayMyfriend.length())) + 75));

                }
                myFriendDao.insertOrReplaceInTx(tempListMyFriend);
            }

            Intent i = new Intent(INTENT_CONTACT_UPDATE);
            LocalBroadcastManager.getInstance(this).sendBroadcast(i);
            sp.edit().putString(DefineValue.CONTACT_FIRST_TIME, DefineValue.NO).apply();


        } catch (JSONException e) {
            e.printStackTrace();
        }

        sp.edit().putBoolean(DefineValue.CONTACT_WS, false).apply();
    }
}
