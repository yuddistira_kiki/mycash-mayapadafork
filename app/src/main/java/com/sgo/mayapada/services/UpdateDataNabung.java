package com.sgo.mayapada.services;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.util.Log;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.securepreferences.SecurePreferences;
import com.sgo.mayapada.entityRealm.List_Account_Nabung;
import com.sgo.mayapada.entityRealm.List_Bank_Nabung;
import com.sgo.mayapada.entityRealm.Target_Saving_Model;
import com.sgo.mayapada.coreclass.CustomSecurePref;
import com.sgo.mayapada.coreclass.DefineValue;
import com.sgo.mayapada.coreclass.ErrorDefinition;
import com.sgo.mayapada.coreclass.MyApiClient;
import com.sgo.mayapada.coreclass.WebParams;
import com.sgo.mayapada.interfaces.DownloadResultReceiver;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import io.realm.Realm;
import timber.log.Timber;

public class UpdateDataNabung extends IntentService {

    public static final int STATUS_RUNNING = 0;
    public static final int STATUS_FINISHED = 1;
    public static final int STATUS_ERROR = 2;

    private SecurePreferences sp;

    private Realm realm;
    private ResultReceiver receiver;
    private Bundle bundle;
    private String userID, accessKey,memberID;

    public UpdateDataNabung() {
        super("UpdateDataNabung");
    }

    public static void startUpdateDataNabung(Context context) {
        Intent intent = new Intent(context, UpdateDataNabung.class);
        DownloadResultReceiver downloadResultReceiver = new DownloadResultReceiver(new Handler());
        downloadResultReceiver.setReceiver((DownloadResultReceiver.Receiver) context);
        intent.putExtra(DefineValue.RECEIVER, downloadResultReceiver);
        context.startService(intent);
    }


    @Override
    protected void onHandleIntent(Intent intent) {

        receiver = intent.getParcelableExtra("receiver");
        bundle = new Bundle();

        if(sp == null)
            sp = CustomSecurePref.getInstance().getmSecurePrefs();

        userID = sp.getString(DefineValue.USERID_PHONE,"");
        accessKey = sp.getString(DefineValue.ACCESS_KEY,"");
        memberID = sp.getString(DefineValue.MEMBER_ID,"");

        realm = Realm.getDefaultInstance();
        getListBankSavingAccount();
    }

    private void EndRealm(){
        if(realm.isInTransaction())
            realm.cancelTransaction();

        if(realm != null && !realm.isClosed())
            realm.close();
    }

    private void getListBankSavingAccount(){
        try{
            RequestParams params = MyApiClient.getSignatureWithParams(MyApiClient.COMM_ID,MyApiClient.LINK_BANK_SAVING_ACCOUNT,
                    userID,accessKey);
            params.put(WebParams.MEMBER_ID, memberID);
            params.put(WebParams.USER_ID, userID);
            params.put(WebParams.COMM_ID, MyApiClient.COMM_ID);

            Timber.d("isi params listbanksavingaccount:" + params.toString());

            receiver.send(STATUS_RUNNING,Bundle.EMPTY);

            MyApiClient.sentListBankSavingAccount(this, params,true,new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    try {
                        String code = response.getString(WebParams.ERROR_CODE);
                        Timber.d("Isi response sentListBankSavingAccount: "+response.toString());
                        if (code.equals(WebParams.SUCCESS_CODE)) {
                            insertToRealm(response.optJSONArray(WebParams.ACCOUNT),response.optJSONArray(WebParams.BANK_SAVING));
                        }
                        else if(code.equals(ErrorDefinition.NO_TRANSACTION)) {
                            realm.executeTransactionAsync(new Realm.Transaction() {
                                @Override
                                public void execute(Realm realm) {
                                    realm.delete(List_Account_Nabung.class);
                                    receiver.send(STATUS_FINISHED,Bundle.EMPTY);
                                }
                            });
                        }else {
                            code = response.getString(WebParams.ERROR_MESSAGE);
                            bundle.putString(DefineValue.MESSAGE, code);
                            receiver.send(STATUS_ERROR,bundle);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        receiver.send(STATUS_ERROR,Bundle.EMPTY);
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    super.onFailure(statusCode, headers, responseString, throwable);
                    failure(throwable);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    failure(throwable);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    failure(throwable);
                }

                private void failure(Throwable throwable) {
                    Timber.w("Error Koneksi biller data list buy:" + throwable.toString());
                    receiver.send(STATUS_ERROR,Bundle.EMPTY);
                }
            });
        }catch (Exception e){
            Log.d("httpclient:",e.getMessage());
        }

        inqSavingTarget();
    }

    private void insertToRealm(JSONArray account, JSONArray bank){
        if(account != null && account.length() > 0){

            realm.beginTransaction();
            realm.delete(List_Account_Nabung.class);

            List_Account_Nabung list_account_nabung;

            for (int i= 0; i < account.length(); i++) {
                try {
                    list_account_nabung = realm.createObjectFromJson(List_Account_Nabung.class, account.getJSONObject(i));
                    list_account_nabung.insertLastUpdate();

                } catch (JSONException e) {
                    e.printStackTrace();
                    realm.cancelTransaction();
                    receiver.send(STATUS_ERROR,Bundle.EMPTY);
                }
            }
        }
        else {
            realm.beginTransaction();
            realm.delete(List_Account_Nabung.class);
            realm.commitTransaction();
        }

        if(bank != null && bank.length() > 0){

            if(!realm.isInTransaction())
                realm.beginTransaction();
            realm.delete(List_Bank_Nabung.class);

            List_Bank_Nabung list_bank_nabung;

            for (int i= 0; i < bank.length(); i++) {
                try {
                    list_bank_nabung = realm.createObjectFromJson(List_Bank_Nabung.class, bank.getJSONObject(i));
                    list_bank_nabung.insertLastUpdate();

                } catch (JSONException e) {
                    e.printStackTrace();
                    realm.cancelTransaction();
                    receiver.send(STATUS_ERROR,Bundle.EMPTY);
                }
            }
        }
        else {
            realm.beginTransaction();
            realm.delete(List_Bank_Nabung.class);
            realm.commitTransaction();
        }

        if(realm.isInTransaction())
            realm.commitTransaction();
        receiver.send(STATUS_FINISHED,Bundle.EMPTY);
    }

    private void inqSavingTarget(){
        try {

            RequestParams params = MyApiClient.getSignatureWithParams(MyApiClient.COMM_ID,MyApiClient.LINK_INQ_SAVING_TARGET,
                    userID,accessKey);
            params.put(WebParams.COMM_ID, MyApiClient.COMM_ID);
            params.put(WebParams.USER_ID, userID);
            params.put(WebParams.MEMBER_ID, memberID);

            Timber.d("isi params inqSavingTarget:" + params.toString());

            MyApiClient.sentInqSavingTarget(this,params,true, new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    try {
                        String code = response.getString(WebParams.ERROR_CODE);
                        Timber.d("isi response inqSavingTarget:" + response.toString());
                        if (code.equals(WebParams.SUCCESS_CODE)) {
                            Target_Saving_Model target_saving_model;
                            realm.beginTransaction();
                            realm.delete(Target_Saving_Model.class);
                            target_saving_model = realm.createObjectFromJson(Target_Saving_Model.class, response);
                            target_saving_model.insertLastUpdate();
                            realm.commitTransaction();
                            Timber.d("inqSavingTarget complete");
                        }
                        else if(code.equals(ErrorDefinition.NO_TRANSACTION)){
                            realm.beginTransaction();
                            realm.delete(Target_Saving_Model.class);
                            realm.commitTransaction();
                        }
                        else {
                            String code_msg = response.getString(WebParams.ERROR_MESSAGE);
                            Timber.d("inqSavingTarget "+ code_msg);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    super.onFailure(statusCode, headers, responseString, throwable);
                    failure(throwable);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    failure(throwable);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                    super.onFailure(statusCode, headers, throwable, errorResponse);
                    failure(throwable);
                }

                private void failure(Throwable throwable){
                    Timber.w("Error Koneksi inqSavingTarget:"+throwable.toString());
                }
            });
        }catch (Exception e){
            Timber.d("httpclient:" + e.getMessage());
        }
        EndRealm();
    }

}
