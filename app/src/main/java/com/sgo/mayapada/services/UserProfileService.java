package com.sgo.mayapada.services;

import android.app.Activity;
import android.app.Service;
import android.content.Intent;
import android.os.*;
import android.os.Process;
import android.support.v4.content.LocalBroadcastManager;

import com.sgo.mayapada.loader.UserProfileHandler;
import com.sgo.mayapada.interfaces.OnLoadDataListener;

import timber.log.Timber;

/**
 * Created by thinkpad on 4/12/2016.
 */
public class UserProfileService extends Service {
    public static final String INTENT_ACTION_USER_PROFILE = "com.sgo.mayapada.INTENT_ACTION_USER_PROFILE";
    private final IBinder testBinder = new MyLocalBinder();
    private boolean isServiceDestroyed;
    private Activity mainPageContext = null;
    private static final long LOOPING_TIME = 95000;

    private static class MyHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {

        }
    }

    private MyHandler mHandler = new MyHandler();

    private Runnable callUserProfile = new Runnable() {
        @Override
        public void run() {
            Process.setThreadPriority(Process.THREAD_PRIORITY_BACKGROUND);
            if(mainPageContext != null) {
                runUserProfile();
            }
            if(!isServiceDestroyed)mHandler.postDelayed(this, LOOPING_TIME);
            Timber.i("Service jalankan call UserProfile Service");
        }
    };

    public void runUserProfile(){
        if(!isServiceDestroyed) {
            UserProfileHandler mBH = new UserProfileHandler(mainPageContext);
            mBH.sentUserProfile(new OnLoadDataListener() {
                @Override
                public void onSuccess(Object deData) {
                    Intent i = new Intent(INTENT_ACTION_USER_PROFILE);
                    LocalBroadcastManager.getInstance(UserProfileService.this)
                            .sendBroadcast(i);
                }

                @Override
                public void onFail(Bundle message) {

                }

                @Override
                public void onFailure(String message) {

                }
            });
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Timber.i("Masuk onCreate call UserProfile Service");
        setServiceDestroyed(false);
        mHandler.removeCallbacks(callUserProfile);
        mHandler.postDelayed(callUserProfile, LOOPING_TIME);
    }

    @Override
    public IBinder onBind(Intent intent) {
        Timber.i("Masuk onBind Service UserProfile");
        return testBinder;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Timber.i("Masuk onStartCommand");
        return START_STICKY;
    }

    public boolean isServiceDestroyed() {
        return isServiceDestroyed;
    }

    private void setServiceDestroyed(boolean isServiceDestroyed) {
        this.isServiceDestroyed = isServiceDestroyed;
    }

    public class MyLocalBinder extends Binder {
        public UserProfileService getService() {
            return UserProfileService.this;
        }
    }

    public void setMainPageContext(Activity _context){
        mainPageContext = _context;
    }

    public void StopCallUserProfile(){
        mHandler.removeCallbacks(callUserProfile);
    }

    public void StartCallUserProfile(){
        mHandler.postDelayed(callUserProfile, LOOPING_TIME);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Timber.i("Masuk onDestroy UserProfile Service");
        setServiceDestroyed(true);
        mHandler.removeCallbacks(callUserProfile);
    }

}
