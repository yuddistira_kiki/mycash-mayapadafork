package com.sgo.mayapada.widgets;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.RectF;
import android.os.Build;
import android.support.v4.app.Fragment;
import android.util.AttributeSet;
import android.view.View;

import com.facebook.AccessToken;
import com.facebook.FacebookException;
import com.sgo.mayapada.R;
import com.sgo.mayapada.coreclass.FacebookFunction;

import timber.log.Timber;

/**
 * Created by yuddistirakiki on 11/7/16.
 */

public class CustomFacebookButton extends View implements FacebookFunction.AccessTokenTrackerListener{

    private Bitmap imageLight;
    private Bitmap imageDark;
    private Boolean isPressed;
    private RectF roundRect;
    private Rect imageRect;
    private FacebookFunction facebookFunction;
    private Fragment mFragment;
    private ButtonLoginListener loginListener;

    public interface ButtonLoginListener{
        void onSuccessLogin();
        void onFailedLogin();
        void onCheckedChange(Boolean isChecked);
    }

    public CustomFacebookButton(Context context) {
        super(context);
        init();
    }

    public CustomFacebookButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CustomFacebookButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public CustomFacebookButton(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    public FacebookFunction getFacebookFunction(){
        if (facebookFunction == null)
            facebookFunction = new FacebookFunction(getContext());
        return facebookFunction;
    }


    private void init(){
        imageDark = BitmapFactory.decodeResource(getResources(), R.drawable.dark_face_icon90);
        imageLight = BitmapFactory.decodeResource(getResources(),R.drawable.light_face_icon90);
        setClickable(true);
        isPressed = false;
        setOnClickListener(clickListener);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int desiredWidth = 80;
        int desiredHeight = 80;

        int widthMode = MeasureSpec.getMode(widthMeasureSpec);
        int widthSize = MeasureSpec.getSize(widthMeasureSpec);
        int heightMode = MeasureSpec.getMode(heightMeasureSpec);
        int heightSize = MeasureSpec.getSize(heightMeasureSpec);

        int width;
        int height;

        //Measure Width
        if (widthMode == MeasureSpec.EXACTLY) {
            //Must be this size
            width = widthSize;
        } else if (widthMode == MeasureSpec.AT_MOST) {
            //Can't be bigger than...
            width = Math.min(desiredWidth, widthSize);
        } else {
            //Be whatever you want
            width = imageDark.getWidth();
        }

        //Measure Height
        if (heightMode == MeasureSpec.EXACTLY) {
            //Must be this size
            height = heightSize;
        } else if (heightMode == MeasureSpec.AT_MOST) {
            //Can't be bigger than...
            height = Math.min(desiredHeight, heightSize);
        } else {
            //Be whatever you want
            height = imageDark.getHeight();
        }

        //MUST CALL THIS
        setMeasuredDimension(width, height);
    }

    private OnClickListener clickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            if(!getFacebookFunction().isLoginAndPublish()) {
                if (getFragment() != null)
                    getFacebookFunction().loginPublish(getFragment(), true);
                else
                    getFacebookFunction().loginPublish(getActivity(), true);
            }
            else {
                isPressed = !isPressed;
                invalidate();
            }
        }
    };

    @Override
    protected void onDraw(Canvas canvas) {
        if(imageRect == null) {
            imageRect = new Rect(0, 0, getWidth(), getHeight());
        }
        if(roundRect == null) {
            roundRect = new RectF(0, 0, getWidth(), getHeight());
        }
        Bitmap image;

        if(isPressed)
            image = this.imageLight;
        else
            image = this.imageDark;

        Timber.d("minheightImage : "+ String.valueOf(image.getHeight()));
        float xoffset = (imageRect.width() - image.getWidth()) / 2;
        float yoffset = (imageRect.height() - image.getHeight()) / 2;
        canvas.drawBitmap(image, xoffset, yoffset, null);
        loginListener.onCheckedChange(isPressed);
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (facebookFunction.getmATT() != null && !facebookFunction.getmATT().isTracking()) {
            facebookFunction.getmATT().startTracking();
        }

        if(getContext() instanceof ButtonLoginListener)
            loginListener = (ButtonLoginListener) getContext();
        else {
            if(loginListener == null)
                throw new RuntimeException("ButtonLoginListener not implemented in activity/fragment");
        }
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (facebookFunction.getmATT()!= null) {
            facebookFunction.getmATT().stopTracking();
        }
    }


    private Fragment getFragment() {
        return mFragment;
    }

    public Boolean isChecked(){
        return isPressed;
    }

    public void callBackFacebook(int requestCode, int resultCode, Intent data){
        getFacebookFunction().getmCallBackManager().onActivityResult(requestCode,resultCode,data);
    }

    public void setFragment(Fragment mFragment) {
        this.mFragment = mFragment;
        if(mFragment instanceof ButtonLoginListener)
            loginListener = (ButtonLoginListener) mFragment;
        getFacebookFunction().setTargetFragment(mFragment, new FacebookFunction.LoginFBListener() {
            @Override
            public void OnLoginFBSuccess() {
                if(loginListener != null)
                    loginListener.onSuccessLogin();
                isPressed = getFacebookFunction().isLoginAndPublish();
                invalidate();
            }

            @Override
            public void OnLoginFBFailed(FacebookException error) {
                if(loginListener != null)
                    loginListener.onFailedLogin();
                isPressed = getFacebookFunction().isLoginAndPublish();
                invalidate();
            }

            @Override
            public void OnProfileChange() {

            }
        });
        isPressed = getFacebookFunction().isLoginAndPublish();
    }

    private Activity getActivity() {
        Context context = getContext();
        while (!(context instanceof Activity) && context instanceof ContextWrapper) {
            context = ((ContextWrapper) context).getBaseContext();
        }

        if (context instanceof Activity) {
            return (Activity) context;
        }
        throw new FacebookException("Unable to get Activity.");
    }

    @Override
    public void OnAccessTokenChange(AccessToken accessToken) {
        isPressed = accessToken != null;
        invalidate();
    }
}
