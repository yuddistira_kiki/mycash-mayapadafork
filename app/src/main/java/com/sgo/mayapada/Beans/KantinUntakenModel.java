package com.sgo.mayapada.Beans;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by thinkpad on 10/3/2016.
 */

public class KantinUntakenModel implements Parcelable {
    private String order_id;
    private String no_hp;
    private String item;

    public KantinUntakenModel() {

    }

    private KantinUntakenModel(Parcel in) {
        order_id = in.readString();
        no_hp = in.readString();
        item = in.readString();
    }

    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }

    public String getNo_hp() {
        return no_hp;
    }

    public void setNo_hp(String no_hp) {
        this.no_hp = no_hp;
    }

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(order_id);
        dest.writeString(no_hp);
        dest.writeString(item);
    }


    public static final Parcelable.Creator<KantinUntakenModel> CREATOR = new Parcelable.Creator<KantinUntakenModel>() {
        public KantinUntakenModel createFromParcel(Parcel in) {
            return new KantinUntakenModel(in);
        }

        public KantinUntakenModel[] newArray(int size) {
            return new KantinUntakenModel[size];

        }
    };
}
