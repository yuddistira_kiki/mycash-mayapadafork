package com.sgo.mayapada.Beans;

/**
 * Created by thinkpad on 5/27/2016.
 */
public class CategorySchoolModel {
    private String id;
    private String name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
