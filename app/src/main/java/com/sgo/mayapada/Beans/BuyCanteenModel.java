package com.sgo.mayapada.Beans;

/**
 * Created by thinkpad on 8/3/2016.
 */
public class BuyCanteenModel {
    private String coupon;
    private String canteen;

    public String getCanteen() {
        return canteen;
    }

    public void setCanteen(String canteen) {
        this.canteen = canteen;
    }

    public String getCoupon() {
        return coupon;
    }

    public void setCoupon(String coupon) {
        this.coupon = coupon;
    }
}
