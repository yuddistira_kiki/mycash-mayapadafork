package com.sgo.mayapada.Beans;

import com.sgo.mayapada.coreclass.CurrencyFormat;
import com.sgo.mayapada.coreclass.MyApiClient;

import java.util.ArrayList;

/**
 * Created by yuddistirakiki on 12/3/16.
 */

public class BankDataTopUp {

    private ArrayList<ListBankDataTopup> bankData;
    private String noVa;
    private String fee;
    private String bankCode;

    public BankDataTopUp(ArrayList<ListBankDataTopup> bankData,String bankCode, String noVa, String fee){
        this.setBankData(bankData);
        this.setNoVa(noVa);
        if (fee != null && !fee.isEmpty() && !fee.equals("null")) {
            if (!fee.equals("0.00"))
                this.setFee(MyApiClient.CCY_VALUE + " " + CurrencyFormat.format(fee));
        }
        this.setBankCode(bankCode);
    }



    public String getNoVa() {
        return noVa;
    }

    private void setNoVa(String noVa) {
        this.noVa = noVa;
    }


    public String getBankCode() {
        return bankCode;
    }

    public String getFee() {
        return fee;
    }

    private void setFee(String fee) {
        this.fee = fee;
    }

    public ArrayList<ListBankDataTopup> getBankData() {
        return bankData;
    }

    private void setBankData(ArrayList<ListBankDataTopup> bankData) {
        this.bankData = bankData;
    }

    private void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }
}
