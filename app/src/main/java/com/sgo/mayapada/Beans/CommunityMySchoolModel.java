package com.sgo.mayapada.Beans;

/**
 * Created by thinkpad on 5/24/2016.
 */
public class CommunityMySchoolModel {
    private String comm_id;
    private String comm_code;
    private String comm_name;
    private String api_key;
    private String callback_url;
    private String cust_name;
    private String pairing_id;

    public String getCust_name() {
        return cust_name;
    }

    public void setCust_name(String cust_name) {
        this.cust_name = cust_name;
    }

    public String getApi_key() {
        return api_key;
    }

    public void setApi_key(String api_key) {
        this.api_key = api_key;
    }

    public String getCallback_url() {
        return callback_url;
    }

    public void setCallback_url(String callback_url) {
        this.callback_url = callback_url;
    }

    public String getComm_code() {
        return comm_code;
    }

    public void setComm_code(String comm_code) {
        this.comm_code = comm_code;
    }

    public String getComm_id() {
        return comm_id;
    }

    public void setComm_id(String comm_id) {
        this.comm_id = comm_id;
    }

    public String getComm_name() {
        return comm_name;
    }

    public void setComm_name(String comm_name) {
        this.comm_name = comm_name;
    }

    public String getPairing_id() {
        return pairing_id;
    }

    public void setPairing_id(String pairing_id) {
        this.pairing_id = pairing_id;
    }
}
