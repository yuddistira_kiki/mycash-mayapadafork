package com.sgo.mayapada.Beans;

/**
 * Created by thinkpad on 9/21/2016.
 */
public class CanteenMerchantModel {
    private String id;
    private String name;
    private String category;

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
