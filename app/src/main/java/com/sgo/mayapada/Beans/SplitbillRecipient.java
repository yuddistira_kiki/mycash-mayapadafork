package com.sgo.mayapada.Beans;

/**
 * Created by thinkpad on 7/31/2017.
 */

public class SplitbillRecipient {
    private String img_url;
    private String name;
    private String hp_number;
    private String amount;

    public String getImg_url() {
        return img_url;
    }

    public void setImg_url(String img_url) {
        this.img_url = img_url;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHp_number() {
        return hp_number;
    }

    public void setHp_number(String hp_number) {
        this.hp_number = hp_number;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }
}
