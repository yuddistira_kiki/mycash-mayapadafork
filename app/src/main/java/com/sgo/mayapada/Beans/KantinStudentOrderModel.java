package com.sgo.mayapada.Beans;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by thinkpad on 8/4/2016.
 */
public class KantinStudentOrderModel implements Parcelable{
    private String id;
    private String title;
    private String price;
    private String ccy_id;
    private String stock_type;
    private String qty;
    private String count;
    private String status;
    private String img_url;

    public KantinStudentOrderModel() {

    }

    private KantinStudentOrderModel(Parcel in) {
        id = in.readString();
        title = in.readString();
        ccy_id = in.readString();
        price = in.readString();
        stock_type = in.readString();
        qty = in.readString();
        status = in.readString();
        img_url = in.readString();
    }

    public String getImg_url() {
        return img_url;
    }

    public void setImg_url(String img_url) {
        this.img_url = img_url;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public String getCcy_id() {
        return ccy_id;
    }

    public void setCcy_id(String ccy_id) {
        this.ccy_id = ccy_id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getStock_type() {
        return stock_type;
    }

    public void setStock_type(String stock_type) {
        this.stock_type = stock_type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(title);
        dest.writeString(ccy_id);
        dest.writeString(price);
        dest.writeString(stock_type);
        dest.writeString(qty);
        dest.writeString(status);
        dest.writeString(img_url);
    }

    public static final Parcelable.Creator<KantinStudentOrderModel> CREATOR = new Parcelable.Creator<KantinStudentOrderModel>() {
        public KantinStudentOrderModel createFromParcel(Parcel in) {
            return new KantinStudentOrderModel(in);
        }

        public KantinStudentOrderModel[] newArray(int size) {
            return new KantinStudentOrderModel[size];

        }
    };

}
