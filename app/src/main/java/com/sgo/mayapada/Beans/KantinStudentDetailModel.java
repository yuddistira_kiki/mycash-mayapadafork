package com.sgo.mayapada.Beans;

/**
 * Created by thinkpad on 8/8/2016.
 */
public class KantinStudentDetailModel {

    private String name;
    private String count;
    private String amount;

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }
}
