package com.sgo.mayapada.Beans;

/**
 * Created by thinkpad on 10/6/2016.
 */

public class KantinManajemenStokModel {
    private String code;
    private String name;
    private String status;
    private boolean isStop;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public boolean isStop() {
        return isStop;
    }

    public void setIsStop(boolean stop) {
        isStop = stop;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
