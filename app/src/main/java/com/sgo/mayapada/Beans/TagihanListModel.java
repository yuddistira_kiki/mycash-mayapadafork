package com.sgo.mayapada.Beans;

/**
 * Created by thinkpad on 3/31/2016.
 */
public class TagihanListModel {

    private boolean cb_checked;
    private String doc_id;
    private String doc_no;
    private String doc_name;
    private String doc_amount;
    private String remain_amount;
    private String ccy;
    private String desc;
    private String input_amount;

    public boolean isCb_checked() {
        return cb_checked;
    }

    public void setCb_checked(boolean cb_checked) {
        this.cb_checked = cb_checked;
    }

    public String getCcy() {
        return ccy;
    }

    public void setCcy(String ccy) {
        this.ccy = ccy;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getDoc_amount() {
        return doc_amount;
    }

    public void setDoc_amount(String doc_amount) {
        this.doc_amount = doc_amount;
    }

    public String getDoc_id() {
        return doc_id;
    }

    public void setDoc_id(String doc_id) {
        this.doc_id = doc_id;
    }

    public String getDoc_name() {
        return doc_name;
    }

    public void setDoc_name(String doc_name) {
        this.doc_name = doc_name;
    }

    public String getDoc_no() {
        return doc_no;
    }

    public void setDoc_no(String doc_no) {
        this.doc_no = doc_no;
    }

    public String getInput_amount() {
        return input_amount;
    }

    public void setInput_amount(String input_amount) {
        this.input_amount = input_amount;
    }

    public String getRemain_amount() {
        return remain_amount;
    }

    public void setRemain_amount(String remain_amount) {
        this.remain_amount = remain_amount;
    }
}
