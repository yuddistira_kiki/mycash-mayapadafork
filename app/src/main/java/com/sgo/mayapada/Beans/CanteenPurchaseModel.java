package com.sgo.mayapada.Beans;

/**
 * Created by thinkpad on 9/27/2016.
 */
public class CanteenPurchaseModel {
    private String order_id;
    private String merchant_id;
    private String merchant_name;
    private String kupon;
    private String tx_datetime;
    private String kadaluarsa;
    private String ccy_id;
    private String amount;
    private String status;
    private String item;

    public CanteenPurchaseModel(String _order_id, String _merchant_id, String _merchant_name, String _kupon,
                                String _tx_datetime, String _kadaluarsa, String _ccy_id, String _amount,
                                String _status, String _item){
        setOrder_id(_order_id);
        setMerchant_id(_merchant_id);
        setMerchant_name(_merchant_name);
        setKupon(_kupon);
        setTx_datetime(_tx_datetime);
        setKadaluarsa(_kadaluarsa);
        setCcy_id(_ccy_id);
        setAmount(_amount);
        setStatus(_status);
        setItem(_item);
    }

    public String getAmount() {
        return amount;
    }

    private void setAmount(String amount) {
        this.amount = amount;
    }

    public String getCcy_id() {
        return ccy_id;
    }

    private void setCcy_id(String ccy_id) {
        this.ccy_id = ccy_id;
    }

    public String getItem() {
        return item;
    }

    private void setItem(String item) {
        this.item = item;
    }

    public String getKadaluarsa() {
        return kadaluarsa;
    }

    private void setKadaluarsa(String kadaluarsa) {
        this.kadaluarsa = kadaluarsa;
    }

    public String getKupon() {
        return kupon;
    }

    private void setKupon(String kupon) {
        this.kupon = kupon;
    }

    public String getMerchant_id() {
        return merchant_id;
    }

    private void setMerchant_id(String merchant_id) {
        this.merchant_id = merchant_id;
    }

    public String getMerchant_name() {
        return merchant_name;
    }

    private void setMerchant_name(String merchant_name) {
        this.merchant_name = merchant_name;
    }

    public String getOrder_id() {
        return order_id;
    }

    private void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public String getStatus() {
        return status;
    }

    private void setStatus(String status) {
        this.status = status;
    }

    public String getTx_datetime() {
        return tx_datetime;
    }

    private void setTx_datetime(String tx_datetime) {
        this.tx_datetime = tx_datetime;
    }
}
