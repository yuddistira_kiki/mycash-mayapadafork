package com.sgo.mayapada.Beans;

/**
 * Created by thinkpad on 7/25/2016.
 */
public class ReportListLKDModel {
    private String tx_id;
    private String comm_id;
    private String comm_code;
    private String comm_name;
    private String datetime;
    private String bbs_type;
    private String comm_fee;
    private String tx_status;
    private String ccy_id;
    private String amount;
    private String admin_fee;
    private String total_amount;

    public ReportListLKDModel(String _tx_id, String _comm_id, String _comm_code, String _comm_name, String _datetime,
                              String _bbs_type,  String _comm_fee, String _tx_status, String _ccy_id, String _amount,
                              String _admin_fee, String _total_amount){

        this.setTx_id(_tx_id);
        this.setComm_id(_comm_id);
        this.setComm_code(_comm_code);
        this.setComm_name(_comm_name);
        this.setDatetime(_datetime);
        this.setBbs_type(_bbs_type);
        this.setComm_fee(_comm_fee);
        this.setTx_status(_tx_status);
        this.setCcy_id(_ccy_id);
        this.setAmount(_amount);
        this.setAdmin_fee(_admin_fee);
        this.setTotal_amount(_total_amount);
    }

    public String getAdmin_fee() {
        return admin_fee;
    }

    private void setAdmin_fee(String admin_fee) {
        this.admin_fee = admin_fee;
    }

    public String getAmount() {
        return amount;
    }

    private void setAmount(String amount) {
        this.amount = amount;
    }

    public String getTotal_amount() {
        return total_amount;
    }

    private void setTotal_amount(String total_amount) {
        this.total_amount = total_amount;
    }

    public String getBbs_type() {
        return bbs_type;
    }

    private void setBbs_type(String bbs_type) {
        this.bbs_type = bbs_type;
    }

    public String getCcy_id() {
        return ccy_id;
    }

    private void setCcy_id(String ccy_id) {
        this.ccy_id = ccy_id;
    }

    public String getComm_code() {
        return comm_code;
    }

    private void setComm_code(String comm_code) {
        this.comm_code = comm_code;
    }

    public String getComm_fee() {
        return comm_fee;
    }

    private void setComm_fee(String comm_fee) {
        this.comm_fee = comm_fee;
    }

    public String getComm_id() {
        return comm_id;
    }

    private void setComm_id(String comm_id) {
        this.comm_id = comm_id;
    }

    public String getComm_name() {
        return comm_name;
    }

    private void setComm_name(String comm_name) {
        this.comm_name = comm_name;
    }

    public String getDatetime() {
        return datetime;
    }

    private void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    public String getTx_id() {
        return tx_id;
    }

    private void setTx_id(String tx_id) {
        this.tx_id = tx_id;
    }

    public String getTx_status() {
        return tx_status;
    }

    private void setTx_status(String tx_status) {
        this.tx_status = tx_status;
    }
}
