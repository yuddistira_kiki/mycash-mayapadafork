package com.sgo.mayapada.Beans;

/**
 * Created by thinkpad on 8/10/2016.
 */
public class KantinDaftarTerjualModel {
    private String code;
    private String title;
    private String terjual;
    private String terambil;
    private String belum_terambil;
    private boolean isStop;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public boolean isStop() {
        return isStop;
    }

    public void setIsStop(boolean isStop) {
        this.isStop = isStop;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBelum_terambil() {
        return belum_terambil;
    }

    public void setBelum_terambil(String belum_terambil) {
        this.belum_terambil = belum_terambil;
    }

    public String getTerambil() {
        return terambil;
    }

    public void setTerambil(String terambil) {
        this.terambil = terambil;
    }

    public String getTerjual() {
        return terjual;
    }

    public void setTerjual(String terjual) {
        this.terjual = terjual;
    }
}
