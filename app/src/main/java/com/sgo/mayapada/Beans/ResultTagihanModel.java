package com.sgo.mayapada.Beans;

/**
 * Created by thinkpad on 5/16/2016.
 */
public class ResultTagihanModel {
    private String doc_id;
    private String doc_name;
    private String ccy_id;
    private String payment_amount;
    private String status;
    private String remark;

    public ResultTagihanModel(String _doc_id, String _doc_name, String _ccy_id, String _payment_amount, String _status, String _remark) {
        this.setDoc_id(_doc_id);
        this.setDoc_name(_doc_name);
        this.setCcy_id(_ccy_id);
        this.setPayment_amount(_payment_amount);
        this.setStatus(_status);
        this.setRemark(_remark);
    }

    public String getCcy_id() {
        return ccy_id;
    }

    private void setCcy_id(String ccy_id) {
        this.ccy_id = ccy_id;
    }

    public String getDoc_name() {
        return doc_name;
    }

    private void setDoc_name(String doc_name) {
        this.doc_name = doc_name;
    }

    public String getStatus() {
        return status;
    }

    private void setStatus(String status) {
        this.status = status;
    }

    public String getDoc_id() {
        return doc_id;
    }

    private void setDoc_id(String doc_id) {
        this.doc_id = doc_id;
    }

    public String getPayment_amount() {
        return payment_amount;
    }

    private void setPayment_amount(String payment_amount) {
        this.payment_amount = payment_amount;
    }

    public String getRemark() {
        return remark;
    }

    private void setRemark(String remark) {
        this.remark = remark;
    }
}
