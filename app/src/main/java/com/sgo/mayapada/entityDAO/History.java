package com.sgo.mayapada.entityDAO;/*
  Created by Administrator on 4/13/2015.
 */

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Index;
import org.greenrobot.greendao.annotation.NotNull;
import org.greenrobot.greendao.annotation.Generated;

import java.util.List;

@Entity(
        nameInDb = "PostHistory"
)
public class History{
    @Id(autoincrement = true)
    private Long id;

    @Index(unique = true)
    @NotNull
    private int history_id;
    private String post;
    private String amount;
    private String balance;
    private String ccy_id;
    private String datetime;
    private String owner;
    private String owner_id;
    private String owner_profile_picture;
	private String owner_profile_picture_large;
    private String with_id;
    private String with;
    private String with_profile_picture;
    private String with_profile_picture_large;
    private String tx_status;
    private String typepost;
    private String typecaption;
    private String privacy;
    private String numcomments;
    private String numviews;
    private String numlikes;
    private String share;
    private String comments;
    private String likes;
    private String comment_id_1;
    private String comment_id_2;
    private String from_name_1;
    private String from_name_2;
    private String from_profile_picture_1;
    private String from_profile_picture_2;
    private String reply_1;
    private String reply_2;
    private String isLike;

    public History() {}


    @Generated(hash = 741162021)
    public History(Long id, int history_id, String post, String amount,
            String balance, String ccy_id, String datetime, String owner,
            String owner_id, String owner_profile_picture,
            String owner_profile_picture_large, String with_id, String with,
            String with_profile_picture, String with_profile_picture_large,
            String tx_status, String typepost, String typecaption, String privacy,
            String numcomments, String numviews, String numlikes, String share,
            String comments, String likes, String comment_id_1, String comment_id_2,
            String from_name_1, String from_name_2, String from_profile_picture_1,
            String from_profile_picture_2, String reply_1, String reply_2,
            String isLike) {
        this.id = id;
        this.history_id = history_id;
        this.post = post;
        this.amount = amount;
        this.balance = balance;
        this.ccy_id = ccy_id;
        this.datetime = datetime;
        this.owner = owner;
        this.owner_id = owner_id;
        this.owner_profile_picture = owner_profile_picture;
        this.owner_profile_picture_large = owner_profile_picture_large;
        this.with_id = with_id;
        this.with = with;
        this.with_profile_picture = with_profile_picture;
        this.with_profile_picture_large = with_profile_picture_large;
        this.tx_status = tx_status;
        this.typepost = typepost;
        this.typecaption = typecaption;
        this.privacy = privacy;
        this.numcomments = numcomments;
        this.numviews = numviews;
        this.numlikes = numlikes;
        this.share = share;
        this.comments = comments;
        this.likes = likes;
        this.comment_id_1 = comment_id_1;
        this.comment_id_2 = comment_id_2;
        this.from_name_1 = from_name_1;
        this.from_name_2 = from_name_2;
        this.from_profile_picture_1 = from_profile_picture_1;
        this.from_profile_picture_2 = from_profile_picture_2;
        this.reply_1 = reply_1;
        this.reply_2 = reply_2;
        this.isLike = isLike;
    }


    public History(int _history_id, String _post, String _amount,
                   String _balance, String _ccy_id, String _datetime, String _owner,
                   String _owner_id, String _owner_profile_picture, String _owner_profile_picture_large,
                   String _with_id, String _with, String _with_profile_picture, String _with_profile_picture_large, String _tx_status,
                   String _typepost, String _typecaption, String _privacy, String _numcomments,
                   String _numviews, String _numlikes, String _share, String _comments, String _likes,
                   String _comment_id_1, String _from_name_1, String _from_profile_picture_1, String _reply_1,
                   String _comment_id_2, String _from_name_2, String _from_profile_picture_2, String _reply_2, String _isLike){
        this.setHistory_id(_history_id);
        this.setPost(_post);
        this.setDatetime(_datetime);
        this.setAmount(_amount);
        this.setBalance(_balance);
        this.setCcy_id(_ccy_id);
        this.setOwner(_owner);
        this.setOwner_id(_owner_id);
        this.setOwner_profile_picture(_owner_profile_picture);
        this.setOwner_profile_picture_large(_owner_profile_picture_large);
        this.setWith(_with);
        this.setWith_id(_with_id);
        this.setWith_profile_picture(_with_profile_picture);
        this.setWith_profile_picture_large(_with_profile_picture_large);
        this.setTx_status(_tx_status);
        this.setTypepost(_typepost);
        this.setTypecaption(_typecaption);
        this.setPrivacy(_privacy);
        this.setNumcomments(_numcomments);
        this.setNumviews(_numviews);
        this.setNumlikes(_numlikes);
        this.setShare(_share);
        this.setComments(_comments);
        this.setLikes(_likes);
        this.setComment_id_1(_comment_id_1);
        this.setFrom_name_1(_from_name_1);
        this.setFrom_profile_picture_1(_from_profile_picture_1);
        this.setReply_1(_reply_1);
        this.setComment_id_2(_comment_id_2);
        this.setFrom_name_2(_from_name_2);
        this.setFrom_profile_picture_2(_from_profile_picture_2);
        this.setReply_2(_reply_2);
        this.setIsLike(_isLike);
    }

    public static void updateHistoryCommentToEmpty(HistoryDao historyDao,String history_id, String count ){
        History history = getHistoryByPostID(historyDao,history_id);
        history.setNumcomments(count);
        history.setComments("");
        history.setComment_id_1("");
        history.setComment_id_2("");
        history.setFrom_name_1("");
        history.setFrom_name_2("");
        history.setFrom_profile_picture_1("");
        history.setFrom_profile_picture_2("");
        history.setReply_1("");
        history.setReply_2("");
        historyDao.updateInTx(history);
    }

    public static History getHistoryByPostID(HistoryDao historyDao,String post_id){
        return historyDao.queryBuilder().where(HistoryDao.Properties.History_id.eq(post_id)).unique();
    }
    public static List<History> getAllHistory(HistoryDao historyDao){
        return historyDao.queryBuilder().orderDesc(HistoryDao.Properties.History_id).list();
    }
    public static void deleteAll(HistoryDao historyDao){
        historyDao.deleteAll();
        historyDao.detachAll();
    }

    public static void updateHistoryLikes(HistoryDao historyDao,String post_id,String data_likes, String count,String likes){
        if(data_likes.isEmpty() && count.isEmpty() && likes.isEmpty())
            return;

        History history = getHistoryByPostID(historyDao,post_id);
        if(!data_likes.isEmpty())
            history.setLikes(data_likes);
        if(!count.isEmpty())
            history.setNumlikes(count);
        if(!likes.isEmpty())
            history.setIsLike(likes);
        historyDao.updateInTx(history);
    }

    public void updateHistoryCommentToEmpty(HistoryDao historyDao, String count){
        this.setNumcomments(count);
        this.setComments("");
        this.setComment_id_1("");
        this.setComment_id_2("");
        this.setFrom_name_1("");
        this.setFrom_name_2("");
        this.setFrom_profile_picture_1("");
        this.setFrom_profile_picture_2("");
        this.setReply_1("");
        this.setReply_2("");
        historyDao.updateInTx(this);
    }

    public void updateHistoryLikes(HistoryDao historyDao,String data_likes, String count,String likes){
        if(data_likes.isEmpty() && count.isEmpty() && likes.isEmpty())
            return;

        if(!data_likes.isEmpty())
            this.setLikes(data_likes);
        if(!count.isEmpty())
            this.setNumlikes(count);
        if(!likes.isEmpty())
            this.setIsLike(likes);
        historyDao.updateInTx(this);
    }
    public String getStringHistory_id() {
        return String.valueOf(getHistory_id());
    }

    public String getTx_status() {
        return tx_status;
    }

    public void setTx_status(String tx_status) {
        this.tx_status = tx_status;
    }

    public int getHistory_id() {
        return history_id;
    }

    public void setHistory_id(int history_id) {
        this.history_id = history_id;
    }

    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    public String getCcy_id() {
        return ccy_id;
    }

    public void setCcy_id(String ccy_id) {
        this.ccy_id = ccy_id;
    }

    public String getNumcomments() {
        return numcomments;
    }

    public void setNumcomments(String numcomments) {
        this.numcomments = numcomments;
    }

    public String getNumlikes() {
        return numlikes;
    }

    public void setNumlikes(String numlikes) {
        this.numlikes = numlikes;
    }

    public String getNumviews() {
        return numviews;
    }

    public void setNumviews(String numviews) {
        this.numviews = numviews;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getOwner_id() {
        return owner_id;
    }

    public void setOwner_id(String owner_id) {
        this.owner_id = owner_id;
    }

    public String getOwner_profile_picture() {
        return owner_profile_picture;
    }

	public String getOwner_profile_picture_large() {
        return owner_profile_picture_large;
    }
    public void setOwner_profile_picture(String owner_profile_picture) {
        this.owner_profile_picture = owner_profile_picture;
}

    public void setOwner_profile_picture_large(String owner_profile_picture_large) {
        this.owner_profile_picture_large = owner_profile_picture_large;
    }

    public String getPost() {
        return post;
    }

    public void setPost(String post) {
        this.post = post;
    }

    public String getPrivacy() {
        return privacy;
    }

    public void setPrivacy(String privacy) {
        this.privacy = privacy;
    }

    public String getShare() {
        return share;
    }

    public void setShare(String share) {
        this.share = share;
    }

    public String getTypecaption() {
        return typecaption;
    }

    public void setTypecaption(String typecaption) {
        this.typecaption = typecaption;
    }

    public String getTypepost() {
        return typepost;
    }

    public void setTypepost(String typepost) {
        this.typepost = typepost;
    }

    public String getWith() {
        return with;
    }

    public void setWith(String with) {
        this.with = with;
    }

    public String getWith_id() {
        return with_id;
    }

    public void setWith_id(String with_id) {
        this.with_id = with_id;
    }

    public String getWith_profile_picture() {
        return with_profile_picture;
    }

	public String getWith_profile_picture_large() {
        return with_profile_picture_large;
    }
    public void setWith_profile_picture(String with_profile_picture) {
        this.with_profile_picture = with_profile_picture;
}

    public void setWith_profile_picture_large(String with_profile_picture_large) {
        this.with_profile_picture_large = with_profile_picture_large;
    }

    public String getComment_id_1() {
        return comment_id_1;
    }

    public void setComment_id_1(String comment_id_1) {
        this.comment_id_1 = comment_id_1;
    }

    public String getComment_id_2() {
        return comment_id_2;
    }

    public void setComment_id_2(String comment_id_2) {
        this.comment_id_2 = comment_id_2;
    }

    public String getFrom_name_1() {
        return from_name_1;
    }

    public void setFrom_name_1(String from_name_1) {
        this.from_name_1 = from_name_1;
    }

    public String getFrom_name_2() {
        return from_name_2;
    }

    public void setFrom_name_2(String from_name_2) {
        this.from_name_2 = from_name_2;
    }

    public String getFrom_profile_picture_1() {
        return from_profile_picture_1;
    }

    public void setFrom_profile_picture_1(String from_profile_picture_1) {
        this.from_profile_picture_1 = from_profile_picture_1;
    }

    public String getFrom_profile_picture_2() {
        return from_profile_picture_2;
    }

    public void setFrom_profile_picture_2(String from_profile_picture_2) {
        this.from_profile_picture_2 = from_profile_picture_2;
    }

    public String getReply_1() {
        return reply_1;
    }

    public void setReply_1(String reply_1) {
        this.reply_1 = reply_1;
    }

    public String getReply_2() {
        return reply_2;
    }

    public void setReply_2(String reply_2) {
        this.reply_2 = reply_2;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getIsLike() {
        return isLike;
    }

    public void setIsLike(String isLike) {
        this.isLike = isLike;
    }

    public String getLikes() {
        return likes;
    }

    public void setLikes(String likes) {
        this.likes = likes;
    }


    public Long getId() {
        return this.id;
    }


    public void setId(Long id) {
        this.id = id;
    }
}
