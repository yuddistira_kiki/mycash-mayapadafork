package com.sgo.mayapada.entityDAO;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Index;
import org.greenrobot.greendao.annotation.NotNull;

import java.util.List;

/**
 * Created by thinkpad on 5/13/2015.
 */

@Entity
public class Like{
    @Id(autoincrement = true)
    private Long id;

    @Index(unique = true)
    @NotNull
    private int like_id;
    private String post_id;
    private String from_id;
    private String from_name;
    private String from_profile_picture;
    private String to_id;
    private String to_name;
    private String to_profile_picture;
    private String datetime;

    public Like() {}

    @Generated(hash = 1071570479)
    public Like(Long id, int like_id, String post_id, String from_id,
            String from_name, String from_profile_picture, String to_id,
            String to_name, String to_profile_picture, String datetime) {
        this.id = id;
        this.like_id = like_id;
        this.post_id = post_id;
        this.from_id = from_id;
        this.from_name = from_name;
        this.from_profile_picture = from_profile_picture;
        this.to_id = to_id;
        this.to_name = to_name;
        this.to_profile_picture = to_profile_picture;
        this.datetime = datetime;
    }

    public Like(int _like_id, String _post_id, String _from_id, String _from_name, String _from_profile_picture,
                String _to_id, String _to_name, String _to_profile_picture, String _datetime) {
        this.setLike_id(_like_id);
        this.setPost_id(_post_id);
        this.setFrom_id(_from_id);
        this.setFrom_name(_from_name);
        this.setFrom_profile_picture(_from_profile_picture);
        this.setTo_id(_to_id);
        this.setTo_name(_to_name);
        this.setTo_profile_picture(_to_profile_picture);
        this.setDatetime(_datetime);
    }

    public static void deleteLike(LikeDao likeDao,String post_id){
        List<Like> test = getLikeByPostid(likeDao,post_id);
        likeDao.deleteInTx(test);
    }

    public static List<Like> getLikeByPostid(LikeDao likeDao, String post_id){
        return likeDao.queryBuilder().where(LikeDao.Properties.Post_id.eq(post_id)).orderAsc(LikeDao.Properties.Like_id).list();
    }
    
    public String getFrom_profile_picture() {
        return from_profile_picture;
    }

    public void setFrom_profile_picture(String from_profile_picture) {
        this.from_profile_picture = from_profile_picture;
    }

    public String getTo_profile_picture() {
        return to_profile_picture;
    }

    public void setTo_profile_picture(String to_profile_picture) {
        this.to_profile_picture = to_profile_picture;
    }

    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    public String getFrom_id() {
        return from_id;
    }

    public void setFrom_id(String from_id) {
        this.from_id = from_id;
    }

    public String getFrom_name() {
        return from_name;
    }

    public void setFrom_name(String from_name) {
        this.from_name = from_name;
    }

    public int getLike_id() {
        return like_id;
    }

    public void setLike_id(int like_id) {
        this.like_id = like_id;
    }

    public String getPost_id() {
        return post_id;
    }

    public void setPost_id(String post_id) {
        this.post_id = post_id;
    }

    public String getTo_id() {
        return to_id;
    }

    public void setTo_id(String to_id) {
        this.to_id = to_id;
    }

    public String getTo_name() {
        return to_name;
    }

    public void setTo_name(String to_name) {
        this.to_name = to_name;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
