package com.sgo.mayapada.entityDAO;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Index;
import org.greenrobot.greendao.annotation.NotNull;

import java.util.List;

/**
 * Created by thinkpad on 5/13/2015.
 */

@Entity(
        nameInDb = "Comment"
)
public class Comment{

    @Id(autoincrement = true)
    private Long id;

    @Index(unique = true)
    @NotNull
    private int comment_id;

    private String post_id;
    private String from_id;
    private String from_name;
    private String from_profile_picture;
    private String to_id;
    private String to_name;
    private String to_profile_picture;
    private String reply;
    private String datetime;

    public Comment() {

    }

    public Comment(int _comment_id, String _post_id, String _from_id, String _from_name, String _from_profile_picture,
                   String _to_id, String _to_name, String _to_profile_picture, String _reply, String _datetime) {
        this.setComment_id(_comment_id);
        this.setPost_id(_post_id);
        this.setFrom_id(_from_id);
        this.setFrom_name(_from_name);
        this.setFrom_profile_picture(_from_profile_picture);
        this.setTo_id(_to_id);
        this.setTo_name(_to_name);
        this.setTo_profile_picture(_to_profile_picture);
        this.setReply(_reply);
        this.setDatetime(_datetime);
    }

    @Generated(hash = 713291586)
    public Comment(Long id, int comment_id, String post_id, String from_id, String from_name, String from_profile_picture,
            String to_id, String to_name, String to_profile_picture, String reply, String datetime) {
        this.id = id;
        this.comment_id = comment_id;
        this.post_id = post_id;
        this.from_id = from_id;
        this.from_name = from_name;
        this.from_profile_picture = from_profile_picture;
        this.to_id = to_id;
        this.to_name = to_name;
        this.to_profile_picture = to_profile_picture;
        this.reply = reply;
        this.datetime = datetime;
    }

    public String getFrom_profile_picture() {
        return from_profile_picture;
    }

    public void setFrom_profile_picture(String from_profile_picture) {
        this.from_profile_picture = from_profile_picture;
    }

    public String getTo_profile_picture() {
        return to_profile_picture;
    }

    public void setTo_profile_picture(String to_profile_picture) {
        this.to_profile_picture = to_profile_picture;
    }

    public int getComment_id() {
        return comment_id;
    }

    public void setComment_id(int comment_id) {
        this.comment_id = comment_id;
    }

    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    public String getFrom_id() {
        return from_id;
    }

    public void setFrom_id(String from_id) {
        this.from_id = from_id;
    }

    public String getFrom_name() {
        return from_name;
    }

    public void setFrom_name(String from_name) {
        this.from_name = from_name;
    }

    public String getPost_id() {
        return post_id;
    }

    public void setPost_id(String post_id) {
        this.post_id = post_id;
    }

    public String getReply() {
        return reply;
    }

    public void setReply(String reply) {
        this.reply = reply;
    }

    public String getTo_id() {
        return to_id;
    }

    public void setTo_id(String to_id) {
        this.to_id = to_id;
    }

    public String getTo_name() {
        return to_name;
    }

    public void setTo_name(String to_name) {
        this.to_name = to_name;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public static void deletePost(CommentDao commentDao,String post_id){
        List<Comment> test = getCommentByPostid(commentDao,post_id);
        commentDao.deleteInTx(test);
    }

    public static List<Comment> getCommentByPostid(CommentDao commentDao, String post_id){
        return commentDao.queryBuilder().where(CommentDao.Properties.Post_id.eq(post_id)).orderAsc(CommentDao.Properties.Comment_id).list();
    }
}
