package com.sgo.mayapada.entityDAO;/*
  Created by Administrator on 2/3/2015.
 */

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Index;
import org.greenrobot.greendao.annotation.NotNull;

import java.util.Date;
import java.util.List;

import org.greenrobot.greendao.annotation.Generated;


@Entity(
        nameInDb = "Friend"
)
public class Friend{
    @Id(autoincrement = true)
    private Long id;

    @Index(unique = true)
    @NotNull
    private int contact_id;
    private String full_name;
    private String mobile_number;
    private String mobile_number2;
    private String mobile_number3;
    private String email;
    private String owner_id;
    private int is_friend;
    private Date created_date;
    private Date update_date;

    public Friend(){
    }

    @Generated(hash = 1337621254)
    public Friend(Long id, int contact_id, String full_name, String mobile_number,
            String mobile_number2, String mobile_number3, String email,
            String owner_id, int is_friend, Date created_date, Date update_date) {
        this.id = id;
        this.contact_id = contact_id;
        this.full_name = full_name;
        this.mobile_number = mobile_number;
        this.mobile_number2 = mobile_number2;
        this.mobile_number3 = mobile_number3;
        this.email = email;
        this.owner_id = owner_id;
        this.is_friend = is_friend;
        this.created_date = created_date;
        this.update_date = update_date;
    }

    public Friend(String _full_name, String _mobile_number, String _mobile_number2,String _mobile_number3,
                       String _email, String _owner_id){
        this.setFull_name(_full_name);
        this.setMobile_number(_mobile_number);
        this.setMobile_number2(_mobile_number2);
        this.setMobile_number3(_mobile_number3);
        this.setEmail(_email);
        this.setOwner_id(_owner_id);
    }

    public static void deleteAllnCache(FriendDao friendDao){
        friendDao.deleteAll();
        friendDao.detachAll();
    }

    public static List<Friend> getAllFriend(FriendDao friendDao){
        return friendDao.queryBuilder().where(FriendDao.Properties.Is_friend.notEq(1)).list();

    }

    public int getContact_id() {
        return contact_id;
    }

    public void setContact_id(int contact_id) {
        this.contact_id = contact_id;
    }

    public String getFull_name() {
        return full_name;
    }

    public void setFull_name(String full_name) {
        this.full_name = full_name;
    }

    public String getMobile_number() {
        return mobile_number;
    }

    public void setMobile_number(String mobile_number) {
        this.mobile_number = mobile_number;
    }

    public String getMobile_number2() {
        return mobile_number2;
    }

    public void setMobile_number2(String mobile_number2) {
        this.mobile_number2 = mobile_number2;
    }

    public String getMobile_number3() {
        return mobile_number3;
    }

    public void setMobile_number3(String mobile_number3) {
        this.mobile_number3 = mobile_number3;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getOwner_id() {
        return owner_id;
    }

    public void setOwner_id(String owner_id) {
        this.owner_id = owner_id;
    }

    public Date getCreated_date() {
        return created_date;
    }

    public void setCreated_date(Date created_date) {
        this.created_date = created_date;
    }

    public Date getUpdate_date() {
        return update_date;
    }

    public void setUpdate_date(Date update_date) {
        this.update_date = update_date;
    }

    public int getIs_friend() {
        return is_friend;
    }

    public void setIs_friend(int is_friend) {
        this.is_friend = is_friend;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
