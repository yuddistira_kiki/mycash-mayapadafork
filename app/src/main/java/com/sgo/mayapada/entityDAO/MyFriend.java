package com.sgo.mayapada.entityDAO;/*
  Created by Administrator on 2/3/2015.
 */

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Index;
import org.greenrobot.greendao.annotation.NotNull;
import org.greenrobot.greendao.annotation.Generated;

import java.util.List;


@Entity(
        nameInDb = "MyFriend"
)
public class MyFriend{
    @Id(autoincrement = true)
    private Long id;

    @Index(unique = true)
    @NotNull
    private int contact_id;
    private String full_name;
    private String friend_number;
    private String email;
    private String user_id;
    private String img_url;
    private boolean isEnabled;

    public MyFriend(){}

    @Generated(hash = 271491170)
    public MyFriend(Long id, int contact_id, String full_name, String friend_number,
            String email, String user_id, String img_url, boolean isEnabled) {
        this.id = id;
        this.contact_id = contact_id;
        this.full_name = full_name;
        this.friend_number = friend_number;
        this.email = email;
        this.user_id = user_id;
        this.img_url = img_url;
        this.isEnabled = isEnabled;
    }

    public static void deleteAllnCache(MyFriendDao myFriendDao){
        myFriendDao.deleteAll();
        myFriendDao.detachAll();
    }

    public static List<MyFriend> getAllMyFriend(MyFriendDao myFriendDao){
        return myFriendDao.queryBuilder().orderAsc(MyFriendDao.Properties.Full_name).list();
    }

    public int getContact_id() {
        return contact_id;
    }

    public void setContact_id(int contact_id) {
        this.contact_id = contact_id;
    }

    public String getFull_name() {
        return full_name;
    }

    public void setFull_name(String full_name) {
        this.full_name = full_name;
    }

    public String getFriend_number() {
        return friend_number;
    }

    public void setFriend_number(String friend_number) {
        this.friend_number = friend_number;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getImg_url() {
        return img_url;
    }

    public void setImg_url(String img_url) {
        this.img_url = img_url;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public boolean isEnabled() {
        return isEnabled;
    }

    public void setEnabled(boolean enabled) {
        isEnabled = enabled;
    }

    public boolean getIsEnabled() {
        return this.isEnabled;
    }

    public void setIsEnabled(boolean isEnabled) {
        this.isEnabled = isEnabled;
    }
}
