# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in /Users/yuddistirakiki/Library/android-sdk-macosx/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

-dontusemixedcaseclassnames
-dontskipnonpubliclibraryclasses
-verbose
-adaptresourcefilenames
-adaptresourcefilecontents
# Optimization is turned off by default. Dex does not like code run
# through the ProGuard optimize and preverify steps (and performs some
# of these optimizations on its own).
-dontoptimize
-dontpreverify
# Note that if you want to enable optimization, you cannot just
# include optimization flags in your own project configuration file;
# instead you will need to point to the
# "proguard-android-optimize.txt" file instead of this one from your
# project.properties file.
-keep class com.google.zxing.** { *; }
-keep class android.widget.** { *; }
-keep class android.support.v7.appcompat.** { *; }
-keep class android.support.v4.app.** { *; }
-keep interface android.support.v4.app.** { *; }
-keep class com.actionbarsherlock.** { *; }
-keep interface com.actionbarsherlock.** { *; }
-keep public class com.activeandroid.** { *; }
-keep class sun.misc.Unsafe { *; }
-keep class com.google.gson.** { *; }
-keep class org.ocpsoft.prettytime.** { *; }
-keep public class com.google.vending.licensing.ILicensingService
-keep public class com.android.vending.licensing.ILicensingService
-keepattributes Exception
-keepattributes Signature
-keepattributes InnerClasses
-keepattributes *Annotation*
-keep public class com.sgo.mayapada.Beans.** extends com.activeandroid.Model { *; }
-keep public class com.sgo.mayapada.Beans.** { *; }

-keep class com.sgo.mayapada.activities.CreateGroupActivity$TempObjectData { *; }

-keep class com.sgo.mayapada.coreclass.BalanceHandler { *; }
-keep class com.sgo.mayapada.coreclass.CollapseExpandAnimation { *; }
-keep class com.sgo.mayapada.coreclass.Contents { *; }
-keep class com.sgo.mayapada.coreclass.CoreApp { *; }
-keep class com.sgo.mayapada.coreclass.CurrencyFormat { *; }
-keep class com.sgo.mayapada.coreclass.HideKeyboard { *; }
-keep class com.sgo.mayapada.coreclass.InetHandler { *; }
-keep class com.sgo.mayapada.coreclass.LifeCycleHandler { *; }
-keep class com.sgo.mayapada.securities.Md5 { *; }
-keep class com.sgo.mayapada.coreclass.MyApiClient { *; }
-keep class com.sgo.mayapada.coreclass.MyPicasso { *; }
-keep class com.sgo.mayapada.coreclass.NotificationActionView { *; }
-keep class com.sgo.mayapada.coreclass.PeriodTime { *; }
-keep class com.sgo.mayapada.coreclass.RoundedImageView { *; }
-keep class com.sgo.mayapada.coreclass.QRCodeEncoder { *; }
-keep class com.sgo.mayapada.coreclass.WebParams { *; }
-keep class com.loopj.android.http.HttpGet { *; }
-keep class com.loopj.android.http.AsyncHttpResponseHandler { *; }

-keep class com.loopj.android.http.RequestParams { *; }

-keep class com.sgo.mayapada.dialogs.DefinedDialog { *; }
-keep class com.sgo.mayapada.dialogs.ReportBillerDialog { *; }
-keep class com.sgo.mayapada.dialogs.AlertDialogFrag { *; }

-keep class com.sgo.mayapada.fragments.ListCollectionPayment$TempObjectData { *; }
-keep class com.sgo.mayapada.fragments.FragTagihan$TempObjectData { *; }
-keep class com.sgo.mayapada.fragments.RekeningTabungan$SavingID { *; }

-keep class com.sgo.mayapada.fragments.FragAskForMoney$TempObjectData { *; }
-keep class com.sgo.mayapada.fragments.FragPayFriends$TempObjectData { *; }
-keep class com.sgo.mayapada.fragments.FragPayFriendsConfirm$TempTxID { *; }
-keep class com.sgo.mayapada.fragments.ListBillerMerchant$ListObject{ *; }
-keep class com.sgo.mayapada.fragments.TabunganMenuInput{ *; }
-keep class com.sgo.mayapada.activities.AskRewardActivity$TempObjectData { *; }
-keep class com.sgo.mayapada.fragments.FragKantinStudentOrder$TempObjectData { *; }
-keep class com.sgo.mayapada.activities.PaymentQRActivity$TempObjectData { *; }

-keep class com.sgo.mayapada.services.BalanceService { *; }
-keep class io.codetail.animation.arcanimator.** { *; }
-keep class org.joda.time.** { *; }
-keep interface org.joda.time.** { *; }
-keep class net.sourceforge.zbar.** { *; }
-keep interface net.sourceforge.zbar.** { *; }
# For native methods, see http://proguard.sourceforge.net/manual/examples.html#native
-keepclasseswithmembernames class * {
    native <methods>;
}

# keep setters in Views so that animations can still work.
# see http://proguard.sourceforge.net/manual/examples.html#beans
-keepclassmembers public class * extends android.view.View {
   void set*(***);
   *** get*();
}

# We want to keep methods in Activity that could be used in the XML attribute onClick
-keepclassmembers class * extends android.app.Activity {
   public void *(android.view.View);
}

# For enumeration classes, see http://proguard.sourceforge.net/manual/examples.html#enumerations
-keepclassmembers enum * {
    public static **[] values();
    public static ** valueOf(java.lang.String);
}

-keep class * implements android.os.Parcelable {
  public static final android.os.Parcelable$Creator *;
}

-keepclassmembers class **.R$* {
    public static <fields>;
}


# The support library contains references to newer platform versions.
# Don't warn about those in case this app is linking against an older
# platform version.  We know about them, and they are safe.
-dontwarn android.support.**
-dontwarn okio.**
-dontwarn org.joda.convert.**
-dontwarn org.joda.time.**
-dontwarn net.sourceforge.zbar.**

# RxAndroid
-dontwarn rx.internal.util.unsafe.**

-keepattributes EnclosingMethod

### greenDAO 3
-keepclassmembers class * extends org.greenrobot.greendao.AbstractDao {
public static java.lang.String TABLENAME;
}
-keep class **$Properties

# If you do not use SQLCipher:
-dontwarn org.greenrobot.greendao.database.**
# If you do not use RxJava:
-dontwarn rx.**
